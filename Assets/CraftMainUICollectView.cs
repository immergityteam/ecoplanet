using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ColLobOK
{
    public class CraftMainUICollectView : MonoBehaviour, IDisposable, IPointerClickHandler
    {
        public Action ActionOnCLick;
        public SpriteRenderer Icon;
        
        public void Dispose()
        {
            ActionOnCLick = null;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            ActionOnCLick?.Invoke();
        }

        public void OnDestroy()
        {
            Dispose();
        }
    }
}
