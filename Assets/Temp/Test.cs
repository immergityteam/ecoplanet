using ColLobOK;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Developing
{
    public class Test : MonoBehaviour
    {

#if UNITY_EDITOR
        public StringsSO[] strings;

        [ContextMenu("GenCVS")]
        void GenCVS()
        {

            string path = $"{Application.persistentDataPath}/{Application.productName}_Lang.cvs";

            string bigStr = "";

            for (int i = 0; i < strings.Length; i++)
            {
                bigStr += strings[i].ID + ";" + strings[i].GetStrings()[0] + ";" + strings[i].GetStrings()[1] + '\n';
            }

            StreamWriter sw = File.CreateText(path);
            sw.WriteLine(bigStr);
            sw.Close();
        }

        [Header("�����")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

        [ContextMenu("SetStringLanguage")]
        void SetAllStringID()
        {
            exel = MetodForEditor.Parsing(CVS);

            for (int i = 0; i < strings.Length; i++)
            {
                for (int j = 0; j < exel.Count; j++)
                {
                    if (exel[j]["ID"] == "") continue;
                    if (strings[i].ID != int.Parse(exel[j]["ID"])) continue;
                    strings[i].SetString(exel[j]["Rus"].Trim('\"'), 0);
                    strings[i].SetString(exel[j]["Eng"].Trim('\"'), 1);
                    EditorUtility.SetDirty(strings[i]);
                }
            }
            AssetDatabase.SaveAssets();
        }
#endif
    }
}