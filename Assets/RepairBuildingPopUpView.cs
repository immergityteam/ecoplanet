using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class RepairBuildingPopUpView : MonoBehaviour, IPointerClickHandler
{
    public Action PopUpPressed;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        PopUpPressed?.Invoke();
    }

    public void OnDisable()
    {
        PopUpPressed = null;
    }
}
