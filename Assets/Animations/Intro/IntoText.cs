using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ColLobOK
{
    public class IntoText : MonoBehaviour
    {
        int i = -1;
        public StringsSO[] SSO;
        public TMP_Text text;

        public void NextText()
        {
            i++;
            if (i >= SSO.Length) return;
            text.text = SSO[i].GetString();
        }
        public void HidenText()
        {
            if (i >= SSO.Length-1)
            {
                text.transform.parent.gameObject.SetActive(false);
            }
        }
        public void NextScene()
        {
            SceneManager.LoadScene(1);
        }
     }
}