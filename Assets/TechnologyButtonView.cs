using System.Globalization;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class TechnologyButtonView : MonoBehaviour
    {
        public UnityAction<int> buttonPressed;
        public UnityAction<Sprite> buttonPressedImage;
        public UnityAction<Sprite> progressButtonPressedImage;
        public int BranchID;
        public Sprite Icon;
        public Sprite BubbleIcon;
        public Sprite ProgressBubbleIcon;
        
        public void SendID()
        {
            buttonPressedImage?.Invoke(BubbleIcon);
            progressButtonPressedImage?.Invoke(ProgressBubbleIcon);
            buttonPressed?.Invoke(BranchID);
        }
    }
}
