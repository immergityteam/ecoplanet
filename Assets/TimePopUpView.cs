using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ColLobOK
{
    public class TimePopUpView : MonoBehaviour, IPointerClickHandler
    {
        public TMP_Text TimeRemaining;
        public int TimeRemainingInIntegerVariable;
        public UnityAction<int> ShowRemainigTimeBooster;

        public void OnPointerClick(PointerEventData eventData)
        {
            ShowRemainigTimeBooster?.Invoke(TimeRemainingInIntegerVariable);
        }
    }
}
