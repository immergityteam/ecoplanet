﻿using UnityEngine;

namespace ColLobOK
{
    public class ImageTransparency : MonoBehaviour
    {
        private SpriteRenderer[] sprite;
        private int defSortingOrder;
        void Awake()
        {
            sprite = GetComponentsInChildren<SpriteRenderer>();
            defSortingOrder = sprite[0].sortingOrder;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.GetComponent<IPlayer>() != null)
            {
                foreach (var item in sprite)
                {
                    item.sortingOrder = defSortingOrder + 100;
                    item.color = Color.white - Color.black * 0.5f;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.GetComponent<IPlayer>() != null)
            {
                foreach (var item in sprite)
                {
                    item.sortingOrder = defSortingOrder;
                    item.color = Color.white;
                }
            }
        }
    }
}

