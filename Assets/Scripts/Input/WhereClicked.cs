﻿using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class WhereClicked : IWhereClicked, IDependent {

        Camera camera;

        private LayerMask layerMask;

        public void SetLayerMask(LayerMask mask) {
            layerMask = mask;
        }

        public void OnInjected() {
            camera = Camera.main;
        }

        public Collider2D CheckedCollider(Vector3 point) {
            RaycastHit2D rayHit = Physics2D.GetRayIntersection(camera.ScreenPointToRay(point), Mathf.Infinity, layerMask);
            if (rayHit) {
                return rayHit.collider;
            } else {
                return null;
            }
        }
    }
}

