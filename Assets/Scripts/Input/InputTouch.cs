﻿using UnityEngine;

namespace ColLobOK
{
    public class InputTouch : IClickedPoint {
        public bool IsDown { get; private set; }
        public bool IsUp { get; private set; }
        public Vector3 Point {
            get {
                if (Input.touchCount > 0) {
                    return Input.GetTouch(0).position;
                } else {
                    return Vector3.positiveInfinity;
                } 
            }
        }

        public void Update() {
            IsDown = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
            IsUp = Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended;
        }
    }
}

