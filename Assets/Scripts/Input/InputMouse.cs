﻿using UnityEngine;

namespace ColLobOK
{
    public class InputMouse : IClickedPoint {

        public bool IsDown { get; private set; }
        public bool IsUp { get; private set; }
        public Vector3 Point { get => Input.mousePosition; }

        public void Update() {
            IsDown = Input.GetMouseButtonDown(0);
            IsUp = Input.GetMouseButtonUp(0);
        }
    }
}
