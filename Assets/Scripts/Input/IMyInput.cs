﻿using System;
using UnityEngine;

namespace ColLobOK
{
    public interface IMyInput {
        Action EvUp { get; set; }
        Action EvCliked { get; set; }
        Action EvDown { get; set; }
        Action EvPressed { get; set; }
        Vector3 DeltaPoint { get; }
        Vector3 DeltaPointFrame { get; }
        Vector3 PointCamera { get; }
        Vector3 GetPointWorld();
        Vector3 GetPointWorld(Vector3 UIPoint);

        bool GetPressed();
        void Update();
    }

}
