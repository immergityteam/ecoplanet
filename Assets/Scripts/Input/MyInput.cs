﻿using System;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ColLobOK
{

    public class MyInput : IMyInput, IDependent
    {
        [Dependency] public IClickedPoint ClickedPoint { private get; set; }
        public Action EvUp { get; set; }
        public Action EvCliked { get; set; }
        public Action EvDown { get; set; }
        public Action EvPressed { get; set; }

        private const float DELTA_FOR_CLIKED = 100f;
        private Camera camera;

        private Vector3 startPoint;
        private bool pressed;
        private Vector3 oldPoint;

        public void OnInjected() { camera = Camera.main; }

        public bool GetPressed() => pressed;

        public void Update()
        {
            ClickedPoint.Update();
            if (ClickedPoint.IsDown && !IsPointerOverUIElement)
            {
                startPoint = ClickedPoint.Point;
                EvDown?.Invoke();
                pressed = true;
            }
            if (pressed)
            {
                EvPressed?.Invoke();
                oldPoint = ClickedPoint.Point;
            }

            if (ClickedPoint.IsUp)
            {
                EvUp?.Invoke();
                if (DeltaPoint.sqrMagnitude < DELTA_FOR_CLIKED)
                    EvCliked?.Invoke();
                pressed = false;
            }
        }

        public Vector3 GetPointWorld()
        {
            return GetPointWorld(ClickedPoint.Point);
        }

        public Vector3 GetPointWorld(Vector3 UIPoint)
        {
            Vector3 worldPosition = camera.ScreenToWorldPoint(UIPoint);
            worldPosition.z = 0;
            return worldPosition;
        }

        public Vector3 PointCamera => ClickedPoint.Point;
        public Vector3 DeltaPoint => startPoint - ClickedPoint.Point;
        public Vector3 DeltaPointFrame => oldPoint - ClickedPoint.Point;

        #region IsPointerOverUIElement
        int UILayer;
        public MyInput() { UILayer = LayerMask.NameToLayer("UI"); }

        private bool IsPointerOverUIElement => _IsPointerOverUIElement(GetEventSystemRaycastResults());

        //Returns 'true' if we touched or hovering on Unity UI element.
        private bool _IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults)
        {
            for (int index = 0; index < eventSystemRaysastResults.Count; index++)
            {
                RaycastResult curRaysastResult = eventSystemRaysastResults[index];
                if (curRaysastResult.gameObject.layer == UILayer)
                    return true;
            }
            return false;
        }

        //Gets all event system raycast results of current mouse or touch position.
        private List<RaycastResult> GetEventSystemRaycastResults()
        {
            PointerEventData eventData = new PointerEventData(EventSystem.current);
            eventData.position = Input.mousePosition;
            List<RaycastResult> raysastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventData, raysastResults);
            return raysastResults;
        }
        #endregion
    }
}

