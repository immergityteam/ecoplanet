﻿using UnityEngine;

namespace ColLobOK
{
    public interface IClickedPoint {
        bool IsDown { get;}
        bool IsUp { get; }
        Vector3 Point { get;}
        void Update();
    }
}