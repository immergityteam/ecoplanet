﻿using UnityEngine;

namespace ColLobOK
{
    public interface IWhereClicked {
        void SetLayerMask(LayerMask mask);
        Collider2D CheckedCollider(Vector3 point);
    }
}

