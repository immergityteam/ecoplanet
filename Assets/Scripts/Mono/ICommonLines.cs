﻿namespace ColLobOK
{
    public interface ICommonLines
    {
        StringsSO FormatTime { get; }
        StringsSO NeedDonat { get; }
        StringsSO NeedEnergy { get; }
    }
}