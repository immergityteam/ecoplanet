using System;
using UnityEngine;

namespace ColLobOK
{
    [Serializable]
    public struct Targets
    {
        public Transform Transform;
        public ItemsTargets TargetName;
    }

    public interface IContainerMono
    {
        GameObject BaseItem { get; }
        float TimePressedForMove { get; }
        int StrengthMining { get; }
        FlyingItem ItemData { get; }
        GameObject IntersectionSquare { get; }
        GameObject GhostSquare { get; }
        TypeConfigSO[] TypeConfig { get; }
        Targets[] Targets { get; }
        StringsSO Money { get; }
        StringsSO Donate { get; }
        StringsSO Energy { get; }
        float EnergyDropChance { get; }
        int CountMaxEnergy { get; }
        int TimeRechargeEnergy { get; }
        StringsSO AddCellWarehouse { get; }
        int[] PriceCellWarehouse { get; }

#if UNITY_EDITOR
        bool BuildOffResurse { get;}
        bool OffSaveAndLoad { get;}

#endif
    }
}