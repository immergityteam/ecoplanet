using UnityEngine;

namespace ColLobOK
{
    public class ContainerMono : MonoBehaviour, IContainerMono
    {
        [field: SerializeField] public GameObject BaseItem { get; private set; }
        [field: SerializeField] public float TimePressedForMove { get; private set; }
        [field: SerializeField] public int StrengthMining { get; private set; }
        [field: SerializeField] public FlyingItem ItemData { get; private set; }
        [field: SerializeField] public GameObject IntersectionSquare { get; private set; }
        [field: SerializeField] public GameObject GhostSquare { get; private set; }
        [field: SerializeField] public TypeConfigSO[] TypeConfig { get; private set; }
        [field: SerializeField] public Targets[] Targets { get; private set; }
        [field: SerializeField] public StringsSO Money { get; private set; }
        [field: SerializeField] public StringsSO Donate { get; private set; }
        [field: SerializeField] public StringsSO Energy { get; private set; }
        [field: SerializeField] public float EnergyDropChance { get; private set; }

        [field: Header("Energy")]
        [field: SerializeField] public int CountMaxEnergy { get; private set; }
        [field: SerializeField] public int TimeRechargeEnergy { get; private set; }

        [field: SerializeField] public StringsSO AddCellWarehouse { get; private set; }
        [field: SerializeField] public int[] PriceCellWarehouse { get; private set; }
#if UNITY_EDITOR
        [field: Header("Debug")]
        [field: SerializeField] public bool BuildOffResurse { get; private set; }
        [field: SerializeField] public bool OffSaveAndLoad { get; private set; }
#endif
    }
}