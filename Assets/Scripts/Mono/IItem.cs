using System;
using UnityEngine;

namespace ColLobOK
{
    public interface IItem
    {
        Action<IItem> EvCliked { get; set; }
        void SetTarget(Transform target);
        Transform GetTarget();
        int IdResName { get; set; }
    }
}