using UnityEngine;

namespace ColLobOK
{
    public class CommonLines : MonoBehaviour, ICommonLines
    {
        [field: SerializeField] public StringsSO FormatTime { get; private set; }
        [field: SerializeField] public StringsSO NeedDonat { get; private set; }
        [field: SerializeField] public StringsSO NeedEnergy { get; private set; }
    }
}