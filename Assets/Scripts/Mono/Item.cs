using System;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class Item : MonoBehaviour, IItem, IGameObjCliked
    {
        private Transform _target;
        private int _idResname;

        public Action<IItem> EvCliked { get; set; }
        public int IdResName { get => _idResname; set => _idResname = value; }
        UnityAction<int> IGameObjCliked.EvCliked { get; set; }

        public void GameObjCliked()
        {
            EvCliked?.Invoke(this);
        }

        public void SetTarget(Transform target)
        {
            _target = target;
        }

        public Transform GetTarget()
        {
            return _target;
        }
    }
}