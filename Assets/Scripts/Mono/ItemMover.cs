using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class ItemMover : MonoBehaviour, IItemMover
    {
        private List<Transform> _items = new List<Transform>();
        private Vector3 _randomOffset;
        private float _speed = 8.5f;
        private GameObject _tempItem;

        public void MoveItem(GameObject item)
        {
            _tempItem = item;
            //StartCoroutine(Fly(item, 4f));

            FlyToTarget();
        }

        private void FlyToTarget()
        {
            _tempItem.GetComponent<SpriteRenderer>().sortingOrder = Random.Range(101, 110);
            _tempItem.transform.parent = _tempItem.GetComponent<Item>().GetTarget();

            _items.Add(_tempItem.transform);
        }

        private void Update()
        {
            if (_items.Count > 0)
            {
                for (int i = 0; i < _items.Count; i++)
                {
                    Debug.Log("Move");
                    Vector3 randomOffset = new Vector3(0, 0, 0);
                    _items[i].localPosition = Vector3.MoveTowards(_items[i].localPosition, _items[i].GetComponent<Item>().GetTarget().position + randomOffset, _speed * Time.deltaTime);
                    
                    if (_items[i].transform.position == _items[i].GetComponent<Item>().GetTarget().position)
                    {
                        _items.RemoveAt(i);
                    }
                }
            }
        }

        private float GetRandomRange(float min, float max)
        {
            return Random.Range(min, max);
        }
    }

}
