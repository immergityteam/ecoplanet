using UnityEngine;

namespace ColLobOK
{
    public interface IItemMover
    {
        void MoveItem(GameObject item);
    }
}
