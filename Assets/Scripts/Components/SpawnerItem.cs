using DG.Tweening;
using System.Collections;
using System.Linq;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class SpawnerItem : ISpawnerItem, IDependent
    {
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IContainerAllResources AllRes { private get; set; }
        [Dependency] public IMyRandom MyRand { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }

        private Transform cameraTransform;
        public void OnInjected()
        {
            cameraTransform = Camera.main.transform;
            Creator.StartCourutine(DelayedStart());
        }
        public IEnumerator DelayedStart()
        {
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < ContainerMono.Targets.Length; i++)
            {
                ContainerMono.Targets[i].Transform.parent = cameraTransform;
                ContainerMono.Targets[i].Transform.position = MyInput.GetPointWorld(ContainerMono.Targets[i].Transform.position);
                ContainerMono.Targets[i].Transform.localScale = Vector3.one;
            }
        }

        public void SpawnItem(GameObject item, Vector3 pos)
        {
            GameObject temp = Creator.GetGameObjectPool(item);
        }

        public void SpawnItem(Sprite itemSprite, Vector3 pos)
        {
            GameObject temp = Creator.GetGameObjectPool(ContainerMono.BaseItem);
            temp.GetComponent<SpriteRenderer>().sprite = itemSprite;
        }

        public void SpawnItem(Resource item, Vector3 pos, Transform target = null)
        {
            GameObject temp = Creator.GetGameObjectPool(ContainerMono.BaseItem);
            temp.GetComponent<SpriteRenderer>().sprite = AllRes.GetConfigRes(item.ID).sprite;
            //temp.GetComponent<IItem>().IdResName = item.ID;
            ScatterItems(temp, pos, SetTarget(item.ID));
        }

        private Transform SetTarget(int idRes)
        {
            ItemsTargets itemTarget = AllRes.GetConfigRes(idRes).Target;
            return ContainerMono.Targets.First(x => x.TargetName == itemTarget).Transform;
        }

        private void ScatterItems(GameObject item, Vector3 pos, Transform parent)
        {
            item.transform.position = new Vector3(pos.x, pos.y, 0);

            float randX = MyRand.GetRandRange(ContainerMono.ItemData.MinSpawnOffset, ContainerMono.ItemData.MaxSpawnOffset);
            Vector3[] way = new Vector3[2];
            way[0] = pos + Vector3.up + Vector3.right * randX / 2;
            way[1] = pos + Vector3.right * randX - new Vector3(0, 0.5f, 0);

            var sequence = DOTween.Sequence();

            sequence.Append(item.transform.DOPath(way, ContainerMono.ItemData.ScatterDuration)
                .SetEase(Ease.OutSine))
                .AppendInterval(ContainerMono.ItemData.SecondsBeforeFlyToTarget)
                .AppendCallback(() =>
                    {
                        item.transform.parent = parent;
                    })
                .Append(item.transform.DOLocalMove(Vector3.zero, ContainerMono.ItemData.MoveToTargetTime))
                .Append(item.transform.DOScale(Vector3.zero, ContainerMono.ItemData.FadeOutDuration)
                .OnComplete(() =>
                    {
                        Creator.MyDestroyPool(item.gameObject);
                        item.transform.localScale = Vector3.one;
                        sequence.Kill();
                    }));
        }
    }
}