using UnityDI;

namespace ColLobOK
{
    public class CoinsView : ICoinsView, IDependent
    {
        [Dependency] public IContainerMono ContainerMono { get; private set; }
        [Dependency] public IWarehouseController WarehouseController { get; private set; }
        [Dependency] public IGameUI GameUI { get; private set; }

        public void OnInjected()
        {
            WarehouseController.EvChangeRes += RefreshViews;
            GameUI.Coins.text = WarehouseController.LookOneRes(ContainerMono.Money.GetID()).ToString();
            GameUI.Donate.text = WarehouseController.LookOneRes(ContainerMono.Donate.GetID()).ToString();
        }

        private void RefreshViews(int id)
        {
            if (id == ContainerMono.Money.GetID())
            {
                GameUI.Coins.text = WarehouseController.LookOneRes(ContainerMono.Money.GetID()).ToString();
            }
            if (id == ContainerMono.Donate.GetID())
            {
                GameUI.Donate.text = WarehouseController.LookOneRes(ContainerMono.Donate.GetID()).ToString();
            }
        }
    }
}