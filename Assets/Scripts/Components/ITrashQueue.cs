using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface ITrashQueue
    {
        void ClearQueue();
        void TakePlayerToPoint();
        void AddToQueue(UnityAction action, Vector3 position);
        bool IsQueueEmpty { get; }
    }
}
