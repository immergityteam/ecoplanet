using UnityEngine;

namespace ColLobOK
{
    public interface ISpawnerItem {
        void SpawnItem(GameObject item, Vector3 pos);
        void SpawnItem(Sprite itemSprite, Vector3 pos);
        void SpawnItem(Resource item, Vector3 pos, Transform target = null);
    }
}