﻿using System;
using ColLobOK.Mechanics.Tutorial.Controller;
using ColLobOK.Mechanics.Tutorial.Interface;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    /// <summary>
    /// Класс, запускающий игру
    /// </summary>
    public class GameStarter : MonoBehaviour
    {
        public static Container container;
        private GameMain gameMain;
        private LocalizationID localization;
        [HideInInspector] public static bool CrutchLoading = false;

        public void Awake()
        {
            if (!localization)
                localization = FindObjectOfType<LocalizationID>();
            if (!localization)
                localization = new GameObject("LocalizationID").AddComponent<LocalizationID>();

            localization.Awake();
            SetupContainer();

            gameMain = container.Resolve<GameMain>();
            gameMain.Start();
            CrutchLoading = true;
        }

        public void Update()
        {
            gameMain.Update(Time.deltaTime);
        }

        private void SetupContainer()
        {
            container = new Container();

            //Main
            container.RegisterType<GameMain>();
            container.RegisterSceneObject<IContainerMono>("Conteiner");
            container.RegisterSceneObject<ICommonLines>("Conteiner");
            container.RegisterSceneObject<IGameUI>("Windows/GameUI");

            //Master
            container.RegisterSingleton<IMainMove, MainMove>();
            container.RegisterSceneObject<ICreator>("Creator");
            container.RegisterType<ITiker, Tiker>();
            container.RegisterSingleton<IMyTime, MyTime>();
            container.RegisterSingleton<IMyInput, MyInput>();
            container.RegisterSingleton<IMyRandom, MyRandom>();

            //Other
            container.RegisterType<IGridPathfinding, GridPathfinding>();
            container.RegisterType<IMovePath, MovePath>();
            container.RegisterSingleton<ISpawnerItem, SpawnerItem>();
            container.RegisterSingleton<ICoinsView, CoinsView>();

            //Input
#if UNITY_EDITOR
            container.RegisterType<IClickedPoint, InputMouse>();
#endif
#if !UNITY_EDITOR
            container.RegisterType<IClickedPoint, InputTouch>();
#endif
            container.RegisterType<IWhereClicked, WhereClicked>();
            container.RegisterSingleton<ICameraMoving, CameraMoving>();

            //Item
            container.RegisterType<IItem, Item>();
            container.RegisterSingleton<IItemMover, ItemMover>();

            //Player
            container.RegisterSceneObject<IPlayer>("Player");
            container.RegisterSceneObject<IMyGrid>("GridPathFinding");
            container.RegisterSingleton<IPlayerController, PlayerController2D>();


            //Warehouse
            container.RegisterSingleton<IWarehouseController, WarehouseController>();
            container.RegisterSceneObject<IWarehouseUI>("Windows/Warehouse");
            container.RegisterSingleton<IResourceFactory, ResourceFactory>();
            container.RegisterSingleton<IGenerationWarehouseUIPanels, GenerationWarehouseUIPanels>();
            container.RegisterSingleton<IContainerAllResources, ContainerAllResources>();
            container.RegisterSceneObject<IContainerAllResourcesMono>("Conteiner");
            container.RegisterSceneObject<IWarehouseAddition>("Conteiner");

            //PassiveRes
            container.RegisterSingleton<IPassiveResourcesController, PassiveResourcesController>();
            container.RegisterSceneObject<IPassiveResourcesUI>("Windows/GameUI");

            //Trash
            container.RegisterSingleton<ITrashController, TrashController>();
            container.RegisterSingleton<ITrashQueue, TrashQueue>();
            container.RegisterSceneObject<ITrashsContainerMono>("World/Grid/Trashs");

            //Builder
            container.RegisterSingleton<IBuilderPlayer, BuilderPlayer>();
            container.RegisterSingleton<IGenerationBuilderUIPanels, GenerationBuilderUIPanels>();
            container.RegisterSceneObject<IAllStructures>("Conteiner");
            container.RegisterSceneObject<IBaseStructureIDContainerMono>("World/*/BaseStructure");
            container.RegisterSceneObject<IBuilderUI>("Windows/BuildStructures");
            container.RegisterSceneObject<IBuilderWindow>("Windows/BuildStructures/BackGroung");

            //Craft
            container.RegisterSingleton<ICraftController, CraftController>();
            container.RegisterSingleton<IFactoryCraftPanel, FactoryCraftPanel>();
            container.RegisterSceneObject<ICraftUI>("Windows/Craft");
            container.RegisterSceneObject<IContainerRecipesMono>("Conteiner");
            container.RegisterSingleton<IContainerRecipes, ContainerRecipes>();
            container.RegisterSingleton<ICraftBonusController, CraftBonusController>();
            container.RegisterType<IRecipe, Recipe>();

            //Buisness
            container.RegisterSingleton<IBusinessController, BusinessController>();
            container.RegisterSceneObject<IBusinessUI>("Windows/Business");

            //Tutorial
            container.RegisterSingleton<ITutorialController, TutorialController>();
            container.RegisterSceneObject<ITutorialContainerMono>("Conteiner");
            container.RegisterSceneObject<ITutorialUIView>("Windows/TutorialWindow");

            //NewTutorial
            container.RegisterSingleton<INewTutorialController, NewTutorialController>();
            container.RegisterSceneObject<INewTutorialContainer>("Conteiner");
            container.RegisterSceneObject<INewTutorialUI>("Windows/NewTutorial");


            //Quests
            container.RegisterSingleton<IQuestController, QuestController>();
            container.RegisterSceneObject<IQuestContainerMono>("Conteiner");
            container.RegisterSingleton<IQuestContainer, QuestContainer>();
            container.RegisterSceneObject<IQuestUI>("Windows/Quests");
            container.RegisterSingleton<IQuestPanel, QuestPanel>();

            //Sound
            container.RegisterSingleton<ISoundController, SoundController>();
            container.RegisterSceneObject<ISoundSourceView>("SoundSource");
            container.RegisterSceneObject<ISoundsContainer>("Conteiner");

            //Settings
            container.RegisterSingleton<ISettingsController, SettingsController>();
            container.RegisterSceneObject<ISettingsUI>("Windows/Settings");

            //Messager
            container.RegisterSingleton<IMessageSystemController, MessageSystemController>();
            container.RegisterSceneObject<IMessageSystemUI>("Windows/MessageSystem");

            //SaveAndLoad
            container.RegisterSingleton<ISaveAndLoad, SaveAndLoadBinary>();
            container.RegisterSingleton<IFileManager, FileManagerBinary>();

            //Technology
            container.RegisterSingleton<ITechnologyController, TechnologyController>();
            container.RegisterSingleton<ICraftBonusController, CraftBonusController>();
            container.RegisterSingleton<IBusinessBonusController, BusinessBonusController>();
            container.RegisterSingleton<ITechnologyUIController, TechnologyUIController>();
            container.RegisterSceneObject<ITechnologyConfigContainer>("Conteiner");
            container.RegisterSceneObject<ITechnologyUIView>("Windows/Technology");

            //Shop
            container.RegisterSingleton<IShopController, ShopController>();
            container.RegisterSceneObject<IShopUI>("Windows/Shop");

            //Energy
            container.RegisterSingleton<IEnergyController, EnergyController>();

            //Donat
            container.RegisterSingleton<IDonatController, DonatController>();
            container.RegisterSingleton<IMyInAppManager, MyInAppManager>();

            //Metric
            container.RegisterSingleton<IMyMetrica, MyMetrica>();
            
            //RewardWindow
            container.RegisterSceneObject<IRewardUI>("Windows/MessageSystem/Reward");
            
            //PlayPointer
            container.RegisterSingleton<IPlayerPointer, PlayerPointer>();
            container.RegisterSceneObject<IPlayerPointerUI>("Windows/PointerPanel");

            //DryLand
            container.RegisterSingleton<IDryLandController, DryLandController>();
            container.RegisterSingleton<IVegetationCorruptControl, VegetationCorruptControl>();
            container.RegisterSceneObject<IVegetationCorruptMono>("CorruptVegetation");
            
            // GhostMovement
            container.RegisterSceneObject<ICameraParameters>("MainCamera");
        }
        
        private void OnApplicationFocus(bool focus)
        {
            gameMain.Focus(focus);
        }
        
        private void OnApplicationPause(bool pause)
        {
            //gameMain.Focus(pause);
        }
        
        private void OnDestroy()
        {
            gameMain.OnDestroy();
        }
    }
}
