using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/FlyingItem", order = 1)]
    public class FlyingItem : ScriptableObject
    {
        public float MinSpawnOffset;
        public float MaxSpawnOffset;
        public float ScatterDuration;
        public float FadeOutDuration;
        public float MoveToTargetTime;
        public float SecondsBeforeFlyToTarget;
    }
}
