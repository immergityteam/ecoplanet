using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/TypeConfig", order = 1)]
    public class TypeConfigSO : ScriptableObject {
        public StringsSO Name;
        public TypeRes type;
        public int maxCountCell;
    }
}