﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/ResurceConfig", order = 1)]
    public class ResurceConfigSO : ScriptableObject {
        public StringsSO nameRes;
        public Sprite sprite;
        public int maxCountOneCell;
        public TypeRes type;
        public bool canSell;
        public bool canPut;
        public ItemsTargets Target;
    }
}
