﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "UpPanelData", menuName = "SO/UpPanelData", order = 1)]
    public class UpPanelData : ScriptableObject {
        public LeftPanelData[] leftPanelDatas;
    }
}