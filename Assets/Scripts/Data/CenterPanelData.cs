﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "CenterPanelData", menuName = "SO/CenterPanelData", order = 1)]
    public class CenterPanelData : ScriptableObject {
        public StringsSO Name;
        public StructureData[] structureDatas;
    }
}