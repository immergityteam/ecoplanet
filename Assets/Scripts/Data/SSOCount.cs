using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "SSOCount", menuName = "SO/Quest/SSOCount", order = 1)]
    public class SSOCount : ScriptableObject
    {
        public StringsSO SSO;
        public int count;
        public TypeFieldQuests type;
    }
}