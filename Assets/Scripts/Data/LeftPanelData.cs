﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "LeftPanelData", menuName = "SO/LeftPanelData", order = 1)]
    public class LeftPanelData : ScriptableObject {
        public StringsSO Name;
        public CenterPanelData[] centerPanelDatas;
    }
}