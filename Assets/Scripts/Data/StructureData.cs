﻿using UnityEngine;

namespace ColLobOK {
    [CreateAssetMenu(fileName = "Data", menuName = "SO/StructureData", order = 1)]
    public class StructureData : ScriptableObject {
        public StringsSO Name;
        public Sprite sprite;
        public int timeBuild;
        public Resource[] needResours;
        public Resource[] returnOnDestruction;
        public Resource[] requirementsPasRes;
        public Resource[] productionPasRes;
        public GameObject prefab;
        public bool buildingMoreOne;

        public int ID { get => Name.GetID(); }
    }
}