﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/TrashData", order = 1)]
    public class TrashData : ScriptableObject {
        public StringsSO myName;
        public Resource[] resources;
        public bool onlyRobots;
    }
}
