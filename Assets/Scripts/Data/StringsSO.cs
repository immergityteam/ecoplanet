using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "String", menuName = "SO/String", order = 1)]
    public class StringsSO : ScriptableObject
    {
        [SerializeField] private int id;
        [Header("RU, ENG...")]
        [TextArea(1,100)]
        [SerializeField] private string[] text;
        public string GetString() => text[LocalizationID.Instance.LanguageID];

        public int GetID() => id;
        public int ID { get => id; }
#if UNITY_EDITOR
        public string GetStringEdit() => text[0];
        public void SetID(int val) => id = val;
        public void SetStrings(string[] str) => text = str;
        public void SetString(string str, int id) => text[id] = str;
        public string[] GetStrings() => text;
#endif
    }
}