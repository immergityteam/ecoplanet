﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace ColLobOK
{
    [Serializable] public struct WhereWhat {
        public Transform parent;
        public GameObject prefab;
    }
    public enum StateBuilder {
        None,
        Ground,
        Structure,
        Moving,
        Deleting
    }

    public enum MessageType
    {
        Warning,
        DeleteWarning
    }
    [Serializable] public struct Resource {
        public StringsSO name;
        public int count;

        public int ID => name.GetID();
        public string Name => name.GetString();
        public Resource(Resource val) {
            name = val.name;
            count = val.count;
        }
        public Resource(StringsSO name, int count) {
            this.name = name;
            this.count = count;
        }
    }
    public enum EventName {
        None,
        Trash,
        Structure,
        AddTrashToQueue
    }
    [Serializable] public struct Vector2MinMax {
        public Vector2 min;
        public Vector2 max;
    }
    public enum TypeRes {
        RawMaterials,
        Materials,
        Products,
        Rations,
        Wallet
    }

    public enum ItemsTargets
    {
        Storage,
        Prestige,
        Money,
        Donate,
        Energy,
    }
    [Serializable]  public struct ActiveRecipe {
        public int recipeID;
        public double timeStart;
    }

    public enum TypeFieldQuests
    {
        Create,     //Создать
        Raise,      //Поднять
        Get,        //Добыть
        Build,      //Построить
        Accelerate, //Ускорить
        GiveAway,   //Отдать
        LvlUp,      //ЛвлУп
        Character,  //Персонаж
        Resource,   //Ресурс
        Experience	//Опыт
    }

    public struct TimeFocus
    {
        public DateTime OnFocus;
        public double DeltaFocus;
        public DateTime OffFocus;
    }
    [Serializable] public struct TileBasePersent
    {
        public TileBase tile;
        public int persent;
    }

}