﻿using UnityEngine.Purchasing;

public interface IMyMetrica
{
    bool CheckNet();
    void ReportInApp(PurchaseEventArgs purch);
    void ReportPurchase(string purchaseName);
    void ReportBuilder(string builderName);
    void ReportQuest(string questName);
    void ReportMetric(string eventName, string questName);
    void ReportTutorial(string block, string step);
}