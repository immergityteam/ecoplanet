﻿using UnityEngine.Events;
using UnityEngine.Purchasing;

public interface IMyInAppManager
{
    UnityAction<PurchaseEventArgs> EvbuyGood { get; set; }
    Product GetProduct(string purchaseID);
    void OnPurchaseClicked(string productId);
    string[] GetIdPurchase();
}