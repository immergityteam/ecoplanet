using ColLobOK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityDI;
using UnityEngine;
using UnityEngine.Purchasing;

public class MyMetrica : IMyMetrica,IDependent
{
    [HideInInspector] public bool IsNet;

    [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
    [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
    [Dependency] public IMyInAppManager MyInAppManager { private get; set; }
    [Dependency] public IMyTime MyTime { private get; set; }

    public void OnInjected()
    {
        SaveAndLoad.EvSave += AppMetrica.Instance.SendEventsBuffer;
        BuilderPlayer.EvBuiltConstruction += (x) => ReportMetric("BuiltConstruction", BuilderPlayer.GetStructureData(x).Name.GetString());
        BuilderPlayer.EvBuildingDelete += (x) => ReportMetric("BuildingDelete", BuilderPlayer.GetStructureData(x).Name.GetString());
        MyInAppManager.EvbuyGood += ReportInApp;
        MyTime.EvTimeFocus += ReportFocus;
        ReportMetric("DeltaTime", MyTime.GetDeltaTime().ToString());
        ReportMetric("StartTime", MyTime.GetDateTime().ToString());
    }

    public bool CheckNet()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            return false;
        }
        else if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
                 Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            return true;
        }
        return false;
    }

    public void ReportInApp(PurchaseEventArgs purch)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("ad_type", purch.purchasedProduct.definition.id);
        dictionary.Add("currency", purch.purchasedProduct.metadata.isoCurrencyCode);
        dictionary.Add("price", (float)purch.purchasedProduct.metadata.localizedPrice); //success not_available
        dictionary.Add("inapp_type", purch.purchasedProduct.definition.type.ToString());

        AppMetrica.Instance.ReportEvent("payment_succeed", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }

    public void ReportPurchase(string purchaseName)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("purchase", purchaseName);
        AppMetrica.Instance.ReportEvent("builder", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }

    public void ReportBuilder(string builderName)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("builder", builderName);
        AppMetrica.Instance.ReportEvent("builder", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }

    public void ReportQuest(string questName)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("quest", questName);
        AppMetrica.Instance.ReportEvent("quest", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }

    public void ReportMetric(string messageName, string info)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add(messageName, info);
        AppMetrica.Instance.ReportEvent(messageName, dictionary);
    }

    private void ReportFocus(TimeFocus arg0)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add("OnFocus", arg0.OnFocus.ToString("G", CultureInfo.CreateSpecificCulture("es-ES")));
        dictionary.Add("OffFocus", arg0.OffFocus.ToString("G", CultureInfo.CreateSpecificCulture("es-ES")));
        dictionary.Add("DeltaFocus", arg0.DeltaFocus.ToString("0.0"));

        AppMetrica.Instance.ReportEvent("TimeFocus", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }
    public void ReportTutorial(string block, string step)
    {
        Dictionary<string, object> dictionary = new Dictionary<string, object>();
        dictionary.Add(block, step);

        AppMetrica.Instance.ReportEvent("Tutorial", dictionary);
        AppMetrica.Instance.SendEventsBuffer();
    }
}
