﻿using System;
using System.Collections.Generic;
using ColLobOK;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;


public class MyInAppManager : IMyInAppManager, IStoreListener, IDependent
{
    [Dependency] public IDonatController DonatController { private get; set; }

    public string[] IdPurchase;

    private IStoreController controller;
    private IExtensionProvider extensions;
    private IAppleExtensions m_AppleExtensions;

    public UnityAction<PurchaseEventArgs> EvbuyGood { get; set; }

    //Рабочие настройки магазина

    private bool isPurchaseClicked;

    private IWarehouseController WarehouseController;
    private IContainerMono _containerMono;

    public enum RewardEnum
    {
        Subscribe,
        Default,
        Halloween
    }

    public void OnInjected()
    {
        IdPurchase = new[]
        {
            "immergity.eco.planet.150hard",
            "immergity.eco.planet.750hard",
            "immergity.eco.planet.1800hard",
            "immergity.eco.planet.3000hard",
            "immergity.eco.planet.6000hard"
        };
        MyIAPManagerInit();
    }
    private void AddHard(string s)
    {
        int cost = 0;
        switch (s)
        {
            case "immergity.eco.planet.150hard":
                {
                    cost = 150;
                    break;
                }
            case "immergity.eco.planet.750hard":
                {
                    cost = 750;
                    break;
                }
            case "immergity.eco.planet.1800hard":
                {
                    cost = 1800;
                    break;
                }
            case "immergity.eco.planet.3000hard":
                {
                    cost = 3000;
                    break;
                }
            case "immergity.eco.planet.6000hard":
                {
                    cost = 6000;
                    break;
                }
        }
        DonatController.AddDonat(cost);
    }

    public string[] GetIdPurchase()
    {
        return IdPurchase;
    }

    public Product GetProduct(string purchaseID)
    {
        if (controller != null)
        {
            Product product = controller.products.WithID(purchaseID);
            return product;
        }
        return null;
    }

    public void MyIAPManagerInit()
    {
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //IF PRODUCTS NAMES ARE SAME
        //builder.AddProduct(SUB_Month, ProductType.Subscription);

        foreach (var s in IdPurchase)
        {
            builder.AddProduct(s, ProductType.Consumable);
        }

        //builder.AddProduct(Default, ProductType.NonConsumable);

        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        this.controller = controller;
        this.extensions = extensions;
        // m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
        //
        // Product product = controller.products.WithID(_myProduct.IdProduct);
        // if (product != null)
        // {
        //     if (product.hasReceipt)
        //     {
        //         //Покупка куплена!!!
        //         _myProduct.SoldOut();
        //         Debug.Log("Sold");
        //     }
        //     else
        //     {
        //         //тогда настраиваем кнопку
        //         _myProduct.Init(product);
        //     }
        // }
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogError("XXX_Failed to initialize purchases! Reason: " + error);
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        // isPurchaseClicked = false;
        // _myShop.CloseLoadBanner();
        Debug.LogError(String.Format("XXX_OnPurchaseFailed. Product '{0}' PurchaseFailureReason: '{1}'", i.definition,
            p.ToString()));
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) //Когда оплата прошла
    {
        foreach (var s in IdPurchase)
        {
            if (String.Equals(e.purchasedProduct.definition.id, s, StringComparison.Ordinal))
            {
                AddHard(s);
                EvbuyGood?.Invoke(e);
                return PurchaseProcessingResult.Complete;
            }
        }
        Debug.LogError(String.Format("XXX_ProcessPurchase: FAIL. Unrecognized product: '{0}'",
                e.purchasedProduct.definition));


        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseClicked(string productId)
    {
        controller.InitiatePurchase(productId);
    }

    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!(controller != null && extensions != null))
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.Android)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = extensions.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result +
                          ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

}