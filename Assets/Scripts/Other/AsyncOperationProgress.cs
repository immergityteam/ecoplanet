using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ColLobOK
{
    public class AsyncOperationProgress : MonoBehaviour
    {
        [SerializeField] private TMP_Text m_Text;
        [SerializeField] private StringsSO loadingText;
        [SerializeField] private RectTransform fillArea;

        private float defWidth;
        void Start()
        {
            StartCoroutine(LoadScene());
        }
        IEnumerator LoadScene()
        {
            yield return null;
            defWidth = fillArea.sizeDelta.x;
            fillArea.sizeDelta = new Vector2(0, fillArea.sizeDelta.y);
            AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("MainScene");
            while (!asyncOperation.isDone)
            {
                m_Text.text = loadingText.GetString() + " " + (asyncOperation.progress * 111).ToString("0") + "%";
                fillArea.sizeDelta = new Vector2(Mathf.Lerp(0, defWidth, asyncOperation.progress * 1.11f), fillArea.sizeDelta.y);
                yield return null;
            }
        }
    }
}