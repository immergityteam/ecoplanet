using UnityEngine;

public static class MyMethods
{
    /// <summary>
    /// ������� ������ � ������ �����, �����, ������
    /// </summary>
    /// <param name="val">����������� ������</param>
    /// <param name="HMS">���������� ����, ��� ������ ������</param>
    /// <returns></returns>
    public static string ToTimeHMS(int val, string HMS)
    {
        string time = "";
        int count = 0;
        int temp = val / 3600;
        if (temp > 0) { time += temp + HMS[0].ToString() + " "; count++; }
        val -= temp * 3600;
        temp = val / 60;
        if (temp > 0) { time += temp + HMS[1].ToString() + " "; count++; }
        if (count == 2) return time;
        val -= temp * 60;
        if (val >= 0) time += val + HMS[2].ToString();
        return time;
    }
    public static string PaintText(string text, Color color)
    {
        return "<color=#" + ColorUtility.ToHtmlStringRGB(color) + ">" + text + "</color>";
    }
    public static string PaintText(int count, Color color)
    {
        return PaintText(count.ToString(), color);
    }
}
