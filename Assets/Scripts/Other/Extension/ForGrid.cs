﻿using UnityEngine;

public static class ForGrid {
    public static Vector3Int NearestCell(this Grid grid, Vector3 point) {
        float distance = (grid.CellToWorld(Vector3Int.right + Vector3Int.up).x - grid.CellToWorld(Vector3Int.zero).x) / 2;
        return grid.LocalToCell(point + Vector3.up * distance);
    }
    public static Vector3 NearestPointOnGrid(this Grid grid, Vector3 point) {
        return grid.CellToLocal(grid.NearestCell(point));
    }
}
