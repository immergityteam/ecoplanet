using System.Collections.Generic;
using UnityEngine;
using ColLobOK;
using Object = UnityEngine.Object;
using UnityEditor;

namespace Developing
{
    public class GeneratorRecipeSO : MonoBehaviour
    {
        [Header("Материалы")]
        public Object CVS;
        int IDName;
        List<Dictionary<string, string>> exel;

#if UNITY_EDITOR

        [ContextMenu("GeneratorRecipeSO")]
        void Generator()
        {
            exel = MetodForEditor.Parsing(CVS);
            IDName = 1;
            foreach (Dictionary<string, string> item in exel)
            {
                IDName++;
                if (item["Ресурс"] == "") continue;
                if (item["Будет в демо"] != "Да") continue;

                GenRecipe(item);
            }

            AssetDatabase.SaveAssets();
        }
        
        private void GenRecipe(Dictionary<string, string> item)
        {
            RecipeSO RSO = MetodForEditor.GetRecipeSO(IDName + " " + item["Ресурс"]);

            RSO.product.name = MetodForEditor.GetStringsSO(item["Ресурс"]);
            RSO.product.count = int.Parse(item["Количество"]);
            RSO.timeSec = int.Parse(item["Время создания (с)"]);

            ResurceConfigSO RCSO = MetodForEditor.GetResurceConfig(RSO.product.name.name);
            Sprite image = MetodForEditor.GetSprite(item["Icon"]);
            RCSO.sprite = image;
            EditorUtility.SetDirty(RCSO);

            RSO.needResours = MetodForEditor.GetResurse(item["Состав"]);
            EditorUtility.SetDirty(RSO);

            SetStrucrureData(item["Место"], RSO);
        }


        private void SetStrucrureData(string structure, RecipeSO RSO)
        {
            StructureRecipeProductionSO SRPSO = MetodForEditor.GetRecipeProduction(structure);
            SRPSO.nameStructure = MetodForEditor.GetStringsSO(structure);

            List<RecipeSO> recipes = new List<RecipeSO>();
            if (SRPSO.recipes != null) recipes.AddRange(SRPSO.recipes);

            bool thereIs = false;
            int i = 0;
            while (i < recipes.Count)
            {
                if (recipes[i] == RSO) thereIs = true;
                if (recipes[i] == null) recipes.RemoveAt(i);
                else i++;
            }
            if (!thereIs) recipes.Add(RSO);
            SRPSO.recipes = recipes.ToArray();
            EditorUtility.SetDirty(SRPSO);
        }
#endif
    }
}