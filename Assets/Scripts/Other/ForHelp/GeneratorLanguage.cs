using ColLobOK;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Developing
{
    public class GeneratorLanguage : MonoBehaviour
    {
#if UNITY_EDITOR

        [Header("�����")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

        [ContextMenu("SetStringLanguage")]
        void SetAllStringID()
        {
            exel = MetodForEditor.Parsing(CVS);

            StringsSO temp;

            for (int i = 0; i < exel.Count; i++)
            {
                if (exel[i]["ID"] != "")
                {
                    temp = MetodForEditor.GetStringsSO(int.Parse(exel[i]["ID"]));
                    if (temp)
                    {
                        if (temp.GetID() == int.Parse(exel[i]["ID"]))
                        {
                            temp.SetString(exel[i]["�������"], 0);
                            temp.SetString(exel[i]["English"], 1);
                            EditorUtility.SetDirty(temp);
                        }
                        else
                        {
                            Debug.Log("ID �� ���������???");
                        }
                    }
                }
            }
            AssetDatabase.SaveAssets();
        }
#endif
    }
}