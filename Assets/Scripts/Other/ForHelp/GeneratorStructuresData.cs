using System.Collections.Generic;
using UnityEngine;
using ColLobOK;
using Object = UnityEngine.Object;
using UnityEditor;
using System.Linq;

namespace Developing
{
    public class GeneratorStructuresData : MonoBehaviour
    {
        [Header("Крафт зданий")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

#if UNITY_EDITOR

        [ContextMenu("GeneratorStructuresData")]
        void Generator()
        {
            exel = MetodForEditor.Parsing(CVS);

            for (int testInt = 0; testInt < exel.Count; testInt++) GenStructuresData(exel[testInt]);

            AssetDatabase.SaveAssets();
        }

        private void GenStructuresData(Dictionary<string, string> valuePairs)
        {
            if (valuePairs["Название"] == "") return;
            Debug.Log(valuePairs["Название"]);
            //Название
            StructureData SD = MetodForEditor.GetStructuresDataSO(valuePairs["Название"]);

            //Время стройки 1
            SD.timeBuild = int.Parse(valuePairs["Время стройки 1"]);

            //Арт
            SD.sprite = MetodForEditor.GetSprite(valuePairs["Арт"]);
            SD.prefab = MetodForEditor.GetPrefab(valuePairs["Арт"]);

            //Можно строить несколько
            SD.buildingMoreOne = valuePairs["Можно строить несколько"] == "Да";

            //Материалы 1
            if (valuePairs["Материалы 1"] != "") SD.needResours = MetodForEditor.GetResurse(valuePairs["Материалы 1"]);

            //Требования
            if (valuePairs["Требования"] != "") SD.requirementsPasRes = MetodForEditor.GetResurse(valuePairs["Требования"]);

            //Выроботка
            if (valuePairs["Выроботка"] != "") SD.productionPasRes = MetodForEditor.GetResurse(valuePairs["Выроботка"]);

            //Возврат при разрушении
            if (valuePairs["Возврат при разрушении"] != "") SD.returnOnDestruction = MetodForEditor.GetResurse(valuePairs["Возврат при разрушении"]);
            EditorUtility.SetDirty(SD);

            //Где
            if (valuePairs["Где"] != "") Tabs(SD, valuePairs["Где"]);
        }

        private void Tabs(StructureData SD, string str)
        {
            string assetPathSD = "Assets/Generator/StructuresGroup/";
            string[] tab = str.Split('|');
            tab[0] = tab[0].Trim(' ');
            tab[1] = tab[1].Trim(' ');

            CenterPanelData[] centerPanelDatas = MetodForEditor.GetAllAssets<CenterPanelData>();
            foreach (var item in centerPanelDatas)
            {
                if (item.Name.GetStringEdit() == tab[1])
                {
                    List<StructureData> tempSD = item.structureDatas.ToList();
                    //while(tempSD.Contains(null)) tempSD.Remove(null);
                    if (!tempSD.Contains(SD)) tempSD.Add(SD);
                    item.structureDatas = tempSD.ToArray();
                    EditorUtility.SetDirty(item);
                    return;
                }
            }
            CenterPanelData CPD = ScriptableObject.CreateInstance<CenterPanelData>();
            CPD.Name = MetodForEditor.GetStringsSO(tab[1]);
            CPD.structureDatas = new StructureData[1] { SD };
            AssetDatabase.CreateAsset(CPD, assetPathSD + tab[1] + ".asset");
            AssetDatabase.SaveAssets();

            LeftPanelData[] leftPanelDatas = MetodForEditor.GetAllAssets<LeftPanelData>();
            foreach (var item in leftPanelDatas)
            {
                if (item.Name.GetStringEdit() == tab[0])
                {
                    List<CenterPanelData> tempCPD = item.centerPanelDatas.ToList();
                    //while (tempCPD.Contains(null)) tempCPD.Remove(null);
                    if (!tempCPD.Contains(CPD)) tempCPD.Add(CPD);
                    item.centerPanelDatas = tempCPD.ToArray();
                    EditorUtility.SetDirty(item);
                    return;
                } 
            }
            LeftPanelData LPD = ScriptableObject.CreateInstance<LeftPanelData>();
            LPD.Name = MetodForEditor.GetStringsSO(tab[0]);
            LPD.centerPanelDatas = new CenterPanelData[1] { CPD };
            AssetDatabase.CreateAsset(LPD, assetPathSD + tab[0] + ".asset");
            AssetDatabase.SaveAssets();

            UpPanelData upPanelData = MetodForEditor.GetAllAssets<UpPanelData>()[0];
            List<LeftPanelData> tempLPD = upPanelData.leftPanelDatas.ToList();
            if (!tempLPD.Contains(LPD)) tempLPD.Add(LPD);
            upPanelData.leftPanelDatas = tempLPD.ToArray();
            EditorUtility.SetDirty(upPanelData);
        }
#endif
    }
}