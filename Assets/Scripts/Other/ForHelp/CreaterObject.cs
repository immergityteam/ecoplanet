using UnityEngine;
using UnityEditor;
using ColLobOK;

namespace Developing
{
    public class CreaterObject : MonoBehaviour
    {
        public GameObject basePrefab;
        public GameObject trashPrefab;

        public bool creatObjectScene;

#if UNITY_EDITOR
        [ContextMenu("Create")]
        public void Create()
        {
            StructureData[] SD = MetodForEditor.GetAllAssets<StructureData>();

            GameObject prefab;
            for (int i = 0; i < SD.Length; i++)
            {
                if (SD[i].sprite.name == "coin") continue;
                prefab = MetodForEditor.GetPrefab(SD[i].sprite.name);
                if (prefab == null)
                {
                    prefab = CreatePrefab(basePrefab, SD[i].sprite);
                }
                if (creatObjectScene) CreatGO(prefab);
                MonoID monoID = prefab.GetComponent<MonoID>();
                if (monoID)
                {
                    monoID.SetGlobalID(SD[i].Name);
                    EditorUtility.SetDirty(prefab);
                }
                
                SD[i].prefab = prefab;
                EditorUtility.SetDirty(SD[i]);
            }
        }

        [ContextMenu("CreateTrash")]
        public void CreateTrash()
        {
            TrashData[] TD = MetodForEditor.GetAllAssets<TrashData>();

            GameObject prefab;
            for (int i = 0; i < TD.Length; i++)
            {
                prefab = MetodForEditor.GetPrefab(TD[i].name);
                if (prefab == null)
                {
                    prefab = CreatePrefab(trashPrefab, MetodForEditor.GetSprite(TD[i].name));
                }
                prefab.GetComponent<Trash>().SetTrashData(TD[i]);
                EditorUtility.SetDirty(prefab);
                if (creatObjectScene) CreatGO(prefab);
                EditorUtility.SetDirty(TD[i]);
            }
        }

        private GameObject CreatePrefab(GameObject pref, Sprite sprite)
        {
            string variantAssetPath = "Assets/Generator/Building/";
            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(pref);
            obj.name = sprite.name;
            obj.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>().sprite = sprite;
            obj.transform.position = Vector3.zero;

            GameObject newPref = PrefabUtility.SaveAsPrefabAsset(obj, variantAssetPath + sprite.name + ".prefab");
            DestroyImmediate(obj);
            return newPref;
        }
        private void CreatGO(GameObject GO)
        {
            GameObject obj = (GameObject)PrefabUtility.InstantiatePrefab(GO);
            obj.transform.parent = transform;
            obj = obj.transform.GetChild(0).GetChild(0).gameObject;
            obj = Instantiate(obj, obj.transform);
            obj.GetComponent<SpriteRenderer>().sprite = MetodForEditor.GetSprite(obj.GetComponent<SpriteRenderer>().sprite.name + "_z");
        }
#endif
    }
}