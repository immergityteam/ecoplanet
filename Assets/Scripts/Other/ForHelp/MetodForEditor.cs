using ColLobOK;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Developing
{
    public static class MetodForEditor
    {
#if UNITY_EDITOR

        private static readonly string reg = "(?>= ^|,)(\"(?:[^\"]| \"\")*\"|[^,]*)";
        public static List<Dictionary<string, string>> Parsing(object CVS)
        {
            List<Dictionary<string, string>> exel = new List<Dictionary<string, string>>();
            string bigStr = CVS.ToString();

            string[] lines = bigStr.Split('\n');
            lines = lines.Select((x) => x.Trim('\r')).ToArray();

            string[] collum = Regex.Split(lines[0], reg);

            for (int i = 0; i < collum.Length; i++) { collum[i] = collum[i].Trim(' '); }

            for (int i = 1; i < lines.Length; i++)
            {
                string[] cell = Regex.Split(lines[i], reg);

                Dictionary<string, string> temp = new Dictionary<string, string>();
                for (int j = 0; j < collum.Length; j++)
                {
                    if (collum[j] != "")
                    {

                        //Debug.Log(collum[j] + " " + cell[j].Trim(' '));
                        temp.Add(collum[j], cell[j].Trim(' '));
                    }
                }

                exel.Add(temp);
            }
            return exel;
        }

        public static StringsSO GetStringsSO(string name)
        {
            string variantAssetPath = "Assets/Generator/GetStringSO/";
            name = name.Trim(' ');
            name = name.Substring(0, 1).ToUpper() + name.Substring(1, name.Length - 1);

            StringsSO[] SSO = GetAllAssets<StringsSO>();

            foreach (StringsSO item in SSO)
            {
                //Debug.Log(item.name);
                if (item.GetStringEdit() == name)
                {
                    return item;
                }
            }

            StringsSO newSSO = ScriptableObject.CreateInstance<StringsSO>();
            string[] newStr = new string[2];
            newStr[0] = name;
            newStr[1] = name;
            newSSO.SetStrings(newStr);
            name = Replase(name);
            Debug.Log("������ " + name);
            AssetDatabase.CreateAsset(newSSO, variantAssetPath + name + ".asset");
            AssetDatabase.SaveAssets();

            return newSSO;
        }
        public static StringsSO GetStringsSO(int ID)
        {
            StringsSO[] SSO = GetAllAssets<StringsSO>();

            foreach (StringsSO item in SSO)
            {
                if (item.GetID() == ID)
                {
                    return item;
                }
            }
            return null;
        }
        public static Sprite GetSprite(string icon)
        {
            Sprite[] sprites = GetAllAssets<Sprite>();

            Sprite image = null;
            Sprite coin = null;
            for (int i = 0; i < sprites.Length; i++)
            {
                if (sprites[i].name == "coin")
                {
                    coin = sprites[i];
                }
                if (sprites[i].name == icon)
                {
                    image = sprites[i];
                    break;
                }
            }

            if (!image)
            {
                Debug.Log("��� ������: " + icon);
                image = coin;
            }
            return image;
        }
        public static GameObject GetPrefab(string name)
        {
            List<GameObject> allPrefab = new List<GameObject>();

            foreach (var asset in AssetDatabase.FindAssets("t:Prefab"))
            {
                var path = AssetDatabase.GUIDToAssetPath(asset);
                var aaa = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                allPrefab.Add(aaa);
            }

            for (int i = 0; i < allPrefab.Count; i++)
            {
                if (allPrefab[i].name == name)
                {
                    return allPrefab[i];
                }
            }

            return null;
        }

        public static Resource[] GetResurse(string str)
        {
            string[] needResStr = str.Split('+');
            Resource[] needResStruc = new Resource[needResStr.Length];
            for (int i = 0; i < needResStr.Length; i++)
            {
                string[] bee = needResStr[i].Split('x');
                needResStruc[i].name = GetStringsSO(bee[0]);
                if (bee.Length == 1)
                    needResStruc[i].count = 1;
                else
                {
                    //Debug.Log(bee[1]);
                    needResStruc[i].count = int.Parse(bee[1]);
                }
            }
            return needResStruc;
        }

        public static StructureData GetStructuresDataSO(string name)
        {
            string assetPathSD = "Assets/Generator/GeneratorStructureData/";
            name = name.Trim(' ');
            StructureData[] SD = GetAllAssets<StructureData>();

            for (int i = 0; i < SD.Length; i++)
            {
                if (SD[i].name == name || SD[i].Name?.GetStringEdit() == name)
                {
                    return SD[i];
                }
            }

            StructureData newSD = ScriptableObject.CreateInstance<StructureData>();
            newSD.Name = GetStringsSO(name);

            name = Replase(name);
            AssetDatabase.CreateAsset(newSD, assetPathSD + name + ".asset");
            AssetDatabase.SaveAssets();
            return newSD;
        }
        public static StructureRecipeProductionSO GetRecipeProduction(string name)
        {
            string assetPathSRPSO = "Assets/Generator/SRPSO/";
            StructureRecipeProductionSO[] SRPSO = GetAllAssets<StructureRecipeProductionSO>();

            for (int i = 0; i < SRPSO.Length; i++)
            {
                if (SRPSO[i].name == name || (SRPSO[i].nameStructure && SRPSO[i].nameStructure.GetStringEdit() == name))
                {
                    return SRPSO[i];
                }
            }

            StructureRecipeProductionSO newSRPSO = ScriptableObject.CreateInstance<StructureRecipeProductionSO>();

            newSRPSO.defCounActiveRicepes = 3;

            name = Replase(name);
            AssetDatabase.CreateAsset(newSRPSO, assetPathSRPSO + name + ".asset");
            AssetDatabase.SaveAssets();
            return newSRPSO;
        }
        public static RecipeSO GetRecipeSO(string name)
        {
            string assetPathRSO = "Assets/Generator/GeneratorRecipeSO/";
            RecipeSO[] RSO = GetAllAssets<RecipeSO>();

            for (int i = 0; i < RSO.Length; i++)
            {
                if (RSO[i].name == name || RSO[i].product.name?.GetStringEdit() == name)
                {
                    return RSO[i];
                }
            }

            RecipeSO newRSO = ScriptableObject.CreateInstance<RecipeSO>();
            name = Replase(name);
            AssetDatabase.CreateAsset(newRSO, assetPathRSO + name + ".asset");
            AssetDatabase.SaveAssets();
            return newRSO;
        }
        public static BusinessSO GetBusinessSO(string name)
        {
            string assetPathBSO = "Assets/Generator/GeneratorBusinessSO/";
            BusinessSO[] BSO = GetAllAssets<BusinessSO>();

            for (int i = 0; i < BSO.Length; i++)
            {
                if (BSO[i].name == name || BSO[i].nameSO?.GetStringEdit() == name)
                {
                    return BSO[i];
                }
            }

            BusinessSO newBSO = ScriptableObject.CreateInstance<BusinessSO>();
            newBSO.nameSO = GetStringsSO(name);
            name = Replase(name);
            AssetDatabase.CreateAsset(newBSO, assetPathBSO + name + ".asset");
            AssetDatabase.SaveAssets();
            return newBSO;
        }

        public static BoostSO GetBoostSO(string name)
        {
            string assetPathBSO = "Assets/Generator/GeneratorBusinessSO/BoostSO/";
            BoostSO[] BSO = GetAllAssets<BoostSO>();

            for (int i = 0; i < BSO.Length; i++)
            {
                if (BSO[i].name == name || BSO[i].nameSO?.GetStringEdit() == name)
                {
                    return BSO[i];
                }
            }

            BoostSO newBSO = ScriptableObject.CreateInstance<BoostSO>();
            newBSO.nameSO = GetStringsSO(name);
            name = Replase(name);
            AssetDatabase.CreateAsset(newBSO, assetPathBSO + name + ".asset");
            AssetDatabase.SaveAssets();
            return newBSO;
        }
        public static TrashData GetTrashData(string name)
        {
            string assetPathRSO = "Assets/Generator/GeneratorTrashSO/";
            TrashData[] TD = GetAllAssets<TrashData>();

            for (int i = 0; i < TD.Length; i++)
            {
                if (TD[i].name == name)
                {
                    return TD[i];
                }
            }

            TrashData newTD = ScriptableObject.CreateInstance<TrashData>();
            name = Replase(name);
            AssetDatabase.CreateAsset(newTD, assetPathRSO + name + ".asset");
            AssetDatabase.SaveAssets();
            return newTD;
        }

        public static T[] GetAllAssets<T>()
        {
            List<object> obj = new List<object>();

            foreach (var asset in AssetDatabase.FindAssets("t:" + typeof(T).Name))
            {
                var path = AssetDatabase.GUIDToAssetPath(asset);
                var aaa = AssetDatabase.LoadAssetAtPath(path, typeof(T));
                obj.Add(aaa);
            }

            return obj.Select(x => (T)x).ToArray();
        }

        public static MainQuest GetQuestMain(string name)
        {
            string QuestAssetPath = "Assets/Generator/QuestGenerator/Quest/Main/";
            var quests = GetAllAssets<MainQuest>();

            foreach (var quest in quests)
            {
                if (quest.GetLabel().GetStringEdit() == name)
                    return quest;
            }

            var newQuest = ScriptableObject.CreateInstance<MainQuest>();
            newQuest.SetLabel(GetStringsSO(name));

            
            name = Replase(name);
            //Debug.Log();
            AssetDatabase.CreateAsset(newQuest, QuestAssetPath + name + ".asset");
            AssetDatabase.SaveAssets();

            return newQuest;
        }
        
        public static TechQuest GetQuestTech(string name)
        {
            string QuestAssetPath = "Assets/Generator/QuestGenerator/Quest/Tech/";
            var quests = GetAllAssets<TechQuest>();

            foreach (var quest in quests)
            {
                if (quest.GetLabel().GetStringEdit() == name)
                    return quest;
            }

            var newQuest = ScriptableObject.CreateInstance<TechQuest>();
            newQuest.SetLabel(GetStringsSO(name));

            
            name = Replase(name);
            //Debug.Log();
            AssetDatabase.CreateAsset(newQuest, QuestAssetPath + name + ".asset");
            AssetDatabase.SaveAssets();

            return newQuest;
        }

        public static ResurceConfigSO GetResurceConfig(string name)
        {
            string assetPathRCSO = "Assets/Generator/Resur�eConfigSO/";

            ResurceConfigSO[] RCSOs = GetAllAssets<ResurceConfigSO>();

            ResurceConfigSO RCSO;
            for (int i = 0; i < RCSOs.Length; i++)
            {
                if (RCSOs[i].nameRes.name == name)
                {
                    return RCSOs[i];
                }
            }

            RCSO = ScriptableObject.CreateInstance<ResurceConfigSO>();

            RCSO.maxCountOneCell = 100;
            RCSO.nameRes = GetStringsSO(name);
            RCSO.type = TypeRes.Materials;
            AssetDatabase.CreateAsset(RCSO, assetPathRCSO + name + "Data.asset");
            AssetDatabase.SaveAssets();
            return RCSO;
        }

        public static string Replase(string str)
        {
            str = str.Replace('\"', ':');
            str = Regex.Replace(str, @"!|\?|:", "");
            return str;
        }
#endif
    }
}