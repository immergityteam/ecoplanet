using System.Collections.Generic;
using UnityEngine;
using ColLobOK;
using Object = UnityEngine.Object;
using UnityEditor;

namespace Developing
{
    public class GeneratorTrash : MonoBehaviour
    {
        [Header("Мусор")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

#if UNITY_EDITOR

        [ContextMenu("GeneratorTrash")]
        void Generator()
        {
            exel = MetodForEditor.Parsing(CVS);

            for (int testInt = 0; testInt < exel.Count; testInt++) GenTrashSO(exel[testInt]);

            AssetDatabase.SaveAssets();
        }

        private void GenTrashSO(Dictionary<string, string> valuePairs)
        {
            if (valuePairs["Мусор"] == "") return;
            //Арты
            TrashData TD = MetodForEditor.GetTrashData(valuePairs["Арты"]);
            //Мусор
            TD.myName = MetodForEditor.GetStringsSO(valuePairs["Мусор"]);
            //Материалы
            TD.resources = MetodForEditor.GetResurse(valuePairs["Материалы"]);
            //Можно разобрать руками
            TD.onlyRobots = valuePairs["Можно разобрать руками"] == "Нет" ? true : false;
            EditorUtility.SetDirty(TD);
        }
#endif
    }
}