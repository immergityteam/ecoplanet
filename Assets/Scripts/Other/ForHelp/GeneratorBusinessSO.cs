using System.Collections.Generic;
using UnityEngine;
using ColLobOK;
using Object = UnityEngine.Object;
using UnityEditor;

namespace Developing
{
    public class GeneratorBusinessSO : MonoBehaviour
    {
        [Header("Доход зданий")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

#if UNITY_EDITOR

        [ContextMenu("GeneratorBusinessSO")]
        void Generator()
        {
            exel = MetodForEditor.Parsing(CVS);

            for (int testInt = 0; testInt < exel.Count; testInt++) GenBusinessSO(exel[testInt]);

            AssetDatabase.SaveAssets();
        }

        private void GenBusinessSO(Dictionary<string, string> valuePairs)
        {
            if (valuePairs["Здание"] == "") return;
            //Здание
            BusinessSO BSO = MetodForEditor.GetBusinessSO(valuePairs["Здание"]);
            //Цена услуги
            BSO.price = int.Parse(valuePairs["Цена услуги"]);
            //Периодичность выплат
            BSO.time = int.Parse(valuePairs["Периодичность выплат"]);
            //Требуемый рейтинг
            BSO.prestige = int.Parse(valuePairs["Требуемый рейтинг"]);
            //Коэффициент ремонта
            BSO.countPay = int.Parse(valuePairs["Коэффициент ремонта"]);
            //Стоимость ремонта
            BSO.repair = int.Parse(valuePairs["Стоимость ремонта"]);
            //Буст
            BSO.trafficAndBoost = valuePairs["Буст"] == "Да" ? true : false;
            EditorUtility.SetDirty(BSO);
        }
#endif
    }
}