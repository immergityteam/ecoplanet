﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Developing
{
    public class PolygonColliderSize : MonoBehaviour
    {

        public Vector2 scaleSize = Vector2.one;

        public int scaleGrid = 1;
        public float scaleBound = 0.3f;

        private float baseScaleGrid = 0.25f;
        private Grid grid;
        private PolygonCollider2D myCollider;
#if UNITY_EDITOR

        [ContextMenu("SetScaleSize")]
        public void SetScaleSize()
        {
            grid = FindObjectOfType<Grid>();
            myCollider = GetComponent<PolygonCollider2D>();
            List<Vector2> vectors = new List<Vector2>();
            Vector2 toX = grid.cellSize * (baseScaleGrid * scaleGrid) * (scaleSize.x - scaleBound);
            Vector2 toY = grid.cellSize * (baseScaleGrid * scaleGrid) * (scaleSize.y - scaleBound);
            toY.x *= -1;

            vectors.Add(toX + toY);
            vectors.Add(toX - toY);
            vectors.Add(-(toX + toY));
            vectors.Add(-(toX - toY));

            Undo.RecordObject(myCollider, "SetScaleSize");
            myCollider.points = vectors.ToArray();
        }

        public bool gizmos = true;

        private void OnDrawGizmos()
        {
            if (gizmos)
            {
                myCollider = GetComponent<PolygonCollider2D>();
                Gizmos.color = Color.red;
                int i;
                for (i = 0; i < myCollider.points.Length - 1; i++)
                {
                    Gizmos.DrawLine(myCollider.points[i] + (Vector2)transform.position, myCollider.points[i + 1] + (Vector2)transform.position);
                }
                Gizmos.DrawLine(myCollider.points[i] + (Vector2)transform.position, myCollider.points[0] + (Vector2)transform.position);
            }
        }
#endif
    }
}