using ColLobOK;
using System.IO;
using UnityEditor;
using UnityEngine;

public class GeneratorCVS : MonoBehaviour
{
#if UNITY_EDITOR
    public StringsSO[] Strings;

    [ContextMenu("GenCVS")]

    void GenCVS()
    {
        string path = $"{Application.persistentDataPath}/{Application.productName}.cvs";

        string bigStr = "";

        foreach (var item in Strings)
        {
            bigStr += item.GetID().ToString() + ";" + item.GetStrings()[0] + ";" + item.GetStrings()[1] + '\n';
        }

        StreamWriter sw = File.CreateText(path);
        sw.WriteLine(bigStr);
        sw.Close();
    }
#endif
}
