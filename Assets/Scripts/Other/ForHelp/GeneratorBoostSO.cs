using System.Collections.Generic;
using UnityEngine;
using ColLobOK;
using Object = UnityEngine.Object;
using UnityEditor;

namespace Developing
{
    public class GeneratorBoostSO : MonoBehaviour
    {
        [Header("Буст еды")]
        public Object CVS;
        List<Dictionary<string, string>> exel;

#if UNITY_EDITOR

        [ContextMenu("GeneratorBoostsSO")]
        void Generator()
        {
            exel = MetodForEditor.Parsing(CVS);

            for (int testInt = 0; testInt < exel.Count; testInt++) GenBoostsSO(exel[testInt]);

            AssetDatabase.SaveAssets();
        }

        private void GenBoostsSO(Dictionary<string, string> valuePairs)
        {
            if (valuePairs["Название"] == "") return;
            //Название
            BoostSO BSO = MetodForEditor.GetBoostSO(valuePairs["Название"]);
            //Сила еды
            BSO.power = float.Parse(valuePairs["Сила еды"].Trim('"'));
            //Уровень
            BSO.level = int.Parse(valuePairs["Уровень"]);
            //Время работы
            BSO.time = int.Parse(valuePairs["Время работы"]);

            ResurceConfigSO RSO = MetodForEditor.GetResurceConfig(BSO.nameSO.GetStringEdit());
            RSO.type = TypeRes.Rations;

            EditorUtility.SetDirty(RSO);
            EditorUtility.SetDirty(BSO);
        }
#endif
    }
}