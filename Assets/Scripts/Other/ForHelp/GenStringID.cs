#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using ColLobOK;
using System.Linq;
using System.IO;

namespace Developing
{
    public class GenStringID : MonoBehaviour
    {
        [MenuItem("MyMenu/GenStringID")]
        static void SetAllStringID()
        {
            int ID = 1;
            var Strings = MetodForEditor.GetAllAssets<StringsSO>();
            ID = Strings.Max(StringsSO => StringsSO.GetID());
            ID++;
            foreach (var item in Strings)
            {
                if (item.GetID() != 0) continue;
                item.SetID(ID++);
                EditorUtility.SetDirty(item);
            }
            //ID = 1;
            //var Recipe = MetodForEditor.GetAllAssets<RecipeSO>();
            //ID = Recipe.Max(x => x.id);
            //ID++;
            //foreach (var item in Recipe)
            //{
            //    if (item.id != 0) continue;
            //    item.SetID(ID++);
            //    EditorUtility.SetDirty(item);
            //}
        }

        [MenuItem("MyMenu/GenCVS")]
        static void GenCVS()
        {
            string path = $"{Application.persistentDataPath}/{Application.productName}.cvs";

            string bigStr = "";
            var Strings = MetodForEditor.GetAllAssets<StringsSO>();

            foreach (var item in Strings)
            {
                bigStr += item.GetID().ToString() + ";" + item.GetStrings()[0] + ";" + item.GetStrings()[1] + '\n';
            }

            StreamWriter sw = File.CreateText(path);
            sw.WriteLine(bigStr);
            sw.Close();
        }
    }
}
#endif
