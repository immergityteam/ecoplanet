using TMPro;
using UnityEngine;

public class ToggleColorChange : MonoBehaviour
{
    [SerializeField] private TMP_Text text; 
    [SerializeField] private Color off;
    [SerializeField] private Color on;

    public void ColorChange(bool state) => text.color = state ? on : off;
}
