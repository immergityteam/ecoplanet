using System.Collections;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class Tiker : ITiker
    {
        [Dependency] public ICreator Creator { private get; set; }
        public UnityAction EvTickSec { get; set; }
        IEnumerator coroutine;
        private float timeTiker;
        public void StartTiker(float deltaSecond)
        {
            SetTikerTime(deltaSecond);
            StartTiker();
        }
        public void StartTiker()
        {
            if (coroutine == null)
            {
                coroutine = TikerSecond(timeTiker);
                Creator.StartCourutine(coroutine);
            }
        }
        public void StopTiker()
        {
            if (coroutine != null)
            {
                Creator.StopCourutine(coroutine);
                coroutine = null;
            }
        }

        public void Restart()
        {
            if (coroutine != null)
            {
                StopTiker();
                StartTiker();
            }
            else
            {
                StartTiker();
            }
        }

        public void SetTikerTime(float timeBeforeDismiss)
        {
            StopTiker();
            timeTiker = timeBeforeDismiss;
        }

        private IEnumerator TikerSecond(float deltaSecond)
        {
            while (true)
            {
                yield return new WaitForSeconds(deltaSecond);
                EvTickSec?.Invoke();
            }
        }
    }
}