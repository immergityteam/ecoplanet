﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class Creator : MonoBehaviour, ICreator
    {
        public MonoBehaviour MonoBehaviour => this;

        private void Awake()
        {
            other = new GameObject("OtherObj").transform;
            other.parent = transform;
            other.position = Vector3.zero;
        }

        private struct Data
        {
            public List<GameObject> GO;
            public Transform parent;
        }

        private Dictionary<GameObject, Data> poolGO = new Dictionary<GameObject, Data>();
        private Transform other;
        private void CreateKey(GameObject target)
        {
            List<GameObject> GO = new List<GameObject>();
            Transform parent = new GameObject("Pool: " + target.name).transform;
            parent.parent = transform;
            parent.localPosition = Vector3.zero;
            Data data = new Data
            {
                GO = GO,
                parent = parent
            };

            poolGO.Add(target, data);
        }

        public GameObject GetGameObjectPool(GameObject prefab, Transform parent = null)
        {
            if (!poolGO.ContainsKey(prefab))
            {
                CreateKey(prefab);
            }

            GameObject temp = poolGO[prefab].GO.Find(x => x.activeSelf == false);
            if (temp)
            {
                temp.SetActive(true);
                temp.transform.parent = parent ? parent : poolGO[prefab].parent;
            }
            else
            {
                temp = GetGameObject(prefab, parent ? parent : poolGO[prefab].parent);
                poolGO[prefab].GO.Add(temp);
            }
            return temp;
        }

        public GameObject GetGameObject(GameObject prefab, Transform parent = null)
        {
            GameObject temp = Instantiate(prefab, parent ? parent : other);
            return temp;
        }

        public GameObject GetGameObject()
        {
            GameObject temp = new GameObject();
            temp.transform.parent = other;
            return temp;
        }

        public void MyDestroyPool(GameObject obj) => obj.SetActive(false);

        public void MyDestroy(GameObject obj) => Destroy(obj);

        public void ClearKids(Transform parent)
        {
            for (int i = 0; i < parent.childCount; i++)
                Destroy(parent.GetChild(i).gameObject);
        }

        public void MyDestroy(Component component) => Destroy(component);

        public void MyDestroy(Component[] components)
        {
            for (int i = 0; i < components.Length; i++)
                Destroy(components[i]);
        }

        public void StartCourutine(IEnumerator courutine) => StartCoroutine(courutine);

        public void StopCourutine(IEnumerator courutine) => StopCoroutine(courutine);

        public void OnDestroy()
        {
            StopAllCoroutines();
        }
    }
}

