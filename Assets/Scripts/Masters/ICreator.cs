﻿using System.Collections;
using UnityEngine;

namespace ColLobOK
{

    /// <summary>
    /// Интерфейс, создания объектов
    /// </summary>

    public interface ICreator{

        GameObject GetGameObjectPool(GameObject prefab, Transform parent = null);
        GameObject GetGameObject(GameObject prefab, Transform parent = null);
        GameObject GetGameObject();
        void MyDestroyPool(GameObject obj);
        void MyDestroy(GameObject obj);
        void MyDestroy(Component component);
        void MyDestroy(Component[] components);
        void ClearKids(Transform parent);
        MonoBehaviour MonoBehaviour { get; }
        void StartCourutine(IEnumerator courutine);
        void StopCourutine(IEnumerator courutine);
        void OnDestroy();
    }
}

