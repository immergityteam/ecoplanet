using System;
using UnityDI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ColLobOK
{
    public class MyRandom : IMyRandom, IDependent {
        public void OnInjected() {
            int t = (int) DateTime.Now.Ticks;
            Random.InitState(t);
            Debug.Log("Random Seed: " + t);
        }
        public float GetRand() {
            return Random.value;
        }
        public float GetRand(float max) {
            return Random.Range(0, max);
        }
        public int GetRand(int max) {
            return Random.Range(0, max);
        }
        public float GetRandRange(float min, float max) {
            return Random.Range(min, max);
        }
        public int GetRandRange(int min, int max) {
            return Random.Range(min, max);
        }
    }
}