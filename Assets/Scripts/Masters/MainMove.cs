﻿using UnityDI;

namespace ColLobOK
{
    /// <summary>
    /// Класс, контролирующий движение
    /// </summary>
    public class MainMove : IMainMove
    {
        [Dependency] public IPlayerController PlayerController { private get; set; }

        public void Update(float deltaTime)
        {
            PlayerController.Update(deltaTime);
        }
    }
}

