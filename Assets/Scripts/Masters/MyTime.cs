using System;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class MyTime : IMyTime, IDependent
    {
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }

        public UnityAction<TimeFocus> EvTimeFocus { get; set; }
        private TimeFocus data;
        public DateTime GetDateTime() => DateTime.UtcNow;
        public double GetTimeSecUTC() => new TimeSpan(DateTime.UtcNow.Ticks).TotalSeconds;
        public double GetTimeSec() => new TimeSpan(DateTime.Now.Ticks).TotalSeconds;
        public double GetLastTime() => lastTime;
        public double GetDeltaTime() => GetTimeSecUTC() - lastTime;

        private double lastTime;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            SaveAndLoad.EvLoadOnFocus += Load;
            Load();
        }

        public void Focus(bool focus)
        {
            if (focus)
            {
                data.OnFocus = GetDateTime();
            }
            else
            {
                data.OffFocus = GetDateTime();
                data.DeltaFocus = new TimeSpan((data.OffFocus - data.OnFocus).Ticks).TotalSeconds;
                EvTimeFocus?.Invoke(data);
            }
        }

        private void Save() => SaveAndLoad.Save("MyTime", GetTimeSecUTC().ToString());
        private void Load() => double.TryParse(SaveAndLoad.Load("MyTime"), out lastTime);
    }
}