﻿using System;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IMyTime
    {
        UnityAction<TimeFocus> EvTimeFocus { get; set; }
        DateTime GetDateTime();
        double GetTimeSecUTC();
        double GetTimeSec();
        double GetLastTime();
        double GetDeltaTime();
        void Focus(bool focus);
    }
}