﻿using ColLobOK.Mechanics.Tutorial.Interface;
using UnityDI;

namespace ColLobOK
{
    /// <summary>
    /// Главный класс игры
    /// </summary>
    public class GameMain {
        [Dependency] public IMainMove MainMove { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IMyTime MyTime { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IGenerationBuilderUIPanels GenerationBuilderUIPanels { private get; set; }
        [Dependency] public ITrashController TrashController { private get; set; }
        [Dependency] public IGenerationWarehouseUIPanels GenerationWarehouseUIPanels { private get; set; }
        [Dependency] public ICameraMoving CameraMove { private get; set; }
        [Dependency] public IEnergyController EnergyController { private get; set; }
        [Dependency] public ICraftController CraftController { private get; set; }
        [Dependency] public ICoinsView CoinsView { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IContainerRecipes ContainerRecipes { private get; set; }
        [Dependency] public ISoundsContainer SoundContainer { private get; set; }
        [Dependency] public ISettingsController SettingsController { private get; set; }
        [Dependency] public ISoundController SoundController { private get; set; }
        [Dependency] public IPassiveResourcesController PassiveResourcesController { private get; set; }
        [Dependency] public IBusinessController  BusinessController { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public ISpawnerItem SpawnerItem { private get; set; }
        //[Dependency] public ITechnologyController TechnologyController { private get; set; }
        [Dependency] public ITechnologyUIController TechnologyUIController { private get; set; }
        [Dependency] public IShopController ShopController { private get; set; }
        [Dependency] public IQuestController QuestController { private get; set; }
        [Dependency] public IDonatController DonatController { private get; set; }
        [Dependency] public IPlayerPointer PlayerPointer { private get; set; }
        [Dependency] public IDryLandController DryLandController { private get; set; }
        [Dependency] public IVegetationCorruptControl VegetationCorruptControl { private get; set; }
        //[Dependency] public ITutorialController TutorialController { private get; set; }
        [Dependency] public INewTutorialController NewTutorialController { private get; set; }
        
        public void Update(float deltaTime) 
        {
            MainMove.Update(deltaTime);
            MyInput.Update();
            //TutorialController.Update(deltaTime);
        }

        public void Start() {
        }

        public void OnDestroy()
        {
            Creator.OnDestroy();
            PlayerPointer.OnDestroy();
            GenerationWarehouseUIPanels.OnDestroy();
        }

        public void Focus(bool focus)
        {
            if (!focus) SaveAndLoad.StartSave();
            else SaveAndLoad.LoadOnFocus();

            MyTime.Focus(focus);
        }
    }
}