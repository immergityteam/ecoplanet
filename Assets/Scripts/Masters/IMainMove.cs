﻿namespace ColLobOK
{
    /// <summary>
    /// Интерфейс движения
    /// </summary>
    public interface IMainMove {
        void Update(float deltaTime);
    }
}


