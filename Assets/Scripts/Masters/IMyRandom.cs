﻿namespace ColLobOK {
    public interface IMyRandom {
        public float GetRand();
        public float GetRand(float max);
        public int GetRand(int max);
        public float GetRandRange(float min, float max);
        public int GetRandRange(int min, int max);
    }
}