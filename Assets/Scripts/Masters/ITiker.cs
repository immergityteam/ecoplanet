﻿using UnityEngine.Events;

namespace ColLobOK {
    public interface ITiker {
        UnityAction EvTickSec { get; set; }
        void StartTiker(float deltaSecond);
        void StartTiker();
        void StopTiker();
        void Restart();
        void SetTikerTime(float timeBeforeDismiss);
    }
}