using System;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class MonoID : MonoBehaviour, ISetID, IGetID, IGameObjCliked
    {
        [SerializeField] private int ID;
        [SerializeField] private StringsSO GlobalID;

        public UnityAction<int> EvCliked { get; set; }

        public Animator animator;

        public int GetID() => ID;
        public int GetGlobalID() => GlobalID ? GlobalID.GetID() : 0;
        public void SetID(int id) => ID = id;
        public void GameObjCliked()
        {
            EvCliked?.Invoke(GetID());
        }

#if UNITY_EDITOR
        public void SetGlobalID(StringsSO id) => GlobalID = id;

#endif
    }
}