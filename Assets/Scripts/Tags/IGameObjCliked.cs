using UnityEngine.Events;

namespace ColLobOK
{
    public interface IGameObjCliked {
        UnityAction<int> EvCliked { get; set; }
        
        void GameObjCliked();
    }
}