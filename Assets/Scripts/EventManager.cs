using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace ColLobOK
{
    public class EventManager : MonoBehaviour {

        private Dictionary<EventName, UnityAction<int>> eventDictionary;
        public UnityAction<AudioClip> EvBee;

        private static EventManager eventManager;

        public static EventManager Instance {
            get {
                if (!eventManager) {
                    eventManager = FindObjectOfType(typeof(EventManager)) as EventManager;

                    if (!eventManager) {
                        Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                    } else {
                        eventManager.Init();
                    }
                }
                return eventManager;
            }
        }

        void Init() {
            if (eventDictionary == null) {
                eventDictionary = new Dictionary<EventName, UnityAction<int>>();
            }
        }

        public static void StartListening(EventName eventName, UnityAction<int> listener) {
            UnityAction<int> thisEvent;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
                thisEvent += listener;
                //Instance.eventDictionary[eventName] += listener;
            } else {
                thisEvent += listener;
                Instance.eventDictionary.Add(eventName, thisEvent);
            }
        }

        public static void StopListening(EventName eventName, UnityAction<int> listener) {
            if (eventManager == null) return;
            UnityAction<int> thisEvent;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
                thisEvent -= listener;
            }
        }

        public static void TriggerEvent(EventName eventName, int id) {
            UnityAction<int> thisEvent;
            if (Instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
                thisEvent.Invoke(id);
            }
        }
    }
}