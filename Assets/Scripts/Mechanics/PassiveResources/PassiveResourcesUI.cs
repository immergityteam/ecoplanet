using UnityEngine;
using TMPro;
using ColLobOK;

public class PassiveResourcesUI : MonoBehaviour, IPassiveResourcesUI
{
    [field: SerializeField] public TMP_Text Traffic { get; private set; }
    [field: SerializeField] public TMP_Text Food { get; private set; }
    [field: SerializeField] public TMP_Text Water { get; private set; }
    [field: SerializeField] public TMP_Text Electricity { get; private set; }
    [field: SerializeField] public TMP_Text Prestige { get; private set; }
    [field: SerializeField] public  StringsSO TrafficSO { get; private set; }
    [field: SerializeField] public  StringsSO FoodSO { get; private set; }
    [field: SerializeField] public  StringsSO WaterSO { get; private set; }
    [field: SerializeField] public StringsSO ElectricitySO { get; private set; }
    [field: SerializeField] public StringsSO PrestigeSO { get; private set; }
}
