using UnityEngine.Events;

namespace ColLobOK
{
    public interface IPassiveResourcesController
    {
        UnityAction<int> EvChangeRasRes { get; set; }
        void ChangePasRes(int id, int count, int idStriuct);
        int LookOnePasRes(int id);
        void Rollback(int id);
        void Recalculate(int id);
    }
}