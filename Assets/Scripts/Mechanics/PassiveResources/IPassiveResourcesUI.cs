﻿using TMPro;

namespace ColLobOK
{
    public interface IPassiveResourcesUI
    {
        TMP_Text Traffic { get; }
        TMP_Text Food { get; }
        TMP_Text Water { get; }
        TMP_Text Electricity { get; }
        TMP_Text Prestige { get; }
        StringsSO TrafficSO { get; }
        StringsSO FoodSO { get; }
        StringsSO WaterSO { get; }
        StringsSO ElectricitySO { get; }
        StringsSO PrestigeSO { get; }
    }
}