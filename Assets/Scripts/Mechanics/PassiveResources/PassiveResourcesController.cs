using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class PassiveResourcesController : IPassiveResourcesController, IDependent
    {
        [Dependency] public IPassiveResourcesUI PassiveResourcesUI { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IBusinessBonusController BusinessBonusController { private get; set; }

        private Dictionary<int, int> idCount;

        public UnityAction<int> EvChangeRasRes { get; set; }

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            idCount = new Dictionary<int, int>();
            EvChangeRasRes += (x) => UpdateText();
            BuilderPlayer.EvBuiltConstruction += AddStructure;
            BuilderPlayer.EvBuildingDelete += RemoveStructure;
            
            Load();
            UpdateText();
        }

        private void RemoveStructure(int arg0)
        {
            StructureData SD = BuilderPlayer.GetStructureData(arg0);
            TakePasRes(SD.productionPasRes, SD.ID);
            AddPasRes(SD.requirementsPasRes, SD.ID);
        }

        private void AddStructure(int arg0)
        {
            StructureData SD = BuilderPlayer.GetStructureData(arg0);
            AddPasRes(SD.productionPasRes, SD.ID);
            TakePasRes(SD.requirementsPasRes, SD.ID);
        }

        public void ChangePasRes(int id, int count, int idStriuct)
        {
            if (!idCount.ContainsKey(id)) idCount.Add(id, 0);
            if (id == PassiveResourcesUI.PrestigeSO.GetID())
            {
                count += BusinessBonusController.GetDeltaMaxPrestige(idStriuct) * (int) Mathf.Sign(count);
            }
            idCount[id] += count;
            EvChangeRasRes?.Invoke(id);
        }

        private void TakePasRes(Resource[] res, int idStriuct)
        {
            foreach (var item in res) ChangePasRes(item.ID, -item.count, idStriuct);
        }

        private void AddPasRes(Resource res, int idStriuct)
        {
            ChangePasRes(res.ID, res.count, idStriuct);
        }
        private void AddPasRes(Resource[] res, int idStriuct)
        {
            foreach (var item in res) ChangePasRes(item.ID, item.count, idStriuct);
        }

        public int LookOnePasRes(int id)
        {
            return idCount.ContainsKey(id) ? idCount[id] : 0;
        }

        public void Rollback(int id)
        {
            RemoveStructure(id);
        }

        public void Recalculate(int id)
        {
            AddStructure(id);
        }

        private void UpdateText()
        {
            PassiveResourcesUI.Traffic.text = LookOnePasRes(PassiveResourcesUI.TrafficSO.GetID()).ToString();
            PassiveResourcesUI.Water.text = LookOnePasRes(PassiveResourcesUI.WaterSO.GetID()).ToString();
            PassiveResourcesUI.Food.text = LookOnePasRes(PassiveResourcesUI.FoodSO.GetID()).ToString();
            PassiveResourcesUI.Electricity.text = LookOnePasRes(PassiveResourcesUI.ElectricitySO.GetID()).ToString();
            PassiveResourcesUI.Prestige.text = LookOnePasRes(PassiveResourcesUI.PrestigeSO.GetID()).ToString();
        }

        ConvertDataBinary<Dictionary<int, int>> saveData
            = new ConvertDataBinary<Dictionary<int, int>>("PassiveResourcesController");

        private void Save() => SaveAndLoad.Save(saveData.name, saveData.ToConvert(idCount));
        private void Load()
        {
            var str = SaveAndLoad.Load(saveData.name);
            if (str == "")
            {
                ChangePasRes(PassiveResourcesUI.ElectricitySO.GetID(), 2, 0);
                return;
            }
            idCount = saveData.FromConvert(str);
        }
    }
}