﻿namespace ColLobOK
{
    public interface ISoundController
    {
        void ChangeMusic();
        void ChangeSound();
    }
}