﻿using UnityEngine;

namespace ColLobOK
{
    public interface ISoundSourceView
    {
        AudioSource Sound { get; }
        
        AudioSource Music { get; }
    }
}