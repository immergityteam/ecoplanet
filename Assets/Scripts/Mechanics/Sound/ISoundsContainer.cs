﻿using UnityEngine;

namespace ColLobOK
{
    public interface ISoundsContainer
    {
        AudioClip DefaultClick { get; }
    }
}