﻿using System;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class SoundController : ISoundController, IDependent
    {
        [Dependency] public ISoundSourceView SoundSourceView { private get; set; }
        [Dependency] public ISoundsContainer SoundsContainer { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public ISettingsUI SettingsUI{ private get; set; }

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            SettingsUI.Music.onClick.AddListener(ChangeMusic);
            SettingsUI.Sound.onClick.AddListener(ChangeSound);
            EventManager.Instance.EvBee += PlayDefaultClick;
            Load();
            UpdateSoundAndMusic();
        }

        [Serializable]
        struct SaveData
        {
            public bool music;
            public bool sound;
        }

        SaveData data;

        public void ChangeMusic()
        {
            data.music = !data.music;
            UpdateSoundAndMusic();
        }

        public void ChangeSound()
        {
            data.sound = !data.sound;
            UpdateSoundAndMusic();
        }

        private void UpdateSoundAndMusic()
        {
            SoundSourceView.Music.outputAudioMixerGroup.audioMixer.SetFloat("VolumeMusic", data.music ? 0 : -80.0f);
            SoundSourceView.Sound.outputAudioMixerGroup.audioMixer.SetFloat("VolumeSounds", data.sound ? 0 : -80.0f);
            SettingsUI.MusicImageOn.SetActive(data.music);
            SettingsUI.MusicImageOff.SetActive(!data.music);
            SettingsUI.SoundImageOn.SetActive(data.sound);
            SettingsUI.SoundImageOff.SetActive(!data.sound);
        }

        private void PlayDefaultClick(AudioClip clip)
        {
            if (clip == null)
            {
                clip = SoundsContainer.DefaultClick;
                SoundSourceView.Sound.clip = clip;
            }
            else
            {
                SoundSourceView.Sound.clip = clip;
            }
            SoundSourceView.Sound.Play();
        }

        ConvertDataBinary<SaveData> saveData
            = new ConvertDataBinary<SaveData>("SoundController");

        private void Save() => SaveAndLoad.Save(saveData.name, saveData.ToConvert(data));
        private void Load()
        {
            string str = SaveAndLoad.Load(saveData.name);
            if (str != "") data = saveData.FromConvert(str);
        }
    }
}