using UnityEngine;

namespace ColLobOK
{
    public class SoundSourceView : MonoBehaviour, ISoundSourceView
    {
        [field: SerializeField] public AudioSource Sound { get; private set; }
        [field: SerializeField] public AudioSource Music { get; private set; }
    }
}

