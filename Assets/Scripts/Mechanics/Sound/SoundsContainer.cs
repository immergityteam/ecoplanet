using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class SoundsContainer : MonoBehaviour, ISoundsContainer, IDependent
    {
        [field: SerializeField] public AudioClip DefaultClick { get; private set; }
        public void OnInjected()
        {
            
        }
    }
}
