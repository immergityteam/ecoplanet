using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class DonatController : IDonatController, IDependent
    {
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IShopController ShopController { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public ICommonLines CommonLines { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        
        Resource donatRes;
        public void OnInjected()
        {
            donatRes.name = ContainerMono.Donate;
            UpdateUI();
        }

        public bool TakeDonat(int val)
        {
            if (GetCountDonat() - val < 0)
            {
                ShopController.OpenDonat();
                MessageSystemController.ShowPopUpWindow(CommonLines.NeedDonat);
                return false;
            }

            donatRes.count = val;

            WarehouseController.TakeRes(donatRes);
            UpdateUI();
            return true;
        }

        public void TakeDonat(int val, StringsSO text, UnityAction callYes, UnityAction callNo = null)
        {
            MessageSystemController.SendMessageWindow(text, val, () => Bee(val, callYes), callNo);
        }

        private void Bee(int val, UnityAction call)
        {
            if (GetCountDonat() - val < 0)
            {
                ShopController.OpenDonat();
                MessageSystemController.ShowPopUpWindow(CommonLines.NeedDonat);
                return;
            }

            donatRes.count = val;

            WarehouseController.TakeRes(donatRes);
            UpdateUI();
            call?.Invoke();
        }

        public void AddDonat(int val)
        {
            donatRes.count = val;
            WarehouseController.AddRes(donatRes);
            UpdateUI();
        }

        private void UpdateUI()
        {
            GameUI.Donate.text = GetCountDonat().ToString();
        }
        private int GetCountDonat()
        {
            return WarehouseController.LookOneRes(donatRes.name.GetID());
        }
    }
}