﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface IDonatController
    {
        bool TakeDonat(int val);
        void TakeDonat(int val, StringsSO text, UnityAction callYes, UnityAction callNo = null);
        void AddDonat(int val);
    }
}