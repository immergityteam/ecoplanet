﻿using UnityEngine;

namespace ColLobOK {
    public interface ITrash {
        Vector2Int SizeTile { get; }
        TrashData DefResource { get; }
        GameObject GameObject { get; }
        void SetID(int id);
        int MaxClicks { get; set; }
        bool IsSetMaxClicks { get; set; }
        int ClicksTaked { get; set; }
    }
}