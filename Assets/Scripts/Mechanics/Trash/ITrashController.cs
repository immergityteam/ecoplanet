﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace ColLobOK {
    [Serializable] public struct IdResCount
    {
        public int ID;
        public int count;

        public IdResCount(Resource a)
        {
            ID = a.name.GetID();
            count = a.count;
        }
    }

    public interface ITrashController {

        void ClearCliked();
        void TrachClikedListener(int id);
        void SetLockMining(bool val);

        List<Resource> GetRandomResourceData(int id);
        Dictionary<int, List<IdResCount>> idRes { get; set; }
        int GetIdNearestTrash(TrashData data);
        Dictionary<int, bool> Permissions { get; set; }
        UnityAction<List<Resource>> EvGarbageUnitsMined { get; set; }
        UnityAction<int> EvTrashOverID { get; set; }
    }
}