using UnityEditor;
using UnityEngine;

namespace ColLobOK
{
    public class TrashsContainerMono : MonoBehaviour, ITrashsContainerMono
    {
        [field: SerializeField] public Trash[] AllTrash { get; private set; }

#if UNITY_EDITOR
        [SerializeField] private int allCountTrash;
        [ContextMenu("SetAllTrash")]
        private void SetAllTrash()
        {
            AllTrash = GetComponentsInChildren<Trash>();
            SetAllTrashsID();
            allCountTrash = AllTrash.Length;
        }
        public void SetAllTrashsID()
        {
            for (int i = 0; i < AllTrash.Length; i++)
            {
                Undo.RecordObject(AllTrash[i], "SetID");
                AllTrash[i].SetID(i);
            }
        }
#endif
    }
}