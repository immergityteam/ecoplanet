using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class TrashController : ITrashController, IDependent
    {
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IWhereClicked WhereClicked { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public IMyRandom MyRand { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public ISpawnerItem SpawnerItem { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public ITrashQueue TrashQueue { private get; set; }
        [Dependency] public ITrashsContainerMono TrashsContainerMono { get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { get; set; }
        [Dependency] public IContainerAllResources ContainerAllResources { get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { get; set; }
        [Dependency] public IGameUI GameUI { get; set; }
        [Dependency] public IEnergyController EnergyController { get; set; }
        
        private Dictionary<int, int> _clicksTaked = new Dictionary<int, int>();
        private Dictionary<int, int> _maxClicks = new Dictionary<int, int>();

        public bool PermissionToShowAmountOfTrash;
        public Dictionary<int, List<IdResCount>> idRes { get; set; }
        public Dictionary<int, bool> Permissions { get; set; }
        public UnityAction<int, int> AddExpierenceToBranch { get; set; }

        public UnityAction<List<Resource>> EvGarbageUnitsMined { get; set; }
        public UnityAction<int> EvTrashOverID { get; set; }

        private bool isLockMinung;
        public void OnInjected()
        {
            WhereClicked.SetLayerMask(MyGrid.MaskMoving);
            for (int i = 0; i < TrashsContainerMono.AllTrash.Length; i++)
                TrashsContainerMono.AllTrash[i].EvCliked += TrachClikedListener;
            idRes = new Dictionary<int, List<IdResCount>>();
            Permissions = new Dictionary<int, bool>();
            SaveAndLoad.EvSave += Save; 
            GameUI.CanselMining.onClick.AddListener(ClearCliked);
            Load();
        }

        public int GetIdNearestTrash(TrashData data)
        {
            float distance = short.MaxValue;
            float tempDis;

            int k = 0;
            for (int i = 0; i < TrashsContainerMono.AllTrash.Length; i++)
            {
                if (TrashsContainerMono.AllTrash[i].DefResource == data)
                {
                    tempDis = Vector3.SqrMagnitude(TrashsContainerMono.AllTrash[i].transform.position);
                    if (distance > tempDis)
                    {
                        distance = tempDis;
                        k = i;
                    }
                }
            }
            return k;
        }

        public void ClearCliked()
        {
            _clicksTaked.Clear();
            _maxClicks.Clear();
        }

        public void TrachClikedListener(int id)
        {
            if (isLockMinung) return;

            if (!idRes.ContainsKey(id)) AddDictionary(id);
                
            if (_clicksTaked.ContainsKey(id))
            {
                if (_clicksTaked[id] < _maxClicks[id])
                    _clicksTaked[id]++;
                else
                {
                    if (Permissions.ContainsKey(0) && Permissions[0])
                    {  
                        MessageSystemController.ShowTrashMessage(
                            TrashsContainerMono.AllTrash[id].gameObject.transform.position,
                            _maxClicks[id], _clicksTaked[id]);
                    }
                    return;
                }
            }
            else
            {
                int maxClicks = 0;
                for (int i = 0; i < idRes[id].Count; i++)
                {
                    maxClicks += idRes[id][i].count;
                }

                maxClicks += ContainerMono.StrengthMining - 1;
                maxClicks /= ContainerMono.StrengthMining;

                _maxClicks.Add(id, maxClicks);
                _clicksTaked.Add(id, 1);
            }

            if (Permissions.ContainsKey(0) && Permissions[0])
            {
                MessageSystemController.ShowTrashMessage(TrashsContainerMono.AllTrash[id].gameObject.transform.position,
                    _maxClicks[id], _clicksTaked[id]);
            }

           
            TrashQueue.AddToQueue(() => OnPointReached(id),
                TrashsContainerMono.AllTrash[id].gameObject.transform.position);
        }
        
        public void SetLockMining(bool val) => isLockMinung = val;

        private void OnPointReached(int id)
        {
            List<Resource> miningResource = GetRandomResourceData(id);
            WarehouseController.AddRes(miningResource);
            if (MyRand.GetRand() < ContainerMono.EnergyDropChance)
            {
                EnergyController.AddEnergy();
                SpawnerItem.SpawnItem(new Resource(ContainerMono.Energy, 1), TrashsContainerMono.AllTrash[id].transform.position, ContainerMono.Targets[0].Transform);
            }

            EvGarbageUnitsMined?.Invoke(miningResource);
        }

        public List<Resource> GetRandomResourceData(int id)
        {
            List<Resource> resources = new List<Resource>();
            for (int i = 0; i < ContainerMono.StrengthMining; i++)
            {
                if (idRes[id] == null) break;
                resources.Add(GetRandomResource(id));
            }
            return resources;
        }

        private Resource GetRandomResource(int id)
        {
            int rand = MyRand.GetRand(idRes[id].Count);

            IdResCount res = idRes[id][rand];
            res.count -= 1;
            idRes[id][rand] = res;

            if (idRes[id][rand].count == 0)
            {
                idRes[id].Remove(idRes[id][rand]);
                if (idRes[id].Count == 0)
                {
                    Creator.MyDestroy(TrashsContainerMono.AllTrash[id].gameObject);
                    EvTrashOverID?.Invoke(id);
                    idRes[id] = null;
                }
            }

            res.count = 1;

            Resource temp = new Resource(ContainerAllResources.GetConfigRes(res.ID).nameRes, res.count);

            SpawnerItem.SpawnItem(temp, TrashsContainerMono.AllTrash[id].transform.position, ContainerMono.Targets[0].Transform);

            return temp;
        }
        private void AddDictionary(int id)
        {
            Resource[] temp = TrashsContainerMono.AllTrash[id].DefResource.resources;
            List<IdResCount> res = new List<IdResCount>(); 
            for (int i = 0; i < temp.Length; i++)
            {
                res.Add(new IdResCount(temp[i]));
            }
            idRes.Add(id, res);
        }

        ConvertDataBinary<Dictionary<int, List<IdResCount>>> saveData
            = new ConvertDataBinary<Dictionary<int, List<IdResCount>>>("TrashController");
        private void Save() => SaveAndLoad.Save(saveData.name, saveData.ToConvert(idRes));

        private void Load()
        {
            idRes = saveData.FromConvert(SaveAndLoad.Load(saveData.name));
            foreach (var item in idRes)
                if (item.Value == null)
                {
                    TrashsContainerMono.AllTrash[item.Key].gameObject.SetActive(false);
                    Creator.MyDestroy(TrashsContainerMono.AllTrash[item.Key].gameObject);
                }
                    
        }
    }
}