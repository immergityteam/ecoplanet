using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class Trash : MonoBehaviour, ITrash, ISetID, IGameObjCliked
    {
        [field: SerializeField] public Vector2Int SizeTile { get; private set; }
        [field: SerializeField] public TrashData DefResource { get; private set; }


        private int _totalCount = 0;

        public bool IsSetMaxClicks { get; set; }

        public int MaxClicks { get; set; }
        public int ClicksTaked { get; set; }
        public GameObject GameObject => gameObject;

        public UnityAction<int> EvCliked { get; set; }

        EventName eventName = EventName.Trash;

        public int ID = 0;

        public void SetID(int id) => ID = id;

        public void GameObjCliked()
        {
            EvCliked?.Invoke(ID);
            //EventManager.TriggerEvent(eventName, ID);
        }

#if UNITY_EDITOR
        public void SetTrashData(TrashData data) => DefResource = data;
#endif

    }
}