using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System;
using DG.Tweening;

namespace ColLobOK
{
    public class TrashQueue : ITrashQueue, IDependent
    {
        [Dependency] public IPlayerController PlayerController { get; private set; }
        [Dependency] public IPlayer Player { get; private set; }
        [Dependency] public ICreator Creator { get; private set; }
        [Dependency] public ITrashController TrashController { get; private set; }
        [Dependency] public IContainerMono ContainerMono { get; private set; }
        [Dependency] public IGameUI GameUI { get; private set; }

        private bool _isGoingToPoint = false;

        struct MyStruct
        {
            public UnityAction Action;
            public Vector3 Position;
        }

        private Queue<MyStruct> TrashsQueue = new Queue<MyStruct>();

        public void OnInjected()
        {
            GameUI.CanselMining.onClick.AddListener(()=> HidenButton());
        }

        private void HidenButton()
        {
            GameUI.CanselMining.transform.DOMoveX(-220, 0.5f);
        }

        public void ClearQueue()
        {
            TrashsQueue.Clear();
            _isGoingToPoint = false;
        }

        public void AddToQueue(UnityAction action, Vector3 position)
        {
            GameUI.CanselMining.transform.DOMoveX(0, 0.5f);
            MyStruct myStruct = new MyStruct();
            myStruct.Action = action;
            myStruct.Position = position;
            TrashsQueue.Enqueue(myStruct);
            //Debug.Log(TrashsQueue.Count);

            if (TrashsQueue.Count == 1 && !_isGoingToPoint)
            {
                TakePlayerToPoint();
                _isGoingToPoint = true;
            }
        }

        public void TakePlayerToPoint()
        {
            if (TrashsQueue.Count > 0)
            {
                MyStruct currentTrash = TrashsQueue.Dequeue();

                //TODO: PlayerController and TrashQueue
                //PlayerController.EvStopMoving = null;
                //PlayerController.EvStopMoving += NextPoint;

                PlayerController.MovePlayerToMining(currentTrash.Action + NextPoint, currentTrash.Position);
            }
        }
        
        public void NextPoint()
        {
            _isGoingToPoint = false;
            if (TrashsQueue.Count > 0)
            {
                TakePlayerToPoint();
                 _isGoingToPoint = true;
            }
            else
            {
                GameUI.CanselMining.onClick.Invoke();
            }
        }

        public bool IsQueueEmpty => TrashsQueue.Count == 0 && !_isGoingToPoint;
    }
}