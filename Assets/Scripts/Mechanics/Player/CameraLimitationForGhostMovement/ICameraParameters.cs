namespace ColLobOK
{
    public interface ICameraParameters
    {
        public Vector2MinMax DeltaMoveCamera { get; }
        public float DeltaMoveCameraRomb { get; }
        public int GhostLimitation { get; }
        public float GhostDuration { get; }
        public float PointDuration { get; }
    } 
}
