using UnityEngine;

namespace ColLobOK
{
    public class CameraParameters : MonoBehaviour, ICameraParameters
    {
        [field: SerializeField] public Vector2MinMax DeltaMoveCamera { get; private set; }
        [field: SerializeField] public float DeltaMoveCameraRomb { get; private set; }
        [field: SerializeField] public int GhostLimitation { get; private set; }
        [field: SerializeField] public float GhostDuration { get; private set; }
        [field: SerializeField] public float PointDuration { get; private set; }
    }
}