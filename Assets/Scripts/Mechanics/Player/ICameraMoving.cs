﻿using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface ICameraMoving
    {
        public void MoveToPoint(Vector3 point, UnityAction action);
        public void MoveToPoint();
        void Freezing(bool val);
    }
}