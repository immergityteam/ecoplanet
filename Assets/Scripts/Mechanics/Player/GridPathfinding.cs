﻿using System.Collections.Generic;
using System.Linq;
using UnityDI;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace ColLobOK
{
    public class GridPathfinding : IGridPathfinding, IDependent
    {
        [Dependency] public IMyGrid MyGrid { private get; set; }

        private Dictionary<Vector3Int, int> allPointsInt;

        private const int pathValue = 1;

        public void OnInjected()
        {
            allPointsInt = new Dictionary<Vector3Int, int>();

            MyGrid.Direction = new Vector3[MyGrid.DirectionInt.Length];
            for (int i = 0; i < MyGrid.DirectionInt.Length; i++)
            {
                MyGrid.Direction[i] = MyGrid.Grid.CellToLocal(MyGrid.DirectionInt[i]);
            }
        }


        public List<Vector3> GetPath(Vector3 start, Vector3 end, out bool isTeleport)
        {
            Collider2D collider2D = Physics2D.OverlapCircle(start, 0.001f, MyGrid.MaskMoving);
            int layerMaskDef = 0;
            if (collider2D)
            {
                layerMaskDef = collider2D.gameObject.layer;
                collider2D.gameObject.layer = MyGrid.IgnoringPathfindingMask;
            }
            List<Vector3> path = new List<Vector3>();
            path.Clear();
            path.Add(start);

            if (MyGrid.Grid.NearestCell(end) == MyGrid.Grid.NearestCell(start))
            {
                path.Add(end);
                isTeleport = false;
                if (collider2D)
                {
                    collider2D.gameObject.layer = layerMaskDef;
                }
                return path;
            }

            Vector3Int fastStart = PathCalculation(MyGrid.Grid.NearestCell(end), MyGrid.Grid.NearestCell(start));
            if (fastStart != Vector3Int.back)
            {
                path.AddRange(SearchPath(fastStart));
                isTeleport = true;
            }
            else
            {
                path.AddRange(SearchPath(MyGrid.Grid.NearestCell(start)));
                isTeleport = false;
            }

            path = SmoothingPath(SmoothingPath(path));
            if (collider2D)
            {
                collider2D.gameObject.layer = layerMaskDef;
            }
            return path;
        }

#if UNITY_EDITOR
        List<GameObject> bee = new List<GameObject>();
        private void ShowPath(List<Vector3Int> path)
        {
            ShowPath(path.Select(x => MyGrid.Grid.CellToLocal(x)).ToList());
        }
        private void ShowPath(List<Vector3> path)
        {
            for (int i = 0; i < bee.Count; i++)
            {
                UnityEngine.Object.Destroy(bee[i]);
            }
            bee.Clear();
            GameObject temp;
            for (int i = 0; i < path.Count; i++)
            {
                temp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                temp.transform.position = path[i];
                temp.transform.localScale = Vector3.one * 0.1f;
                temp.name = "Path_" + i;
                bee.Add(temp);
            }
        }
#endif

        private List<Vector3> SmoothingPath(List<Vector3> path)
        {
            RaycastHit2D hit;
            int i = 0;
            int count = 0;
            while (i < path.Count - 2)
            {
                hit = Physics2D.Raycast(path[i], path[i + 2] - path[i], Vector3.Distance(path[i + 2], path[i]), MyGrid.MaskMoving);
                //hit = Physics2D.CircleCast(path[i], 0.05f, path[i + 2] - path[i], Vector3.Distance(path[i + 2], path[i]), myGrid.LayerMask);

                if (hit.collider == null)
                {
                    path.RemoveAt(i + 1);
                    count++;
                }
                else
                {
                    i++;
                }
            }

            return path;
        }

        private int disOut = 10;
        private Vector3Int PathCalculation(Vector3Int start, Vector3Int end)
        {
            allPointsInt.Clear();

            Collider2D collider = Physics2D.OverlapCircle(MyGrid.Grid.CellToLocal(start), 0.1f, MyGrid.MaskMoving);
            if (collider)
            {
                Vector3 direct = MyGrid.Grid.NearestPointOnGrid(MyGrid.Direction[0]);
                RaycastHit2D[] rayHits = new RaycastHit2D[10];
                collider.Raycast(direct, rayHits, disOut, MyGrid.MaskMoving);

                if (rayHits[0])
                {
                    rayHits[0] = Physics2D.Raycast(rayHits[0].point - (Vector2)direct * 0.05f, -direct, disOut, MyGrid.MaskMoving);
                    start = MyGrid.Grid.NearestCell(rayHits[0].point + Vector2.up * 0.1f);
                }
                else
                {
                    rayHits[0] = Physics2D.Raycast(collider.transform.position + direct * disOut, -direct, disOut, MyGrid.MaskMoving);

                    start = MyGrid.Grid.NearestCell(rayHits[0].point + (Vector2)direct * 0.1f);
                }
            }

            List<Vector3Int> cells = new List<Vector3Int>();

            allPointsInt.Add(start, 0);
            cells.Add(start);

            float distansePoint = Vector3.SqrMagnitude(MyGrid.Grid.CellToLocal(start) - MyGrid.Grid.CellToLocal(end));
            RaycastHit2D hit;
            Vector3Int newPos;

            float distanceOnSell = MyGrid.Grid.CellToLocal(MyGrid.DirectionInt[0]).magnitude;

            bool isGood = false;

            List<Vector3Int> pointsMaxDistanse = new List<Vector3Int>();
            int k = 0;
            while (cells.Count > k)
            {
                for (int i = 0; i < MyGrid.DirectionInt.Length; i++)
                {
                    hit = Physics2D.Raycast(MyGrid.Grid.CellToLocal(cells[k]), MyGrid.Grid.CellToLocal(MyGrid.DirectionInt[i]), distanceOnSell, MyGrid.MaskMoving);
                    if (!hit.collider)
                    {
                        newPos = cells[k] + MyGrid.DirectionInt[i];
                        if (!allPointsInt.ContainsKey(newPos))
                        {
                            if (!isGood && Vector3.SqrMagnitude(MyGrid.Grid.CellToLocal(newPos) - MyGrid.Grid.CellToLocal(end)) < distansePoint * 1.2f)
                            {
                                if (allPointsInt[cells[k]] + pathValue < MyGrid.MaxPrice)
                                {
                                    allPointsInt.Add(newPos, allPointsInt[cells[k]] + pathValue);
                                    cells.Add(newPos);
                                }
                                else
                                {
                                    pointsMaxDistanse.Add(newPos);
                                    allPointsInt.Add(newPos, allPointsInt[cells[k]] + pathValue);
                                }
                            }
                        }
                        else if (allPointsInt[newPos] > allPointsInt[cells[k]] + pathValue)
                        {
                            allPointsInt[newPos] = allPointsInt[cells[k]] + pathValue;
                            cells.Add(newPos);
                        }
                    }
                }

                isGood = allPointsInt.ContainsKey(end);

                k++;

#if UNITY_EDITOR
                if (k > 100000)
                {
                    Debug.LogError("Ты слишком зациклился на себе");
                    return Vector3Int.back;
                }
#endif
            }

            if (!isGood)
            {
                Vector3Int fastEnd = end;
                float sqr = float.MaxValue;
                float sqrTemp;
                for (int i = 0; i < pointsMaxDistanse.Count; i++)
                {
                    sqrTemp = Vector3.SqrMagnitude(pointsMaxDistanse[i] - end);
                    if (sqrTemp < sqr)
                    {
                        fastEnd = pointsMaxDistanse[i];
                        sqr = sqrTemp;
                    }
                }
                return fastEnd;
            }


            return Vector3Int.back;
        }

        private List<Vector3> SearchPath(Vector3Int end)
        {
            List<Vector3> pathPoints = new List<Vector3>();
            Vector3Int newPos = end;
            Vector3Int oldPoint;
            Vector3Int temp;

            if (!allPointsInt.ContainsKey(newPos))
            {
                Debug.Log("Конечная точка не найдена");
                pathPoints.Add(newPos);
                return pathPoints;
            }

#if UNITY_EDITOR
            int k = 0;
#endif
            do
            {
                oldPoint = newPos;
                pathPoints.Add(MyGrid.Grid.CellToLocal(oldPoint));
                for (int i = 0; i < MyGrid.DirectionInt.Length; i++)
                {
                    temp = oldPoint + MyGrid.DirectionInt[i];

                    if (allPointsInt.ContainsKey(temp) && allPointsInt[newPos] - 1 == allPointsInt[temp])
                    {
                        newPos = temp;
                    }
                }

#if UNITY_EDITOR
                k++;
                if (k > 100000)
                {
                    Debug.LogError("Ты слишком зациклился на себе");
                    break;
                }
#endif
            } while (allPointsInt[oldPoint] != 0);

            //pathPoints.Reverse();
            return pathPoints;
        }
    }
}