﻿using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class MovePath : IMovePath
    {
        public List<Vector3> path { get; set; }
        public bool isNext { get { return path != null && path.Count > 0; } set { isNext = value; } }

        Vector3 dir = Vector3.zero;

        public Vector3 NextPosition(Vector3 nowPos, float speed, float deltaTime)
        {
            Vector3 point = path[0];
            Vector3 v = point - nowPos;
            dir = deltaTime * speed * v.normalized;

            if (dir == Vector3.zero || dir.sqrMagnitude >= v.sqrMagnitude)
            {
                path.RemoveAt(0);
                if (path.Count > 0)
                {
                    v = path[0] - nowPos;
                    dir = deltaTime * speed * v.normalized;
                }
                return point;
            }
            else
            {
                return nowPos + dir;
            }
        }
        public Vector3 NextPosition()
        {
            Vector3 point = path[0];
            path.RemoveAt(0);
            return point;
        }
        public Vector3 GetDirection()
        {
            return dir;
        }
        
        public void Clear()
        {
            path.Clear();
        }
    }

}
