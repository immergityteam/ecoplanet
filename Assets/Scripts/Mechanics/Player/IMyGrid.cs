﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace ColLobOK
{
    public interface IMyGrid
    {
        Grid Grid { get; }
        Grid GridX2 { get; }
        float MaxPrice { get; }
        LayerMask MaskMoving { get; }
        int IgnoringPathfindingMask { get; }
        Tilemap TilemapGround { get; }
        Tilemap TilemapDryLand { get; }
        Tilemap TilemapWater { get; }
        TileBasePersent[] DryTiles { get; }
        Vector3Int[] DirectionInt { get; }
        Vector3[] Direction { get; set; }
    }
}
