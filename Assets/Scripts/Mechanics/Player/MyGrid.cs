﻿using UnityEngine;
using UnityEngine.Tilemaps;

namespace ColLobOK
{
    public class MyGrid : MonoBehaviour, IMyGrid
    {
        [field: SerializeField] public Grid Grid { get; private set; }
        [field: SerializeField] public Grid GridX2 { get; private set; }
        [field: SerializeField] public float MaxPrice { get; private set; }
        [field: SerializeField] public LayerMask MaskMoving { get; private set; }
        [field: SerializeField] public int IgnoringPathfindingMask { get; private set; }
        [field: SerializeField] public Tilemap TilemapGround { get; private set; }
        [field: SerializeField] public Tilemap TilemapDryLand { get; private set; }
        [field: SerializeField] public Tilemap TilemapWater { get; private set; }
        [field: SerializeField] public TileBasePersent[] DryTiles { get; private set; }

        public Vector3Int[] DirectionInt { get; } = { Vector3Int.right, Vector3Int.up, Vector3Int.left, Vector3Int.down };
        public Vector3[] Direction { get; set; }
    }
}