﻿using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    /// <summary>
    /// Интерфейс, управления игроком
    /// </summary>
    public interface IPlayerController
    {
        void Update(float deltaTime);
        //IEnumerator MovePlayerToPosition(UnityAction onTargetReached, Vector3 point);

        void MovePlayerToMining(UnityAction onTargetReached, Vector3 point);
        UnityAction EvStopMoving { get; set; }

    }
}
