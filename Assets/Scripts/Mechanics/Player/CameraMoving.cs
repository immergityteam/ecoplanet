using UnityDI;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

namespace ColLobOK
{
    public class CameraMoving : ICameraMoving, IDependent
    {

        private const float MAGIC = 0.01f;

        private Camera camera;
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public ICameraParameters CameraParameters { private get; set; }

        public void OnInjected()
        {
            camera = Camera.main;
            MyInput.EvDown += SetStartPosition;
            MyInput.EvPressed += MoveCamera;
            coefficient = MyGrid.Grid.cellSize.x / MyGrid.Grid.cellSize.y;
        }

        private float coefficient;

        private void SetStartPosition()
        {
            startMoveCam = camera.transform.position;
        }

        private Vector3 startMoveCam;
        private Vector3 tempPosCam;

        private Vector3 _pointToReturn;
        private bool _isPossibleToMove = true;

        private const float DELTA_FOR_CLIKED = 100f;
        private bool freeze = false;
        private void MoveCamera()
        {
            if (freeze) return;
            if (BuilderPlayer.MovableBuilding == null)
            {
                if (MyInput.DeltaPoint.sqrMagnitude < DELTA_FOR_CLIKED) return;

                tempPosCam = camera.transform.position + MyInput.DeltaPointFrame * MAGIC;

                tempPosCam = startMoveCam + MyInput.DeltaPoint * MAGIC;
                if ((Mathf.Abs(tempPosCam.x) / coefficient) + Mathf.Abs(tempPosCam.y) > CameraParameters.DeltaMoveCameraRomb)
                    tempPosCam.x = (CameraParameters.DeltaMoveCameraRomb - Mathf.Abs(tempPosCam.y)) * coefficient * Mathf.Sign(tempPosCam.x);

                if ((Mathf.Abs(tempPosCam.x) / coefficient) + Mathf.Abs(tempPosCam.y) < CameraParameters.DeltaMoveCameraRomb)
                    camera.transform.position = tempPosCam;

                camera.transform.position = LimitCamera(tempPosCam);
            }
            else
            {
                if (MyInput.PointCamera.x >= Screen.width - CameraParameters.GhostLimitation ||
                    MyInput.PointCamera.x < CameraParameters.GhostLimitation ||
                    MyInput.PointCamera.y > Screen.height - CameraParameters.GhostLimitation ||
                    MyInput.PointCamera.y < CameraParameters.GhostLimitation)
                {
                    MoveToReturnPoint();
                }
            }
        }

        private void MoveToReturnPoint()
        {
            if (_isPossibleToMove)
            {
                _isPossibleToMove = false;
                _pointToReturn = new Vector3(MyInput.GetPointWorld().x, MyInput.GetPointWorld().y, -10);
                _pointToReturn = LimitCamera(_pointToReturn);

                camera.transform.DOMove(_pointToReturn, CameraParameters.GhostDuration).onKill += () => _isPossibleToMove = true;
            }
        }

        private Vector3 LimitCamera(Vector3 point)
        {
            point.x = Mathf.Clamp(point.x, CameraParameters.DeltaMoveCamera.min.x, CameraParameters.DeltaMoveCamera.max.x);
            point.y = Mathf.Clamp(point.y, CameraParameters.DeltaMoveCamera.min.y, CameraParameters.DeltaMoveCamera.max.y);

            var interpolatorXPositive = Mathf.InverseLerp(CameraParameters.DeltaMoveCamera.max.x, 0.0f, point.x);
            var interpolatorXNegative = Mathf.InverseLerp(CameraParameters.DeltaMoveCamera.min.x, 0.0f, point.x);

            if (point.x < CameraParameters.DeltaMoveCamera.max.x - MAGIC &&
                point.y > Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.max.y, interpolatorXPositive))
            {
                point.y = Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.max.y, interpolatorXPositive);
            }

            if (point.x > CameraParameters.DeltaMoveCamera.min.x + MAGIC &&
                point.y > Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.max.y, interpolatorXNegative))
            {
                point.y = Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.max.y, interpolatorXNegative);
            }

            if (point.x < CameraParameters.DeltaMoveCamera.max.x - MAGIC &&
                point.y < Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.min.y, interpolatorXPositive))
            {
                point.y = Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.min.y, interpolatorXPositive);
            }

            if (point.x > CameraParameters.DeltaMoveCamera.min.x + MAGIC &&
                point.y < Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.min.y, interpolatorXNegative))
            {
                point.y = Mathf.Lerp(0.0f, CameraParameters.DeltaMoveCamera.min.y, interpolatorXNegative);
            }

            if (point.x == CameraParameters.DeltaMoveCamera.max.x || point.x == CameraParameters.DeltaMoveCamera.min.x)
            {
                point.y = 0.0f;
            }

            return point;
        }

        public void MoveToPoint(Vector3 point, UnityAction action)
        {
            point.z = camera.transform.position.z;
            camera.transform.DOMove(point, 1).OnKill(() => action?.Invoke());
        }

        public void MoveToPoint()
        {

        }

        public void Freezing(bool val) => freeze = val;
    }
}