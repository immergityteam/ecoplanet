using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class AnimaControl : MonoBehaviour
    {
        [SerializeField] private Animator front;
        [SerializeField] private Animator back;

        [SerializeField] private string[] breathes;
        [SerializeField] private string pick;
        [SerializeField] private string walk;
        [SerializeField] private string walkBack;

        public AnimaEvent animaEvent;

        private IMyRandom myRandom;

        public void Init()
        {
            myRandom = GameStarter.container.Resolve<IMyRandom>();
        }

        public void Breathes()
        {
            ChangeFront(true);
            front.Play(breathes[myRandom.GetRand(breathes.Length)]);
        }
        public void Pick()
        {
            ChangeFront(true);
            front.Play(pick);
        }
        public void Walk()
        {
            ChangeFront(true);
            front.Play(walk);
        }
        public void WalkBack()
        {
            ChangeFront(false);
            back.Play(walkBack);
        }

        private void ChangeFront(bool val)
        {
            front.StopPlayback();
            //front.CrossFade("Base", 0);
            front.gameObject.SetActive(val);
            back.gameObject.SetActive(!val);
        }
    }
}