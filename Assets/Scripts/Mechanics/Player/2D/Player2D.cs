﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class Player2D : MonoBehaviour, IPlayer
    {

        [SerializeField] private float _speed = 1.0f;
        [SerializeField] private Animator _animator;
        [SerializeField] private AnimaControl _animaControl;
        [SerializeField] private Transform _model;

        public UnityAction EvStopMining { get; set; }
        public UnityAction<bool> PlayerDisappeared { get; set; }

        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public Vector3 Pos
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Animator Animator
        {
            get { return _animator; }
            set { _animator = value; }
        }

        public Transform Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public AnimaControl AnimaControl { get => _animaControl; set => _animaControl = value; }

        public void StopMiningInvoke() => EvStopMining?.Invoke();
        
        private void OnBecameVisible()
        {
            PlayerDisappeared?.Invoke(false);
        }
        
        private void OnBecameInvisible()
        {
            PlayerDisappeared?.Invoke(true);
        }
    }
}