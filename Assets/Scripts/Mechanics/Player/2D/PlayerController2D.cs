﻿using System;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class PlayerController2D : IPlayerController, IDependent
    {
        [Dependency] public IPlayer Player { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public IWhereClicked WhereClicked { private get; set; }
        [Dependency] public IGridPathfinding PathFinding { private get; set; }
        [Dependency] public IMovePath MovePath { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public ITrashQueue TrashQueue { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IEnergyController EnergyController { private get; set; }
        [Dependency] public IGameUI GameUI{ private get; set; }
        [Dependency] public ICameraParameters CameraParameters { private get; set; }

        private StateBuilder stateBuilder;

        public UnityAction EvStopMoving { get; set; }

        public bool CanMove { get; set; }
        private float coefficient;

        public void OnInjected()
        {
            WhereClicked.SetLayerMask(MyGrid.MaskMoving);
            GameUI.CanselMining.onClick.AddListener(()=> StopAll());
            MyInput.EvCliked += Cliked;
            BuilderPlayer.EvChangeStateBuilder += SetStateBuilder;
            CanMove = true;
            coefficient = MyGrid.Grid.cellSize.x / MyGrid.Grid.cellSize.y;
            Player.AnimaControl.Init();
            Player.AnimaControl.animaEvent.EvStopMining += ()=> Player.EvStopMining?.Invoke();
        }

        private void StopAll()
        {
            TrashQueue.ClearQueue();
            EvStopMoving -= voidMaining;
            Player.EvStopMining = null;
            voidMaining = null;
            MovePath.Clear();
            Player.AnimaControl.Breathes();
            Player.Model.eulerAngles = Vector3.zero;
        }

        private void Cliked()
        {
            if (stateBuilder == StateBuilder.None)
            {
                if ((Mathf.Abs(MyInput.GetPointWorld().x) / coefficient) + Mathf.Abs(MyInput.GetPointWorld().y) > CameraParameters.DeltaMoveCameraRomb) return;

                Collider2D colliderCliked = WhereClicked.CheckedCollider(MyInput.PointCamera);

                if (!colliderCliked)
                {
                    if (TrashQueue.IsQueueEmpty)
                    {
                        GO(MyInput.GetPointWorld());
                    }
                }
                else
                {
                    colliderCliked.GetComponent<IGameObjCliked>()?.GameObjCliked();
                }
            }
        }

        private void GO(Vector3 point)
        {
            MovePath.path = PathFinding.GetPath(Player.Pos, point, out bool fast);
            if (fast)
            {
                Player.Pos = MovePath.NextPosition();
                Player.Pos = MovePath.NextPosition();
                Debug.Log("Teleport");
            }
        }
        UnityAction voidMaining;
        public void MovePlayerToMining(UnityAction onTargetReached, Vector3 point)
        {
            Player.EvStopMining = null;
            voidMaining = () => { PlayAnimaMining(onTargetReached); EvStopMoving -= voidMaining; };
            EvStopMoving += voidMaining;
            GO(point);
        }

        private void PlayAnimaMining(UnityAction onTargetReached)
        {
            Player.AnimaControl.Pick();
            Player.EvStopMining += () => 
            {
                if (EnergyController.TakeEnergy())
                {
                    onTargetReached?.Invoke();
                }
                else
                {
                    TrashQueue.ClearQueue();
                } 
            };
        }

        public void Update(float deltaTime)
        {
            if (MovePath.isNext)
            {
                Player.Pos = MovePath.NextPosition(Player.Pos, Player.Speed, deltaTime);

                if (MovePath.GetDirection().x <= 0) Player.Model.eulerAngles = Vector3.zero;
                else Player.Model.eulerAngles = Vector3.up * 180;

                if (MovePath.GetDirection().y <= 0) Player.AnimaControl.Walk();
                else Player.AnimaControl.WalkBack();

                if (!MovePath.isNext)
                {
                    Player.AnimaControl.Breathes();
                    Player.Model.eulerAngles = Vector3.zero;
                    EvStopMoving?.Invoke();
                }
            }
        }

        private void SetStateBuilder(StateBuilder state)
        {
            stateBuilder = state;
        }
    }
}