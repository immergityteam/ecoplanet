﻿using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IPlayer {

        float Speed { get; set; }

        Vector3 Pos { get; set; }

        Transform Model { get; set; }

        Animator Animator { get; set; }
        UnityAction EvStopMining { get; set; }
        UnityAction<bool> PlayerDisappeared { get; set; }
        AnimaControl AnimaControl { get; set; }
    }
}