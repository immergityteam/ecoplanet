﻿using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public interface IGridPathfinding {
        List<Vector3> GetPath(Vector3 start, Vector3 end, out bool fast);
    }
}

