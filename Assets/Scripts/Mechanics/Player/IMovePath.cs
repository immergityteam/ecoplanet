﻿using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public interface IMovePath
    {

        List<Vector3> path { get; set; }
        bool isNext { get; set; }
        Vector3 NextPosition(Vector3 nowPos, float speed, float deltaTime);
        Vector3 NextPosition();
        Vector3 GetDirection();
        void Clear();
    }
}
