using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class AnimaEvent : MonoBehaviour
    {
        public UnityAction EvStopMining { get; set; }
        public void StopMiningInvoke() => EvStopMining?.Invoke();
    }
}