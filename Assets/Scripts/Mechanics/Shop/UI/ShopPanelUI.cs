using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace ColLobOK
{
    public class ShopPanelUI : MonoBehaviour
    {
        public GameObject GameObject => gameObject;

        public LotBaseSO[] lots;

        public TMP_Text Header;
        public TMP_Text Description;

        public Transform Parent;
        public GameObject Prefab;

        public StringsSO HeaderSSO;
        public StringsSO DescriptionSSO;
    }
}
