using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class CardUI : MonoBehaviour
    {
        public TMP_Text Header;
        public Image icon;

        public Button buy;
        public TMP_Text buyText;
    }
}