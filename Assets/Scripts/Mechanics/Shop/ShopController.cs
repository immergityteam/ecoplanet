using System;
using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Purchasing;

namespace ColLobOK
{
    public class ShopController : IShopController, IDependent
    {
        [Dependency] public IShopUI ShopUI { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IMyMetrica MyMetrica { private get; set; }
        [Dependency] public IMyInAppManager MyInAppManager { private get; set; }

        //IMyInAppManager MyInAppManager;

        public void OnInjected()
        {
            GenerateUI();

            LocalizationID.Instance.EvChangeLanguage += ()=>GenerateLots(ShopUI.Energy);
        }

        private void BuyLot(LotBaseSO lot) => lot.GetLot();

        public void OpenEnergy()
        {
            MyMetrica.ReportMetric("EndedEnergy", "EndedEnergy");
            OpenShop(ShopUI.Energy);
        }
        public void OpenDonat()
        {
            MyMetrica.ReportMetric("EndedDonat", "EndedDonat");
            OpenShop(ShopUI.Donat);
        }

        #region UI
        private void GenerateUI()
        {
            ShopUI.Donat.Button.onClick.AddListener(() => OpenShop(ShopUI.Donat));
            ShopUI.Energy.Button.onClick.AddListener(() => OpenShop(ShopUI.Energy));
            GameUI.AddDonate.onClick.AddListener(() => OpenShop(ShopUI.Donat));
            GameUI.AddEnegry.onClick.AddListener(() => OpenShop(ShopUI.Energy));

            GenerateLots(ShopUI.Energy);
        }

        private void GenerateLotsDonate(ShopWindow target)
        {
            CardUI temp;
            string[] purchase = MyInAppManager.GetIdPurchase();
            for (int i = 0; i < purchase.Length; i++)
            {
                Product pro = MyInAppManager.GetProduct(purchase[i]);
                if (pro == null)
                {
                    Debug.LogError("Not Product");
                    return;
                }

                temp = Creator.GetGameObject(target.Panel.Prefab, target.Panel.Parent).GetComponent<CardUI>();
                temp.Header.text = "" + target.Panel.lots[i].count;
                temp.icon.sprite = target.Panel.lots[i].icon;
                temp.buyText.text = pro.metadata.localizedPriceString + pro.metadata.isoCurrencyCode;

                int k = i;
                temp.buy.onClick.AddListener(() => DONAT(purchase[k]));
            }

        }

        private void DONAT(string ids)
        {
            MyInAppManager.OnPurchaseClicked(ids);
        }

        private void GenerateLots(ShopWindow target)
        {
            //MyInAppManager = GameStarter.container.Resolve<IMyInAppManager>();
            CardUI temp;
            Creator.ClearKids(target.Panel.Parent);
            for (int i = 0; i < target.Panel.lots.Length; i++)
            {
                temp = Creator.GetGameObject(target.Panel.Prefab, target.Panel.Parent).GetComponent<CardUI>();
                temp.Header.text = "" + target.Panel.lots[i].count + " " + target.Panel.lots[i].text?.GetString();
                temp.icon.sprite = target.Panel.lots[i].icon;
                temp.buyText.text = target.Panel.lots[i].price.ToString();
                int k = i;
                temp.buy.onClick.AddListener(() => BuyLot(target.Panel.lots[k]));
            }
        }

        private ShopWindow OldPanel;

        private bool needLoad = true;

        private void OpenShop(ShopWindow target)
        {
            if (needLoad)
            {
                try
                {
                    GenerateLotsDonate(ShopUI.Donat);
                    needLoad = false;
                }
                catch (Exception)
                {
                    Debug.LogError("NeedLoad");
                    throw;
                }

            }

            ShopUI.GameObject.SetActive(true);
            if (OldPanel.Panel)
            {
                ChangePanel(OldPanel, false);
            }
            OldPanel = target;
            ChangePanel(target, true);
            MyMetrica.ReportMetric("OpenShop", target.Panel.name);
        }

        private void ChangePanel(ShopWindow target, bool val)
        {
            target.Panel.gameObject.SetActive(val);
            target.Button.gameObject.SetActive(!val);
            target.ButtonOn.gameObject.SetActive(val);

            target.Panel.Header.text = target.Panel.HeaderSSO.GetString();
            target.Panel.Description.text = target.Panel.DescriptionSSO.GetString();
        }
        #endregion
    }
}