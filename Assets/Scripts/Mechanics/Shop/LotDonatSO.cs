using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{

    [CreateAssetMenu(fileName = "LotDonatSO", menuName = "SO/Lot/Donat", order = 1)]
    public class LotDonatSO : LotBaseSO
    {
        string IdPurchase;
        public override void GetLot()
        {
            GameStarter.container.Resolve<IDonatController>().AddDonat(count);
        }
    }
}