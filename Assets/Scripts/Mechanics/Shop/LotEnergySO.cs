using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{

    [CreateAssetMenu(fileName = "LotEnergySO", menuName = "SO/Lot/Energy", order = 1)]
    public class LotEnergySO : LotBaseSO
    {
        public override void GetLot()
        {
            if (GameStarter.container.Resolve<IDonatController>().TakeDonat(price))
            {
                GameStarter.container.Resolve<IEnergyController>().AddEnergy(count);
            }
        }
    }
}