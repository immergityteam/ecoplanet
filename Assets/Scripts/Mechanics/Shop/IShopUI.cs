﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    [Serializable]
    public struct ShopWindow
    {
        public ShopPanelUI Panel;
        public Button Button;
        public GameObject ButtonOn;

    }
    public interface IShopUI
    {
        GameObject GameObject { get; }
        Button Close { get; }
        ShopWindow Donat { get; }
        ShopWindow Energy { get; }

    }
}