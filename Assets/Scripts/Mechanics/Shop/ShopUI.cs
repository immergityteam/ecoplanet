using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class ShopUI : MonoBehaviour, IShopUI
    {
        public GameObject GameObject => gameObject;
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public ShopWindow Donat { get; private set; }
        [field: SerializeField] public ShopWindow Energy { get; private set; }
    }
}