using UnityEngine;

namespace ColLobOK
{
    public class LotBaseSO : ScriptableObject
    {
        public int count;
        public StringsSO text;
        public Sprite icon;
        public int price;

        public virtual void GetLot() { }
    }
}