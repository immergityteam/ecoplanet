using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class ConfirmMessageView : MonoBehaviour
    {
        public TMP_Text HeadText;
        public TMP_Text MainText;

        public GameObject PanelEcoCoin;
        public TMP_Text EcoCoin;

        public Button ButtonYes;
        public Button ButtonNo;
        public TMP_Text ButtonYesText;
        public TMP_Text ButtonNoText;
        public ContentSizeFitter ContentSize;

        public StringsSO Yes;
        public StringsSO No;
    }
}
