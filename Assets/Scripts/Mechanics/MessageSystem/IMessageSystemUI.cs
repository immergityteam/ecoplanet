﻿using UnityEngine;

namespace ColLobOK
{
    public interface IMessageSystemUI
    {
        ConfirmMessageView ComfirmMessageWindow { get; }
        PopUpWindowView PopUpWindow { get; }

        GameObject TimePopUp { get; }

        TrashPopUpView TrashPopUp { get; }
        GameObject CraftPopUp { get; }
        GameObject RepairBuildingPopUpObject { get; }
        RewardUI RewardWindow { get; }
        StringsSO DemolitionBan { get; }
    }
}