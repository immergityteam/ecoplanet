using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class MessageSystemController : IMessageSystemController, IDependent
    {
        [Dependency] public IMessageSystemUI MessageSystemUI { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public ITiker Tiker { private get; set; }
        [Dependency] public ICommonLines CommonLines { private get; set; }

        private float timeBeforeDismiss = 4.0f;

        private Dictionary<int, TimePopUpView> timePopUps = new Dictionary<int, TimePopUpView>();
        public void OnInjected()
        {
            Tiker.SetTikerTime(timeBeforeDismiss);
            Tiker.EvTickSec += (() =>
            {
                Tiker.StopTiker();
                MessageSystemUI.TrashPopUp.gameObject.SetActive(false);
            });

            LocalizationID.Instance.EvChangeLanguage += ChangeText;
            ChangeText();

            MessageSystemUI.RewardWindow.AcceptButton.onClick.AddListener(DisableRewardWindow);
        }

        public StringsSO ShowDemolitionBan()
        {
            return MessageSystemUI.DemolitionBan;
        }

        private void ChangeText()
        {
            MessageSystemUI.ComfirmMessageWindow.ButtonNoText.text = MessageSystemUI.ComfirmMessageWindow.No.GetString();
            MessageSystemUI.ComfirmMessageWindow.ButtonYesText.text = MessageSystemUI.ComfirmMessageWindow.Yes.GetString();
        }

        public void SendMessageWindow(UnityAction action, StringsSO main)
        {
            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(true);
            MessageSystemUI.ComfirmMessageWindow.MainText.text = main.GetString();
            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.AddListener(() =>
            {
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
                EvMessageWindowClose?.Invoke();
                EvMessageWindowClose = null;
            });
            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.AddListener(() =>
            {
                action?.Invoke();
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
            });
        }

        public void SendMessageWindow(UnityAction action, StringsSO main, BuildingTimer popUpView)
        {
            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(true);
            MessageSystemUI.ComfirmMessageWindow.MainText.text = main.GetString();
            MessageSystemUI.ComfirmMessageWindow.PanelEcoCoin.SetActive(true);
            MessageSystemUI.ComfirmMessageWindow.EcoCoin.text = ((popUpView.TimeRemaining + 59) / 60).ToString();
            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.AddListener(() =>
            {
                MessageSystemUI.ComfirmMessageWindow.PanelEcoCoin.SetActive(false);
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
                EvMessageWindowClose?.Invoke();
                EvMessageWindowClose = null;
            });
            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.AddListener(() =>
            {
                action?.Invoke();
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
            });
        }

        public void SendMessageWindow(StringsSO main, int coinAmount, UnityAction callYes, UnityAction callNo = null)
        {
            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.RemoveAllListeners();
            MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(true);
            MessageSystemUI.ComfirmMessageWindow.MainText.text = main.GetString();
            MessageSystemUI.ComfirmMessageWindow.PanelEcoCoin.SetActive(true);
            MessageSystemUI.ComfirmMessageWindow.EcoCoin.text = coinAmount.ToString();
            MessageSystemUI.ComfirmMessageWindow.ContentSize.SetLayoutHorizontal();

            MessageSystemUI.ComfirmMessageWindow.ButtonNo.onClick.AddListener(() =>
            {
                MessageSystemUI.ComfirmMessageWindow.PanelEcoCoin.SetActive(false);
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
                EvMessageWindowClose?.Invoke();
                callNo?.Invoke();
                callNo = null;
                callYes = null;
                EvMessageWindowClose = null;
            });

            MessageSystemUI.ComfirmMessageWindow.ButtonYes.onClick.AddListener(() =>
            {
                callYes?.Invoke();
                MessageSystemUI.ComfirmMessageWindow.gameObject.SetActive(false);
                callNo = null;
                callYes = null;
            });

        }

        public void UpdateCoins(BuildingTimer popUp)
        {
            if (MessageSystemUI.ComfirmMessageWindow.isActiveAndEnabled)
            {
                if (MessageSystemUI.ComfirmMessageWindow.PanelEcoCoin.activeInHierarchy)
                {
                    MessageSystemUI.ComfirmMessageWindow.EcoCoin.text = ((popUp.TimeRemaining + 59) / 60).ToString();

                }
            }
        }

        public void ShowPopUpWindow(StringsSO message)
        {
            MessageSystemUI.PopUpWindow.gameObject.SetActive(true);
            MessageSystemUI.PopUpWindow.message.text = message.GetString();
            var sequence1 = DOTween.Sequence();
            sequence1.AppendInterval(1.3f).onComplete += () =>
            {
                MessageSystemUI.PopUpWindow.gameObject.SetActive(false);
                sequence1.Kill();
            };
        }

        public void ShowTrashMessage(Vector3 position, int maxClicks, int clicksTaken)
        {
            Tiker.Restart();
            MessageSystemUI.TrashPopUp.gameObject.transform.position = position + Vector3.up;
            MessageSystemUI.TrashPopUp.gameObject.SetActive(true);
            MessageSystemUI.TrashPopUp.message.text = $"{clicksTaken}/{maxClicks}";
        }

        public void ShowTimeMessage(Transform transform, int timeRemainig, int id)
        {
            if (timePopUps.ContainsKey(id)) return;
            var popUp = Creator.GetGameObjectPool(MessageSystemUI.TimePopUp);
            var popUpView = popUp.GetComponent<TimePopUpView>();
            popUpView.TimeRemainingInIntegerVariable = timeRemainig;
            popUpView.TimeRemaining.text = MyMethods.ToTimeHMS(popUpView.TimeRemainingInIntegerVariable, CommonLines.FormatTime.GetString());
            popUp.gameObject.SetActive(true);
            popUp.transform.position = transform.position + Vector3.up * 2.2f;
            popUp.transform.SetParent(transform);
            popUpView.ShowRemainigTimeBooster += (x) =>
            {
                //Debug.Log($"УСКОРЕНИЕ: ВРЕМЕНИ ОСТАЛОСЬ {timeRemainig}");
            };
            timePopUps.Add(id, popUpView);
            foreach (var VARIABLE in timePopUps)
            {
                //Debug.Log($"{VARIABLE.Key} : {VARIABLE.Value}");
            }
        }

        public void UpdateTime(int ID, int timeRemaining)
        {
            //Debug.Log(timePopUps[ID]);
            //Debug.Log(ID);
            if (timePopUps.ContainsKey(ID))
            {
                timePopUps[ID].TimeRemaining.text = MyMethods.ToTimeHMS(timePopUps[ID].TimeRemainingInIntegerVariable, "чмс");
                timePopUps[ID].TimeRemainingInIntegerVariable = timeRemaining;
            }
        }

        public void RemoveTime(int ID)
        {
            Creator.MyDestroyPool(timePopUps[ID].gameObject);
            timePopUps.Remove(ID);
        }
        
        int buildingMessageID = 0;
        Dictionary<int, GameObject> buildingMessage = new Dictionary<int, GameObject>();

        public int ShowBuildingMessage(Vector3 position, Action action, Sprite icon = null)
        {
            var popUp = Creator.GetGameObjectPool(MessageSystemUI.RepairBuildingPopUpObject);
            var popUpView = popUp.GetComponent<RepairBuildingPopUpView>();
            popUp.gameObject.SetActive(true);
            popUp.transform.position = position + Vector3.up;
            popUpView.PopUpPressed += () =>
            {
                action?.Invoke();
                popUpView.PopUpPressed = null;
                Creator.MyDestroyPool(popUp);
            };
            buildingMessageID++;
            buildingMessage.Add(buildingMessageID, popUp);
            return buildingMessageID;
        }

        public int ShowBuildingMessage(Vector3 position, Sprite icon, Action action)
        {
            var popUp = Creator.GetGameObjectPool(MessageSystemUI.CraftPopUp);
            var popUpView = popUp.GetComponent<CraftPopUpBackgroundView>();
            popUp.gameObject.SetActive(true);
            popUp.transform.position = position + Vector3.up * 2.2f;
            popUpView.Actions.ActionOnCLick += () =>
            {
                action?.Invoke();
                popUpView.Actions.ActionOnCLick = null;
                Creator.MyDestroyPool(popUp);
            };
            popUpView.Actions.Icon.sprite = icon;

            buildingMessageID++;
            buildingMessage.Add(buildingMessageID, popUp);
            return buildingMessageID;
        }
        
        public void DestroyBuildingMessage(int id)
        {
            if (buildingMessage.ContainsKey(id))
            {
                buildingMessage[id].GetComponent<RepairBuildingPopUpView>().PopUpPressed = null;
                Creator.MyDestroyPool(buildingMessage[id]);
            }
        }
        
        public Action EvMessageWindowClose { get; set; }
        
        public void RefreshRewardWindow(string questName)
        {
            if (questName != null)
            {
                SubstituteBaseLabels();
                
                RefreshTechnologyBranch();
                RefreshIcon(MessageSystemUI.RewardWindow.RewardSO.QuestReward);
                RefreshMessage(false);
                RefreshReasonForAppearance(questName);
                RefreshRightGirl(MessageSystemUI.RewardWindow.RewardSO.QuestGirl);
                
                MessageSystemUI.RewardWindow.gameObject.SetActive(true);
            }
        }
        
        public void RefreshRewardWindow(int branchNumber = -1, int branchIndex = -1)
        {
            if (branchNumber > -1 && branchIndex > -1)
            {
                SubstituteBaseLabels();
                
                RefreshTechnologyBranch(branchNumber);
                RefreshIcon(FindIcon(branchIndex));
                RefreshMessage(true);
                RefreshReasonForAppearance(FindBranchName(branchIndex));
                RefreshRightGirl(FindGirl(branchIndex));
                
                MessageSystemUI.RewardWindow.gameObject.SetActive(true);
            }
        }
        
        private void SubstituteBaseLabels()
        {
            MessageSystemUI.RewardWindow.Congratulations.text = MessageSystemUI.RewardWindow.RewardSO.Congratulations.GetString();
            MessageSystemUI.RewardWindow.Excellent.text = MessageSystemUI.RewardWindow.RewardSO.Excellent.GetString();
        }
        
        private void RefreshMessage(bool isTechnologicalTransition)
        {
            MessageSystemUI.RewardWindow.MessageText.text = isTechnologicalTransition ? 
                MessageSystemUI.RewardWindow.RewardSO.StageTransition.GetString() : 
                MessageSystemUI.RewardWindow.RewardSO.CompletedQuest.GetString();
        }
        
        private void RefreshReasonForAppearance(string message)
        {
            MessageSystemUI.RewardWindow.ReasonForAppearance.text = message;
        }
        
        private void RefreshTechnologyBranch(int branch = -1)
        {
            if (branch > -1 && branch < MessageSystemUI.RewardWindow.RewardSO.RomanNumerals.Length)
                MessageSystemUI.RewardWindow.TechnologyBranch.text =
                    MessageSystemUI.RewardWindow.RewardSO.RomanNumerals[branch];
            else
                MessageSystemUI.RewardWindow.TechnologyBranch.text = "";
        }
        
        private void RefreshIcon(Sprite icon)
        {
            MessageSystemUI.RewardWindow.Icon.sprite = icon;
        }
        
        private void RefreshRightGirl(Sprite girl)
        {
            MessageSystemUI.RewardWindow.RightGirl.sprite = girl;
        }
        
        private Sprite FindIcon(int id)
        {
            var icon = MessageSystemUI.RewardWindow.RewardSO.TechnologyMainConfig.TechBranches[0].TechBranch.Icon;
            
            foreach (var branchData in MessageSystemUI.RewardWindow.RewardSO.TechnologyMainConfig.TechBranches)
            {
                if (branchData.ID == id)
                    icon = branchData.TechBranch.Icon;
            }
            
            return icon;
        }
        
        private string FindBranchName(int id)
        {
            var branchName = "";
            
            foreach (var branchData in MessageSystemUI.RewardWindow.RewardSO.TechnologyMainConfig.TechBranches)
            {
                if (branchData.ID == id)
                    branchName = branchData.TechBranch.BranchName.GetString();
            }
            
            return branchName;
        }
        
        private Sprite FindGirl(int id)
        {
            var girl = MessageSystemUI.RewardWindow.RewardSO.TechnologyMainConfig.TechBranches[0].TechBranch.Girl;
            
            foreach (var branchData in MessageSystemUI.RewardWindow.RewardSO.TechnologyMainConfig.TechBranches)
            {
                if (branchData.ID == id)
                    girl = branchData.TechBranch.Girl;
            }
            
            return girl;
        }
        
        private void DisableRewardWindow()
        {
            MessageSystemUI.RewardWindow.gameObject.SetActive(false);
        }
    }
}