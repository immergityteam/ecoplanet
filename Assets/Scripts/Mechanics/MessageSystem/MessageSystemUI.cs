using ColLobOK;
using UnityEngine;

public class MessageSystemUI : MonoBehaviour, IMessageSystemUI
{
    [field: SerializeField] public ConfirmMessageView ComfirmMessageWindow { get; private set; }
    [field: SerializeField] public PopUpWindowView PopUpWindow { get; private set; }
    [field: SerializeField] public TrashPopUpView TrashPopUp { get; private set; }
    [field: SerializeField] public GameObject CraftPopUp { get; private set;}
    [field: SerializeField] public GameObject RepairBuildingPopUpObject { get; private set;}
    [field: SerializeField] public RewardUI RewardWindow { get; private set;}
    
    [field: SerializeField] public GameObject TimePopUp { get; private set; }

    [field: SerializeField] public StringsSO DemolitionBan { get; private set; }
}
