using TMPro;
using UnityEngine;

namespace ColLobOK
{
    public class PopUpWindowView : MonoBehaviour
    {
        public TMP_Text message;
        public Transform messageTransform;
    }
}
