﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IMessageSystemController
    {
        void SendMessageWindow(StringsSO main, int coinAmount, UnityAction callYes, UnityAction callNo = null);
        void SendMessageWindow(UnityAction action, StringsSO main);
        void SendMessageWindow(UnityAction action, StringsSO main, BuildingTimer popUpView);
        void UpdateCoins(BuildingTimer popUp);
        void ShowPopUpWindow(StringsSO message);

        void ShowTrashMessage(Vector3 position, int maxClicks, int clicksTaken);
        int ShowBuildingMessage(Vector3 position, Action action, Sprite icon = null);
        void ShowTimeMessage(Transform transform, int timeRemainig, int id);
        void UpdateTime(int ID, int timeRemaining);
        int ShowBuildingMessage(Vector3 position, Sprite icon, Action action);
        void DestroyBuildingMessage(int id);
        void RemoveTime(int ID);
        void RefreshRewardWindow(int branchNumber = -1, int branchIndex = -1);
        void RefreshRewardWindow(string questName);
        StringsSO ShowDemolitionBan();


        Action EvMessageWindowClose { get; set; }
    }
}