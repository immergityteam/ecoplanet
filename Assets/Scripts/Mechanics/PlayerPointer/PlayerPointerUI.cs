using UnityEngine;

public class PlayerPointerUI : MonoBehaviour, IPlayerPointerUI
{
    [field: SerializeField] public RectTransform PointerPanel { get; private set; }
    [field: SerializeField] public GameObject Panel { get; private set; }
}