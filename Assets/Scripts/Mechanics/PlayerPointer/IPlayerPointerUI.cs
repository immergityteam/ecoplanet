using UnityEngine;

public interface IPlayerPointerUI
{
    public RectTransform PointerPanel { get; }
    public GameObject Panel { get; }
}