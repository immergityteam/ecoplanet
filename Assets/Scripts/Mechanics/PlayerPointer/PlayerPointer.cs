using System.Collections;
using UnityDI;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class PlayerPointer : IPlayerPointer, IDependent
    {
        [Dependency] public IPlayer Player { private get; set; }
        [Dependency] public IPlayerPointerUI PlayerPointerUI { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        
        private GameObject _pointerImage;
        private Vector3 _pointerPosition;
        private IEnumerator _coroutine;
        
        public void OnInjected()
        {
            //Player.PlayerDisappeared += OnPlayerDisappeared;
            _pointerImage = PlayerPointerUI.Panel;
        }
        
        private void OnPlayerDisappeared(bool state)
        {
            if (PlayerPointerUI.Panel.gameObject != null)
            {
                PlayerPointerUI.Panel.gameObject.SetActive(state);
                
                if (state)
                {
                    _coroutine = ChangePointerLocation();
                    Creator.StartCourutine(_coroutine);
                }
                else
                {
                    if (_coroutine != null)
                    {
                        Creator.StopCourutine(_coroutine);
                        _coroutine = null;
                    }
                }
            }
        }
        
        private IEnumerator ChangePointerLocation()
        {
            while (true)
            {
                if (Camera.main is { }) _pointerPosition = Camera.main.WorldToScreenPoint(Player.Pos);
                
                _pointerImage.transform.up = (_pointerPosition - _pointerImage.transform.position).normalized;
                
                _pointerPosition.x = Mathf.Clamp(_pointerPosition.x, PlayerPointerUI.PointerPanel.rect.xMin + PlayerPointerUI.PointerPanel.transform.position.x, PlayerPointerUI.PointerPanel.rect.xMax + PlayerPointerUI.PointerPanel.transform.position.x);
                _pointerPosition.y = Mathf.Clamp(_pointerPosition.y, PlayerPointerUI.PointerPanel.rect.yMin + PlayerPointerUI.PointerPanel.transform.position.y, PlayerPointerUI.PointerPanel.rect.yMax + PlayerPointerUI.PointerPanel.transform.position.y);
                _pointerImage.transform.position = _pointerPosition;
                
                yield return null;   
            }
        }
        
        public void OnDestroy()
        {
            Player.PlayerDisappeared -= OnPlayerDisappeared;
        }
    }
}