using System;
using System.Collections.Generic;
using System.Linq;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [Serializable]
    class TypeResData
    {
        public int countCell;
        public int countCellMax;
        public Dictionary<int, int> res;
        public TypeRes type;
    }

    public class WarehouseController : IWarehouseController, IDependent
    {
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IContainerAllResources AllResources { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }

        Dictionary<TypeRes, TypeResData> allMyRes;

        public UnityAction<int> EvChangeRes { get; set; }

        public void OnInjected()
        {
            allMyRes = new Dictionary<TypeRes, TypeResData>(ContainerMono.TypeConfig.Length);
            SaveAndLoad.EvSave += Save;
            for (int i = 0; i < ContainerMono.TypeConfig.Length; i++)
            {
                TypeResData typeResData = new TypeResData();
                typeResData.res = new Dictionary<int, int>();
                typeResData.countCell = 0;
                typeResData.countCellMax = ContainerMono.TypeConfig[i].maxCountCell;
                typeResData.type = ContainerMono.TypeConfig[i].type;
                allMyRes.Add(ContainerMono.TypeConfig[i].type, typeResData);
            }

            Resource start = new Resource(ContainerMono.Donate, 5);
            AddRes(start);
            start = new Resource(ContainerMono.Money, 25);
            AddRes(start);

            Load();
        }

        ConvertDataBinary<Dictionary<TypeRes, TypeResData>> saveData
            = new ConvertDataBinary<Dictionary<TypeRes, TypeResData>>("WarehouseController");
        private void Save() => SaveAndLoad.Save(saveData.name, saveData.ToConvert(allMyRes));
        private void Load()
        {
            string str = SaveAndLoad.Load(saveData.name);
            if (str != "") allMyRes = saveData.FromConvert(str);
        }

        #region Methods For AllMyRes
        public void AddRes(List<Resource> data)
        {
            for (int i = 0; i < data.Count; i++)
            {
                AddRes(data[i]);
            }
        }
        public void AddRes(Resource[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                AddRes(data[i]);
            }
        }
        public void AddRes(Resource data)
        {
            if (ContainsRes(data))
            {
                ChangeCountCell(data, IsChangeCountRes(data));
                ChangeCountRes(data);
            }
            else
            {
                ChangeCountCell(data, IsChangeCountRes(data));
                AddNewRes(data);
            }
            EvChangeRes?.Invoke(data.ID);
        }
        public void TakeRes(Resource[] data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                TakeRes(data[i]);
            }
        }
        public void TakeRes(Resource data)
        {
            data.count = Mathf.Abs(data.count) * -1;
            ChangeCountCell(data, IsChangeCountRes(data));
            ChangeCountRes(data);
            EvChangeRes?.Invoke(data.ID);
        }
        public bool CheckFreeSpace(Resource data)
        {
            return allMyRes[GetTypeRes(data.ID)].countCell < allMyRes[GetTypeRes(data.ID)].countCellMax
                || IsChangeCountRes(data) == 0;
        }
        public bool CheckFreeSpace(Resource[] data)
        {
            bool spaseFree = true;
            for (int i = 0; i < data.Length; i++) spaseFree &= CheckFreeSpace(data[i]);
            return spaseFree;
        }
        public bool CheckingRequiredResources(Resource[] data)
        {
            bool requiredResources = true;
            for (int i = 0; i < data.Length; i++) requiredResources &= (LookOneRes(data[i].ID) >= data[i].count);
            return requiredResources;
        }
        public int LookOneRes(int id)
        {
            if (ContainsRes(id)) return GetCountRes(id);
            else return 0;
        }
        public List<Resource> LookAllResType(TypeRes type)
        {
            List<Resource> allResTypeList = new List<Resource>();
            foreach (var item in allMyRes[type].res) allResTypeList.Add(new Resource(AllResources.GetConfigRes(item.Key).nameRes, item.Value));
            return allResTypeList;
        }

        public void AddMaxCellType(TypeRes type, UnityAction call)
        {
            GameStarter.container.Resolve<IDonatController>().TakeDonat(GetPriceCell(allMyRes[type].countCellMax, type), ContainerMono.AddCellWarehouse, () => { allMyRes[type].countCellMax += 1; call?.Invoke(); });
        }

        private int GetPriceCell(int countCell, TypeRes type)
        {
            int count = ContainerMono.TypeConfig.First((x) => x.type == type).maxCountCell;
            if (countCell < 15 + count)
            {
                return ContainerMono.PriceCellWarehouse[0];
            }
            if (countCell < 30 + count)
            {
                return ContainerMono.PriceCellWarehouse[1];
            }
            return ContainerMono.PriceCellWarehouse[2];
        }

        public int GetMaxCellType(TypeRes type)
        {
            return allMyRes[type].countCellMax;
        }
        public int GetCellType(TypeRes type)
        {
            return allMyRes[type].countCell;
        }

        private void AddNewRes(Resource data)
        {
            allMyRes[GetTypeRes(data.ID)].res.Add(data.ID, data.count);
        }
        private void ChangeCountRes(Resource data)
        {
            allMyRes[GetTypeRes(data.ID)].res[data.ID] += data.count;
        }
        private int IsChangeCountRes(Resource data)
        {
            return (GetCountRes(data) + data.count + GetMaxCountResOneCell(data)) / (GetMaxCountResOneCell(data) + 1)
                 - (GetCountRes(data) + GetMaxCountResOneCell(data)) / (GetMaxCountResOneCell(data) + 1);
        }
        private void ChangeCountCell(Resource data, int val)
        {
            allMyRes[GetTypeRes(data.ID)].countCell += val;
        }
        private void ChangeCountCell(int id, int val)
        {
            allMyRes[GetTypeRes(id)].countCell += val;
        }
        private bool ContainsRes(Resource data)
        {
            return allMyRes[GetTypeRes(data.ID)].res.ContainsKey(data.ID);
        }
        private bool ContainsRes(int id)
        {
            return allMyRes[GetTypeRes(id)].res.ContainsKey(id);
        }
        private int GetCountRes(Resource data)
        {
            return allMyRes[GetTypeRes(data.ID)].res.ContainsKey(data.ID) ? allMyRes[GetTypeRes(data.ID)].res[data.ID] : 0;
        }
        private int GetCountRes(int id)
        {
            return allMyRes[GetTypeRes(id)].res.ContainsKey(id) ? allMyRes[GetTypeRes(id)].res[id] : 0;
        }
        private int GetMaxCountResOneCell(Resource data)
        {
            return AllResources.GetConfigRes(data.ID).maxCountOneCell;
        }
        private int GetMaxCountResOneCell(int id)
        {
            return AllResources.GetConfigRes(id).maxCountOneCell;
        }
        private TypeRes GetTypeRes(int IDRes)
        {
            return AllResources.GetConfigRes(IDRes).type;
        }

        #endregion
    }
}