﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface IResourceFactory
    {
        UnityAction<int> BuildResoursePanel(Resource resource);
        void BuildResoursePanel(TypeRes type, UnityAction<TypeRes> call);

    }
}