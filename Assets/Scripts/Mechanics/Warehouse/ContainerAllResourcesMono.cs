using Developing;
using UnityEngine;

namespace ColLobOK
{
    public class ContainerAllResourcesMono : MonoBehaviour, IContainerAllResourcesMono {
        [field: SerializeField] public ResurceConfigSO[] ResurceConfig { get; private set; }
#if UNITY_EDITOR
        [ContextMenu("GetAllResurceConfigSO")]
        private void GetAllResurceConfigSO()
        {
            ResurceConfig =  MetodForEditor.GetAllAssets<ResurceConfigSO>();
        }
#endif
    }
}