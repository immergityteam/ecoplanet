using System.Collections.Generic;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class ContainerAllResources : IContainerAllResources, IDependent
    {
        [Dependency] public IContainerAllResourcesMono ContainerAllResourcesMono { private get; set; }

        private Dictionary<int, ResurceConfigSO> idResConfig;
        public ResurceConfigSO GetConfigRes(int name)
        {
            return idResConfig.ContainsKey(name) ? idResConfig[name] : idResConfig[-1];
        }
        public void OnInjected()
        {
            idResConfig = new Dictionary<int, ResurceConfigSO>();
            for (int i = 0; i < ContainerAllResourcesMono.ResurceConfig.Length; i++)
            {
                idResConfig.Add(ContainerAllResourcesMono.ResurceConfig[i].nameRes.GetID(), ContainerAllResourcesMono.ResurceConfig[i]);
            }
        }
    }
}
