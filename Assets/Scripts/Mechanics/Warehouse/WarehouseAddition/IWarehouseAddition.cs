namespace ColLobOK
{
    public interface IWarehouseAddition
    {
        public StringsSO ProcessingPlant { get; }
        public StringsSO Warehouse { get; }
        public StringsSO Barn { get; }
    }
}