using UnityEngine;

namespace ColLobOK
{
    public class WarehouseAddition : MonoBehaviour, IWarehouseAddition
    {
        [field: SerializeField] public StringsSO ProcessingPlant { get; private set; }
        [field: SerializeField] public StringsSO Warehouse { get; private set; }
        [field: SerializeField] public StringsSO Barn { get; private set; }
    }
}