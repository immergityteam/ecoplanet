using System.Collections.Generic;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IWarehouseController {
        UnityAction<int> EvChangeRes { get; set; }
        void AddRes(List<Resource> data);
        void AddRes(Resource[] data);
        void AddRes(Resource data);
        void TakeRes(Resource data);
        void TakeRes(Resource[] data);
        bool CheckFreeSpace(Resource data);
        bool CheckFreeSpace(Resource[] data);
        bool CheckingRequiredResources(Resource[] data);
        int LookOneRes(int name);
        List<Resource> LookAllResType(TypeRes type);
        int GetMaxCellType(TypeRes type);
        int GetCellType(TypeRes type);
        void AddMaxCellType(TypeRes type, UnityAction call);
    }
}