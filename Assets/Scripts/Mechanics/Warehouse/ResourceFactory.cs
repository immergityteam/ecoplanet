using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public class ResourceFactory : IResourceFactory
    {
        [Dependency] public IContainerAllResources AllRes { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public IWarehouseUI WarehouseUI { private get; set; }

        public UnityAction<int> BuildResoursePanel(Resource data)
        {
            ResurceConfigSO config = AllRes.GetConfigRes(data.ID);

            IResourceMonoUI resPanel;
            UnityAction<int> temp;
            temp = null;
            while (data.count > 0)
            {
                resPanel = Creator.GetGameObject(WarehouseUI.CenterPanel.prefab, WarehouseUI.CenterPanel.parent).GetComponent<IResourceMonoUI>();
                resPanel.ResurseSprite.sprite = config.sprite;
                resPanel.TextCount.text = config.maxCountOneCell.ToString();

                if (config.canPut || config.canSell)
                {
                    resPanel.ButtonPanel.gameObject.SetActive(true);

                    resPanel.BoxPanel.position = resPanel.BoxPanel.position + Vector3.up * resPanel.ButtonPanel.sizeDelta.y / 2;
                    resPanel.BoxPanel.sizeDelta = resPanel.BoxPanel.sizeDelta - Vector2.up * resPanel.ButtonPanel.sizeDelta.y;

                    resPanel.Sell.gameObject.SetActive(config.canSell);
                    resPanel.Put.gameObject.SetActive(config.canPut);
                    resPanel.Sell.onClick.AddListener(() => SellRes(data.Name));
                    resPanel.Put.onClick.AddListener(() => Put(data.Name));
                }

                if (data.count < config.maxCountOneCell || config.maxCountOneCell == 0)
                {
                    resPanel.TextCount.text = data.count.ToString();
                    temp += (x) => UpdateResCount(x, data.ID, resPanel);

                    break;
                }

                data.count -= config.maxCountOneCell;
            }

            return temp;
        }

        public void BuildResoursePanel(TypeRes type, UnityAction<TypeRes> call)
        {
            IResourceMonoUI resPanel;
            for (int i = WarehouseController.GetCellType(type); i < WarehouseController.GetMaxCellType(type); i++)
            {
                resPanel = Creator.GetGameObject(WarehouseUI.CenterPanel.prefab, WarehouseUI.CenterPanel.parent).GetComponent<IResourceMonoUI>();
                resPanel.ResurseSprite.gameObject.SetActive(false);
                resPanel.TextCount.text = "";
            }
            if (type != TypeRes.RawMaterials)
            {
                GameObject temp = Creator.GetGameObject(WarehouseUI.PrefPlus, WarehouseUI.CenterPanel.parent);
                temp.AddComponent<Button>().onClick.AddListener(() => call(type));
            }
        }

        private void UpdateResCount(int idResChange, int idRes, IResourceMonoUI resource)
        {
            if (idResChange == idRes)
            {
                resource.TextCount.text = WarehouseController.LookOneRes(idRes).ToString();
            }
        }

        private void SellRes(string name)
        {
            Debug.Log("Продать" + name);
        }
        private void Put(string name)
        {
            Debug.Log("Поставить" + name);
        }
    }
}