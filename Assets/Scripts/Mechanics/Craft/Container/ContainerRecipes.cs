using System.Collections.Generic;
using UnityDI;

namespace ColLobOK
{
    public class ContainerRecipes : IContainerRecipes, IDependent
    {
        [Dependency] public IContainerRecipesMono ContainerRecipesMono { private get; set; }

        Dictionary<int, RecipeSO> recipes;
        Dictionary<int, StructureRecipeProductionSO> structure;

        public void OnInjected()
        {
            recipes = new Dictionary<int, RecipeSO>();
            foreach (var item in ContainerRecipesMono.RecipeSO)
            {
                recipes.Add(item.id, item);
            }
            structure = new Dictionary<int, StructureRecipeProductionSO>();
            foreach (var item in ContainerRecipesMono.StructureRecipeProductionSO)
            {
                structure.Add(item.nameStructure.GetID(), item);
            }
        }

        public RecipeSO GetRecipeSO(int id)
        {
            return recipes[id];
        }

        public void ReplaceRecipe(int id, RecipeSO newRecipe)
        {
            recipes[id] = newRecipe;
        }

        public StructureRecipeProductionSO GetStructureRecipeProductionSO(int id)
        {
            return structure[id];
        }
        public bool IsStructureRecipeProductionSO(int id)
        {
            return structure.ContainsKey(id);
        }
    }
}