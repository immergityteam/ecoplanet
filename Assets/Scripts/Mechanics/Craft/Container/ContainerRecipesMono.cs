using Developing;
using UnityEngine;

namespace ColLobOK
{
    public class ContainerRecipesMono : MonoBehaviour, IContainerRecipesMono
    {
        [field: SerializeField] public RecipeSO[] RecipeSO { get; private set; }
        [field: SerializeField] public RecipeSO[] BlockedRecipeSO { get; private set; }
        [field: SerializeField] public StructureRecipeProductionSO[] StructureRecipeProductionSO { get; private set; }

#if UNITY_EDITOR
        [ContextMenu("GetAllRecipeSO")]
        private void GetAllRecipeSO()
        {
            RecipeSO = MetodForEditor.GetAllAssets<RecipeSO>();
            StructureRecipeProductionSO = MetodForEditor.GetAllAssets<StructureRecipeProductionSO>();
        }
#endif
    }
}