﻿namespace ColLobOK
{
    public interface IContainerRecipes
    {
        RecipeSO GetRecipeSO(int id);
        void ReplaceRecipe(int id, RecipeSO newRecipe);
        StructureRecipeProductionSO GetStructureRecipeProductionSO(int id);
        bool IsStructureRecipeProductionSO(int id);
    }
}