﻿namespace ColLobOK
{
    public interface IContainerRecipesMono
    {
        RecipeSO[] RecipeSO { get; }
        RecipeSO[] BlockedRecipeSO { get; }
        StructureRecipeProductionSO[] StructureRecipeProductionSO { get; }

    }
}