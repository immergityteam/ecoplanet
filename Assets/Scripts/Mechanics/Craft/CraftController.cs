using System;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;


namespace ColLobOK
{
    public class CraftController : ICraftController, IDependent
    {
        [Dependency] public IFactoryCraftPanel FactoryCraftPanel { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public ISpawnerItem SpawnerItem { private get; set; }
        [Dependency] public ICraftUI CraftUI { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IContainerRecipes ContainerRecipes { private get; set; }
        [Dependency] public IMyTime MyTime { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public ITechnologyController TechnologyController { private get; set; }
        [Dependency] public ITechnologyConfigContainer TechnologyConfigContainer { private get; set; }
        [Dependency] public ICraftBonusController CraftBonusController { private get; set; }
        [Dependency] public IDonatController DonatController { private get; set; }

        Dictionary<int, Recipe[]> structurRecipe;

        public UnityAction<int> EvTakeRecipe { get; set; }
        public UnityAction<int> EvRecipeGood { get; set; }

        int activeLocalStructur;
        int activeRecipeIndex;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            SaveAndLoad.EvLoadOnFocus += Calculation;
            structurRecipe = new Dictionary<int, Recipe[]>();
            Load();
            BuilderPlayer.EvClikedStructures += OpenCraftWindow;
            BuilderPlayer.CraftController = this;
            CraftUI.Close.onClick.AddListener(() => activeLocalStructur = 0);
        }

        public void OpenCraftWindow(int id)
        {
            if (!ContainerRecipes.IsStructureRecipeProductionSO(BuilderPlayer.GetGlobalID(id))) return;
            activeLocalStructur = id;
            StructureRecipeProductionSO structureRecipe = ContainerRecipes.GetStructureRecipeProductionSO(BuilderPlayer.GetGlobalID(activeLocalStructur));
            FactoryCraftPanel.SetHeader(structureRecipe.nameStructure.GetString());
            FactoryCraftPanel.GenRecipe(structureRecipe.recipes, StartNewRecipe);

            if (!structurRecipe.ContainsKey(activeLocalStructur))
            {
                Recipe[] recipe = new Recipe[structureRecipe.defCounActiveRicepes];
                structurRecipe.Add(activeLocalStructur, recipe);
            }
            UpdatePanel();
        }

        private void UpdatePanel()
        {
            if (activeLocalStructur == 0) return;
            FactoryCraftPanel.GenActiveRecipe(structurRecipe[activeLocalStructur], (x) => Receive(activeLocalStructur, x), MessageSpeedUp, Cancel, MessageBuy, UpdatePanel);
        }

        private void MessageBuy()
        {
            int coun = CraftUI.PriceBuyCellCraft[structurRecipe[activeLocalStructur].Length - 3];
            int k = activeRecipeIndex;
            DonatController.TakeDonat(coun, CraftUI.AddCellCraft, Buy);
        }

        private void Buy()
        {
            Recipe[] activeRecipe = new Recipe[structurRecipe[activeLocalStructur].Length + 1];
            for (int i = 0; i < structurRecipe[activeLocalStructur].Length; i++)
            {
                activeRecipe[i] = structurRecipe[activeLocalStructur][i];
            }
            structurRecipe[activeLocalStructur] = activeRecipe;
            UpdatePanel();
        }

        private void Cancel(int k)
        {
            Resource[] needRes = ContainerRecipes.GetRecipeSO(structurRecipe[activeLocalStructur][k].GetIdRecipe()).needResours;

            if (!WarehouseController.CheckFreeSpace(needRes))
            {
                MessageSystemController.ShowPopUpWindow(CraftUI.WarehouseFull);
                return;
            }
            WarehouseController.AddRes(needRes);
            structurRecipe[activeLocalStructur][k].Deactivate();
            structurRecipe[activeLocalStructur][k] = null;
            ClearNullRecipe(activeLocalStructur);
        }

        UnityAction previewEvent;
        private void MessageSpeedUp()
        {
            UpdateMessage();
            previewEvent = () => UpdateMessage();
            structurRecipe[activeLocalStructur][activeRecipeIndex].EvTik += previewEvent;
        }

        private void UpdateMessage()
        {
            int coun = (structurRecipe[activeLocalStructur][activeRecipeIndex].TimeLeft + 59) / 60;
            int k = activeRecipeIndex;
            DonatController.TakeDonat(coun, CraftUI.AddCellCraft, () => { SpeedUp(k); }, ClearEvent);
        }

        private void ClearEvent()
        {
            if (previewEvent != null)
            {
                structurRecipe[activeLocalStructur][activeRecipeIndex].EvTik -= previewEvent;
                previewEvent = null;
            }
        }

        private void SpeedUp(int val)
        {
            if (val != activeRecipeIndex) return;
            structurRecipe[activeLocalStructur][activeRecipeIndex].SpeedUp();
            UpdatePanel();
        }

        private void Receive(int structureRecipe, int k)
        {
            RecipeSO RSO = ContainerRecipes.GetRecipeSO(structurRecipe[structureRecipe][k].GetIdRecipe());

            for (int i = 0; i < RSO.product.count; i++)
                SpawnerItem.SpawnItem(RSO.product, BuilderPlayer.GetPositionStructure(structureRecipe));
            structurRecipe[structureRecipe][k] = null; 
            activeRecipeIndex--;
            ClearNullRecipe(structureRecipe);
            EvTakeRecipe?.Invoke(RSO.id);
            WarehouseController.AddRes(RSO.product);
        }

        private void ClearNullRecipe(int structureRecipe)
        {
            for (int i = 0; i < structurRecipe[structureRecipe].Length - 1; i++)
            {
                if (structurRecipe[structureRecipe][i] == null)
                {
                    structurRecipe[structureRecipe][i] = structurRecipe[structureRecipe][i + 1];
                    structurRecipe[structureRecipe][i + 1] = null;
                }
            }
            UpdatePanel();
        }

        private void StartNewRecipe(RecipeSO recipe)
        {
            //Checking Required Resources
            if (!WarehouseController.CheckingRequiredResources(recipe.needResours))
            {
                MessageSystemController.ShowPopUpWindow(CraftUI.NotEnoughResources);
                return;
            }
            //Check Free Space
            if (!WarehouseController.CheckFreeSpace(recipe.product))
            {
                MessageSystemController.ShowPopUpWindow(CraftUI.WarehouseFull);
                return;
            }

            bool addSuccessfully = false;
            for (int i = 0; i < structurRecipe[activeLocalStructur].Length; i++)
            {
                if (structurRecipe[activeLocalStructur][i] == null)
                {
                    structurRecipe[activeLocalStructur][i] = new Recipe();
                    int time = (int)(recipe.timeSec * CraftBonusController.GetMultiplier(activeLocalStructur, recipe.id));
                    structurRecipe[activeLocalStructur][i].Fill(recipe.id, activeLocalStructur, time);

                    structurRecipe[activeLocalStructur][i].EvGoodCraft += GoodCraft;
                    int bee = activeLocalStructur;
                    structurRecipe[activeLocalStructur][i].EvTik += () => Tik(GetIdBranch(bee));


                    if (i == 0 || structurRecipe[activeLocalStructur][i - 1].TimeLeft <= 0)
                    {
                        //анимацию старт сюда
                        BuilderPlayer.GetMonoID(activeLocalStructur).animator.SetBool("Work", true);
                        structurRecipe[activeLocalStructur][i].Activate();
                        activeRecipeIndex = i;
                    }
                    addSuccessfully = true;
                    break;
                }
            }
            //Failed to add
            if (!addSuccessfully)
            {
                MessageSystemController.ShowPopUpWindow(CraftUI.QueueFull);
                return;
            }

            WarehouseController.TakeRes(recipe.needResours);
            UpdatePanel();
        }

        private int GetIdBranch(int localStructID)
        {
            int globalId = BuilderPlayer.GetGlobalID(localStructID);
            if (globalId == 0) return -1;
            if (!ContainerRecipes.GetStructureRecipeProductionSO(globalId).technology) return -1;

            foreach (var item in TechnologyConfigContainer.TechnologyConfig.TechBranches)
            {
                if (item.TechBranch.BranchName == ContainerRecipes.GetStructureRecipeProductionSO(globalId).technology)
                    return item.ID;
            }
            return -1;
        }
        
        Dictionary<int, int> branchTik = new Dictionary<int, int>();

        int a = 8;

        private void Tik(int branch, int count = 1)
        {
            if (branch == -1) return;
            if (!branchTik.ContainsKey(branch))
            {
                branchTik.Add(branch, 0);
            }

            branchTik[branch] += count;
            if (branchTik[branch] >= a)
            {
                TechnologyController.AddExpToBranch(branch, branchTik[branch] / a);
                branchTik[branch] %= a;
            }
        }

        private void GoodCraft(int idStructure, int idRecipe)
        {
            EvRecipeGood?.Invoke(idRecipe);

            for (int i = 0; i < structurRecipe[idStructure].Length; i++)
            {
                if (structurRecipe[idStructure][i] != null)
                {
                    //Вырубаем анимацию
                    structurRecipe[idStructure][i].Deactivate();
                    BuilderPlayer.GetMonoID(idStructure).animator.SetBool("Work", false);
                    //Debug.Log(idStructure);
                }
                if (structurRecipe[idStructure][i] != null && structurRecipe[idStructure][i].TimeLeft > 0)
                {
                    structurRecipe[idStructure][i].Activate();
                    //анимацию старт сюда
                    BuilderPlayer.GetMonoID(idStructure).animator.SetBool("Work", true);
                    ClearEvent();
                    activeRecipeIndex = i;
                 
                    break;
                }
            }
            UpdatePanel();
        }

        ConvertDataBinary<List<string>> saveData
            = new ConvertDataBinary<List<string>>("CraftController");

        private void Save()
        {
            List<string> save = new List<string>();

            foreach (var item in structurRecipe)
            {
                for (int i = 0; i < item.Value.Length; i++)
                {
                    if (item.Value[i] != null)
                    {
                        save.Add(item.Value[i].Save());
                    }
                    else
                    {
                        save.Add(item.Key.ToString());
                    }
                }
            }

            SaveAndLoad.Save(saveData.name, saveData.ToConvert(save));
        }
        private void Load()
        {
            List<string> save = saveData.FromConvert(SaveAndLoad.Load(saveData.name));
            for (int i = 0; i < save.Count; i++)
            {
                int localStructure;
                if (int.TryParse(save[i], out localStructure))
                {
                    AddDictionary(localStructure);
                }
                else
                {
                    Recipe newRecipe = new Recipe();
                    newRecipe.Load(save[i]);

                    newRecipe.EvGoodCraft += GoodCraft;

                    newRecipe.EvTik += () => Tik(GetIdBranch(newRecipe.GetIdStructure()));

                    AddDictionary(newRecipe.GetIdStructure(), newRecipe);
                }
            }
            Calculation();
        }

        private void Calculation()
        {
            double delta;
            foreach (var item in structurRecipe)
            {
                delta = MyTime.GetDeltaTime();
                for (int i = 0; i < item.Value.Length; i++)
                {
                    if (item.Value[i] == null) break;
                    if (item.Value[i].TimeLeft > 0)
                    {
                        if (delta > item.Value[i].TimeLeft)
                        {
                            Tik(GetIdBranch(item.Value[i].GetIdStructure()), item.Value[i].TimeLeft);
                            delta -= item.Value[i].TimeLeft;
                            item.Value[i].TimeLeft = 0;
                        }
                        else
                        {
                            Tik(GetIdBranch(item.Value[i].GetIdStructure()), (int)delta);
                            item.Value[i].TimeLeft -= (int)delta;
                            item.Value[i].Activate();
                            break;
                        }
                    }
                }
            }
        }

        private void AddDictionary(int key, Recipe recipe = null)
        {
            if (!structurRecipe.ContainsKey(key))
            {
                structurRecipe.Add(key, null);
            }

            if (structurRecipe[key] == null)
            {
                structurRecipe[key] = new Recipe[1] { recipe };
                return;
            }
            List<Recipe> recipes = new List<Recipe>(structurRecipe[key]);
            recipes.Add(recipe);
            structurRecipe[key] = recipes.ToArray();
        }

        public bool CheckPossibilityToDestroy(int localId)
        {
            var result = true;

            if (structurRecipe[localId][0] == null)
            {
                result = false;
            }

            return result;
        }

        public bool CheckPresenceOfBuilding(int localId)
        {
            var result = false;

            if (structurRecipe.ContainsKey(localId))
            {
                result = true;
            }

            return result;
        }
    }
}