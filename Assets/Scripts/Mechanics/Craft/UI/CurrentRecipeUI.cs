using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class CurrentRecipeUI : MonoBehaviour, ICurrentRecipeUI {
        [field: SerializeField] public TMP_Text Info { get; private set; }
        [field: SerializeField] public TMP_Text TextButton { get; private set; }
        [field: SerializeField] public Image Icon { get; private set; }
        [field: SerializeField] public Button Button { get; private set; }
        [field: SerializeField] public GameObject FakeButton { get; private set; }
        [field: SerializeField] public GameObject FakeButtonGlow { get; private set; }
    }
}