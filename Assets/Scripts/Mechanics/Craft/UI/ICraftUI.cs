﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK {
    public interface ICraftUI {
        GameObject CraftWindow { get; }
        TMP_Text Header { get; }
        GameObject PrefabRecipe { get; }
        Transform ParentRecipe { get; }
        GameObject PrefabBoxCraft { get; }
        Transform ParentRecipeQueue { get; }
        GameObject PlusCrat { get; }
        Button Close { get; }
        Color LackOfResources { get; }
        int MaxCountActiveRicepe { get; }
        int DefCountActiveRicepe { get; }
        StringsSO HMS { get; }
        
        StringsSO WarehouseFull { get; }
        StringsSO NotEnoughResources { get; }
        StringsSO QueueFull { get; }
        StringsSO Create { get; }
        int[] PriceBuyCellCraft { get; }
        StringsSO AddCellCraft { get; }
        StringsSO SpeedUpCraft { get; }

        StringsSO CanselSSO { get; }
        StringsSO GetSSO { get; }
        StringsSO SpeedUpSSO { get; }
    }
}