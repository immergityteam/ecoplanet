﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK {
    public interface IRecipeUI {
        GameObject GameObject { get; }
        Image Product { get; }
        TMP_Text Count { get; }
        TMP_Text Time { get; }
        TMP_Text CreateText { get; }
        Button StartCraft { get; }
        Button Info { get; }
        Transform ParentNeedRes { get; }
        GameObject PrefabNeedRes { get; }
        ContentSizeFitter ContentSizeFitter { get; }
    }
}