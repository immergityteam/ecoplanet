using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class RecipeUI : MonoBehaviour, IRecipeUI {

        public GameObject GameObject { get => gameObject; }
        [field: SerializeField] public Image Product { get; private set; }
        [field: SerializeField] public TMP_Text Count { get; private set; }
        [field: SerializeField] public TMP_Text Time { get; private set; }
        [field: SerializeField] public TMP_Text CreateText { get; private set; }
        [field: SerializeField] public Button StartCraft { get; private set; }
        [field: SerializeField] public Button Info { get; private set; }
        [field: SerializeField] public Transform ParentNeedRes { get; private set; }
        [field: SerializeField] public GameObject PrefabNeedRes { get; private set; }
        [field: SerializeField] public ContentSizeFitter ContentSizeFitter { get; private set; }

    }
}