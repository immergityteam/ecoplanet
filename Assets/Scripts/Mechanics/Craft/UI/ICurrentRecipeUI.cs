﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK {
    public interface ICurrentRecipeUI {
        TMP_Text Info { get; }
        TMP_Text TextButton { get; }
        Image Icon { get; }
        Button Button { get; }
        GameObject FakeButton { get; }
        GameObject FakeButtonGlow { get; }
    }
}