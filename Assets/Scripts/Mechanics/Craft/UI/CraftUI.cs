using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class CraftUI : MonoBehaviour, ICraftUI
    {
        public GameObject CraftWindow => gameObject;
        [field: SerializeField] public TMP_Text Header { get; private set; }
        [field: SerializeField] public GameObject PrefabRecipe { get; private set; }
        [field: SerializeField] public Transform ParentRecipe { get; private set; }
        [field: SerializeField] public GameObject PrefabBoxCraft { get; private set; }
        [field: SerializeField] public Transform ParentRecipeQueue { get; private set; }
        [field: SerializeField] public GameObject PlusCrat { get; private set; }
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public Color LackOfResources { get; private set; }
        [field: SerializeField] public int MaxCountActiveRicepe { get; private set; }
        [field: SerializeField] public int DefCountActiveRicepe { get; private set; }
        [field: SerializeField] public StringsSO HMS { get; private set; }
        [field: SerializeField] public StringsSO WarehouseFull { get; private set; }
        [field: SerializeField] public StringsSO NotEnoughResources { get; private set; }
        [field: SerializeField] public StringsSO QueueFull { get; private set; }
        [field: SerializeField] public StringsSO Create { get; private set; }
        [field: SerializeField] public int[] PriceBuyCellCraft { get; private set; }
        [field: SerializeField] public StringsSO AddCellCraft { get; private set; }
        [field: SerializeField] public StringsSO SpeedUpCraft { get; private set; }

        [field: SerializeField] public StringsSO CanselSSO { get; private set; }
        [field: SerializeField] public StringsSO GetSSO { get; private set; }
        [field: SerializeField] public StringsSO SpeedUpSSO { get; private set; }
    }
}