﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface IRecipe
    {
        UnityAction<int, int> EvGoodCraft { get; set; }
        UnityAction EvTik { get; set; }
        int GetIdRecipe();
        int TimeLeft { get; set; }
        int GetIdStructure();
        void Fill(int idRecipe, int idStructure, int time);
        void Activate();
        void Deactivate();
        void SpeedUp();
        string Save();
        void Load(string str);
    }
}