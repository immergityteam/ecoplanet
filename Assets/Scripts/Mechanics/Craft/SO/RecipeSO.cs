using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Recipe", menuName = "SO/Recipe", order = 1)]
    public class RecipeSO : ScriptableObject {
        public int id;
        public Resource product;
        public int timeSec;
        public Resource[] needResours;
#if UNITY_EDITOR
        public void SetID(int val) => id = val;
#endif
    }
}