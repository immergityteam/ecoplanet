using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/StructureRecipeProduction", order = 1)]
    public class StructureRecipeProductionSO : ScriptableObject
    {
        public StringsSO nameStructure;
        public StringsSO technology;
        public RecipeSO[] recipes;
        public int defCounActiveRicepes;
    }
}