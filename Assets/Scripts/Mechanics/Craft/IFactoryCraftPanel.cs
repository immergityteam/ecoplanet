﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface IFactoryCraftPanel {
        void SetHeader(string name);
        void GenActiveRecipe(
            Recipe[] recipe,
            UnityAction<int> receive,
            UnityAction speedUp,
            UnityAction<int> cancel,
            UnityAction buy,
            UnityAction updatePanel
            );
        void GenRecipe(RecipeSO[] recipes, UnityAction<RecipeSO> call);
        IRecipeUI GetIRecipeUI(RecipeSO RSO);
    }
}