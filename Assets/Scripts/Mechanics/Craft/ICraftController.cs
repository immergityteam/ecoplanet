﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface ICraftController
    {
        UnityAction<int> EvTakeRecipe { get; set; }
        UnityAction<int> EvRecipeGood { get; set; }
        void OpenCraftWindow(int id);
    }
}