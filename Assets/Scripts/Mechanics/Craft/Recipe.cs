using UnityEngine.Events;

namespace ColLobOK
{
    public class Recipe : IRecipe
    {
        private ITiker Tiker;

        private int idRecipe;
        private int idStructure;
        private int timeLeft;
        public UnityAction<int, int> EvGoodCraft { get; set; }
        public UnityAction EvTik { get; set; }

        public int GetIdRecipe() => idRecipe;
        public int TimeLeft { get => timeLeft; set => timeLeft = value; }
        public int GetIdStructure() => idStructure;

        public void Fill(int idRecipe, int idStructure, int time)
        {
            this.idRecipe = idRecipe;
            this.idStructure = idStructure;
            timeLeft = time;
        }

        public void Activate()
        {
            Tiker = GameStarter.container.Resolve<ITiker>();
            Tiker.StartTiker(1);
            Tiker.EvTickSec += Tik;
        }
        public void Deactivate()
        {
            if (Tiker != null)
            {
                Tiker.StopTiker();
                Tiker.EvTickSec -= Tik;
                Tiker = null;
            }
        }

        private void Tik()
        {
            timeLeft--;
            EvTik.Invoke();
            if (timeLeft <= 0)
            {
                EvGoodCraft?.Invoke(idStructure, idRecipe);
                Deactivate();
            }
        }

        public void SpeedUp()
        {
            timeLeft = 0;
            EvGoodCraft?.Invoke(idStructure, idRecipe);
            Deactivate();
        }

        public string Save()
        {
            return "" + idRecipe + '|' + idStructure + '|' + timeLeft;
        }

        public void Load(string str)
        {
            string[] load = str.Split('|');
            Fill(int.Parse(load[0]), int.Parse(load[1]), int.Parse(load[2]));
        }
    }
}