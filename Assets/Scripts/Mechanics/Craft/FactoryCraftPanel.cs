using TMPro;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public class FactoryCraftPanel : IFactoryCraftPanel, IDependent
    {
        [Dependency] public ICraftUI CraftUI { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IContainerAllResources AllRes { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public ITiker Timer { private get; set; }
        [Dependency] public IContainerRecipes ContainerRecipes { private get; set; }
        [Dependency] public IMyTime MyTime { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public ICraftBonusController CraftBonusController { private get; set; }

        public void OnInjected()
        {
            Timer.StartTiker(1);
        }

        public void SetHeader(string name)
        {
            CraftUI.Header.text = name;
        }

        private IRecipeUI[] recipeUIs;

        public void GenActiveRecipe(Recipe[] recipe, UnityAction<int> receive, UnityAction speedUp, UnityAction<int> cancel, UnityAction buy, UnityAction updatePanel)
        {
            Creator.ClearKids(CraftUI.ParentRecipeQueue);
            Timer.EvTickSec = null;
            bool activeCratf = false;
            int i = 0;
            for (; i < recipe.Length; i++)
            {

                ICurrentRecipeUI temp = Creator.GetGameObject(CraftUI.PrefabBoxCraft, CraftUI.ParentRecipeQueue).GetComponent<ICurrentRecipeUI>();
                //Пустой рецепт
                if (recipe[i] == null)
                {
                    SetCurrentRecipe(temp, "", "");
                    temp.Icon.gameObject.SetActive(false);
                    continue;
                }

                temp.Icon.sprite = AllRes.GetConfigRes(ContainerRecipes.GetRecipeSO(recipe[i].GetIdRecipe()).product.ID).sprite;

                int k = i;
                //Готовый рецепт
                if (recipe[i].TimeLeft <= 0)
                {
                    temp.FakeButtonGlow.gameObject.SetActive(true);
                    SetCurrentRecipe(temp, "", CraftUI.GetSSO.GetString(), () => receive(k));
                    continue;
                }
                //Активный рецепт
                if (!activeCratf)
                {
                    temp.FakeButton.gameObject.SetActive(true);
                    SetCurrentRecipe(temp, MyMethods.ToTimeHMS(recipe[i].TimeLeft, CraftUI.HMS.GetString()), CraftUI.SpeedUpSSO.GetString(), speedUp);
                    Timer.EvTickSec += () => UpdateTime(temp.Info, recipe[k], updatePanel);
                    activeCratf = true;
                    continue;
                }
                //В очереди рецепт
                temp.FakeButton.gameObject.SetActive(true);
                SetCurrentRecipe(temp, "", CraftUI.CanselSSO.GetString(), () => cancel(k));
            }

            if (i < CraftUI.MaxCountActiveRicepe)
            {
                Button temp = Creator.GetGameObject(CraftUI.PlusCrat, CraftUI.ParentRecipeQueue).AddComponent<Button>();
                temp.onClick.AddListener(buy);
            }
        }

        private void SetCurrentRecipe(ICurrentRecipeUI currentRecipe, string info, string button, UnityAction call = null)
        {
            currentRecipe.Info.text = info;
            currentRecipe.TextButton.text = button;
            if (call != null) currentRecipe.Button.onClick.AddListener(call);
        }

        private void UpdateTime(TMP_Text text, Recipe recipe, UnityAction updatePanel)
        {
            if (recipe.TimeLeft > 0)
            {
                text.text = MyMethods.ToTimeHMS(recipe.TimeLeft, CraftUI.HMS.GetString());
            }
        }

        public void GenRecipe(RecipeSO[] recipes, UnityAction<RecipeSO> call)
        {
            recipeUIs = new IRecipeUI[recipes.Length];
            CraftUI.CraftWindow.SetActive(true);
            ClearKids(CraftUI.ParentRecipe);
            for (int i = 0; i < recipes.Length; i++)
            {
                if (!CraftBonusController.CheckIfRecipeIsUnblocked(recipes[i].id)) continue;
                recipeUIs[i] = Creator.GetGameObject(CraftUI.PrefabRecipe, CraftUI.ParentRecipe).GetComponent<IRecipeUI>();
                recipeUIs[i].Product.sprite = AllRes.GetConfigRes(recipes[i].product.ID).sprite;
                recipeUIs[i].Count.text = recipes[i].product.count.ToString();
                recipeUIs[i].Time.text = MyMethods.ToTimeHMS(recipes[i].timeSec, CraftUI.HMS.GetString());
                recipeUIs[i].ContentSizeFitter.SetLayoutHorizontal();
                recipeUIs[i].CreateText.text = CraftUI.Create.GetString();
                int k = i;
                recipeUIs[i].StartCraft.onClick.AddListener(() => call(recipes[k]));

                for (int j = 0; j < recipes[i].needResours.Length; j++)
                {
                    GameObject temp = Creator.GetGameObject(recipeUIs[i].PrefabNeedRes, recipeUIs[i].ParentNeedRes);
                    TMP_Text text = temp.GetComponentInChildren<TMP_Text>();
                    Resource resource = recipes[i].needResours[j];
                    temp.transform.GetChild(0).GetComponent<Image>().sprite = AllRes.GetConfigRes(resource.ID).sprite;
                    SetNeedCountRes(text, resource);
                    WarehouseController.EvChangeRes += (x) => ChangeCountRes(x, text, resource);
                }
            }
            CraftUI.Close.onClick.AddListener(() =>
            {
                ClearKids(CraftUI.ParentRecipe);
                recipeUIs = null;
            });
        }

        public IRecipeUI GetIRecipeUI(RecipeSO RSO)
        {
            Sprite bee = AllRes.GetConfigRes(RSO.product.ID).sprite;

            for (int i = 0; i < recipeUIs.Length; i++)
            {
                if (recipeUIs[i] != null && recipeUIs[i].Product.sprite == bee && recipeUIs[i].Count.text == RSO.product.count.ToString())
                    return recipeUIs[i];
            }
            return null;
        }

        private void SetNeedCountRes(TMP_Text text, Resource resource)
        {
            int count = WarehouseController.LookOneRes(resource.ID);
            text.text =
                (count >= resource.count ?
                count.ToString() :
                MyMethods.PaintText(count.ToString(), CraftUI.LackOfResources))
                + "/" + resource.count;
        }

        private void ChangeCountRes(int call, TMP_Text text, Resource resource)
        {
            if (call == resource.ID)
            {
                SetNeedCountRes(text, resource);
            }
        }

        private void ClearKids(Transform tr)
        {
            Creator.ClearKids(tr);
        }

    }
}