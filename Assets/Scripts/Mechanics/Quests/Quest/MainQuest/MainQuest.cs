using System.Linq;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Quest/MainQuest", order = 1)]
    public class MainQuest : Quest
    {
        public StringsSO[] _questsToActivate;
        
        public int[] GetQuestsToActivate() => ShowQuestsIds();
        
        private int[] ShowQuestsIds()
        {
            return _questsToActivate.Select(quest => quest.GetID()).ToArray();
        }
        
#if UNITY_EDITOR
        public StringsSO[] ToActivate
        {
            get => _questsToActivate;
            set => _questsToActivate = value;
        }
#endif
    }
}