using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Quest/TechQuest", order = 1)]
    public class TechQuest : Quest
    {
        [SerializeField] private StringsSO _techQuestTree;
        
        public StringsSO TechQuestTree => _techQuestTree;
        
#if UNITY_EDITOR
        public StringsSO TechQuestTreeStringsSo
        {
            get => _techQuestTree;
            set => _techQuestTree = value;
        }
#endif
    }
}