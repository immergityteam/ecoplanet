using System;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Quest", order = 1)]
    public class Quest: ScriptableObject
    {
        [Header("Information for user")]
        [SerializeField] private StringsSO _label;
        [SerializeField] private StringsSO[] _description;
        [SerializeField] private Requirement[] _requirements;
        [SerializeField] private Reward[] _rewards;
        [SerializeField] private int _score;
        
#if UNITY_EDITOR
        // Для Quest Generator
        public void SetLabel(StringsSO label) => _label = label;
        public void SetDescription(StringsSO[] description) => _description = description;
        public void SetRequirements(Requirement[] requirements) => _requirements = requirements;
        public void SetRewards(Reward[] rewards) => _rewards = rewards;
        public void SetScore(int score) => _score = score;
#endif
        
        // Показать, что есть внутри
        public int GetId() => _label.GetID();
        public StringsSO GetLabel() => _label;
        public Requirement[] GetRequirements() => _requirements;
        public Reward[] GetRewards() => _rewards;
        public int GetScore() => _score;
        
        // Начать квест
        public void Begin(Action<int> action)
        {
            foreach (var requirement in _requirements)
            {
                requirement.SubscribeToEvent(action);
            }
        }
        
        // Завершить квест
        public void Complete(Action<int> action)
        {
            foreach (var requirement in _requirements)
            {
                requirement.UnsubscribeFromEvent(action);
            }
        }
        
        // Начислить награду
        public void AccrueReward()
        {
            foreach (var reward in _rewards)
            {
                reward.GiveReward();
            }
        }
        
        // Проверить возможность начислить награду
        public bool TryAccrueReward()
        {
            var possibility = true;
            
            foreach (var reward in _rewards)
            {
                possibility &= reward.ShowOpportunityToGiveAward();
            }
            
            return possibility;
        }
    }
}