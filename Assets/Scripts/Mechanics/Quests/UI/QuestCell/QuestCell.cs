using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class QuestCell : MonoBehaviour, IQuestCell
    {
        [field: SerializeField] public Toggle Toggle { get; set; }
        [field: SerializeField] public TMP_Text QuestName { get; set; }
        public StringsSO QuestNameStringSo { get; set; }
    }
}