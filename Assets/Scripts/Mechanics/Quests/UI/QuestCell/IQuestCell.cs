using TMPro;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IQuestCell
    {
        public Toggle Toggle { get; set; }
        public TMP_Text QuestName { get; set; }
        public StringsSO QuestNameStringSo { get; set; }
    }
}