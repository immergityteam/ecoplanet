using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class RequirementCell : MonoBehaviour, IRequirementCell
    {
        [field: SerializeField] public Image Icon { get; set; }
        [field: SerializeField] public TMP_Text Progress { get; set; }
        [field: SerializeField] public TMP_Text Description { get; set; }
    }
}