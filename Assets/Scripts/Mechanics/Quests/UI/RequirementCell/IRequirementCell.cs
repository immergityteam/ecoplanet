using TMPro;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IRequirementCell
    {
        public Image Icon { get; set; }
        public TMP_Text Progress { get; set; }
        public TMP_Text Description { get; set; }
    }
}