using System;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public class QuestPanel : IQuestPanel, IDependent
    {
        [Dependency] public IQuestUI QuestUI { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public IContainerAllResources ContainerAllResources { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        
        public void OnInjected()
        {
            SetHeaders();
            GameUI.Quest.onClick.AddListener(SetHeaders);
        }
        
        public void MakeGameUISubscription(UnityAction refreshPanel)
        {
            GameUI.Quest.onClick.AddListener(refreshPanel);
        }
        
        // Установить текст
        public void SetHeaders()
        {
            QuestUI.MainQuestButtonText.text = QuestUI.QuestLabelSO.GetString();
            QuestUI.QuestLabel.text = QuestUI.MainQuestButtonTextSO.GetString();
            QuestUI.TechQuestButtonText.text = QuestUI.TechQuestButtonTextSO.GetString();
            QuestUI.RewardLabel.text = QuestUI.RewardLabelSO.GetString();
            QuestUI.AwardReceivingButtonText.text = QuestUI.AwardReceivingButtonTextSO.GetString();
        }
        
        public void ToggleMainQuests()
        {
            QuestUI.MainQuests.OnSubmit(null);
        }
        
        public void ToggleTechQuests()
        {
            QuestUI.TechQuests.OnSubmit(null);
        }
        
        // Подписки для переключателей
        public void GeneratePanel(Action<bool> selectMainQuests, Action<bool> selectTechQuests, UnityAction selectAwardReceiving)
        { 
            if (selectMainQuests != null) QuestUI.MainQuests.onValueChanged.AddListener((flag) => selectMainQuests(flag));
            if (selectTechQuests != null) QuestUI.TechQuests.onValueChanged.AddListener((flag) => selectTechQuests(flag));
            if (selectAwardReceiving != null) QuestUI.AwardReceivingButton.onClick.AddListener(selectAwardReceiving);
        }
        
        // Обновить quest scroll
        public void RefreshQuestScroll(Action<bool, StringsSO> changeQuest, QuestController.QuestToView[] questToViews)
        {
            ClearKids(QuestUI.QuestsScroll);
            IQuestCell questCellFirst = null;
            for (int i = 0; i < questToViews.Length; i++)
            {
                var questCell = Creator.GetGameObject(QuestUI.QuestCellTemplate, QuestUI.QuestsScroll).GetComponent<IQuestCell>();
                questCell.QuestName.text = questToViews[i].Label.GetString();
                questCell.QuestNameStringSo = questToViews[i].Label;
                questCell.Toggle.group = QuestUI.QuestsScroll.gameObject.GetComponent<ToggleGroup>();
                int k = i;
                if (changeQuest != null) questCell.Toggle.onValueChanged.AddListener((flag) => changeQuest(flag, questToViews[k].Label));
                if (i == 0) questCellFirst = questCell;
            }

            questCellFirst?.Toggle.OnSubmit(null);
        }
        
        // Обновить requirements scroll
        public void RefreshRequirements(QuestController.RequirementToView[] requirementsToView)
        {
            ClearKids(QuestUI.RequirementScroll);
            
            foreach (var requirement in requirementsToView)
            {
                var requirementCell = Creator.GetGameObject(QuestUI.RequirementCellTemplate, QuestUI.RequirementScroll).GetComponent<IRequirementCell>();
                
                requirementCell.Icon.sprite = GetSprite(requirement.IdForSprite);
                requirementCell.Progress.text = requirement.CurrentValue + " / " + requirement.NeedToReceive;
                requirementCell.Description.text = requirement.Name.GetString();
            }
        }

        private Sprite GetSprite(int id)
        {
            Sprite temp = ContainerAllResources.GetConfigRes(id).sprite;
            if (temp.name == "coin")
            {
                temp = BuilderPlayer.GetStructureDataGlobal(id) != null
                    ? BuilderPlayer.GetStructureDataGlobal(id).sprite
                    : temp;
            }
            
            return temp;
        }
        
        // Обновить награды
        public void RefreshRewards(QuestController.RewardToView[] rewardToViews)
        {
            QuestUI.RewardLabel.gameObject.SetActive(true);

            if (rewardToViews != null)
            {
                QuestUI.TechQuestAward.gameObject.SetActive(false);
                
                switch (rewardToViews.Length)
                {
                    case 1:
                        QuestUI.FirstRewardImage.gameObject.SetActive(true);
                        QuestUI.FirstRewardText.gameObject.SetActive(true);
                        QuestUI.SecondRewardImage.gameObject.SetActive(true);
                        QuestUI.SecondRewardText.gameObject.SetActive(true);
                        
                        QuestUI.FirstRewardImage.color = new Color(1, 1, 1, 1);
                        QuestUI.FirstRewardImage.sprite = GetSprite(rewardToViews[0].IdForSprite);
                        QuestUI.FirstRewardText.text = rewardToViews[0].Growth.ToString();
                
                        QuestUI.SecondRewardImage.color = new Color(1, 1, 1, 0);
                        QuestUI.SecondRewardText.text = " ";
                    
                        break;
                    case 2:
                        QuestUI.FirstRewardImage.gameObject.SetActive(true);
                        QuestUI.FirstRewardText.gameObject.SetActive(true);
                        QuestUI.SecondRewardImage.gameObject.SetActive(true);
                        QuestUI.SecondRewardText.gameObject.SetActive(true);
                        
                        QuestUI.FirstRewardImage.color = new Color(1, 1, 1, 1);
                        QuestUI.FirstRewardImage.sprite = GetSprite(rewardToViews[0].IdForSprite);
                        QuestUI.FirstRewardText.text = rewardToViews[0].Growth.ToString();
                
                        QuestUI.SecondRewardImage.color = new Color(1, 1, 1, 1);
                        QuestUI.SecondRewardImage.sprite = GetSprite(rewardToViews[1].IdForSprite);
                        QuestUI.SecondRewardText.text = rewardToViews[1].Growth.ToString();
                    
                        break;
                }
            }
            else // Технологические квесты
            {
                QuestUI.TechQuestAward.gameObject.SetActive(true);
                QuestUI.TechQuestAward.text = QuestUI.TechQuestReward.GetString();
                
                QuestUI.FirstRewardImage.gameObject.SetActive(false);
                QuestUI.FirstRewardText.gameObject.SetActive(false);
                
                QuestUI.SecondRewardImage.gameObject.SetActive(false);
                QuestUI.SecondRewardText.gameObject.SetActive(false);
            }
        }
        
        // Включить или выключить кнопку получения награды
        public void ChangeActivationButtonState(bool state)
        {
            if (state == true)
            {
                QuestUI.AwardReceivingButtonText.text = QuestUI.AwardReceivingButtonTextSO.GetString();
                QuestUI.AwardReceivingButton.gameObject.SetActive(true);
                
                QuestUI.AwardReceivingButton.enabled = true;
            }
            else
            {
                QuestUI.AwardReceivingButtonText.text = QuestUI.AwardReceivingButtonTextSO.GetString();
                QuestUI.AwardReceivingButton.gameObject.SetActive(false);
            }
        }
        
        // Включить или выключить панель квестов
        public void ChangePanelState(bool state)
        {
            QuestUI.QuestPanel.SetActive(state);
        }
        
        // Посмотреть какие квесты активны
        public bool CheckMainsQuestsAreActive()
        {
            return QuestUI.MainQuests.isOn;
        }
        
        public void ChangeCurrentToggle(StringsSO questName)
        {
            var quests = QuestUI.QuestsScroll.gameObject.GetComponentsInChildren<QuestCell>();
            
            foreach (var quest in quests)
            {
                if (quest.QuestNameStringSo == questName)
                    quest.Toggle.OnSubmit(null);
            }
        }
        
        public void ClearEntirePanel()
        {
            ClearKids(QuestUI.QuestsScroll);
            ClearKids(QuestUI.RequirementScroll);
            
            QuestUI.RewardLabel.gameObject.SetActive(false);
            QuestUI.TechQuestAward.gameObject.SetActive(false);
            
            QuestUI.FirstRewardImage.color = new Color(1, 1, 1, 0);
            QuestUI.FirstRewardText.text = " ";
            
            QuestUI.SecondRewardImage.color = new Color(1, 1, 1, 0);
            QuestUI.SecondRewardText.text = " ";
            
            QuestUI.AwardReceivingButton.gameObject.SetActive(false);
        }
        
        private void ClearKids(Transform transform)
        {
            Creator.ClearKids(transform);
        }
    }
}