using System;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IQuestPanel
    {
        public void GeneratePanel(
            Action<bool> selectedMainQuests = null,
            Action<bool> selectedTechQuests = null,
            UnityAction selectedAwardReceiving = null);
        public void RefreshQuestScroll(Action<bool, StringsSO> changeQuest, QuestController.QuestToView[] questToViews);
        public void RefreshRequirements(QuestController.RequirementToView[] requirementsToView);
        public void RefreshRewards(QuestController.RewardToView[] rewardToViews);
        public void ChangeActivationButtonState(bool state);
        public void ChangePanelState(bool state);
        public bool CheckMainsQuestsAreActive();
        public void MakeGameUISubscription(UnityAction refreshPanel);
        public void ChangeCurrentToggle(StringsSO questName);
        public void ClearEntirePanel();
        public void ToggleMainQuests();
        public void ToggleTechQuests();
        public void SetHeaders();
    }
}