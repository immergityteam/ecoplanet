using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class QuestUI : MonoBehaviour, IQuestUI
    {
        // Панель

        [field: SerializeField] public GameObject QuestPanel { get; set; }
        [field: SerializeField] public Button Close { get; set; }


        // Шаблоны
        [field: SerializeField] public GameObject QuestCellTemplate { get; set; }
        [field: SerializeField] public GameObject RequirementCellTemplate { get; set; }
        
        // Заголовок
        [field: SerializeField] public TMP_Text QuestLabel { get; set; }
        
        // Переключение между основными и технологическими квестами
        
        // Главные квесты
        [field: SerializeField] public Toggle MainQuests { get; set; }
        [field: SerializeField] public TMP_Text MainQuestButtonText { get; set; }
        
        // Технологические квесты
        [field: SerializeField] public Toggle TechQuests { get; set; }
        [field: SerializeField] public TMP_Text TechQuestButtonText { get; set; }
        
        // Список квестов
        [field: SerializeField] public Transform QuestsScroll { get; set; }
        
        // Список Требований
        [field: SerializeField] public Transform RequirementScroll { get; set; }
        
        // Награды
        [field: SerializeField] public TMP_Text RewardLabel { get; set; }
        
        [field: SerializeField] public Image FirstRewardImage { get; set; }
        [field: SerializeField] public TMP_Text FirstRewardText { get; set; }
        
        [field: SerializeField] public Image SecondRewardImage { get; set; }
        [field: SerializeField] public TMP_Text SecondRewardText { get; set; }
        
        // Принятие награды
        [field: SerializeField] public Button AwardReceivingButton { get; set; }
        [field: SerializeField] public TMP_Text AwardReceivingButtonText { get; set; }
        [field: SerializeField] public TMP_Text TechQuestAward { get; set; }

        // Scriptable objects
        [field: SerializeField] public StringsSO QuestLabelSO { get; set; }
        [field: SerializeField] public StringsSO MainQuestButtonTextSO { get; set; }
        [field: SerializeField] public StringsSO TechQuestButtonTextSO { get; set; }
        [field: SerializeField] public StringsSO RewardLabelSO { get; set; }
        [field: SerializeField] public StringsSO AwardReceivingButtonTextSO { get; set; }
        [field: SerializeField] public StringsSO TechQuestReward { get; set; }
    }
}