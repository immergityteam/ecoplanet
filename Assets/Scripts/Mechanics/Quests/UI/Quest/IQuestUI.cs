using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IQuestUI
    {
        // Панель
        public GameObject QuestPanel { get; set; }
        Button Close { get; set; }

        // Шаблоны
        public GameObject QuestCellTemplate { get; set; }
        public GameObject RequirementCellTemplate { get; set; }
        
        // Заголовок
        public TMP_Text QuestLabel { get; set; }
        
        // Переключение между основными и технологическими квестами
        public Toggle MainQuests { get; set; }
        public TMP_Text MainQuestButtonText { get; set; }
        public Toggle TechQuests { get; set; }
        public TMP_Text TechQuestButtonText { get; set; }
        
        // Список квестов
        public Transform QuestsScroll { get; set; }
        
        // Список Требований
        public Transform RequirementScroll { get; set; }
        
        // Награды
        public TMP_Text RewardLabel { get; set; }
        
        public Image FirstRewardImage { get; set; }
        public TMP_Text FirstRewardText { get; set; }
        
        public Image SecondRewardImage { get; set; }
        public TMP_Text SecondRewardText { get; set; }
        
        public Button AwardReceivingButton { get; set; }
        public TMP_Text AwardReceivingButtonText { get; set; }
        public TMP_Text TechQuestAward { get; set; }
        
        // Scriptable objects
        public StringsSO QuestLabelSO { get; set; }
        public StringsSO MainQuestButtonTextSO { get; set; }
        public StringsSO TechQuestButtonTextSO { get; set; }
        public StringsSO RewardLabelSO { get; set; }
        public StringsSO AwardReceivingButtonTextSO { get; set; }
        public StringsSO TechQuestReward { get; set; }
    }
}