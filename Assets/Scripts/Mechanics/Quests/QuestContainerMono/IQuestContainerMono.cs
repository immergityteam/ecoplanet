namespace ColLobOK
{
    public interface IQuestContainerMono
    {
        MainQuest[] MainQuests { get; }
        TechQuest[] TechQuests { get; }
        InhabitantsQuest[] InhabitantsQuests { get; }
    }
}