using UnityEngine;
using Developing;

namespace ColLobOK
{
    public class QuestContainerMono : MonoBehaviour, IQuestContainerMono
    {
        [field: SerializeField] public MainQuest[] MainQuests { get; private set; }
        [field: SerializeField] public TechQuest[] TechQuests { get; private set; }
        [field: SerializeField] public InhabitantsQuest[] InhabitantsQuests { get; private set; }
        
#if UNITY_EDITOR
        [ContextMenu("GetAllQuests")]
        private void GetAllQuests()
        {
            MainQuests =  MetodForEditor.GetAllAssets<MainQuest>();
            TechQuests =  MetodForEditor.GetAllAssets<TechQuest>();
            InhabitantsQuests = MetodForEditor.GetAllAssets<InhabitantsQuest>();
        }
        
        [ContextMenu("GetAllMainQuests")]
        private void GetAllMainQuests()
        {
            MainQuests =  MetodForEditor.GetAllAssets<MainQuest>();
        }
        
        [ContextMenu("GetAllTechQuests")]
        private void GetAllClassificationEnhancementQuests()
        {
            TechQuests =  MetodForEditor.GetAllAssets<TechQuest>();
        }
        
        [ContextMenu("GetAllInhabitantsQuests")]
        private void GetAllInhabitantsQuests()
        {
            InhabitantsQuests =  MetodForEditor.GetAllAssets<InhabitantsQuest>();
        }
#endif
    }
}