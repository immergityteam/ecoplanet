using UnityEngine.Events;

namespace ColLobOK
{
    public interface IQuestController
    { 
        UnityAction<int> QuestBegun { get; set; }
        UnityAction<int> QuestEnded { get; set; }
        UnityAction<StringsSO> TechQuestEnded { get; set; }
        
        void StartTechQuest(TechQuest techQuest);
    }
}