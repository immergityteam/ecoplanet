using System;
using System.Linq;
using System.Collections.Generic;
using ColLobOK.Mechanics.Tutorial.Interface;
using UnityEngine;
using UnityEngine.Events;
using UnityDI;

namespace ColLobOK
{
    public class QuestController: IQuestController, IDependent
    {
        [Dependency] public IQuestPanel QuestPanel { get; set; }
        [Dependency] public IQuestContainer QuestContainer { get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { get; set; }
        
        // Квесты из Mono
        private List<MainQuest> _mainQuests;
        private List<TechQuest> _techQuests;

        // Квесты для отображения
        private List<QuestToView> _mainQuestsToDisplay;
        private List<QuestToView> _techQuestsToDisplay;
        
        // Текущие квесты для отображения
        private int _currentMainQuestId;
        private int _currentTechQuestId;
        
        // Выполенные квесты
        private List<int> _completedQuests;
        
        // Сохранение данных
        private QuestData _questData;
        
        // Сообытия
        public UnityAction<int> QuestBegun { get; set; }
        public UnityAction<int> QuestEnded { get; set; }
        public UnityAction<StringsSO> TechQuestEnded { get; set; }
        
        // ID первого квеста
        private int _firstQuestID;
        
        public void OnInjected()
        {
            // Главные квесты
            _mainQuests = new List<MainQuest>(QuestContainer.MainQuests);
            _mainQuestsToDisplay = new List<QuestToView>();
            
            _firstQuestID = _mainQuests[0].GetId();
            
            // Технологические квесты
            _techQuests = new List<TechQuest>(QuestContainer.TechQuests);
            _techQuestsToDisplay = new List<QuestToView>();
            
            _questData = new QuestData();
            _questData.PerformInitialization();
            
            SaveAndLoad.EvSave += Save;
            Load();
            
            _completedQuests = new List<int>();
            
            FindMainQuestsAtStart();
            TryFindTechQuestsAtStart();
            
            QuestPanel.GeneratePanel(SelectMainQuests,  SelectTechQuests, TryGetReward);
            QuestPanel.MakeGameUISubscription(RefreshPanel);
        }
        
        #region MainQuests
        
        private void FindMainQuestsAtStart()
        {
            var results = _questData.ShowInsides();
            
            if (results == 0)
            {
                var firstMainQuest = _mainQuests[0];
                var zeroProgress = firstMainQuest.GetRequirements().Select(requirement => 0).ToArray();
                
                var questToView = new QuestToView(
                    firstMainQuest.GetLabel(),
                    PrepareRequirements(firstMainQuest.GetRequirements(), zeroProgress),
                    PrepareRewardsToView(firstMainQuest.GetRewards()),
                    _questData);
                
                questToView.Completed += CompleteMainQuest;
                
                _mainQuestsToDisplay.Add(questToView);
                _questData.AddNewQuest(firstMainQuest.GetId(), zeroProgress.Length);
                firstMainQuest.Begin(questToView.ChangeValuesInRequirements);
                
                _currentMainQuestId = questToView.Id;
                
                QuestBegun?.Invoke(questToView.Id);
            }
            else
            {
                if (_questData.ReturnUniqueQuests().Length > 1)
                {
                    _questData.RemoveQuest(_firstQuestID);
                }
                
                var uniqueIds = _questData.ReturnUniqueQuests();
                
                foreach (var uniqueId in uniqueIds)
                {
                    var isThereQuest = TryFindMainQuestById(uniqueId);

                    if (isThereQuest)
                    {
                        var quest = _mainQuests[FindMainQuestById(uniqueId)];
                        var requirementProgress = _questData.ShowRequirementsProgress(uniqueId);
                    
                        var questToView = new QuestToView(
                            quest.GetLabel(),
                            PrepareRequirements(quest.GetRequirements(), requirementProgress),
                            PrepareRewardsToView(quest.GetRewards()),
                            _questData);
                    
                        _mainQuestsToDisplay.Add(questToView);
                    
                        questToView.Completed += CompleteMainQuest;
                        questToView.CheckAllRequirements();
                    
                        quest.Begin(questToView.ChangeValuesInRequirements);
                    
                        _currentMainQuestId = questToView.Id;
                    
                        QuestBegun?.Invoke(questToView.Id); 
                    }
                }
            }
        }
        private void FindMainQuestsAfterStart(int[] questsToActivate)
        {
            foreach (var questToActivate in questsToActivate)
            {
                foreach (var unfulfilledMainQuest in _mainQuests.Where(unfulfilledMainQuest => unfulfilledMainQuest.GetId() == questToActivate))
                {
                    var zeroProgress = unfulfilledMainQuest.GetRequirements().Select(requirement => 0).ToArray();
                    
                    var newMainQuest = new QuestToView(
                        unfulfilledMainQuest.GetLabel(),
                        PrepareRequirements(unfulfilledMainQuest.GetRequirements(), zeroProgress),
                        PrepareRewardsToView(unfulfilledMainQuest.GetRewards()),
                        _questData);
                    
                    _mainQuestsToDisplay.Add(newMainQuest);
                    
                    newMainQuest.Completed += CompleteMainQuest;
                    _questData.AddNewQuest(unfulfilledMainQuest.GetId(), unfulfilledMainQuest.GetRequirements().Length);
                    
                    unfulfilledMainQuest.Begin(newMainQuest.ChangeValuesInRequirements);
                    
                    _currentMainQuestId = newMainQuest.Id;
                    
                    QuestBegun?.Invoke(newMainQuest.Id);
                }
            }
        }
        private void CompleteMainQuest(int questId)
        {
            _completedQuests.Add(questId);
            
            QuestPanel.SetHeaders();
            QuestPanel.ChangePanelState(true);
            QuestPanel.ToggleMainQuests();
            
            QuestPanel.RefreshQuestScroll(ChangeCurrentMainQuest, _mainQuestsToDisplay.ToArray());
            QuestPanel.ChangeCurrentToggle(FindStringSOById(questId, _mainQuestsToDisplay));
        }
        
        #endregion
        
        #region TechQuests
        
        public void StartTechQuest(TechQuest techQuest)
        {
            var zeroProgress = techQuest.GetRequirements().Select(requirement => 0).ToArray();
            
            var questToView = new QuestToView(
                techQuest.GetLabel(),
                PrepareRequirements(techQuest.GetRequirements(), zeroProgress),
                _questData);
            
            questToView.Completed += CompleteTechQuest;
            
            _techQuestsToDisplay.Add(questToView);
            _questData.AddNewQuest(techQuest.GetId(), zeroProgress.Length);
            techQuest.Begin(questToView.ChangeValuesInRequirements);
            
            _currentTechQuestId = questToView.Id;
            
            QuestBegun?.Invoke(questToView.Id);
        }
        
        private void TryFindTechQuestsAtStart()
        {
            var uniqueIds = _questData.ReturnUniqueQuests();
            
            foreach (var uniqueId in uniqueIds)
            {
                var isThereQuest = TryFindTechQuestById(uniqueId);
                
                if (isThereQuest)
                {
                    var quest = _techQuests[FindTechQuestById(uniqueId)];
                    var requirementProgress = _questData.ShowRequirementsProgress(uniqueId);
                    
                    var questToView = new QuestToView(
                        quest.GetLabel(),
                        PrepareRequirements(quest.GetRequirements(), requirementProgress),
                        _questData);
                    
                    _techQuestsToDisplay.Add(questToView);
                    
                    questToView.Completed += CompleteTechQuest;
                    questToView.CheckAllRequirements();
                    
                    quest.Begin(questToView.ChangeValuesInRequirements);
                    
                    _currentTechQuestId = questToView.Id;
                    
                    QuestBegun?.Invoke(questToView.Id);
                }
            }
        }
        private void CompleteTechQuest(int questId)
        {
            _completedQuests.Add(questId);
            
            QuestPanel.SetHeaders();
            QuestPanel.ChangePanelState(true);
            QuestPanel.ToggleTechQuests();
            
            QuestPanel.RefreshQuestScroll(ChangeCurrentTechQuest, _techQuestsToDisplay.ToArray());
            QuestPanel.ChangeCurrentToggle(FindStringSOById(questId, _techQuestsToDisplay));
        }
        
        #endregion
        
        #region Refreshing
        
        // Main quests
        private void RefreshAllMainQuests()
        {
            QuestPanel.RefreshQuestScroll(ChangeCurrentMainQuest, _mainQuestsToDisplay.ToArray());
            QuestPanel.ChangeCurrentToggle(FindStringSOById(_currentMainQuestId, _mainQuestsToDisplay));
        }
        private void RefreshMainQuest(QuestToView questToView)
        {
            var completion = CheckQuestState(_completedQuests, questToView.Id);
            QuestPanel.RefreshRequirements(questToView.RequirementsToView);
            QuestPanel.RefreshRewards(questToView.RewardsToView);
            
            if (completion)
                QuestPanel.ChangeActivationButtonState(true);
            else
                QuestPanel.ChangeActivationButtonState(false);
            
        }
        private void ChangeCurrentMainQuest(bool flag, StringsSO currentMainQuestStringSo)
        {
            if (flag)
            {
                _currentMainQuestId = currentMainQuestStringSo.GetID();
                RefreshMainQuest(FindQuestInQuestsToView(_mainQuestsToDisplay, _currentMainQuestId));
            }
        }
        
        // Tech quests
        private void RefreshAllTechQuests()
        {
            QuestPanel.RefreshQuestScroll(ChangeCurrentTechQuest, _techQuestsToDisplay.ToArray());
            QuestPanel.ChangeCurrentToggle(FindStringSOById(_currentTechQuestId, _techQuestsToDisplay));
        }
        private void RefreshTechQuest(QuestToView questToView)
        {
            var completion = CheckQuestState(_completedQuests, questToView.Id);
            
            QuestPanel.RefreshRequirements(questToView.RequirementsToView);
            QuestPanel.RefreshRewards(questToView.RewardsToView);
            
            if (completion)
                QuestPanel.ChangeActivationButtonState(true);
            else
                QuestPanel.ChangeActivationButtonState(false);
            
        }
        private void ChangeCurrentTechQuest(bool flag, StringsSO currentTechQuestStringSo)
        {
            if (flag)
            {
                _currentTechQuestId = currentTechQuestStringSo.GetID();
                RefreshTechQuest(FindQuestInQuestsToView(_techQuestsToDisplay, _currentTechQuestId));
            }
        }
        
        #endregion
        
        #region Buttons
        
        private void SelectMainQuests(bool flag)
        {
            if (flag)
            {
                if (_mainQuestsToDisplay.Count > 0)
                    RefreshAllMainQuests();
                else
                    QuestPanel.ClearEntirePanel();
            }
        }
        private void SelectTechQuests(bool flag)
        {
            if (flag)
            {
                if (_techQuestsToDisplay.Count > 0)
                    RefreshAllTechQuests();
                else
                    QuestPanel.ClearEntirePanel();     
            }
        }
        private void RefreshPanel()
        {
            QuestPanel.ChangePanelState(true);
            
            if (QuestPanel.CheckMainsQuestsAreActive())
                RefreshAllMainQuests();
            else
            {
                RefreshAllTechQuests();
            }
                
        }
        private void TryGetReward()
        {
            if (QuestPanel.CheckMainsQuestsAreActive())
            {
                var mainQuestMono = FindQuestInMainQuests(_currentMainQuestId);
                
                if (mainQuestMono.TryAccrueReward())
                {
                    var mainQuestToView = FindQuestInQuestsToView(_mainQuestsToDisplay, _currentMainQuestId);
                    
                    mainQuestMono.Complete(mainQuestToView.ChangeValuesInRequirements);
                    mainQuestMono.AccrueReward();
                    QuestEnded?.Invoke(_currentMainQuestId);
                    
                    _questData.RemoveQuest(_currentMainQuestId);

                    FindMainQuestsAfterStart(mainQuestMono.GetQuestsToActivate());
                    
                    _mainQuestsToDisplay.Remove(mainQuestToView);
                    
                    SelectMainQuests(true);
                    
                    
                    QuestPanel.ChangePanelState(false);
                    MessageSystemController.RefreshRewardWindow(mainQuestToView.Label.GetString());
                }
                else
                    Debug.Log("Не получилось получить награду");
            }
            else
            {
                var techQuestMono = FindQuestInTechQuests(_currentTechQuestId);
                if (techQuestMono.TryAccrueReward())
                {
                    var techQuestToView = FindQuestInQuestsToView(_techQuestsToDisplay, _currentTechQuestId);
                    
                    techQuestMono.Complete(techQuestToView.ChangeValuesInRequirements);
                    
                    _questData.RemoveQuest(_currentMainQuestId);
                    _techQuestsToDisplay.Remove(techQuestToView);
                    
                    SelectTechQuests(true);
                    
                    QuestEnded?.Invoke(_currentMainQuestId);
                    TechQuestEnded?.Invoke(techQuestMono.TechQuestTree);
                    
                    QuestPanel.ChangePanelState(false);
                    MessageSystemController.RefreshRewardWindow(techQuestToView.Label.GetString());
                }
                else
                    Debug.Log("Не получилось получить награду");
            }
        }
        
        #endregion
        
        #region Methods - helpers
        
        private QuestToView FindQuestInQuestsToView(List<QuestToView> questsToView, int idQuestToView)
        {
            var questToView = new QuestToView();
            
            foreach (var quest in questsToView.Where(quest => quest.Id == idQuestToView))
            {
                questToView = quest;
            }
            
            return questToView;
        }
        private MainQuest FindQuestInMainQuests(int mainQuestId)
        {
            var mainQuest = ScriptableObject.CreateInstance<MainQuest>();
            
            foreach (var quest in _mainQuests.Where(quest => quest.GetId() == mainQuestId))
            {
                mainQuest = quest;
            }
            
            return mainQuest;
        }
        private TechQuest FindQuestInTechQuests(int techQuestId)
        {
            var techQuest = ScriptableObject.CreateInstance<TechQuest>();
            
            foreach (var quest in _techQuests.Where(quest => quest.GetId() == techQuestId))
            {
                techQuest = quest;
            }
            
            return techQuest;
        }
        private bool CheckQuestState(List<int> completedQuests, int id)
        {
            var completion = false;
            
            foreach (var completedQuest in completedQuests.Where(completedQuest => completedQuest == id))
            {
                completion = true;
            }
            
            return completion;
        }
        
        private StringsSO FindStringSOById(int currentQuestID, List<QuestToView> questsToView)
        {
            var questLabel = ScriptableObject.CreateInstance<StringsSO>();
            
            foreach (var quest in questsToView)
            {
                if (quest.Id == currentQuestID)
                    questLabel = quest.Label;
            }
            
            return questLabel;
        }
        
        #endregion
        
        #region OtherMethods
        
        private RequirementToView[] PrepareRequirements(Requirement[] requirements, int[] currentValue)
        {
            var requirementsToView = new List<RequirementToView>();
            
            for (var index = 0; index < requirements.Length; index++)
            {
                var requirement = requirements[index];
                var (id, name, needToReceive) = requirement.ShowAllInformation();
                
                var requirementToView = new RequirementToView(
                    id,
                    name,
                    currentValue[index],
                    needToReceive);

                requirementsToView.Add(requirementToView);
            }
            
            return requirementsToView.ToArray();
        }
        private RewardToView[] PrepareRewardsToView(Reward[] rewards)
        {
            return rewards.Select(reward => new RewardToView(reward.Id, reward.Count)).ToArray();
        }
        private bool TryFindMainQuestById(int questId)
        {
            var result = false;
            
            foreach (var resultQuest in _mainQuests.Where(resultQuest => resultQuest.GetId() == questId))
            {
                result = true;
            }
            
            return result;
        }
        private int FindMainQuestById(int questId)
        {
            var resultQuestId = 0;
            
            for (var index = 0; index < _mainQuests.Count; index++)
            {
                var resultQuest = _mainQuests[index];
                
                if (resultQuest.GetId() == questId)
                    resultQuestId = index;
            }
            
            return resultQuestId;
        }
        private bool TryFindTechQuestById(int questId)
        {
            var result = false;
            
            foreach (var resultQuest in _techQuests.Where(resultQuest => resultQuest.GetId() == questId))
            {
                result = true;
            }
            
            return result;
        }
        private int FindTechQuestById(int questId)
        {
            var resultQuestId = 0;
            
            for (var index = 0; index < _mainQuests.Count; index++)
            {
                var resultQuest = _mainQuests[index];
                
                if (resultQuest.GetId() == questId)
                    resultQuestId = index;
            }
            
            return resultQuestId;
        }
        
        #endregion
        
        #region DataAcceptance
        
        public class QuestToView
        {
            // Поля
            public int Id { get; private set; }
            public StringsSO Label { get; private set; }
            public RequirementToView[] RequirementsToView { get; private set; }
            public RewardToView[] RewardsToView { get; private set; }
            public event UnityAction<int> Completed;
            private int _requirementsFulfilled;
            private QuestData _questData;
            
            // Конструкторы
            public QuestToView()
            {
                
            }
            public QuestToView(StringsSO label, RequirementToView[] requirementsToView, RewardToView[] rewardsToView, QuestData questData)
            {
                Id = label.GetID();
                Label = label;
                RequirementsToView = requirementsToView;
                RewardsToView = rewardsToView;
                _questData = questData;
                SubscribeToRequirements();
            }
            public QuestToView(StringsSO label, RequirementToView[] requirementsToView, QuestData questData)
            {
                Id = label.GetID();
                Label = label;
                RequirementsToView = requirementsToView;
                _questData = questData;
                SubscribeToRequirements();
            }
            
            // Метод для QuestSO
            public void ChangeValuesInRequirements(int requirementId)
            {
                for (var index = 0; index < RequirementsToView.Length; index++)
                {
                    var requirementToView = RequirementsToView[index];
                    if (requirementToView.IdForSprite == requirementId)
                    {
                        requirementToView.IncreaseCounter();
                        _questData.IncreaseProgress(Id, index, requirementToView.CurrentValue);
                    }
                }
            }
            
            public void CheckAllRequirements()
            {
                foreach (var requirement in RequirementsToView)
                {
                    requirement.MakeCompletenessCheck();
                }
            }
            
            // Методы
            private void SubscribeToRequirements()
            {
                foreach (var requirementToView in RequirementsToView)
                {
                    requirementToView.Fulfilled += CallAboutCompletion;
                }
            }
            private void UnsubscribeToRequirements()
            {
                foreach (var requirementToView in RequirementsToView)
                {
                    requirementToView.Fulfilled -= CallAboutCompletion;
                }
            }
            private void CallAboutCompletion()
            {
                _requirementsFulfilled++;
                if (_requirementsFulfilled == RequirementsToView.Length)
                {
                    Completed?.Invoke(Id);
                    UnsubscribeToRequirements();
                }
            }
        }
        
        public class RequirementToView
        {
            // Поля
            public int IdForSprite { get; private set; }
            public StringsSO Name { get; private set; }
            public int CurrentValue { get; private set; }
            public int NeedToReceive { get; private set; }
            
            // Событие
            public event UnityAction Fulfilled;
            
            // Конструкторы
            public RequirementToView(int idForSprite, StringsSO name, int currentValue, int needToReceive)
            {
                IdForSprite = idForSprite;
                Name = name;
                CurrentValue = currentValue;
                NeedToReceive = needToReceive;
            }
            
            // Методы
            public void IncreaseCounter()
            {
                CurrentValue++;
                if (CurrentValue == NeedToReceive)
                    Fulfilled?.Invoke();
            }
            
            public void MakeCompletenessCheck()
            {
                if (CurrentValue >= NeedToReceive)
                    Fulfilled?.Invoke();
            }
        }
        
        public class RewardToView
        {
            // Поля
            public int IdForSprite { get; private set; }
            public int Growth { get; private set; }
            
            // Конструкторы
            public RewardToView(int idForSprite, int growth)
            {
                IdForSprite = idForSprite;
                Growth = growth;
            }
        }
        
        #endregion
        
        #region SaveAndLoad
        
        readonly ConvertDataBinary<QuestData> saveData = new ConvertDataBinary<QuestData>("QuestContainer");
        
        private void Save()
        {
            SaveAndLoad.Save(saveData.name, saveData.ToConvert(_questData));
        }
        
        private void Load()
        {
            var str = SaveAndLoad.Load(saveData.name);
            if (str != "") _questData = saveData.FromConvert(str);
        }
        
        [Serializable]
        public struct QuestData
        {
            private List<(int id, int requirementIndex, int progress)> _questsToView;

            public void PerformInitialization()
            {
                _questsToView = new List<(int id, int requirementIndex, int progress)>();
            }

            public int ShowInsides()
            {
                return _questsToView.Count;
            }
            
            public void AddNewQuest(int id, int requirementsCount)
            {
                for (var i = 0; i < requirementsCount; i++)
                {
                    _questsToView.Add((id, i, 0));
                }
            }
            
            public void RemoveQuest(int id)
            {
                for (var i = _questsToView.Count - 1; i >= 0; i--)
                {
                    if (_questsToView[i].id == id)
                    {
                        _questsToView.Remove(_questsToView[i]);
                    }
                }
            }
            
            private void RemoveQuest(int id, int requirementNumber)
            {
                for (var i = _questsToView.Count - 1; i >= 0; i--)
                {
                    if (_questsToView[i].id == id && _questsToView[i].requirementIndex == requirementNumber)
                    {
                        _questsToView.Remove(_questsToView[i]);
                    }
                }
            }
            
            public void IncreaseProgress(int id, int requirementNumber, int progress)
            {
                RemoveQuest(id, requirementNumber);
                _questsToView.Add((id, requirementNumber, progress));
            }
            
            public int[] ReturnUniqueQuests()
            {
                var uniqueIds = new List<int>();
                
                foreach (var questToView in _questsToView.Where(questToView => !uniqueIds.Contains(questToView.id)))
                {
                    uniqueIds.Add(questToView.id);
                }
                
                return uniqueIds.ToArray();
            }
            
            public int[] ShowRequirementsProgress(int id)
            {
                var suitableRequirements = new List<(int id, int requirementIndex, int progress)>();
                
                foreach (var questToView in _questsToView)
                {
                    if (questToView.id == id)
                        suitableRequirements.Add((questToView.id, questToView.requirementIndex, questToView.progress));
                }
                
                var sortedRequirements = from i in suitableRequirements 
                                                                         orderby i.requirementIndex
                                                                          select i;
                
                var listToExit = new List<int>();
                
                foreach (var sortedRequirementValue in sortedRequirements)
                {
                    listToExit.Add(sortedRequirementValue.progress);
                }
                
                return listToExit.ToArray();
            }
        }
        #endregion
    }
}