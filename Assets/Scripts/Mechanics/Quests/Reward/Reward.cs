using UnityEngine;

namespace ColLobOK
{
    public class Reward : ScriptableObject
    {
        [SerializeField] protected Resource _resource;
        
        public int Id => _resource.ID;
        public int Count => _resource.count;
        
        public virtual void GiveReward() {}
        public virtual bool ShowOpportunityToGiveAward()
        {
            return false;
        }
        
#if UNITY_EDITOR
        public Resource Res
        {
            get => _resource;
            set => _resource = value;
        }
#endif
    }
}