using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Reward/WarehouseReward", order = 1)]
    public class WarehouseReward : Reward
    {
        private IWarehouseController _warehouseController;
        
        public override void GiveReward()
        {
            _warehouseController = GameStarter.container.Resolve<IWarehouseController>();
            
            if (ShowOpportunityToGiveAward())
                _warehouseController.AddRes(_resource);
        }
        
        public override bool ShowOpportunityToGiveAward()
        {
            _warehouseController = GameStarter.container.Resolve<IWarehouseController>();
            
            return _warehouseController.CheckFreeSpace(_resource);
        }
    }
}