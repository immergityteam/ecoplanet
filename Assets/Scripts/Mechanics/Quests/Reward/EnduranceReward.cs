using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Reward/EnduranceReward", order = 1)]
    public class EnduranceReward : Reward
    {
        public override void GiveReward()
        {
            GameStarter.container.Resolve<IEnergyController>().AddEnergy(Count);
        }
        
        public override bool ShowOpportunityToGiveAward()
        {
            return true;
        }
    }
}