using UnityDI;

namespace ColLobOK
{
    public class QuestContainer : IQuestContainer, IDependent
    {
        [Dependency] public IQuestContainerMono QuestContainersMono { private get; set; }
        
        public MainQuest[] MainQuests { get; set; }
        public TechQuest[] TechQuests { get; set; }
        
        public void OnInjected()
        {
            MainQuests = QuestContainersMono.MainQuests;
            TechQuests = QuestContainersMono.TechQuests;
        }
    }
}