namespace ColLobOK
{
    public interface IQuestContainer
    {
        public MainQuest[] MainQuests { get; set; }
        public TechQuest[] TechQuests { get; set; }
    }
}