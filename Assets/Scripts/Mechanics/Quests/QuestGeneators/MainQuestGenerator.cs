using System.Collections.Generic;
using ColLobOK;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Developing
{
    public class MainQuestGenerator : MonoBehaviour
    {
#if UNITY_EDITOR

        [Header("Кветсы")]
        public Object CVS;
        List<Dictionary<string, string>> exel;
        //private int _idName;

        [ContextMenu("QuestGenerator")]
        private void Generate()
        {
            exel = MetodForEditor.Parsing(CVS);
            //_idName = 1;

            foreach (var dictionary in exel)
            {
                //_idName++;
                if (dictionary["Будет в демо"] == "Нет") continue;
                if (dictionary["Тип квеста"] == "Мейн квест") GenerateQuestMain(dictionary);
            }

            AssetDatabase.SaveAssets();
        }

        //+Тип квеста
        //Ограничение
        //+Название
        //+Описание
        //+Требования квеста
        //Требования пользователям
        //+Требования для получения
        //Повторяемый
        //Награда
        //Счет
        //+Будет в демо


        //+Добыть
        //+Построить
        //Ускорить
        //+Поднять
        //Вступить в гильдию
        //Друзья
        //Похвалить друга
        //
        //+Пройти
        //
        //Выносливость
        //Ресурс
        //Опыт
        //Счет
        private void GenerateQuestMain(Dictionary<string, string> data)
        {
            //Название
            var quest = MetodForEditor.GetQuestMain(data["Название"]);
            
            //Описание
            var descriptions = data["Описание"].Split('|');
            var descriptionsSOs = new List<StringsSO>();
            
            foreach (var item in descriptions)
            {
                descriptionsSOs.Add(MetodForEditor.GetStringsSO(item.Trim('\"').Trim(' ')));
            }
            
            quest.SetDescription(descriptionsSOs.ToArray());

            //Требования квеста
            var requirements = data["Требования квеста"].Split('|');
            var requirementsList = new List<Requirement>();
            
            for (int i = 0; i < requirements.Length; i++)
            {
                var req = requirements[i].Trim(' ').Split(':');
                switch (req[0].Trim(' '))
                {
                    case "Добыть":
                        var mining = MetodForEditor.GetResurse(req[1].Trim(' '));
                        for (int j = 0; j < mining.Length; j++)
                            requirementsList.Add(GetResourceRequirement(mining[j]));
                        break;
                    case "Построить":
                        var builder = MetodForEditor.GetResurse(req[1].Trim(' '));
                        for (int j = 0; j < builder.Length; j++)
                            requirementsList.Add(GetBuildRequirement(builder[j]));
                        break;
                    case "Поднять":
                        var pasRes = MetodForEditor.GetResurse(req[1].Trim(' '));
                        for (int j = 0; j < pasRes.Length; j++)
                            requirementsList.Add(GetPassiveResourceRequirement(pasRes[j]));
                        break;
                    case "Ускорить":
                    case "Вступить в гильдию":
                    case "Друзья":
                    case "Похвалить друга":
                    default:
                        Debug.Log("Нет требовыния " + req[0]);
                        break;
                }
            }
            
            quest.SetRequirements(requirementsList.ToArray());

            //Награда
            var rewards = data["Награда"].Split('|');
            var rewardsSOs = new List<Reward>();
            
            foreach (var reward in rewards)
            {
                var rew = reward.Trim(' ').Split(':');
                if (rew[0].Trim(' ') == "Выносливость")
                {
                    var enduranceRewards = MetodForEditor.GetResurse(rew[1].Trim(' '));
                    foreach (var enduranceReward in enduranceRewards)
                    {
                        rewardsSOs.Add(GetEnduranceReward(enduranceReward));
                    }
                }
                else
                {
                    var warehouseRewards = MetodForEditor.GetResurse(rew[1].Trim(' '));
                    foreach (var warehouseReward in warehouseRewards)
                    {
                        rewardsSOs.Add(GetWarehouseReward(warehouseReward));
                    }
                }
            }
            
            quest.SetRewards(rewardsSOs.ToArray());

            //Требования для получения
            if (data["Требования для получения"] != "")
            {
                var questsToActivate = data["Требования для получения"].Split(':');
                
                questsToActivate[0] = questsToActivate[0].Trim(' ');
                questsToActivate[1] = data["Требования для получения"].Substring(questsToActivate[0].Length + 1).Trim(' ');
                
                if (questsToActivate[0] == "Пройти")
                {
                    var questOld = MetodForEditor.GetQuestMain(questsToActivate[1]);
                    var toActive = new List<StringsSO>();
                    
                    if (questOld.ToActivate !=null)
                    {
                        toActive.AddRange(questOld.ToActivate);
                        toActive.RemoveAll(item => item == null);
                    }
                    
                    if (!toActive.Contains(quest.GetLabel())) toActive.Add(quest.GetLabel());

                    questOld.ToActivate = toActive.ToArray();
                    EditorUtility.SetDirty(questOld);
                }
            }
            
            if (data["Требования для получения"] != "") quest.SetScore(int.Parse(data["Счет"]));
            
            EditorUtility.SetDirty(quest);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        
        // Образование требований
        private BuildRequirement GetBuildRequirement(Resource res)
        {
            var buildRequirementAssetPath = "Assets/Generator/QuestGenerator/Requirements/Build/";
            var build = MetodForEditor.GetAllAssets<BuildRequirement>();
            
            for (int i = 0; i < build.Length; i++)
            {
                if (build[i].ShowAllInformation().name == res.name && build[i].ShowAllInformation().needToReceive == res.count)
                {
                    return build[i];
                }
            }

            var newBuild = ScriptableObject.CreateInstance<BuildRequirement>();
            newBuild.Building = res.name;

            AssetDatabase.CreateAsset(newBuild, buildRequirementAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newBuild;
        }
        private ResourceRequirement GetResourceRequirement(Resource res)
        {
            var resourceRequirementAssetPath = "Assets/Generator/QuestGenerator/Requirements/Resource/";
            var resReq = MetodForEditor.GetAllAssets<ResourceRequirement>();
            
            for (int i = 0; i < resReq.Length; i++)
            {
                if (resReq[i].ShowAllInformation().name == res.name && resReq[i].ShowAllInformation().needToReceive == res.count)
                {
                    return resReq[i];
                }
            }

            var newRes = ScriptableObject.CreateInstance<ResourceRequirement>();
            newRes.Res = res;

            AssetDatabase.CreateAsset(newRes, resourceRequirementAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newRes;
        }
        private PassiveResourceRequirement GetPassiveResourceRequirement(Resource res)
        {
            var passiveResourceAssetPath = "Assets/Generator/QuestGenerator/Requirements/PassiveResource/";
            var resReq = MetodForEditor.GetAllAssets<PassiveResourceRequirement>();
            
            for (int i = 0; i < resReq.Length; i++)
            {
                if (resReq[i].Res.name == res.name && resReq[i].Res.count == res.count)
                {
                    return resReq[i];
                }
            }

            var newRes = ScriptableObject.CreateInstance<PassiveResourceRequirement>();
            newRes.Res = res;

            AssetDatabase.CreateAsset(newRes, passiveResourceAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newRes;
        }
        
        // Образование наград
        private WarehouseReward GetWarehouseReward(Resource res)
        {
            var warehouseRewardAssetPath = "Assets/Generator/QuestGenerator/Rewards/WarehouseRewards/";
            var warehouseRewards = MetodForEditor.GetAllAssets<WarehouseReward>();
            
            foreach (var warehouseReward in warehouseRewards)
            {
                if (warehouseReward.Res.name == res.name && warehouseReward.Res.count == res.count)
                    return warehouseReward;
            }
            
            var newWarehouseReward = ScriptableObject.CreateInstance<WarehouseReward>();
            newWarehouseReward.Res = res;
            
            AssetDatabase.CreateAsset(newWarehouseReward, warehouseRewardAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newWarehouseReward;
        }
        private EnduranceReward GetEnduranceReward(Resource res)
        {
            var enduranceRewardAssetPath = "Assets/Generator/QuestGenerator/Rewards/EnduranceRewards/";
            var enduranceRewards = MetodForEditor.GetAllAssets<EnduranceReward>();
            
            foreach (var enduranceReward in enduranceRewards)
            {
                if (enduranceReward.Res.name == res.name && enduranceReward.Res.count == res.count)
                    return enduranceReward;
            }
            
            var newEnduranceReward = ScriptableObject.CreateInstance<EnduranceReward>();
            newEnduranceReward.Res = res;
            
            AssetDatabase.CreateAsset(newEnduranceReward, enduranceRewardAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newEnduranceReward;
        }
#endif
    }
}