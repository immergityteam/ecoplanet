using System.Collections.Generic;
using ColLobOK;
using UnityEngine;
using UnityEditor;
using Object = UnityEngine.Object;

namespace Developing
{
public class TechQuestGenerator: MonoBehaviour
{
#if UNITY_EDITOR

        [Header("Кветсы")]
        public Object CVS;
        List<Dictionary<string, string>> exel;
        //private int _idName;

        [ContextMenu("QuestGenerator")]
        private void Generate()
        {
            exel = MetodForEditor.Parsing(CVS);
            //_idName = 1;

            foreach (var dictionary in exel)
            {
                //_idName++;
                if (dictionary["Будет в демо"] == "Нет") continue;
                if (dictionary["Тип квеста"] == "Квест на повышение квалификации") GenerateTechQuest(dictionary);
            }

            AssetDatabase.SaveAssets();
        }

        //+Тип квеста
        //Ограничение
        //+Название
        //+Описание
        //+Требования квеста
        //Требования пользователям
        //+Требования для получения
        //Повторяемый
        //Награда
        //Счет
        //+Будет в демо


        //+Добыть
        //+Построить
        //Ускорить
        //+Поднять
        //Вступить в гильдию
        //Друзья
        //Похвалить друга
        //
        //+Пройти
        //
        //Выносливость
        //Ресурс
        //Опыт
        //Счет
        private void GenerateTechQuest(Dictionary<string, string> data)
        {
            //Название
            var quest = MetodForEditor.GetQuestTech(data["Название"]);
            
            //Описание
            var descriptions = data["Описание"].Split('|');
            var descriptionsSOs = new List<StringsSO>();
            
            foreach (var item in descriptions)
            {
                descriptionsSOs.Add(MetodForEditor.GetStringsSO(item.Trim('\"').Trim(' ')));
            }
            
            quest.SetDescription(descriptionsSOs.ToArray());
            
            //Требования квеста
            var requirements = data["Требования квеста"].Split('|');
            var requirementsList = new List<Requirement>();
            
            for (int i = 0; i < requirements.Length; i++)
            {
                var req = requirements[i].Trim(' ').Split(':');
                switch (req[0].Trim(' '))
                {
                    case "Создать":
                        var craft = MetodForEditor.GetResurse(req[1].Trim(' '));
                        for (int j = 0; j < craft.Length; j++)
                            requirementsList.Add(GetPassiveResourceRequirement(craft[j]));
                        break;
                    /*case "Собрать":
                        var trash = MetodForEditor.GetResurse(req[1].Trim(' '));
                        requirementsList.Add(GetTrashRequirement(trash[0].count));
                        break;*/
                    case "Поднять":
                        var pasRes = MetodForEditor.GetResurse(req[1].Trim(' '));
                        for (int j = 0; j < pasRes.Length; j++)
                            requirementsList.Add(GetPassiveResourceRequirement(pasRes[j]));
                        break;
                    default:
                        Debug.Log("Нет требовыния " + req[0]);
                        break;
                }
            }
            
            quest.SetRequirements(requirementsList.ToArray());
            
            //Требования для получения
            if (data["Требования для получения"] != "")
            {
                
            }
            
            if (data["Требования для получения"] != "") quest.SetScore(int.Parse(data["Счет"]));
            
            EditorUtility.SetDirty(quest);
        }
        
        // Требования квеста
        private PassiveResourceRequirement GetPassiveResourceRequirement(Resource res)
        {
            var passiveResourceAssetPath = "Assets/Generator/QuestGenerator/Requirements/PassiveResource/";
            var resReq = MetodForEditor.GetAllAssets<PassiveResourceRequirement>();
            
            for (int i = 0; i < resReq.Length; i++)
            {
                if (resReq[i].Res.name == res.name && resReq[i].Res.count == res.count)
                {
                    return resReq[i];
                }
            }
            
            var newRes = ScriptableObject.CreateInstance<PassiveResourceRequirement>();
            newRes.Res = res;
            
            AssetDatabase.CreateAsset(newRes, passiveResourceAssetPath + res.name.GetStringEdit() + "X" + res.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newRes;
        }
        /*private CraftRequirement GetCraftRequirement(Resource recipe)
        {
            var resourceRequirementAssetPath = "Assets/Generator/QuestGenerator/Requirements/Craft/";
            var resReq = MetodForEditor.GetAllAssets<CraftRequirement>();
            
            for (int i = 0; i < resReq.Length; i++)
            {
                if (resReq[i].RecipeResource.name == recipe.name && resReq[i].RecipeResource.count == recipe.count)
                {
                    return resReq[i];
                }
            }
            
            var newRecipe = ScriptableObject.CreateInstance<CraftRequirement>();
            //newRecipe.Recipe = recipe;
            
            AssetDatabase.CreateAsset(newRecipe, resourceRequirementAssetPath + recipe.name + "X" + recipe.count + ".asset");
            AssetDatabase.SaveAssets();
            
            return newRecipe;
        }*/
        /*private TrashRequirement GetTrashRequirement(int trashCount)
        {
            var resourceRequirementAssetPath = "Assets/Generator/QuestGenerator/Requirements/Trash/";
            var resReq = MetodForEditor.GetAllAssets<TrashRequirement>();
            
            for (int i = 0; i < resReq.Length; i++)
            {
                if (resReq[i].Trash == trashCount)
                {
                    return resReq[i];
                }
            }
            
            var newRes = ScriptableObject.CreateInstance<TrashRequirement>();
            newRes.Trash = trashCount;
            
            AssetDatabase.CreateAsset(newRes, resourceRequirementAssetPath + "X" + trashCount + ".asset");
            AssetDatabase.SaveAssets();
            
            return newRes;
        }*/
        
        // Требования для получения
#endif
    }
}