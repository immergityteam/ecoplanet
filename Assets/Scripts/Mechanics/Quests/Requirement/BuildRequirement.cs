using System;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Requirements/BuildRequirement", order = 1)]
    public class BuildRequirement : Requirement
    {
        // Строительство зданий
        [SerializeField] private StringsSO _building;
        
        private IBuilderPlayer _builderPlayer;
        
        // События
        public override void SubscribeToEvent(Action<int> increasedCounter)
        {
            _builderPlayer = GameStarter.container.Resolve<IBuilderPlayer>();
            _builderPlayer.EvBuiltConstructionTimerOff += (id) => ReactToEvent (id, increasedCounter);
        }
        
        public override void UnsubscribeFromEvent(Action<int> increasedCounter)
        {
            _builderPlayer.EvBuiltConstructionTimerOff -= (id) => ReactToEvent (id, increasedCounter);
        }
        
        // Проверка
        public override void ReactToEvent (int id, Action<int> increasedCounter)
        {
            var globalBuilderId = _builderPlayer.GetGlobalID(id);
            
            if (globalBuilderId == _building.GetID())
                increasedCounter.Invoke(_building.GetID());
        }
        
        // Показать внутренности
        public override (int id, StringsSO name, int needToReceive) ShowAllInformation()
        {
            return (_building.GetID(), _building, 1);
        }
        
#if UNITY_EDITOR
        public StringsSO Building
        {
            get => _building;
            set => _building = value;
        }
#endif
    }
}