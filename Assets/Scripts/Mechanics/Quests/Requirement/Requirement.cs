using System;
using UnityEngine;

namespace ColLobOK
{
    public class Requirement : ScriptableObject
    {
        // События
        public virtual void SubscribeToEvent(Action<int> increasedCounter) { }
        public virtual void UnsubscribeFromEvent(Action<int> increasedCounter) { }
        
        // Проверка
        public virtual void ReactToEvent(int id, Action<int> increasedCounter) { }
        
        // Показать внутренности
        public virtual (int id, StringsSO name, int needToReceive) ShowAllInformation()
        {
            return (0, null, 0);
        }
    }
}