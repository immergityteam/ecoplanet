using System;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Requirements/ResourceRequirement", order = 1)]
    public class ResourceRequirement : Requirement
    {
        // Ресурсы
        [SerializeField] private Resource _resource;
        
        private IWarehouseController _warehouseController;
        
        // События
        public override void SubscribeToEvent(Action<int> increasedCounter)
        {
            _warehouseController = GameStarter.container.Resolve<IWarehouseController>();
            _warehouseController.EvChangeRes += (id) => ReactToEvent (id, increasedCounter);
        }
        
        public override void UnsubscribeFromEvent(Action<int> increasedCounter)
        {
            _warehouseController.EvChangeRes -= (id) => ReactToEvent (id, increasedCounter);
        }
        
        // Проверка
        public override void ReactToEvent(int id, Action<int> increasedCounter)
        {
            if (id == _resource.ID)
                increasedCounter?.Invoke(_resource.ID);
        }
        
        // Показать внутренности
        public override (int id, StringsSO name, int needToReceive) ShowAllInformation()
        {
            return (_resource.ID, _resource.name, _resource.count);
        }
        
#if UNITY_EDITOR
        public Resource Res
        {
            get => _resource;
            set => _resource = value;
        }
#endif
    }
}