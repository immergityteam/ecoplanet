using System;
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Data", menuName = "SO/Requirements/PassiveResourceRequirement", order = 1)]
    public class PassiveResourceRequirement : Requirement
    {
        // Пассивные ресурсы
        [SerializeField] private Resource _passiveResource;

        private IPassiveResourcesController _passiveResourcesController;
        
        // События
        public override void SubscribeToEvent(Action<int> call)
        {
            _passiveResourcesController = GameStarter.container.Resolve<IPassiveResourcesController>();
            _passiveResourcesController.EvChangeRasRes += (id) => ReactToEvent (id, call);
        }
        
        public override void UnsubscribeFromEvent(Action<int> call)
        {
            _passiveResourcesController.EvChangeRasRes -= (id) => ReactToEvent (id, call);
        }
        
        // Проверка
        public override void ReactToEvent(int id, Action<int> increasedCounter)
        {
            if (id == _passiveResource.ID)
                increasedCounter?.Invoke(_passiveResource.ID);
        }
        
        // Показать внутренности
        public override (int id, StringsSO name, int needToReceive) ShowAllInformation()
        {
            return (_passiveResource.ID, _passiveResource.name, _passiveResource.count);
        }

#if UNITY_EDITOR
        public Resource Res
        {
            get => _passiveResource;
            set => _passiveResource = value;
        }
#endif
    }
}