using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "RewardWindow", menuName = "SO/RewardWindowSO", order = 1)]
    public class RewardSO : ScriptableObject
    {
        [field: SerializeField] public TechnologyMainConfig TechnologyMainConfig { get; set; }
        
        [field: SerializeField] public StringsSO Congratulations { get; set; }
        [field: SerializeField] public StringsSO Excellent { get; set; }
        
        [field: SerializeField] public StringsSO StageTransition { get; set; }
        [field: SerializeField] public StringsSO CompletedQuest { get; set; }
        
        [field: SerializeField] public StringsSO SkillTree { get; set; }
        [field: SerializeField] public StringsSO Quest { get; set; }
        
        [field: SerializeField] public Sprite QuestReward { get; set; }
        [field: SerializeField] public Sprite QuestGirl { get; set; }
        
        public readonly string[] RomanNumerals = { "I", "II", "III", "IV", "V" };
    }
}