using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class RewardUI : MonoBehaviour, IRewardUI
    {
        // View
        [field: SerializeField] public TMP_Text Congratulations { get; set; }
        [field: SerializeField] public TMP_Text TechnologyBranch { get; set; }
        [field: SerializeField] public TMP_Text MessageText { get; set; }
        [field: SerializeField] public TMP_Text ReasonForAppearance { get; set; }
        [field: SerializeField] public TMP_Text Excellent { get; set; }
        
        [field: SerializeField] public Image Icon { get; set; }
        [field: SerializeField] public Image LeftGirl { get; set; }
        [field: SerializeField] public Image RightGirl { get; set; }
        
        [field: SerializeField] public Button AcceptButton { get; set; }
        
        // Model
        [field: SerializeField] public RewardSO RewardSO { get; set; }
    }
}