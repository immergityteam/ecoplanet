using TMPro;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IRewardUI
    {
        // View
        public TMP_Text Congratulations { get; set; }
        public TMP_Text TechnologyBranch { get; set; }
        public TMP_Text MessageText { get; set; }
        public TMP_Text ReasonForAppearance { get; set; }
        public TMP_Text Excellent { get; set; }
        
        public Image Icon { get; set; }
        public Image LeftGirl { get; set; }
        public Image RightGirl { get; set; }
        
        public Button AcceptButton { get; set; }
        
        // Model
        public RewardSO RewardSO { get; set; }
    }
}