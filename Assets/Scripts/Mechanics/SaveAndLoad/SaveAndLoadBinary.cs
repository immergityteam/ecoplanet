﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class SaveAndLoadBinary : ISaveAndLoad, IDependent
    {
        [Dependency] public IFileManager FileManager { get; set; }
#if UNITY_EDITOR
        [Dependency] public IContainerMono ContainerMono { get; set; }
#endif
        public UnityAction EvSave { get; set; }
        public UnityAction EvLoadOnFocus { get; set; }

        Dictionary<string, string> data;

        readonly string keyNumberSave = "SaveNumber";
        readonly int numberSave = 2;

        public void OnInjected()
        {
            data = new Dictionary<string, string>();

            if (PlayerPrefs.HasKey(keyNumberSave))
            {
                if (PlayerPrefs.GetInt(keyNumberSave) == numberSave)
                {
                    StartLoad();
                }
            }
        }

        public void Save(string name, string str) => data.Add(name, str);
        public string Load(string name)
        {
            if (data.ContainsKey(name)) return data[name];
            else return "";
        }


        public void StartSave()
        {
            PlayerPrefs.SetInt(keyNumberSave, numberSave);
            PlayerPrefs.Save();
#if UNITY_EDITOR
            if (ContainerMono.OffSaveAndLoad)
                return;
#endif
            data.Clear();
            EvSave?.Invoke();
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, data);
                stream.Flush();
                stream.Position = 0;
                FileManager.Write(Convert.ToBase64String(stream.ToArray()));
            }
        }

        public void LoadOnFocus()
        {
#if UNITY_EDITOR
            if (ContainerMono.OffSaveAndLoad)
                return;
#endif
            StartLoad();
            EvLoadOnFocus?.Invoke();
        }

        private void StartLoad()
        {
#if UNITY_EDITOR
            if (ContainerMono.OffSaveAndLoad)
                return;
#endif
            string str = FileManager.Read();

            byte[] b = Convert.FromBase64String(str);
            if (str != "") using (var stream = new MemoryStream(b))
                {
                    var formatter = new BinaryFormatter();
                    stream.Seek(0, SeekOrigin.Begin);
                    data = (Dictionary<string, string>)formatter.Deserialize(stream);
                }
        }
    }
}