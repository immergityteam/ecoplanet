using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ColLobOK
{
    public class ConvertDataBinary<T> where T : new()
    {
        public string name;
        public ConvertDataBinary(string name)
        {
            this.name = name;
        }

        public string ToConvert(T arg)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, arg);
                stream.Flush();
                stream.Position = 0;
                return Convert.ToBase64String(stream.ToArray());
            }
        }
        public T FromConvert(string arg)
        {
            if (arg == "") return new T();
            byte[] b = Convert.FromBase64String(arg);
            using (var stream = new MemoryStream(b))
            {
                var formatter = new BinaryFormatter();
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}