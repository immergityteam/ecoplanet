using Newtonsoft.Json;

namespace ColLobOK
{
    public class SaveLoadData<T> where T : new()
    {
        public string name;
        public SaveLoadData(string name)
        {
            this.name = name;
        }

        public string ToConvert(T arg)
        {
            return JsonConvert.SerializeObject(arg, Formatting.Indented);
        }
        public T FromConvert(string arg)
        {
            if (arg == "") return new T();
            return JsonConvert.DeserializeObject<T>(arg);
        }
    }
}