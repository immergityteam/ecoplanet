using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace ColLobOK
{
    public class FileManagerBinary : IFileManager
    {
        public string Read()
        {
            string path = $"{Application.persistentDataPath}/{Application.productName}";
            Debug.Log("��������� � " + path);
            if (File.Exists(path))
            {
                string str;
                BinaryFormatter binary = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                str = binary.Deserialize(stream) as string;
                stream.Close();
                return str;
            }
            else
            {
                return "";
            }
        }

        public void Write(string str)
        {
            BinaryFormatter binary = new BinaryFormatter();
            string path = $"{Application.persistentDataPath}/{Application.productName}";
            FileStream stream = new FileStream(path, FileMode.Create);
            binary.Serialize(stream, str);
            stream.Close();
        }
    }
}