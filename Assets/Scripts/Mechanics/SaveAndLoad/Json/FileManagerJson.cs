using System.IO;
using UnityEngine;

namespace ColLobOK
{
    public class FileManagerJson : IFileManager
    {
        public void Write(string str)
        {
            string path = $"{Application.persistentDataPath}/{Application.productName}.json";
            Debug.Log("Сохранено в " + path);
            StreamWriter sw = File.CreateText(path);
            sw.WriteLine(str);
            sw.Close();
        }
        public string Read()
        {
            string path = $"{Application.persistentDataPath}/{Application.productName}.json";
            Debug.Log("Загрузка из  " + path);
            if (File.Exists(path))
            {
                string str;
                StreamReader sr = File.OpenText(path);
                str = sr.ReadToEnd();
                sr.Close();
                return str;
            }
            else
            {
                return "";
            }
        }
    }
}