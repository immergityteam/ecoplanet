﻿using Newtonsoft.Json;
using System.Collections.Generic;
using UnityDI;
using UnityEngine.Events;

namespace ColLobOK
{
    public class SaveAndLoad : ISaveAndLoad, IDependent
    {
        [Dependency] public IFileManager FileManager { get; set; }
        public UnityAction EvSave { get; set; }
        public UnityAction EvLoadOnFocus { get; set; }

        Dictionary<string, string> data;
        public void OnInjected()
        {
            data = new Dictionary<string, string>();
            StartLoad();
        }
        public void Save(string name, string str) => data.Add(name, str);
        public string Load(string name)
        {
            if (data.ContainsKey(name)) return data[name];
            else return "";
        }

        public void StartSave()
        {
            data.Clear();
            EvSave?.Invoke();
            FileManager.Write(JsonConvert.SerializeObject(data, Formatting.Indented));
        }
        private void StartLoad()
        {
            string str = FileManager.Read();
            if (str != "")  data = JsonConvert.DeserializeObject<Dictionary<string, string>>(str);
        }

        public void LoadOnFocus()
        {
            StartLoad();
            EvLoadOnFocus?.Invoke();
        }
    }
}