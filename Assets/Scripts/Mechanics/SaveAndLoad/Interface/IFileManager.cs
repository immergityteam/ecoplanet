namespace ColLobOK
{
    public interface IFileManager
    {
        void Write(string str);
        string Read();
    }
}