using UnityEngine.Events;

namespace ColLobOK
{
    public interface ISaveAndLoad
    {
        string Load(string name);
        UnityAction EvSave { get; set; }
        UnityAction EvLoadOnFocus { get; set; }

        void Save(string name, string str);
        void StartSave();
        void LoadOnFocus();
    }
}