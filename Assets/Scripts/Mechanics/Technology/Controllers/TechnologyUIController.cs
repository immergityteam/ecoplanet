﻿using System.Collections.Generic;
using DG.Tweening;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class TechnologyUIController : ITechnologyUIController, IDependent
    {
        [Dependency] public ITechnologyUIView TechnologyUIView { private get; set; }
        [Dependency] public ITechnologyConfigContainer TechnologyConfigContainer { private get; set; }
        [Dependency] public ITechnologyController TechnologyController { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }

        public int CurrentActiveBranchInID { get; set; }

        private List<Vector3> _rankPositions;

        public void OnInjected()
        {
            GameUI.Technologies.onClick.AddListener(() =>
            {
                TechnologyUIView.ViewObject.SetActive(true);
                TechnologyUIView.Header.text = TechnologyUIView.HeaderSSO.GetString();
                ResetEcoBranch();
            });

            //TechnologyUIView.ViewObject.SetActive(true);
            foreach (var item in TechnologyUIView.StageLocks)
            {
                item.gameObject.SetActive(true);
            }

            TechnologyController.ExpAdded += UpdateCurrentExpBarFiller;
            TechnologyController.StageAdded += UpdateCurrentTechStage;
            TechnologyController.StageAdded += SetCurrentStageDescription;



            foreach (var button in TechnologyUIView.Buttons)
            {
                button.buttonPressedImage += SetBranchIcon;
                button.progressButtonPressedImage += SetProgressIcon;
                button.buttonPressed += SetBranchName;
                button.buttonPressed += SetCurrentStage;
                button.buttonPressed += SetCurrentStageDescription;
                button.buttonPressed += SetExpAmount;
                button.buttonPressed += SetActiveBranch;
                button.buttonPressed += ResetButtonListeners;
                button.buttonPressed += SetRankStages;
            }

            _rankPositions = new List<Vector3>();
            
            for (var index = 0; index < TechnologyUIView.RankTabs.Count; index++)
            {
                var index1 = index;
                TechnologyUIView.RankTabs[index].toggle.onValueChanged.AddListener((flag) => RefreshToggle(flag, index1));
                _rankPositions.Add(TechnologyUIView.RankTabs[index].gameObject.GetComponent<RectTransform>().position);
            }

            TechnologyUIView.TabOpened += ResetEcoBranch;
            //ResetEcoBranch();
            //TechnologyUIView.ViewObject.SetActive(false);
        }

        private void SetBranchName(int branchID)
        {
            TechnologyUIView.BranchName.text = TechnologyConfigContainer.TechnologyConfig.TechBranches[branchID]
                .TechBranch.BranchName.GetString();
        }

        private void SetCurrentStage(int branchID)
        {
            CurrentActiveBranchInID = branchID;
            foreach (var item in TechnologyUIView.StageLocks)
            {
                item.gameObject.SetActive(true);
            }
            
            for (int i = 0; i < TechnologyController.GetCurrentStageOrderCount(branchID); i++)
            {
                TechnologyUIView.StageLocks[i].gameObject.SetActive(false);
            }
            
            /*foreach (var item in TechnologyUIView.Stages)
            {
                item.sprite = TechnologyUIView.DefaultTab;
            }
            
            if (TechnologyController.GetCurrentStageOrderCount(branchID) == 0)
            {
                TechnologyUIView.Stages[TechnologyController.GetCurrentStageOrderCount(branchID)].sprite =
                    TechnologyUIView.ActiveTab;
                return;
            }
            
            TechnologyUIView.Stages[TechnologyController.GetCurrentStageOrderCount(branchID) - 1].sprite =
                TechnologyUIView.ActiveTab;*/
        }

        private void SetBranchIcon(Sprite icon)
        {
            TechnologyUIView.BranchIcon.sprite = icon;
        }

        private void SetProgressIcon(Sprite icon)
        {
            foreach (var progressIcon in TechnologyUIView.ProgressIcons)
            {
                progressIcon.sprite = icon;
            }
        }
        
        private void SetCurrentStageDescription(int branchID)
        {
            if (TechnologyController.GetCurrentStageOrderCount(branchID) == 0)
            {
                TechnologyUIView.StageDescription.text =
                    TechnologyController.GetFirstStage(branchID).StageDescription.GetString();
                return;
            }

            TechnologyUIView.StageDescription.text =
            TechnologyController.GetCurrentStage(branchID).StageDescription.GetString();
        }

        private void UpdateCurrentExpBarFiller(int branchID)
        {
            if (CurrentActiveBranchInID == branchID)
            {
                SetExpAmount(branchID);
            }
        }

        private void UpdateCurrentTechStage(int branchID)
        {
            if (CurrentActiveBranchInID == branchID)
            {
                SetCurrentStage(branchID);
            }
        }

        private void SetExpAmount(int branchID)
        {
            CurrentActiveBranchInID = branchID;
            TechnologyUIView.ProgressBarFiller.value = 0;
            var fillAmount = (float)TechnologyController.GetCurrentBranchExpAmount(branchID) /
                             TechnologyController.GetCurrentMaxExp(branchID);
            TechnologyUIView.ProgressBarFiller.value = fillAmount;
            
            TechnologyUIView.HalfProgress.color = fillAmount >= 0.5 ? Color.white : new Color(1, 1, 1, 0);
            TechnologyUIView.AllProgress.color = fillAmount >= 0.98 ? Color.white : new Color(1, 1, 1, 0);
        }

        private void ResetEcoBranch()
        {
            SetBranchIcon(TechnologyUIView.DefaultBranch);
            SetBranchName(0);
            SetCurrentStage(0);
            SetExpAmount(0);
            SetCurrentStageDescription(0);
            SetActiveBranch(0);
            UpdateCurrentExpBarFiller(0);
            SetRankStages(0);
            ResetButtonListeners(0);

            for (var index = 0; index < TechnologyUIView.RankTabs.Count; index++)
            {
                var rankTab = TechnologyUIView.RankTabs[index];
                
                if (rankTab.toggle.isOn)
                {
                    currentShowStage = index;

                    TechnologyUIView.RankName.text = index == 4 ? TechnologyUIView.MasterRank.GetString() : TechnologyUIView.ApprenticeRank.GetString();
                }
            }
            
            ResetButtonListeners(0);
            
            TechnologyUIView.Rank.text = TechnologyUIView.RankSO.GetString() + ": ";
        }

        private void SetActiveBranch(int branchID)
        {
            CurrentActiveBranchInID = branchID;
            currentShowStage = TechnologyController.GetCurrentStageOrderCount(branchID);
        }

        private int currentShowStage;

        private void ResetButtonListeners(int branchID)
        {
            if (TechnologyUIView.RankTabs[currentShowStage].rankStage != null)
            {
                TechnologyUIView.StageDescription.text = TechnologyUIView
                    .RankTabs[currentShowStage]
                    .rankStage.StageDescription.GetString();
            }
        }

        private void RefreshToggle(bool isOn, int index)
        {
            PushRank(isOn, index);

            TechnologyUIView.RankName.text = index == 4 ? TechnologyUIView.MasterRank.GetString() : TechnologyUIView.ApprenticeRank.GetString();
            
            currentShowStage = index;

            if (isOn)
                ResetButtonListeners(0);
        }

        private void SetRankStages(int branchID)
        {
            currentShowStage = TechnologyController.GetCurrentStageOrderCount(branchID);
            for (int i = 0; i < TechnologyUIView.RankTabs.Count; i++)
            {
                TechnologyUIView.RankTabs[i].rankStage = TechnologyConfigContainer.TechnologyConfig
                    .TechBranches[branchID].TechBranch.Stages[i];
            }
        }

        private void PushRank(bool isSelected, int rankNumber)
        {
            var rankPosition = _rankPositions[rankNumber];
            var rankPositionX = rankPosition.x;
            
            if (isSelected)
                TechnologyUIView.RankTabs[rankNumber].gameObject.GetComponent<RectTransform>().DOMoveX(rankPositionX - 34, 0);
            else
                TechnologyUIView.RankTabs[rankNumber].gameObject.GetComponent<RectTransform>().DOMoveX(rankPositionX, 0);
        }
    }
}