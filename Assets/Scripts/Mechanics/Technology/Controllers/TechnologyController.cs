﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class TechnologyController : ITechnologyController, IDependent
    {
        [Dependency] public ITechnologyConfigContainer TechnologyContainer { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IPassiveResourcesUI PassiveResourcesUI { private get; set; }

        [Dependency] public ISaveAndLoad SaveAndLoad { get; set; }
        [Dependency] public ITrashController TrashController { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }

        [Dependency] public IQuestController QuestController { private get; set; }
        
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }

        public UnityAction<int, int> AddExpierence { get; set; }
        public UnityAction<int> CheckIfLevelUp { get; set; }
        public UnityAction<int> ExpAdded { get; set; }
        public UnityAction<int> StageAdded { get; set; }

        //KEY: ID of branch ID in StringsSO, VALUE: Amount of exp;
        private Dictionary<int, int> ExpInBranches;
        private Dictionary<int, StageSO> CurrentActiveStageInBranch;
        private Dictionary<int, int> CurrentActiveStageID;
        private int[] ExpNeededToLvlUp = { 500, 2500, 5000, 10000, 50000 };

        [Serializable]
        struct BrancheStage
        {
            public int brange;
            public int stage;
        }

        List<BrancheStage> disposable = new List<BrancheStage>();

        private readonly ConvertDataBinary<Dictionary<int, int>> _saveExpInBranchData =
            new ConvertDataBinary<Dictionary<int, int>>("ExpierenceInBranchData");
        private readonly ConvertDataBinary<Dictionary<int, int>> _saveActiveStageData =
            new ConvertDataBinary<Dictionary<int, int>>("ActiveTechStageData");
        private readonly ConvertDataBinary<List<BrancheStage>> _saveDisposableStageData =
            new ConvertDataBinary<List<BrancheStage>>("DisposableStageData");

        public void OnInjected()
        {
            ExpInBranches = new Dictionary<int, int>();
            CurrentActiveStageInBranch = new Dictionary<int, StageSO>();
            CurrentActiveStageID = new Dictionary<int, int>();
            foreach (var item in TechnologyContainer.TechnologyConfig.TechBranches)
            {
                ExpInBranches.Add(item.ID, 0);
                CurrentActiveStageInBranch.Add(item.ID, TechnologyContainer.Default);
                CurrentActiveStageID.Add(item.ID, 0);
            }

            SaveAndLoad.EvSave += Save;

            Load();
            //AddExpierence += AddExpToBranch;

            TrashController.EvGarbageUnitsMined += (x) => AddExpToBranch(TechnologyContainer.TechnologyConfig.TechBranches[0].ID, x.Count);
            
            BuilderPlayer.EvBuiltConstruction += AddExpToPRBranch;

            //Debug.Log(
            //    $"Branch: {TechnologyContainer.TechnologyConfig.TechBranches[0].TechBranch.BranchName.name} ==> " +
            //    $"Exp : {ExpInBranches[TechnologyContainer.TechnologyConfig.TechBranches[0].ID]} \n" +
            //    $"Stage : {CurrentActiveStageInBranch[TechnologyContainer.TechnologyConfig.TechBranches[0].ID]}");

        }

        private void AddExpToPRBranch(int buildingID)
        {
            if (!BuilderPlayer.GetStructureData(buildingID).productionPasRes.Any()) return;
            foreach (var item in BuilderPlayer.GetStructureData(buildingID).productionPasRes)
            {
                if (item.name == PassiveResourcesUI.PrestigeSO)
                {
                    AddExpToBranch(1, item.count * 10);
                }
            }
        }

        public void AddExpToBranch(int branchID, int expAmount)
        {
            if (branchID == 0)
            {
                expAmount *= ContainerMono.StrengthMining;
            }
            ExpInBranches[branchID] += expAmount;
            LvlUpCheck(branchID);

            /*Debug.Log($"Tech branch: {TechnologyContainer.TechnologyConfig.TechBranches[branchID].TechBranch.BranchName.name} \n" +
                      $"Branch Expierence: {ExpInBranches[branchID]}");*/

            // foreach (var item in TechnologyConfig.TechnologyConfig.TechBranches)
            // {
            //     Debug.Log($"Branch: {item.TechBranch.BranchName.name} ==> Exp : {ExpInBranches[item.ID]}");
            // }
            ExpAdded?.Invoke(branchID);
        }


        #region StageHandlers

        public StageSO GetCurrentStage(int techBranchID)
        {
            var resultStage = CurrentActiveStageInBranch.ContainsKey(techBranchID)
                ? CurrentActiveStageInBranch[techBranchID]
                : TechnologyContainer.Default;
            return resultStage;
        }

        public StageSO GetFirstStage(int techBranchID)
        {
            return TechnologyContainer.TechnologyConfig
                .TechBranches[techBranchID].TechBranch.Stages.FirstOrDefault();
        }

        public StageSO[] GetStages(int branchID)
        {
            return TechnologyContainer.TechnologyConfig.TechBranches[branchID].TechBranch.Stages;
        }

        #endregion


        public int GetCurrentStageOrderCount(int branchID) => CurrentActiveStageID[branchID];
        public int GetCurrentBranchExpAmount(int branchID) => ExpInBranches[branchID];

        public int GetCurrentMaxExp(int branchID)
        {
            if (CurrentActiveStageID[branchID] == 5)
            {
                return ExpNeededToLvlUp.Last();
            }

            return ExpNeededToLvlUp[CurrentActiveStageID[branchID]];
        }

        private void SetNextStage(int techBranchID)
        {
            if (CurrentActiveStageInBranch[techBranchID] == TechnologyContainer.Default)
            {
                CurrentActiveStageInBranch[techBranchID] = TechnologyContainer.TechnologyConfig
                    .TechBranches[techBranchID].TechBranch.Stages.First();
                CurrentActiveStageID[techBranchID] = CurrentActiveStageInBranch[techBranchID].StageID;
                CurrentActiveStageInBranch[techBranchID].StageBonus.Apply();
                StageAdded?.Invoke(techBranchID);
                return;
            }
            CurrentActiveStageInBranch[techBranchID] =
                    TechnologyContainer.TechnologyConfig.TechBranches[techBranchID].TechBranch.Stages.SkipWhile(i =>
                        !i.Equals(CurrentActiveStageInBranch[techBranchID])).Skip(1).First();
            CurrentActiveStageID[techBranchID] = CurrentActiveStageInBranch[techBranchID].StageID;
            CurrentActiveStageInBranch[techBranchID].StageBonus.Apply();
            StageAdded?.Invoke(techBranchID);
        }

        private void SetStage(int techBranchID, int stageID)
        {
            if (stageID - 1 < 0)
            {
                CurrentActiveStageInBranch[techBranchID] = TechnologyContainer.Default;
                return;
            }
            CurrentActiveStageInBranch[techBranchID] = TechnologyContainer.TechnologyConfig.TechBranches[techBranchID].TechBranch.Stages[stageID - 1];
            for (int i = 0; i < stageID; i++)
            {
                bool next = false;
                for (int j = 0; j < disposable.Count; j++)
                {
                    if (disposable[j].brange == techBranchID && disposable[j].stage == stageID)
                    {
                        next = true;
                        break;
                    }
                }
                if (next) continue;
                BaseBonus baseBonus = TechnologyContainer.TechnologyConfig.TechBranches[techBranchID].TechBranch.Stages[i].StageBonus;
                baseBonus.Apply();
                if (baseBonus.disposable)
                    disposable.Add(new BrancheStage { brange = techBranchID, stage = stageID });
            }
        }

        private void LvlUpCheck(int techBranchID)
        {
            int expNeeded;
            if (CurrentActiveStageInBranch[techBranchID] == TechnologyContainer.Default)
            {
                expNeeded = ExpNeededToLvlUp[0];
            }
            else
            {
                if (ExpNeededToLvlUp.Length > CurrentActiveStageInBranch[techBranchID].StageID)
                    expNeeded = ExpNeededToLvlUp[CurrentActiveStageInBranch[techBranchID].StageID];
                else return;
            }

            if (ExpInBranches[techBranchID] >= expNeeded)
            {
                if (CurrentActiveStageInBranch[techBranchID].GetQuest() == null)
                {
                    MessageSystemController.RefreshRewardWindow(CurrentActiveStageInBranch[techBranchID].StageID, techBranchID);
                    ExpInBranches[techBranchID] = expNeeded;
                    SetNextStage(techBranchID);
                    /*Debug.Log(
                        $"Branch: {TechnologyContainer.TechnologyConfig.TechBranches[techBranchID].TechBranch.BranchName.name} ==> " +
                        $"Exp : {ExpInBranches[TechnologyContainer.TechnologyConfig.TechBranches[techBranchID].ID]}");*/
                }
                else
                {
                    ExpInBranches[techBranchID] = expNeeded;
                    // TODO: Push quest to quest controller;
                    QuestController.StartTechQuest(TechnologyContainer.TechnologyConfig.TechBranches[techBranchID]
                        .TechBranch.Stages[CurrentActiveStageID[techBranchID]].GetQuest());
                    Debug.Log("Need Quest to continue");
                }
            }
        }

        public void SetQuestDone(int techBranchID) => SetNextStage(techBranchID);

        #region SaveLoadHandler
        private void Save()
        {
            SaveAndLoad.Save(_saveExpInBranchData.name, _saveExpInBranchData.ToConvert(ExpInBranches));
            SaveAndLoad.Save(_saveActiveStageData.name, _saveActiveStageData.ToConvert(CurrentActiveStageID));
            SaveAndLoad.Save(_saveDisposableStageData.name, _saveDisposableStageData.ToConvert(disposable));
        }

        private void Load()
        {
            string str = SaveAndLoad.Load(_saveActiveStageData.name);
            if (str == "") return;
            var stageSetter = _saveActiveStageData.FromConvert(str);
            str = SaveAndLoad.Load(_saveDisposableStageData.name);
            if (str != "") disposable = _saveDisposableStageData.FromConvert(str);
            foreach (var item in stageSetter)
            {
                CurrentActiveStageID[item.Key] = item.Value;
                SetStage(item.Key, item.Value);

            }
            str = SaveAndLoad.Load(_saveExpInBranchData.name);
            if (str == "") return;
            ExpInBranches = _saveExpInBranchData.FromConvert(str);
        }
        #endregion

    }
}