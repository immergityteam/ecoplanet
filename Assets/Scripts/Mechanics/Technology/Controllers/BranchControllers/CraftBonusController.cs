using System.Collections.Generic;
using System.Linq;
using UnityDI;

namespace ColLobOK
{
    public class CraftBonusController : ICraftBonusController, IDependent
    {
        [Dependency] public IContainerRecipes ContainerRecipes { private get; set; }
        [Dependency] public IContainerRecipesMono ContainerRecipesMono { private get; set; }

        //ContainerBlockedRecepies;
        public List<RecipeSO> blockedRecipes;
        public Dictionary<int, float> RecipeMultipliersByIDData = new Dictionary<int, float>();
        public Dictionary<int, float> BuildingMultiplierByIDData = new Dictionary<int, float>();

        public void OnInjected()
        {
            blockedRecipes = ContainerRecipesMono.BlockedRecipeSO.ToList();
        }
        
        public void AddBonusRecipe(RecipeSO recipe, float multiplier)
        {
            if (!RecipeMultipliersByIDData.ContainsKey(recipe.id))
            {
                RecipeMultipliersByIDData.Add(recipe.id, multiplier);
            }
            else
            {
                RecipeMultipliersByIDData[recipe.id] += multiplier;
            }
        }

        public void AddBonusBuilding(int ID, float multiplier)
        {
            if (!BuildingMultiplierByIDData.ContainsKey(ID))
            {
                BuildingMultiplierByIDData.Add(ID, multiplier);
            }
            else
            {
                BuildingMultiplierByIDData[ID] += multiplier;
            }
        }

        public void UnblockRecipe(int id)
        {
            foreach (var item in ContainerRecipesMono.RecipeSO)
            {
                if (item.id == id)
                {
                    if (ContainerRecipesMono.BlockedRecipeSO.Contains(item))
                    {
                        blockedRecipes.Remove(item);
                    }
                }
            }
        }

        public void ReplaceRecipe(RecipeSO bonusRecipe,RecipeSO recipeToReplace)
        {
            ContainerRecipes.ReplaceRecipe(recipeToReplace.id, bonusRecipe);
        }

        

        private float GetRecipeMultiplier(int recipeID)
        {
            return RecipeMultipliersByIDData.ContainsKey(recipeID) ? RecipeMultipliersByIDData[recipeID] : 0.0f;
        }

        private float GetBuildingMultiplier(int buildingID)
        {
            return BuildingMultiplierByIDData.ContainsKey(buildingID) ? RecipeMultipliersByIDData[buildingID] : 0.0f;
        }


        #region Kostyan look here!!11

        public float GetMultiplier(int buildingID, int recipeID)
        {
            return 1.0f - GetRecipeMultiplier(recipeID) - GetBuildingMultiplier(buildingID);
        }

        public bool CheckIfRecipeIsUnblocked(int id)
        {
            //for containerblockedrecipes
            //if !contains,return true;
            //if contains return false

            foreach (var item in blockedRecipes)
            {
                if (item.id == id)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion
    }

    public interface ICraftBonusController
    {
        void AddBonusBuilding(int ID, float multiplier);
        void AddBonusRecipe(RecipeSO recipe, float multiplier);
        float GetMultiplier(int buildingID, int recipeID);
        bool CheckIfRecipeIsUnblocked(int id);
        void UnblockRecipe(int id);
        void ReplaceRecipe(RecipeSO bonusRecipe, RecipeSO recipeToReplace);
    }
}