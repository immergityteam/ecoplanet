using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class BusinessBonusController : IBusinessBonusController
    {

        Dictionary<int, float> idStructurMultiplierTime = new Dictionary<int, float>();
        Dictionary<int, int> idStructurUpPrestige = new Dictionary<int, int>();
        List<int> idStructureUnlockMaxPrestige = new List<int>();

        public void AddMultiplierTimeStructure(int id, float scale)
        {
            if (idStructurMultiplierTime.ContainsKey(id))
            {
                idStructurMultiplierTime[id] += scale;
            }
            else
            {
                idStructurMultiplierTime.Add(id, scale);
            }
        }

        public float GetMultiplierTimeStructure(int id)
        {
            if (idStructurMultiplierTime.ContainsKey(id))
                return 1 + idStructurMultiplierTime[id];
            return 1;
        }

        public void AddPrestigeStructure(int id, int count)
        {
            if (idStructurUpPrestige.ContainsKey(id))
            {
                idStructurUpPrestige[id] += count;
            }
            else
            {
                idStructurUpPrestige.Add(id, count);
            }
        }
        public void UnlockMaxPrestigeBusinnes(int id)
        {
            idStructureUnlockMaxPrestige.Add(id);
        }

        public int GetDeltaMaxPrestige(int id)
        {
            if (idStructureUnlockMaxPrestige.Contains(id))
                return int.MaxValue;
            return 0;
        }
    }

    public interface IBusinessBonusController
    {
        float GetMultiplierTimeStructure(int id);
        void AddMultiplierTimeStructure(int id, float scale);
        void AddPrestigeStructure(int iD, int count);
        void UnlockMaxPrestigeBusinnes(int id);
        int GetDeltaMaxPrestige(int id);
    }
}