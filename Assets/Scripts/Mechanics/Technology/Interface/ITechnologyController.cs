﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface ITechnologyController
    {
        UnityAction<int, int> AddExpierence { get; set; }
        UnityAction<int> CheckIfLevelUp { get; set; }
        StageSO GetCurrentStage(int techBranchID);
        void AddExpToBranch(int branchID, int expAmount);
        int GetCurrentMaxExp(int branchID);
        int GetCurrentBranchExpAmount(int branchID);

        int GetCurrentStageOrderCount(int branchID);

        void SetQuestDone(int techBranchID);
        UnityAction<int> ExpAdded { get; set; }
        UnityAction<int> StageAdded { get; set; }
        StageSO GetFirstStage(int techBranchID);
        StageSO[] GetStages(int branchID);
    }
}