﻿namespace ColLobOK
{
    public interface ITechnologyConfigContainer
    {
        TechnologyMainConfig TechnologyConfig { get; }
        StageSO Default { get; }
    }
}