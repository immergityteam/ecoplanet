﻿using UnityEngine;

namespace ColLobOK
{
    public class TecnologyConfigContainer : MonoBehaviour, ITechnologyConfigContainer
    {
        [field: SerializeField] public TechnologyMainConfig TechnologyConfig { get; private set; }
        [field: SerializeField] public StageSO Default { get; private set; }
    }
}