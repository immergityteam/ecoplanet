﻿using System;
using UnityEngine;

namespace ColLobOK
{
    [Serializable]
    [CreateAssetMenu(fileName = "TechnologyStage", menuName = "SO/TechnologyStage", order = 1)]
    public class StageSO : ScriptableObject
    {
        public int StageID;
        public StringsSO StageDescription;
        public BaseBonus StageBonus;
        
        //TODO: Unique effects here;
        // public int AddSubAmount;
        // public float MultipleCoeff;
        // public bool[] Permissions;
        public TechQuest QuestToLvlUp;

        // public bool GetPermission(int index) => Permissions[index];
        // public float GetMultipleCoeff() => MultipleCoeff;
        // public int GetAddSubAmount() => AddSubAmount;
        public TechQuest GetQuest() => QuestToLvlUp;
    }
}