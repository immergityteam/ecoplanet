using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "PlayerEnergyAmountBonus", menuName = "Config/Bonuses/PlayerEnergyAmount", order =1)]
    public class PlayerEnergyBonus : BaseBonus
    {
        [SerializeField] public int BonusAmount;

        public override void Apply()
        {
            GameStarter.container.Resolve<IEnergyController>().AddMaxEnergy(BonusAmount);
        }
    }
}
