using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "SpeedUpBonusStructureTime", menuName = "Config/Bonuses/SpeedUpStructureTime")]
    public class SpeedUpBuildingTimeBonus : BaseBonus
    {
        [SerializeField] private StructureData[] Structure;
        [SerializeField] public float MultiplyCoeffitient;

        public override void Apply()
        {
            var businnesBonusController = GameStarter.container.Resolve<IBusinessBonusController>();
            for (int i = 0; i < Structure.Length; i++)
            {
                businnesBonusController.AddMultiplierTimeStructure(Structure[i].ID, MultiplyCoeffitient);
            }
        }
    }
}
