using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BaseBonus", menuName = "Config/BaseBonus", order = 1)]
    public class BaseBonus : ScriptableObject
    {
        public bool disposable;
        public virtual void Apply()
        {
            Debug.Log("Temp bonus applied");
        }
    }
}
