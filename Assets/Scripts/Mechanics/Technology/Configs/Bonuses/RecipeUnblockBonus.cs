﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "RecipeUnblockBonus", menuName = "Config/Bonuses/RecipeUnblock", order =1)]
    public class RecipeUnblockBonus : BaseBonus
    {
        [SerializeField] public RecipeSO RecipeToUnblock;

        public override void Apply()
        {
            var craftBonusController = GameStarter.container.Resolve<ICraftBonusController>();
            craftBonusController.UnblockRecipe(RecipeToUnblock.id);
        }
    }
}