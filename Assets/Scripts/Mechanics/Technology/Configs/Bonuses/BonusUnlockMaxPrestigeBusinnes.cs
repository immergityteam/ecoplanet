﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BonusUnlockMaxPrestigeBusinnes", menuName = "Config/Bonuses/UnlockMaxPrestigeBusinnes")]
    public class BonusUnlockMaxPrestigeBusinnes : BaseBonus
    {
        public BusinessSO[] businnes;

        public override void Apply()
        {
            for (int i = 0; i < businnes.Length; i++)
            {
                GameStarter.container.Resolve<IBusinessBonusController>().UnlockMaxPrestigeBusinnes(businnes[i].ID);
            }
        }
    }
}
