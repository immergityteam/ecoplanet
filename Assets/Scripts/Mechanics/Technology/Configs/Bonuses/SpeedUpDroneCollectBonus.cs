using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "DroneSpeedUpBonus", menuName = "Config/Bonuses/DroneSpeedUp", order =1)]
    public class SpeedUpDroneCollectBonus : BaseBonus
    {
        [SerializeField] public float MultiplierCoeffitient;

        public override void Apply()
        {
            Debug.Log($"Drone bonus applied - multilier: {MultiplierCoeffitient}");
            //БОнус к скорости дрона тут
        }
    }
}
