﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "RecipeReplaceBonus", menuName = "Config/Bonuses/RecipeReplace", order =1)]
    public class ReplaceRecipeBonus : BaseBonus
    {
        [SerializeField] public RecipeSO[] BonusRecipe;
        [SerializeField] public RecipeSO[] RecipeToReplace;

        public override void Apply()
        {
            var craftBonusController = GameStarter.container.Resolve<ICraftBonusController>();
            for (int i = 0; i < RecipeToReplace.Length; i++)
            {
                craftBonusController.ReplaceRecipe(BonusRecipe[i],RecipeToReplace[i]);
            }
        }
    }
}