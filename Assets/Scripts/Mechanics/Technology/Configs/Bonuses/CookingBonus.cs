﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "CookingBonus", menuName = "Config/Bonuses/CookingBonus", order =1)]
    public class CookingBonus : BaseBonus
    {
        [SerializeField] public float BonusAmount;
        public override void Apply()
        {
            Debug.Log($"Cooking bonus applied - bonus amount: {BonusAmount}");
        }
    }
}