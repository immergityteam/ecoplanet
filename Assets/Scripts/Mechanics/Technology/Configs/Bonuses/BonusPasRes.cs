﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BonusPasRes", menuName = "Config/Bonuses/PasRes")]
    public class BonusPasRes : BaseBonus
    {
        public Resource resource;

        public override void Apply()
        {
            GameStarter.container.Resolve<IPassiveResourcesController>().ChangePasRes(resource.ID, resource.count, 0);
        }
    }
}
