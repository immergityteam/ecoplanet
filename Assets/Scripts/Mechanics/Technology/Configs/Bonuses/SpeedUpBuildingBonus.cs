
using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BuildingSpeedUpBonus", menuName = "Config/Bonuses/BuildingSpeedUp", order =1)]
    public class SpeedUpBuildingBonus : BaseBonus
    {
        [SerializeField] public StructureData[] BuildingStructureData;
        [SerializeField] public float MultiplierCoeffitient;

        public override void Apply()
        {
            var craftBonusController = GameStarter.container.Resolve<ICraftBonusController>();
            foreach (var structureData in BuildingStructureData)
            {
                craftBonusController.AddBonusBuilding(structureData.Name.GetID(),MultiplierCoeffitient);
            }
        }
    }
}
