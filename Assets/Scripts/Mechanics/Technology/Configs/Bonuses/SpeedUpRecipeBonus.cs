

using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "SpeedUpBonus", menuName = "Config/Bonuses/SpeedUp", order =1)]
    public class SpeedUpRecipeBonus : BaseBonus
    {
        [SerializeField] private RecipeSO Recipe;
        [SerializeField] public float MultiplyCoeffitient;

        public override void Apply()
        {
            var craftBonusController = GameStarter.container.Resolve<ICraftBonusController>();
            craftBonusController.AddBonusRecipe(Recipe, MultiplyCoeffitient);
        }
    }
}
