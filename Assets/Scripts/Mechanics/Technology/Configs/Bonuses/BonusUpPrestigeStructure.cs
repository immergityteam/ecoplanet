﻿using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BonusUpPrestigeStructure", menuName = "Config/Bonuses/UpPrestigeStructure")]
    public class BonusUpPrestigeStructure : BaseBonus
    {
        public StructureData[] structureData;
        public int count;

        public override void Apply()
        {
            for (int i = 0; i < structureData.Length; i++)
            {
                if (GameStarter.CrutchLoading)
                    GameStarter.container.Resolve<IPassiveResourcesController>().Rollback(structureData[i].ID);
                GameStarter.container.Resolve<IBusinessBonusController>().AddPrestigeStructure(structureData[i].ID, count);
                if (GameStarter.CrutchLoading)
                    GameStarter.container.Resolve<IPassiveResourcesController>().Recalculate(structureData[i].ID);
            }
        }
    }
}
