using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "EcologyPermissionBonus", menuName = "Config/Bonuses/EcologyPermission", order =1)]
    public class EcologyPermissionBonus : BaseBonus
    {
        public int Stage;
        
        public override void Apply()
        {
            var TrashController = GameStarter.container.Resolve<ITrashController>();
            if (!TrashController.Permissions.ContainsKey(Stage))
            {
                TrashController.Permissions.Add(Stage, true);
              
            }
            else
            {
                  TrashController.Permissions[Stage] = true;
            }
        }
    }
}
