﻿using System;
using UnityEngine;

namespace ColLobOK
{
    [Serializable]
    public class BranchData
    {
        public int ID;
        public TechnologyBranchSO TechBranch;
    }
    [CreateAssetMenu(fileName = "TechnologyMainConfig", menuName = "SO/TechnologyMainConfig", order = 1)]
    public class TechnologyMainConfig : ScriptableObject
    {
        public BranchData[] TechBranches;
    }
}