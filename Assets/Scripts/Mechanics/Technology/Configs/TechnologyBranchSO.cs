using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "TechnologyBranch", menuName = "SO/TechnologyBranch", order = 1)]
    public class TechnologyBranchSO : ScriptableObject
    {
        public StringsSO BranchName;
        public Sprite Girl;
        public Sprite Icon;
        public StageSO[] Stages;
    }
}
