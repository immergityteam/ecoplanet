﻿using System;
using System.Linq;
using ColLobOK.Mechanics.Tutorial.Config;
using ColLobOK.Mechanics.Tutorial.Interface;
using DG.Tweening;
using UnityDI;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace ColLobOK.Mechanics.Tutorial.Controller
{
    public class TutorialController : ITutorialController, IDependent
    {
        [Dependency] public ITutorialContainerMono TutorialContainer { private get; set; }
        [Dependency] public IQuestController QuestController { private get; set; }
        [Dependency] public ITutorialUIView TutorialUIView { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public IBuilderUI BuilderUI { private get; set; }
        
        private TutorialBlockSO _currentTutorialBlock;
        private TutorialBlockStage _currentTutorialBlockStage;
        private Action SetNextMessage;
        public void OnInjected()
        {
            TutorialUIView.Window.SetActive(false);
            Debug.LogWarning($"{this.GetType().FullName} class Injected");
            _currentTutorialBlock = null;

            QuestController.QuestBegun += StartTutorialBlock;
        }
        
        public void Update(float deltaTime)
        {
            
#if UNITY_EDITOR

            if (Input.GetKeyDown(KeyCode.A))
            {
                StartTutorialBlock(240);
            }

#endif
                     
            if(_currentTutorialBlockStage == null) return;
            if (_currentTutorialBlockStage.NeedToScrollAround)
            {
                if (Mathf.Abs(MyInput.DeltaPoint.sqrMagnitude) > TutorialUIView.Delta*TutorialUIView.Delta && MyInput.GetPressed())
                {
                    SetNextMessage?.Invoke();
                }
            }
           
        }

        public void StartTutorialBlock(int QuestID)
        {
            //Debug.Log(QuestID);
            if (TutorialContainer.TutorialConfig.GetTutorialBlock(QuestID) != null)
            {
                if (_currentTutorialBlock == null)
                {
                    _currentTutorialBlock = TutorialContainer.TutorialConfig.GetTutorialBlock(QuestID);
                    _currentTutorialBlockStage = _currentTutorialBlock.TutorialParts.First();
                    ShowTutorialMessage(_currentTutorialBlockStage.TutorialMessage);
                    if (_currentTutorialBlockStage.SimpleNextToContinue)
                    {
                        TutorialUIView.Blackbox.BoxClicked += () =>GetToNextMessage(_currentTutorialBlock);
                    }
                }
            }
        }

        private UnityAction test;
        
        private void GetToNextMessage(TutorialBlockSO TutorialBlock)
        {
            var currentStage = TutorialBlock.TutorialParts
                .SkipWhile(x => !x.Equals(_currentTutorialBlockStage))
                .Skip(1)
                .First();
            _currentTutorialBlockStage = currentStage;
            if (currentStage != TutorialBlock.TutorialParts.Last())
            {
                if (currentStage.SimpleNextToContinue)
                {
                    ShowTutorialMessage(currentStage.TutorialMessage);
                }

                if (_currentTutorialBlockStage.NeedToScrollAround)
                { 
                    DismissMessage();
                    SetNextMessage += () =>
                    ShowTutorialMessage(currentStage.TutorialMessage);
                }

                if (_currentTutorialBlockStage.NeedToOpenBuildMenu)
                {
                    DismissMessage();
                    test = () => ShowTutorialMessage(currentStage.TutorialMessage);
                    BuilderUI.BuildButton.onClick.AddListener(test);
                }
            }
            else
            {
                if (currentStage.SimpleNextToContinue)
                {
                    if (currentStage.ShowResPanel)
                    {
                        TutorialUIView.HandResourcePanel.SetActive(true);
                        var handTween = TutorialUIView.HandResourcePanel.gameObject.transform
                            .DOScale(Vector3.one * 0.5f, 0.5f)
                            .SetLoops(-1, LoopType.Yoyo)
                            .SetEase(Ease.Flash);
                        handTween.Play();
                        var seq = DOTween.Sequence();
                        seq.AppendInterval(5.0f)
                            .onComplete += () =>
                        {
                            handTween.Kill();
                            TutorialUIView.HandResourcePanel.SetActive(false);
                            seq.Kill();

                        };
                    }
                    TutorialUIView.Blackbox.BoxClicked = null;
                    TutorialUIView.Blackbox.BoxClicked += EndTutorialBlock;
                }
                ShowTutorialMessage(currentStage.TutorialMessage);
            }
        }

        private void ShowTutorialMessage(StringsSO message)
        {
            //Проверка условий показа следующего сообщения
            TutorialUIView.Window.SetActive(true);
            TutorialUIView.MessageText.text = message.GetString();
            SetNextMessage = null;
            if (test != null)
            {
                 BuilderUI.BuildButton.onClick.RemoveListener(test);
            }
            //Debug.Log(message.GetString());
            
        }

        private void DismissMessage()
        {
            TutorialUIView.Window.SetActive(false);
        }

        private void EndTutorialBlock()
        {
            //выключить сообщение
            DismissMessage();
            TutorialUIView.Blackbox.BoxClicked = null;
            _currentTutorialBlock = null;
            _currentTutorialBlockStage = null;
        }
    }
}