using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BlackboxView : MonoBehaviour, IPointerClickHandler
{
    public UnityAction BoxClicked;

    public void OnPointerClick(PointerEventData eventData)
    {
        SimpleClickToNextStage();
    }

    public void SimpleClickToNextStage()
    {
        BoxClicked?.Invoke();
    }
}
