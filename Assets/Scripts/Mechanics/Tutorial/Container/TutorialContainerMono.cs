﻿using ColLobOK.Mechanics.Tutorial.Config;
using ColLobOK.Mechanics.Tutorial.Interface;
using UnityEngine;

namespace ColLobOK.Mechanics.Tutorial.Container
{
    public class TutorialContainerMono : MonoBehaviour, ITutorialContainerMono
    {
        [field: SerializeField] public MainTutorialConfig TutorialConfig { get; private set; }
    }
}