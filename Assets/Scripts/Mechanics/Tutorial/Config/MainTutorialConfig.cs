﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ColLobOK.Mechanics.Tutorial.Config
{
    [Serializable]
    public struct QuestTutorialBlock
    {
        public StringsSO QuestID;
        public TutorialBlockSO TutorialBlock;
    }
    
    [CreateAssetMenu(fileName = "MainTutorialConfig", menuName = "Config/Tutorial/MainTutorial", order = 1)]
    public class MainTutorialConfig : ScriptableObject
    {
        public List<QuestTutorialBlock> TutorialBlocks;

        public TutorialBlockSO GetTutorialBlock(int questID)
        {
            return (from item in TutorialBlocks where item.QuestID.GetID() == questID select item.TutorialBlock).FirstOrDefault();
        }
    }
}