﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK.Mechanics.Tutorial.Config
{
    [Serializable]
    public class TutorialBlockStage
    {
        public StringsSO TutorialMessage;
        public bool SimpleNextToContinue;
        public bool NeedToOpenBuildMenu;
        public bool ShowResPanel;
        public bool NeedToScrollAround;
    }
    [CreateAssetMenu(fileName = "TutorialBlock", menuName = "Config/Tutorial/TutorialBlock", order = 1)]
    public class TutorialBlockSO : ScriptableObject
    {
        public List<TutorialBlockStage> TutorialParts;
    }
}