using ColLobOK.Mechanics.Tutorial.Interface;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ColLobOK
{
    public class TutorialWindowUIView : MonoBehaviour, ITutorialUIView
    {
        [field: SerializeField] public BlackboxView Blackbox { get; private set; }
        [field: SerializeField] public GameObject Window { get; private set; }
        [field: SerializeField] public TMP_Text MessageText { get; private set; }
        [field: SerializeField] public Button SimpleNext { get; set; }
        [field: SerializeField] public float Delta { get; set; }
        [field: SerializeField] public GameObject HandResourcePanel { get; set; }
    }
}
