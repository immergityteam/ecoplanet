﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK.Mechanics.Tutorial.Interface
{
    public interface ITutorialUIView
    {
        BlackboxView Blackbox { get; }
        GameObject Window { get; }
        TMP_Text MessageText { get; }
        Button SimpleNext { get; set; }
        float Delta { get; set; }
        GameObject HandResourcePanel { get; set; }
    }
}