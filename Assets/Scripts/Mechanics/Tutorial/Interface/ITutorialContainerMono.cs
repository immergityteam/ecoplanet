﻿using ColLobOK.Mechanics.Tutorial.Config;

namespace ColLobOK.Mechanics.Tutorial.Interface
{
    public interface ITutorialContainerMono
    {
        MainTutorialConfig TutorialConfig { get; }
    }
}