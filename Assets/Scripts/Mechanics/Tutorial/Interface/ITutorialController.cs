﻿namespace ColLobOK.Mechanics.Tutorial.Interface
{
    public interface ITutorialController
    {
        void StartTutorialBlock(int QuestID);
        void Update(float deltaTime);
    }
}