using System;
using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class EnergyController : IEnergyController, IDependent
    {
        [Serializable] public struct SaveData
        {
            public int count;
            public int countMax;
            public int tik;
        }

        [Dependency] public ITiker Tiker { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IShopController ShopController { private get; set; }
        [Dependency] public IMyTime MyTime { private get; set; }
        [Dependency] public ICommonLines CommonLines { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }

        SaveData data;
        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            SaveAndLoad.EvLoadOnFocus += Load;
            Tiker.SetTikerTime(1);
            Tiker.EvTickSec += Tik;
            data.countMax = ContainerMono.CountMaxEnergy;
            data.count = data.countMax;
            data.tik = ContainerMono.TimeRechargeEnergy;

            Load();
            UpdateUI();
        }

        public bool TakeEnergy(int val = 1)
        {
            if (data.count - val < 0)
            {
                ShopController.OpenEnergy();
                GameUI.CanselMining.onClick.Invoke();
                MessageSystemController.ShowPopUpWindow(CommonLines.NeedEnergy);
                return false;
            }
            data.count -= val;
            UpdateUI();
            return true;
        }

        public void AddEnergy(int val = 1)
        {
            data.count += val;
            UpdateUI();
        }

        public void AddMaxEnergy(int val)
        {
            data.countMax += val;
            UpdateUI();
        }

        private void Tik()
        {
            data.tik -= 1;
            if (data.tik <= 0)
            {
                if (data.count < data.countMax) AddEnergy();
                else Tiker.StopTiker();
                data.tik = ContainerMono.TimeRechargeEnergy;
            }
        }

        private void UpdateUI()
        {
            if (data.count < data.countMax) Tiker.StartTiker();
            GameUI.Energy.text = data.count.ToString();
        }

        ConvertDataBinary<SaveData> saveData = new ConvertDataBinary<SaveData>("EnergyController");
        private void Save()
        {
            SaveAndLoad.Save(saveData.name, saveData.ToConvert(data));
        }

        private void Load()
        {
            string save = SaveAndLoad.Load(saveData.name);
            if (save != "")
            {
                data = saveData.FromConvert(save);
                if (data.count < data.countMax)
                {
                    int count = (int)((MyTime.GetDeltaTime() + data.tik) / ContainerMono.TimeRechargeEnergy);
                    data.count += count;
                    if (data.count < data.countMax)
                    {
                        data.tik = (int)((MyTime.GetDeltaTime() + data.tik) % ContainerMono.TimeRechargeEnergy);
                    }
                    else
                    {
                        data.count = data.countMax;
                        data.tik = ContainerMono.TimeRechargeEnergy;
                    }
                }
            }
        }
    }
}