﻿namespace ColLobOK
{
    public interface IEnergyController
    {
        bool TakeEnergy(int val = 1);
        void AddEnergy(int val = 1);
        void AddMaxEnergy(int val);
    }
}