using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "Block", menuName = "SO/Tutorial/Block")]
    public class TutorialBlock : ScriptableObject
    {
        [SerializeField] private TutorialStepBase[] steps;

        UnityAction EvEndBlock;
#if UNITY_EDITOR
        [SerializeField] private int debag;
#endif

        IMyMetrica myMetrica;

        int nowStep;

        public void Activation(UnityAction callBack, string data = "")
        {
            EvEndBlock = callBack;
            myMetrica = GameStarter.container.Resolve<IMyMetrica>();
            if (data == "")
            {
                nowStep = 0;
#if UNITY_EDITOR
                nowStep = debag;
#endif
                steps[nowStep].Activation(NextStep);
            }
            else
            {
                string[] datas = data.Split(':');
                nowStep = int.Parse(datas[0]);

                steps[nowStep].Activation(NextStep);
                if (nowStep != int.Parse(datas[0])) return;
                if (datas.Length > 1) steps[nowStep].Load(datas[1]);
            }
        }

        private void NextStep()
        {
            steps[nowStep].Deactivation();
            myMetrica.ReportTutorial(name, steps[nowStep].name);
            nowStep++;
            if (nowStep < steps.Length) steps[nowStep].Activation(NextStep);
            else EvEndBlock?.Invoke();
        }

        public string SaveStep() => nowStep + ":" + steps[nowStep].Save();
    }
}