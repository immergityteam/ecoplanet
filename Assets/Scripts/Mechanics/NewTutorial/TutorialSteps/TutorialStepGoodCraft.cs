using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepGoodCraft", menuName = "SO/Tutorial/StepGoodCraft")]

    public class TutorialStepGoodCraft : TutorialStepBase
    {
        [SerializeField] private RecipeSO recipeSO;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            GameStarter.container.Resolve<ICraftController>().EvRecipeGood += Checked;
        }

        private void Checked(int arg0)
        {
            if (recipeSO.id == arg0)
            {
                GameStarter.container.Resolve<ICraftController>().EvRecipeGood -= Checked;
                EvEndStep?.Invoke();
            }
        }
    }
}