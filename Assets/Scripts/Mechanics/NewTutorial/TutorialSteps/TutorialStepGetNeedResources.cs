using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepGetNeedResources", menuName = "SO/Tutorial/StepGetNeedResources")]
    public class TutorialStepGetNeedResources : TutorialStepBase
    {
        [SerializeField] private Resource[] needResources;
        private Resource[] minedResources;

        protected ITrashController trashController;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            //GameStarter.container.Resolve<IGameUI>().CanselMining.onClick.Invoke();
            minedResources = new Resource[needResources.Length];
            for (int i = 0; i < needResources.Length; i++)
                minedResources[i] = needResources[i];

            trashController = GameStarter.container.Resolve<ITrashController>();
            trashController.EvGarbageUnitsMined += CheckedResourse;
        }

        public override void Deactivation()
        {
            base.Deactivation();
            trashController.EvGarbageUnitsMined -= CheckedResourse;
            trashController = null;
            minedResources = null;
        }

        private void CheckedResourse(List<Resource> res)
        {

            for (int i = 0; i < res.Count; i++)
                AddRes(res[i]);

            for (int i = 0; i < minedResources.Length; i++)
                if (minedResources[i].count > 0)
                    return;

            //GameStarter.container.Resolve<IGameUI>().CanselMining.onClick.Invoke();
            EvEndStep?.Invoke();
        }

        private void AddRes(Resource res)
        {
            for (int i = 0; i < minedResources.Length; i++)
                if (res.ID == minedResources[i].ID)
                    minedResources[i].count--;
        }

        public override void Load(string str)
        {
            string[] data = str.Split('|');
            for (int i = 0; i < minedResources.Length; i++)
                minedResources[i].count = int.Parse(data[i]);
        }

        public override string Save()
        {
            string data = "";
            for (int i = 0; i < minedResources.Length; i++)
                data += minedResources[i].count + "|";
            return data;
        }
    }
}