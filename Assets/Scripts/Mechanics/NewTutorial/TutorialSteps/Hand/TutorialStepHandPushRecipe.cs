using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepHandPushRecipe", menuName = "SO/Tutorial/StepHandPush/Recipe")]
    public class TutorialStepHandPushRecipe : TutorialStepHandPush
    {
        [SerializeField] private RecipeSO recipeSO;

        IRecipeUI recipeUI;
        public override Vector3 GetPoint()
        {
            return recipeUI.StartCraft.transform.position;
        }

        public override Transform GetParent()
        {
            recipeUI = GameStarter.container.Resolve<IFactoryCraftPanel>().GetIRecipeUI(recipeSO);
            return recipeUI.StartCraft.transform;
        }

        public override void Deactivation()
        {
            base.Deactivation();
            recipeUI = null;
        }
    }
}