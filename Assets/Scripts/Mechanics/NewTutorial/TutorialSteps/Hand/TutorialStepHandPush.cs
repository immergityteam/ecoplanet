using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    //[CreateAssetMenu(fileName = "StepHandPush", menuName = "SO/Tutorial/StepHandPush")]
    public class TutorialStepHandPush : TutorialStepBase
    {
        [SerializeField] private GameObject prefabHand;
        private ICreator creator;
        protected GameObject gameObject;
        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            creator = GameStarter.container.Resolve<ICreator>();
            gameObject = creator.GetGameObject(prefabHand, GetParent());
            gameObject.transform.position = GetPoint();
        }

        public override void Deactivation()
        {
            base.Deactivation();
            creator.MyDestroy(gameObject);
            gameObject = null;
            creator = null;
        }
        public virtual Vector3 GetPoint() => Vector3.zero;
        public virtual Transform GetParent() => null;
    }
}