using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepHandPushTrash", menuName = "SO/Tutorial/StepHandPush/Trash")]
    public class TutorialStepHandPushTrash : TutorialStepHandPush
    {
        [SerializeField] private TrashData trashData;

        public override Vector3 GetPoint()
        {
            int localIdTrash = GameStarter.container.Resolve<ITrashController>().GetIdNearestTrash(trashData);
            return GameStarter.container.Resolve<ITrashsContainerMono>().AllTrash[localIdTrash].transform.position;
        }
    }
}