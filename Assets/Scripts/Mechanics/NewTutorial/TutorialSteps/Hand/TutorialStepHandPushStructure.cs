using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepHandPushStructure", menuName = "SO/Tutorial/StepHandPush/Structure")]
    public class TutorialStepHandPushStructure : TutorialStepHandPush
    {
        [SerializeField] private StructureData structureData;

        public override Vector3 GetPoint() =>
            GameStarter.container.Resolve<IBuilderPlayer>().GetPositionFirstStructureGlobal(structureData.ID);
    }
}