using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepButtonCraft", menuName = "SO/Tutorial/StepButton/Craft")]

    public class TutorialStepButtonCraft : TutorialStepButton
    {
        [SerializeField] private RecipeSO recipeSO;

        public override void SetButton()
        {
            IRecipeUI recipeUI = GameStarter.container.Resolve<IFactoryCraftPanel>().GetIRecipeUI(recipeSO);
            recipeUI.GameObject.transform.SetAsFirstSibling();
            button = recipeUI.StartCraft;
        }
    }
}