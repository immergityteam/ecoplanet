using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    //[CreateAssetMenu(fileName = "StepClikedGameUI", menuName = "SO/Tutorial/StepClikedGameUI")]

    public class TutorialStepButton : TutorialStepBase
    {

        [SerializeField] private Vector3 correction;
        [SerializeField] private bool updatePosition;

        INewTutorialUI newTutorialUI;
        protected Button button;

        ICreator creator;
        IEnumerator coroutine;
        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);

            newTutorialUI = GameStarter.container.Resolve<INewTutorialUI>();
            newTutorialUI.Blackout.SetActive(true);

            SetButton();
            if (updatePosition)
            {
                creator = GameStarter.container.Resolve<ICreator>();
                coroutine = UpdatePosition();
                creator.StartCourutine(coroutine);
            }
            else
            {
                newTutorialUI.HighlightedArea.transform.position = button.transform.position + correction;
            }

            button.onClick.AddListener(()=>EvEndStep?.Invoke());
        }

        public override void Deactivation()
        {
            base.Deactivation();
            button.onClick.RemoveListener(() => EvEndStep?.Invoke());
            newTutorialUI.HighlightedArea.transform.position = Vector3.zero;
            newTutorialUI.Blackout.SetActive(false);
            if (updatePosition)
            {
                creator.StopCourutine(coroutine);
                coroutine = null;
                creator = null;
            }
            button = null;
            newTutorialUI = null;
        }

        public virtual void SetButton() {}

        private IEnumerator UpdatePosition()
        {
            while (true)
            {
                yield return null;
                newTutorialUI.HighlightedArea.transform.position = button.transform.position + correction;
            }
        }
    }
}