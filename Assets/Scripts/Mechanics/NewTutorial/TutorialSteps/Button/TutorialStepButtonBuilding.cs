using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepButtonBuildPanel", menuName = "SO/Tutorial/StepButton/BuildPanel")]

    public class TutorialStepButtonBuilding : TutorialStepBase
    {

        [SerializeField] private StringsSO namePanel;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            GameStarter.container.Resolve<IGenerationBuilderUIPanels>().EvSelectPanel += Cheked;
        }

        public override void Deactivation()
        {
            base.Deactivation();
            GameStarter.container.Resolve<IGenerationBuilderUIPanels>().EvSelectPanel -= Cheked;
        }

        private void Cheked(int arg0)
        {
            if (arg0 == namePanel.ID)
            {
                EvEndStep?.Invoke();
            }
        }
    }
}