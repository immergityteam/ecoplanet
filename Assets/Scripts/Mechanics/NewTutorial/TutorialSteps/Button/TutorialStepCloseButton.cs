using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepCloseButton", menuName = "SO/Tutorial/StepButton/Close")]

    public class TutorialStepCloseButton : TutorialStepButton
    {
        [Serializable]
        enum Target
        {
            BuildStructures,
            Warehouse,
            Craft,
            Settings,
            Business,
            Quests,
            Technology,
            Shop,
            Accept
        }

        [SerializeField] private Target target;

        public override void SetButton()
        {
            button = target switch
            {
                Target.BuildStructures => GameStarter.container.Resolve<IBuilderWindow>().Close,
                Target.Warehouse => GameStarter.container.Resolve<IWarehouseUI>().Close,
                Target.Craft => GameStarter.container.Resolve<ICraftUI>().Close,
                Target.Settings => GameStarter.container.Resolve<ISettingsUI>().Close,
                Target.Business => GameStarter.container.Resolve<IBusinessUI>().Close,
                Target.Quests => GameStarter.container.Resolve<IQuestUI>().Close,
                Target.Technology => GameStarter.container.Resolve<ITechnologyUIView>().Close,
                Target.Shop => GameStarter.container.Resolve<IShopUI>().Close,
                Target.Accept => GameStarter.container.Resolve<IRewardUI>().AcceptButton,
                _ => null,
            };
        }

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            if (!button.gameObject.activeInHierarchy) EvEndStep?.Invoke();
        }
    }
}