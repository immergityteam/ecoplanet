using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepButtonGameUI", menuName = "SO/Tutorial/StepButton/GameUI")]

    public class TutorialStepButtonGameUI : TutorialStepButton
    {
        [Serializable] enum Target
        {
            Build,
            Warehouse,
            Technologies,
            Quest,
            Profile,
            AddDonate,
            AddEnegry,
            CanselMining
        }

        [SerializeField] private Target target;
        
        public override void SetButton()
        {
            IGameUI gameUI = GameStarter.container.Resolve<IGameUI>();

            button = target switch
            {
                Target.Build => gameUI.Build,
                Target.Warehouse => gameUI.Warehouse,
                Target.Technologies => gameUI.Technologies,
                Target.Quest => gameUI.Quest,
                Target.Profile => gameUI.Profile,
                Target.AddDonate => gameUI.AddDonate,
                Target.AddEnegry => gameUI.AddEnegry,
                Target.CanselMining => gameUI.CanselMining,
                _ => null,
            };
        }
    }
}