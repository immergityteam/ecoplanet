using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepButtonBuild", menuName = "SO/Tutorial/StepButton/Build")]

    public class TutorialStepButtonBuild : TutorialStepButton
    {
        [Serializable]
        enum Target
        {
            Select,
            Apply,
            CancelMoving,
            Flip,
            BuildButton,
            MoveButton,
            DeleteButton,
            BuildCancel
        }

        [SerializeField] private Target target;

        public override void SetButton()
        {
            button = target switch
            {
                Target.Select => GameStarter.container.Resolve<IBuilderWindow>().Select,
                Target.Apply => GameStarter.container.Resolve<IBuilderUI>().Apply,
                Target.CancelMoving => GameStarter.container.Resolve<IBuilderUI>().CancelMoving,
                Target.Flip => GameStarter.container.Resolve<IBuilderUI>().Flip,
                Target.BuildButton => GameStarter.container.Resolve<IBuilderUI>().BuildButton,
                Target.MoveButton => GameStarter.container.Resolve<IBuilderUI>().MoveButton,
                Target.DeleteButton => GameStarter.container.Resolve<IBuilderUI>().DeleteButton,
                Target.BuildCancel => GameStarter.container.Resolve<IBuilderUI>().BuildCancel,
                _ => null,
            };
        }
    }
}