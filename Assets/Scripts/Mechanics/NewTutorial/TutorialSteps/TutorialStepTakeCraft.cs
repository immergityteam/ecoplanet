using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepTakeCraft", menuName = "SO/Tutorial/StepTakeCraft")]

    public class TutorialStepTakeCraft : TutorialStepBase
    {
        [SerializeField] private RecipeSO recipeSO;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            GameStarter.container.Resolve<ICraftController>().EvTakeRecipe += Checked;
        }

        private void Checked(int arg0)
        {
            if (recipeSO.id == arg0)
            {
                GameStarter.container.Resolve<ICraftController>().EvTakeRecipe -= Checked;
                EvEndStep?.Invoke();
            }
        }

    }
}