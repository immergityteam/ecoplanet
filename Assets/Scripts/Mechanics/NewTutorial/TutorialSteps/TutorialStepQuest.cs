using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepQuest", menuName = "SO/Tutorial/StepQuest")]

    public class TutorialStepQuest : TutorialStepBase
    {
        [SerializeField] private Quest quest;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            GameStarter.container.Resolve<IQuestController>().QuestEnded += Checked;
        }

        private void Checked(int arg0)
        {
            if (quest.GetId() == arg0)
            {           
                GameStarter.container.Resolve<IQuestController>().QuestEnded -= Checked;
                GameStarter.container.Resolve<IRewardUI>().AcceptButton.onClick.AddListener(CheckedButtonReward);
            }
        }
        void CheckedButtonReward()
        {
            GameStarter.container.Resolve<IRewardUI>().AcceptButton.onClick.RemoveListener(CheckedButtonReward);
            EvEndStep?.Invoke();
        }
    }
}