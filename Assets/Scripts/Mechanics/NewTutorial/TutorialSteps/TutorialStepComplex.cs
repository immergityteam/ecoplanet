using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepComplex", menuName = "SO/Tutorial/StepComplex")]
    public class TutorialStepComplex : TutorialStepBase
    {
        [Serializable] struct MyStruct
        {
            public TutorialStepBase step;
            public bool checkCallBack;
        }

        [SerializeField] private MyStruct[] steps;

        public override void Activation(UnityAction callBack)
        {
            for (int i = 0; i < steps.Length; i++)
                steps[i].step.Activation(steps[i].checkCallBack? callBack : null);
        }

        public override void Deactivation()
        {
            base.Deactivation();
            for (int i = 0; i < steps.Length; i++)
                steps[i].step.Deactivation();
        }

        public override void Load(string str)
        {
            string[] data = str.Split('$');
            for (int i = 0; i < steps.Length; i++)
                steps[i].step.Load(data[i]);
        }

        public override string Save()
        {
            string data = "";
            for (int i = 0; i < steps.Length; i++)
                data += steps[i].step.Save() + "$";
            return data;
        }
    }
}