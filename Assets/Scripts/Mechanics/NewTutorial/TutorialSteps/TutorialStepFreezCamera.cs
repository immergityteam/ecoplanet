using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepFreezCamera", menuName = "SO/Tutorial/StepCamera/Freez")]
    public class TutorialStepFreezCamera : TutorialStepBase
    {
        ICameraMoving cameraMoving;

        public override void Activation(UnityAction callBack)
        {
            cameraMoving = GameStarter.container.Resolve<ICameraMoving>();
            cameraMoving.Freezing(true);
        }

        public override void Deactivation()
        {
            cameraMoving.Freezing(false);
            cameraMoving = null;
        }
    }
}