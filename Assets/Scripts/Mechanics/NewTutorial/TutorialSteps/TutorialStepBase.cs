using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    //[CreateAssetMenu(fileName = "StepBase", menuName = "SO/Tutorial/StepBase")]
    public class TutorialStepBase : ScriptableObject
    {
        public UnityAction EvEndStep;
        public virtual void Activation(UnityAction callBack) => EvEndStep = callBack;
        public virtual void Deactivation() => EvEndStep = null;
        public virtual string Save() => null;
        public virtual void Load(string str) { }
    }
}