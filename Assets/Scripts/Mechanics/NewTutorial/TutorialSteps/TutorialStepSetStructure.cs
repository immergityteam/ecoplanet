using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepSetStructure", menuName = "SO/Tutorial/StepSetStructure")]

    public class TutorialStepSetStructure : TutorialStepBase
    {
        [SerializeField] private StructureData structureData;
        [SerializeField] private bool timerEnd;

        IBuilderPlayer builderPlayer;
        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            builderPlayer = GameStarter.container.Resolve<IBuilderPlayer>();
            if (timerEnd)
                builderPlayer.EvBuiltConstructionTimerOff += Checked;
            else
                builderPlayer.EvBuiltConstruction += Checked;
        }

        public override void Deactivation()
        {
            base.Deactivation();
            if (timerEnd)
                builderPlayer.EvBuiltConstructionTimerOff -= Checked;
            else
                builderPlayer.EvBuiltConstruction -= Checked;
            builderPlayer = null;
        }
        private void Checked(int arg0)
        {
            if (builderPlayer.GetGlobalID(arg0) == structureData.ID)
                EvEndStep?.Invoke();
        }
    }
}