using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepClikingStructure", menuName = "SO/Tutorial/StepClikingStructure")]
    public class TutorialStepClikingStructure : TutorialStepBase
    {
        [SerializeField] private StructureData structure;

        private IBuilderPlayer builderPlayer;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            builderPlayer = GameStarter.container.Resolve<IBuilderPlayer>();
            builderPlayer.EvClikedStructures += Cliked;
        }

        private void Cliked(int arg0)
        {
            if (builderPlayer.GetGlobalID(arg0) == structure.ID)
            {
                EvEndStep?.Invoke();
            }
        }

        public override void Deactivation()
        {
            base.Deactivation();
            builderPlayer.EvClikedStructures -= Cliked;
            builderPlayer = null;
        }
    }
}