using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepPhone", menuName = "SO/Tutorial/StepPhone")]

    public class TutorialStepPhone : TutorialStepBase
    {
        [SerializeField] private float time;

        ICreator creator;
        INewTutorialUI newTutorialUI;

        IEnumerator coroutine;
        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);

            newTutorialUI = GameStarter.container.Resolve<INewTutorialUI>();
            newTutorialUI.PhoneCall.SetActive(true);
            creator = GameStarter.container.Resolve<ICreator>();
            coroutine = Phone(time);
            creator.StartCourutine(coroutine);
        }

        public override void Deactivation()
        {
            base.Deactivation();
            creator.StopCourutine(coroutine);
            newTutorialUI.PhoneCall.SetActive(false);
            creator = null;
            newTutorialUI = null;
            coroutine = null;
        }

        private IEnumerator Phone(float time)
        {
            yield return new WaitForSeconds(time);
            EvEndStep?.Invoke();
        }
    }
}