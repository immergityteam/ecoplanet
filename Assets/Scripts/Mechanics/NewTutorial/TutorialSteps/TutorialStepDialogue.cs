using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepDialogue", menuName = "SO/Tutorial/StepDialogue")]
    public class TutorialStepDialogue : TutorialStepBase
    {
        [SerializeField] private StringsSO[] phrases;
        int num;

        INewTutorialUI newTutorialUI;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            newTutorialUI = GameStarter.container.Resolve<INewTutorialUI>();
            newTutorialUI.DialogueButtonText.onClick.AddListener(NextText);
            newTutorialUI.DialoguePanel.SetActive(true);

            num = -1;
            NextText();
        }

        public override void Deactivation()
        {
            base.Deactivation();
            newTutorialUI.DialogueButtonText.onClick.RemoveListener(NextText);
            newTutorialUI.DialoguePanel.SetActive(false);
            newTutorialUI = null;
        }

        private void NextText()
        {
            num++;
            if (num < phrases.Length) newTutorialUI.DialogueText.text = phrases[num].GetString();
            else EvEndStep?.Invoke();
        }
    }
}