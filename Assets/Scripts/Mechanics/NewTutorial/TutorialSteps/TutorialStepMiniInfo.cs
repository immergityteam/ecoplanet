using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepMiniInfo", menuName = "SO/Tutorial/StepMiniInfo")]
    public class TutorialStepMiniInfo : TutorialStepBase
    {
        [SerializeField] private StringsSO phrases;

        INewTutorialUI newTutorialUI;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            newTutorialUI = GameStarter.container.Resolve<INewTutorialUI>();
            newTutorialUI.MiniInfoPanel.SetActive(true);
            newTutorialUI.MiniInfoText.text = phrases.GetString();
        }

        public override void Deactivation()
        {
            base.Deactivation();
            newTutorialUI.MiniInfoPanel.SetActive(false);
            newTutorialUI = null;
        }
    }
}