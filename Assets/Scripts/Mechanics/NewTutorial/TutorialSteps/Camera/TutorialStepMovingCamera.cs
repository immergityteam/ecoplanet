using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    //[CreateAssetMenu(fileName = "StepMovingCamera", menuName = "SO/Tutorial/StepMovingCamera")]
    public class TutorialStepMovingCamera : TutorialStepBase
    {
        [SerializeField] private bool onFreezingCamera;
        ICameraMoving cameraMoving;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            cameraMoving = GameStarter.container.Resolve<ICameraMoving>();
            if (onFreezingCamera) cameraMoving.Freezing(true);
            cameraMoving.MoveToPoint(GetPoint(), EvEndStep);
        }

        public override void Deactivation()
        {
            base.Deactivation();
            if (onFreezingCamera) cameraMoving.Freezing(false);
            cameraMoving = null;
        }

        public virtual Vector3 GetPoint() => Vector3.zero;
    }
}