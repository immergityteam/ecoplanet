using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepMovingCameraStructure", menuName = "SO/Tutorial/StepCamera/MovingStructure")]
    public class TutorialStepMovingCameraStructure : TutorialStepMovingCamera
    {
        [SerializeField] private StructureData structureData;

        public override Vector3 GetPoint() =>
            GameStarter.container.Resolve<IBuilderPlayer>().GetPositionFirstStructureGlobal(structureData.ID);
    }
}