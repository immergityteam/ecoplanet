using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "StepGetOnlyNeedResources", menuName = "SO/Tutorial/StepGetOnlyNeedResources")]
    public class TutorialStepGetOnlyNeedResources : TutorialStepGetNeedResources
    {
        [SerializeField] private TrashData trashData;

        ITrashsContainerMono trashsContainer;
        int localIdTrash;

        public override void Activation(UnityAction callBack)
        {
            base.Activation(callBack);
            trashController.SetLockMining(true);
            trashsContainer = GameStarter.container.Resolve<ITrashsContainerMono>();
            localIdTrash = trashController.GetIdNearestTrash(trashData);

            trashsContainer.AllTrash[localIdTrash].EvCliked += TrachClikedListener;
        }

        public override void Deactivation()
        {
            if (trashsContainer.AllTrash[localIdTrash])
                trashsContainer.AllTrash[localIdTrash].EvCliked -= TrachClikedListener;
            trashController.SetLockMining(false);
            localIdTrash = 0;
            trashsContainer = null;
            base.Deactivation();
        }
        private void TrachClikedListener(int id)
        {
            trashController.SetLockMining(false);
            trashController.TrachClikedListener(id);
            trashController.SetLockMining(true);
        }
    }
}