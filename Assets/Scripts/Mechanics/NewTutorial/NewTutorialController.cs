using System;
using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class NewTutorialController : INewTutorialController, IDependent
    {
        [Dependency] public INewTutorialContainer Container { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }

        List<string> nowBlock;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            Load();
            if (nowBlock.Count == 0)
                for (int i = 0; i < Container.TutorialBlocks.Length; i++)
                    nowBlock.Add("");

            for (int i = 0; i < Container.TutorialBlocks.Length; i++)
            {
                int k = i;
                if (nowBlock[i] == "GOOD") continue;
                Container.TutorialBlocks[i].Activation(() => AddBlockComplite(k), nowBlock[i]);
            }
        }

        private void AddBlockComplite(int i)
        {
            nowBlock[i] = "GOOD";
        }

        readonly SaveLoadData<List<string>> data = new SaveLoadData<List<string>>("NewTutorialController");

        private void Save()
        {
            for (int i = 0; i < Container.TutorialBlocks.Length; i++)
                nowBlock[i] = Container.TutorialBlocks[i].SaveStep();
            SaveAndLoad.Save(data.name, data.ToConvert(nowBlock));
        }

        private void Load()
        {
            nowBlock = data.FromConvert(SaveAndLoad.Load(data.name));
        }
    }
}