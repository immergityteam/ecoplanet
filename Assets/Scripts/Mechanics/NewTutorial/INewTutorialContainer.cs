﻿namespace ColLobOK
{
    public interface INewTutorialContainer
    {
        TutorialBlock[] TutorialBlocks { get; }
    }
}