using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ColLobOK
{
    public class NewTutorialContainer : MonoBehaviour, INewTutorialContainer
    {
        [field: SerializeField] public TutorialBlock[] TutorialBlocks { get; private set; }

    }
}