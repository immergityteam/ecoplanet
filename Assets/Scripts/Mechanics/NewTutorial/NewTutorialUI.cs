using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class NewTutorialUI : MonoBehaviour, INewTutorialUI
    {
        [field: Header("Parent")]
        [field: SerializeField] public GameObject ParentAll { get; private set; }

        [field: Header("DialoguePanel")]
        [field: SerializeField] public GameObject DialoguePanel { get; private set; }
        [field: SerializeField] public TMP_Text DialogueText { get; private set; }
        [field: SerializeField] public Button DialogueButtonText { get; private set; }

        [field: Header("MiniInfo")]
        [field: SerializeField] public GameObject MiniInfoPanel { get; private set; }
        [field: SerializeField] public TMP_Text MiniInfoText { get; private set; }

        [field: Header("PhoneCall")]
        [field: SerializeField] public GameObject PhoneCall { get; private set; }

        [field: Header("HighlightedArea")]
        [field: SerializeField] public GameObject Blackout { get; private set; }
        [field: SerializeField] public GameObject HighlightedArea { get; private set; }

    }
}