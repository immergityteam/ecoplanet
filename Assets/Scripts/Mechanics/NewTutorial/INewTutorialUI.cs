﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface INewTutorialUI
    {
        GameObject ParentAll { get; }
        GameObject DialoguePanel { get; }
        TMP_Text DialogueText { get; }
        Button DialogueButtonText { get; }
        GameObject MiniInfoPanel { get; }
        TMP_Text MiniInfoText { get; }
        GameObject PhoneCall { get; }
        GameObject Blackout { get; }
        GameObject HighlightedArea { get; }
    }
}