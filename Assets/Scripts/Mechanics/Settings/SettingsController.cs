using System;
using UnityDI;

namespace ColLobOK
{
    public class SettingsController : ISettingsController, IDependent
    {
        [Dependency] public ISettingsUI SettingsUI { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }

        public void OnInjected()
        {
            GameUI.Profile.onClick.AddListener(()=> SettingsUI.GameObject.SetActive(true));
            SettingsUI.Info.text = SettingsUI.InfoSSO.GetString();
            SettingsUI.Share.text = SettingsUI.ShareSSO.GetString();
            SettingsUI.UserName.text = SettingsUI.UserSSO.GetString();
            SettingsUI.SelectRus.onValueChanged.AddListener((x) => ChangeLanguagRus(x));
            SettingsUI.SelectEng.onValueChanged.AddListener((x) => ChangeLanguagEng(x));
            LocalizationID.Instance.EvChangeLanguage += () => {
                SettingsUI.Info.text = SettingsUI.InfoSSO.GetString();
                SettingsUI.Share.text = SettingsUI.ShareSSO.GetString();
                SettingsUI.UserName.text = SettingsUI.UserSSO.GetString();
            };
        }

        private void ChangeLanguagRus(bool val)
        {
            if (val) LocalizationID.Instance.SetLanguage(0);
        }

        private void ChangeLanguagEng(bool val)
        {
            if (val) LocalizationID.Instance.SetLanguage(1);
        }
    }
}
