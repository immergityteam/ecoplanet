﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class BuilderPlayer : IBuilderPlayer, IDependent
    {
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IMyInput MyInput { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public IWhereClicked WhereClicked { private get; set; }
        [Dependency] public IBuilderUI BuilderPlayerUI { private get; set; }
        [Dependency] public IBuilderWindow BuilderWindow { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public IAllStructures AllStructures { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public IBaseStructureIDContainerMono BaseStructureIDContainerMono { private get; set; }
        [Dependency] public IPassiveResourcesController PassiveResourcesController { private get; set; }
        [Dependency] public ITiker Tiker { private get; set; }
        [Dependency] public IDonatController DonatController { private get; set; }
        [Dependency] public IBusinessBonusController BusinessBonusController { private get; set; }
        public CraftController CraftController { get; set; }

        public Action<StateBuilder> EvChangeStateBuilder { get; set; }
        /// <summary>
        /// Send local ID structure
        /// </summary>
        public UnityAction<int> EvBuiltConstruction { get; set; }
        public UnityAction<int> EvBuiltConstructionTimerOff { get; set; }
        public UnityAction<int> EvBuildingDelete { get; set; }
        public UnityAction<int> EvClikedStructures { get; set; }

        private StateBuilder stateBuilder;

        private float time;

        private BuilderUIPanelsTweener panelsTweener;

        private StructureData structureData;
        private StructureGhost ghost;
        private Vector3 oldPosition;
        private List<Collider2D> collisionColliders = new List<Collider2D>();
        private List<Transform> redSquares = new List<Transform>();
        private List<Transform> greenSquares = new List<Transform>();

        private int ID = 1;

        public StructureGhost MovableBuilding => ghost;

        [Serializable]
        public struct SaveDataStructur
        {
            public int id;
            public float posX;
            public float posY;
            public float posZ;
            public float scaleX;

            public Vector3 GetPosition()
            {
                return new Vector3(posX, posY, posZ);
            }

            public void SetPosition(Vector3 pos)
            {
                posX = pos.x;
                posY = pos.y;
                posZ = pos.z;
            }
        }

        //LocalID
        Dictionary<int, SaveDataStructur> structuresSupplied = new Dictionary<int, SaveDataStructur>();

        //GlobalID
        Dictionary<int, StructureData> allStructure = new Dictionary<int, StructureData>();
        private Dictionary<int, bool> productionBuildingsLimitData = new Dictionary<int, bool>();
        private Dictionary<int, MonoID> dictionaryMonoIDByLocalID = new Dictionary<int, MonoID>();

        public StateBuilder StateBuilder
        {
            get { return stateBuilder; }
            set
            {
                stateBuilder = value;
                EvChangeStateBuilder?.Invoke(stateBuilder);
            }
        }


        private readonly Vector3Int spawnPoint = Vector3Int.one * short.MaxValue;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            panelsTweener = new BuilderUIPanelsTweener();
            GameStarter.container.BuildUp(panelsTweener);
            Tiker.StartTiker(1);

            for (int i = 0; i < AllStructures.StructureDatas.Length; i++)
                allStructure.Add(AllStructures.StructureDatas[i].Name.GetID(), AllStructures.StructureDatas[i]);

            StateBuilder = StateBuilder.None;
            WhereClicked.SetLayerMask(MyGrid.MaskMoving);
            MyInput.EvPressed += Cliked;
            MyInput.EvDown += ResetTimer;
            MyInput.EvPressed += TickerTime;
            MyInput.EvCliked += DeleteBuilding;

            BuilderWindow.Close.onClick.AddListener(() => ChangeState(StateBuilder.None));

            BuilderPlayerUI.BuildButton.onClick.AddListener(Cansel);
            BuilderPlayerUI.MoveButton.onClick.AddListener(StartMovingObject);
            BuilderPlayerUI.DeleteButton.onClick.AddListener(StartDeletingObjects);
            BuilderPlayerUI.Apply.onClick.AddListener(ToPut);
            BuilderPlayerUI.CancelMoving.onClick.AddListener(Cansel);
            BuilderPlayerUI.Flip.onClick.AddListener(Flip);
            BuilderPlayerUI.BuildCancel.onClick.AddListener(() =>
            {
                if (stateBuilder != StateBuilder.Deleting)
                    panelsTweener.ChangeBuildModes(false);
                Cansel();
            });
            Load();

            if (ID > 1)
            {
                for (int i = 0; i < BaseStructureIDContainerMono.AllBaseStructureMonoID.Length; i++)
                {
                    Creator.MyDestroy(BaseStructureIDContainerMono.AllBaseStructureMonoID[i].gameObject);
                }
            }
            else
            {
                for (int i = 0; i < BaseStructureIDContainerMono.AllBaseStructureMonoID.Length; i++)
                {
                    SaveDataStructur structure = new SaveDataStructur();
                    structure.id = BaseStructureIDContainerMono.AllBaseStructureMonoID[i].GetGlobalID();
                    structure.scaleX = BaseStructureIDContainerMono.AllBaseStructureMonoID[i].gameObject.transform.lossyScale.x;
                    structure.SetPosition(BaseStructureIDContainerMono.AllBaseStructureMonoID[i].gameObject.transform.position);
                    IGameObjCliked objCliked = BaseStructureIDContainerMono.AllBaseStructureMonoID[i].GetComponent<IGameObjCliked>();
                    if (objCliked != null) objCliked.EvCliked += (x) => EvClikedStructures?.Invoke(x);

                    structuresSupplied.Add(ID, structure);
                    ID++;
                }
            }
        }

        public void SetStructureWorld(StructureData structureData, Vector3 position, float scaleX, int localID = 0)
        {
            bool newStructure = false;
            if (localID == 0)
            {
                localID = ID;
                ID++;
                newStructure = true;
            }
            GameObject temp = Creator.GetGameObjectPool(structureData.prefab);
            temp.transform.position = position;
            temp.transform.localScale = new Vector3(scaleX, 1, 1);
            temp.GetComponent<ISetID>().SetID(localID);
            dictionaryMonoIDByLocalID.Add(localID, temp.GetComponent<MonoID>());
            IGameObjCliked objCliked = temp.GetComponent<IGameObjCliked>();
            if (objCliked != null) objCliked.EvCliked += (x) => EvClikedStructures?.Invoke(x);
            if (newStructure)
            {
                SaveDataStructur structure = new SaveDataStructur
                {
                    id = structureData.Name.GetID(),
                    scaleX = scaleX
                };
                structure.SetPosition(position);
                structuresSupplied.Add(localID, structure);
            }
        }

        public StructureData GetStructureData(int localID) => allStructure[structuresSupplied[localID].id];
        public StructureData GetStructureDataGlobal(int globalID)
        {
            if (allStructure.ContainsKey(globalID))
            {
                return allStructure[globalID];
            }
            return null;
        }
        public Vector3 GetPositionStructure(int localID) => structuresSupplied[localID].GetPosition();
        public Vector3 GetPositionFirstStructureGlobal(int globalID)
        {
            foreach (var item in structuresSupplied.Values)
                if (item.id == globalID)
                    return item.GetPosition();
            return Vector3.zero;
        }
        public int GetGlobalID(int localID) => structuresSupplied[localID].id;
        public MonoID GetMonoID(int localID)
        {
            return dictionaryMonoIDByLocalID[localID];
        }

        #region SavingAndLoadingData
        ConvertDataBinary<Dictionary<int, SaveDataStructur>> saveData
            = new ConvertDataBinary<Dictionary<int, SaveDataStructur>>("BuilderPlayer");

        private ConvertDataBinary<Dictionary<int, bool>> saveLimitData =
            new ConvertDataBinary<Dictionary<int, bool>>("productionBuildingLimit");

        private void Save()
        {
            SaveAndLoad.Save(saveLimitData.name, saveLimitData.ToConvert(productionBuildingsLimitData));
            SaveAndLoad.Save(saveData.name, saveData.ToConvert(structuresSupplied));
        }

        private void Load()
        {
            string str = SaveAndLoad.Load(saveData.name);
            string productionBuildingLimitString = SaveAndLoad.Load(saveLimitData.name);

            if (str == "") return;

            if (productionBuildingLimitString == "") productionBuildingsLimitData.Clear();

            structuresSupplied = saveData.FromConvert(str);
            productionBuildingsLimitData = saveLimitData.FromConvert(productionBuildingLimitString);

            foreach (var item in structuresSupplied)
            {
                SetStructureWorld(allStructure[item.Value.id], item.Value.GetPosition(), item.Value.scaleX, item.Key);
                if (ID <= item.Key) ID = item.Key + 1;
            }
        }

        #endregion

        #region ResourceCheck

        private bool ResourceInWarehouseCheck(StructureData data)
        {
            return WarehouseController.CheckingRequiredResources(data.needResours);
        }

        private bool PassiveResourceCheck(StructureData data)
        {
            for (int i = 0; i < data.requirementsPasRes.Length; i++)
            {
                if (PassiveResourcesController.LookOnePasRes(data.requirementsPasRes[i].ID) <
                    data.requirementsPasRes[i].count)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region OutlineBoxesHandlers
        private void DrawSquares(Collider2D collider, GameObject prefSquare, ref List<Transform> list)
        {
            PolygonCollider2D polygonCollider = collider.GetComponent<PolygonCollider2D>();
            Vector3 down = polygonCollider.points[2];
            Vector3 right;
            if (collider.transform.localScale.x < 0)
            {
                right = polygonCollider.points[3];
                right.x *= -1;
                down.x *= -1;
            }
            else
            {
                right = polygonCollider.points[1];
            }

            Vector3Int downPoint = MyGrid.Grid.NearestCell(down);
            Vector3Int rightPoint = MyGrid.Grid.NearestCell(right);

            int i = 0, j;
            Vector3Int bee;
            do
            {
                j = 0;
                do
                {
                    bee = downPoint + (Vector3Int.right * i + Vector3Int.up * j) * 2;
                    Transform temp = Creator.GetGameObjectPool(prefSquare).transform;
                    temp.position = MyGrid.Grid.CellToLocal(bee) + collider.transform.position;
                    temp.parent = collider.transform;
                    list.Add(temp);
                    j++;
                } while (bee.y < -downPoint.y - 2);
                i++;
            } while (bee.x < rightPoint.x - 1);
        }

        private void RemoveAllRedSquares()
        {
            if (redSquares.Count > 0)
                for (int i = 0; i < redSquares.Count; i++)
                    Creator.MyDestroyPool(redSquares[i].gameObject);

            redSquares.Clear();
        }
        private void RemoveAllGreenSquares()
        {
            if (greenSquares.Count > 0)
                for (int i = 0; i < greenSquares.Count; i++)
                    Creator.MyDestroyPool(greenSquares[i].gameObject);

            greenSquares.Clear();
        }

        #endregion

        #region BuildingLimitCheck

        public bool CheckBuildingLimit(int globalId)
        {
            return productionBuildingsLimitData.ContainsKey(globalId);
        }

        private bool BuildingLimit(StructureData data)
        {
            return productionBuildingsLimitData.ContainsKey(data.Name.GetID()) && productionBuildingsLimitData[data.Name.GetID()];
        }

        #endregion

        #region Timer/Tiker

        private void ResetTimer()
        {
            time = 0;
        }

        private void TickerTime()
        {
            if (StateBuilder != StateBuilder.Moving) return;
            if (!ghost)
            {
                time += Time.deltaTime;
                if (MyInput.DeltaPoint.sqrMagnitude > 0.1f) return;
                if (time > ContainerMono.TimePressedForMove)
                {
                    Collider2D temp = WhereClicked.CheckedCollider(MyInput.PointCamera);
                    if (temp && temp.GetComponent<IMovingGO>() != null)
                    {
                        MovingObj(temp.gameObject);
                    }
                    else
                    {
                        ChangeState(StateBuilder.None);
                    }
                }
            }
        }

        #endregion

        #region BuildingHandlers

        private Dictionary<int, UnityAction> buildingActions = new Dictionary<int, UnityAction>();
        private void ToPut()
        {
            if (!ghost) return;
            if (ghost.transform.position == spawnPoint)
            {
                MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.SetBuilding);
                return;
            }

            if (isConflict)
            {
                MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.CrossBuilding);
                return;
            }

            if (structureData)
            {
                StructureData tempData = structureData;
                IGameObjCliked objCliked = ghost.GetComponent<IGameObjCliked>();

                var Timer = ghost.GetComponent<BuildingTimer>();
                var tempID = ID;
                MessageSystemController.ShowTimeMessage(ghost.Transform, Timer.TimeRemaining, ID);
                buildingActions.Add(tempID, () =>
                 {
                     MessageSystemController.UpdateTime(tempID, Timer.TimeRemaining);
                     MessageSystemController.UpdateCoins(Timer);
                     Timer.TimeRemaining--;
                     if (objCliked != null)
                     {
                         objCliked.EvCliked += (x) =>
                         {
                             DonatController.TakeDonat((Timer.TimeRemaining + 59) / 60, BuilderPlayerUI.BoostUpBuilding,
                                 () =>
                                 {
                                     Creator.MyDestroy(Timer);
                                     EvBuiltConstructionTimerOff?.Invoke(tempID);
                                 });
                         };
                     }

                     if (Timer.TimeRemaining <= 0 || Timer == null)
                     {
                         if (objCliked != null)
                         {
                             objCliked.EvCliked = null;
                             objCliked.EvCliked += (x) => EvClikedStructures?.Invoke(x);
                             MessageSystemController.RemoveTime(tempID);
                             Tiker.EvTickSec -= buildingActions[tempID];
                         }
                         Creator.MyDestroy(Timer);
                         EvBuiltConstructionTimerOff?.Invoke(tempID);
                     }
                 });

                Tiker.EvTickSec += buildingActions[tempID];
                if (!structureData.buildingMoreOne)
                    productionBuildingsLimitData.Add(structureData.Name.GetID(), true);

                ghost.GetComponent<ISetID>().SetID(ID);
                dictionaryMonoIDByLocalID.Add(ID, ghost.GetComponent<MonoID>());

                SaveDataStructur structure = new SaveDataStructur
                {
                    id = structureData.Name.GetID(),
                    scaleX = ghost.Scale.x
                };
                structure.SetPosition(ghost.Position);
                structuresSupplied.Add(ID, structure);

#if UNITY_EDITOR
                if (!ContainerMono.BuildOffResurse)
#endif
                    WarehouseController.TakeRes(structureData.needResours);

                EvBuiltConstruction?.Invoke(ID);
                structureData = null;
                ID++;
            }
            else
            {
                SaveDataStructur structure = structuresSupplied[ghost.GetComponent<IGetID>().GetID()];
                structure.SetPosition(ghost.Position);
                structure.scaleX = ghost.Scale.x;
                structuresSupplied[ghost.GetComponent<IGetID>().GetID()] = structure;
            }

            DestroyGhost();

            StateBuilder = StateBuilder.None;
            panelsTweener.RotatingButtons(false);
            BuilderPlayerUI.Frame.gameObject.SetActive(false);
            panelsTweener.ChangeBuildModes(false);
        }

        private void Flip()
        {
            if (ghost)
            {
                Vector3 scale = ghost.Scale;
                scale.x *= -1;
                ghost.Scale = scale;
                RemoveAllGreenSquares();
                DrawSquares(ghost.GetComponent<PolygonCollider2D>(), ContainerMono.GhostSquare, ref greenSquares);
            }
        }

        private void DeleteBuilding()
        {
            if (StateBuilder != StateBuilder.Deleting) return;

            if (ghost) return;

            Collider2D temp = WhereClicked.CheckedCollider(MyInput.PointCamera);

            if (temp == null) return;

            if (temp.gameObject.GetComponent<Trash>()) return;

            var idCheck = temp.GetComponent<IGetID>().GetID();
            if (GetGlobalID(idCheck) < 0)
            {
                MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.CanNotDeleteThis);
                MessageSystemController.EvMessageWindowClose?.Invoke();
                Cansel();
                return;
            }
            else
            {
                if (CraftController.CheckPresenceOfBuilding(idCheck))
                {
                    if (CraftController.CheckPossibilityToDestroy(idCheck))
                    {
                        MessageSystemController.ShowPopUpWindow(MessageSystemController.ShowDemolitionBan());
                        return;
                    }
                }
            }
            MessageSystemController.EvMessageWindowClose += Cansel;

            MessageSystemController.SendMessageWindow(() =>
            {
                AddGhost(temp.gameObject);
                int temporaryID = ghost.GetComponent<IGetID>().GetID();

                EvBuildingDelete?.Invoke(temporaryID);
                if (WarehouseController.CheckFreeSpace(allStructure[GetGlobalID(temporaryID)].returnOnDestruction))
                {
                    WarehouseController.AddRes(allStructure[GetGlobalID(temporaryID)].returnOnDestruction);
                }
                else
                {
                    MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.DeleteHeader);
                    Cansel();
                }

                productionBuildingsLimitData.Remove(structuresSupplied[temporaryID].id);
                structuresSupplied.Remove(temporaryID);
                Creator.MyDestroyPool(ghost.gameObject);
                ChangeState(StateBuilder.None);

                IGameObjCliked objCliked = temp.GetComponent<IGameObjCliked>();
                if (objCliked != null) objCliked.EvCliked = null;


            }, BuilderPlayerUI.DeleteMain);
        }

        private void AddGhost(GameObject target)
        {
            ghost = target.AddComponent<StructureGhost>();
            oldPosition = target.transform.position;
            target.transform.position = spawnPoint;
            DrawSquares(ghost.GetComponent<PolygonCollider2D>(), ContainerMono.GhostSquare, ref greenSquares);
        }

        private void AddTimer(GameObject target, StructureData data)
        {
            var timer = target.AddComponent<BuildingTimer>();
            timer.TimeRemaining = (int)(data.timeBuild / BusinessBonusController.GetMultiplierTimeStructure(data.ID));
        }

        private void RemoveTimer(BuildingTimer timer)
        {
            Creator.MyDestroy(timer);
        }

        private void DestroyGhost()
        {
            if (ghost)
            {
                Creator.MyDestroy(ghost);
                RemoveAllGreenSquares();
            }
        }


        #endregion

        #region ClickHandler

        private void Cliked()
        {
            if (StateBuilder != StateBuilder.Structure) return;
            if (!ghost) return;
            if (ghost.Position == MyGrid.Grid.NearestPointOnGrid(MyInput.GetPointWorld())) return;
            RemoveAllRedSquares();
            

            collisionColliders = new List<Collider2D>();

            ghost.Position = MyGrid.Grid.NearestPointOnGrid(MyInput.GetPointWorld());

            Physics2D.simulationMode = SimulationMode2D.Script;
            Physics2D.Simulate(1);
            Physics2D.simulationMode = SimulationMode2D.FixedUpdate;

            isConflict = ghost.Collider.GetContacts(BuilderPlayerUI.ContactFilter, collisionColliders) > 0;

            if (!isConflict) return;

            foreach (var collider in collisionColliders)
            {
                if (collider.name ==  "Water")
                {
                    DrawSquares(ghost.Collider, ContainerMono.IntersectionSquare, ref redSquares);

                    continue;
                }
                if (collider.GetComponent<PolygonCollider2D>())
                    DrawSquares(collider, ContainerMono.IntersectionSquare, ref redSquares);
            }
        }

        #endregion

        #region StateHandlers

        private void StartMovingObject()
        {
            ChangeState(StateBuilder.Moving);
            BuilderPlayerUI.Frame.gameObject.SetActive(true);
            BuilderPlayerUI.Frame.color = BuilderPlayerUI.FrameGreen;
        }

        private void StartDeletingObjects()
        {
            ChangeState(StateBuilder.Deleting);
            BuilderPlayerUI.Frame.gameObject.SetActive(true);
            BuilderPlayerUI.Frame.color = BuilderPlayerUI.FrameRed;
        }

        public void SetStructureData(StructureData data)
        {
            if (BuildingLimit(data))
            {
                BuilderWindow.GameObject.SetActive(true);
                MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.AlreadyGotBuilding);
                return;
            }

#if UNITY_EDITOR
            if (!ContainerMono.BuildOffResurse)
#endif
                if (!ResourceInWarehouseCheck(data) || !PassiveResourceCheck(data))
                {
                    MessageSystemController.ShowPopUpWindow(BuilderPlayerUI.NotEnoughResources);
                    return;
                }

            if (StateBuilder != StateBuilder.Structure) StateBuilder = StateBuilder.Structure;

            BuilderWindow.GameObject.SetActive(false);
            panelsTweener.RotatingButtons(true);

            structureData = data;
            BuilderPlayerUI.Frame.gameObject.SetActive(true);
            BuilderPlayerUI.Frame.color = BuilderPlayerUI.FrameGreen;
            var buildingObject = Creator.GetGameObjectPool(data.prefab);
            AddGhost(buildingObject);
            AddTimer(buildingObject, data);
        }

        private void MovingObj(GameObject target)
        {
            oldPosition = target.transform.position;
            StateBuilder = StateBuilder.Structure;
            AddGhost(target);
        }

        private void ChangeState(StateBuilder state)
        {
            Cansel();
            BuilderPlayerUI.Frame.gameObject.SetActive(false);
            StateBuilder = state;
        }

        private bool isConflict;



        private void Cansel()
        {
            if (ghost)
            {
                if (structureData)
                {
                    structureData = null;
                    Creator.MyDestroyPool(ghost.gameObject);
                }
                else
                {
                    ghost.Position = oldPosition;
                    DestroyGhost();
                }
            }

            RemoveAllRedSquares();
            StateBuilder = StateBuilder.None;
            BuilderPlayerUI.Frame.gameObject.SetActive(false);
        }

        #endregion
    }
}