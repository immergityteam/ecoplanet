using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class BoxStructure : MonoBehaviour
    {
        public TMP_Text header;
        public Image icon;
        public IconCount[] needRes;
        [HideInInspector] public StructureData structureData;
    }
}