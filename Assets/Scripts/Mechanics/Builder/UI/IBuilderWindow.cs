﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IBuilderWindow
    {
        GameObject GameObject { get; }

        #region Factory
        UpPanelData UpPanelData { get; }
        WhereWhat UpPanel { get; }
        WhereWhat LeftPanel { get; }
        WhereWhat CenterPanel { get; }
        #endregion

        #region Window
        TMP_Text Header { get; }
        TMP_Text Required { get; }
        TMP_Text WeGet { get; }
        TMP_Text BuildSelect { get; }
        #endregion

        #region Parent
        Transform ParentRequired { get; }
        Transform ParentWeGet { get; }
        #endregion

        #region Button
        Button Close { get; }
        Button Select { get; }
        Button Left { get; }
        Button Right { get; }
        #endregion

        #region Prefabs
        GameObject NeedResourse { get; }
        GameObject IconCount { get; }
        #endregion

        IconCount[] RequiredIconCount { get; }
        IconCount[] WeGetIconCount { get; }

        #region SSO
        StringsSO HeaderSSO { get; }
        StringsSO RequiredSSO { get; }
        StringsSO WeGetSSO { get; }
        StringsSO BuildSelectSSO { get; }
        #endregion

        float DeltaX { get; }
        float Scale { get; }
        float Time { get; }
    }
}