using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class BuilderWindow : MonoBehaviour, IBuilderWindow
    {
        public GameObject GameObject { get => gameObject; }

        #region Factory
        [field: Header("Factory")]
        [field: SerializeField] public UpPanelData UpPanelData { get; private set; }
        [field: SerializeField] public WhereWhat UpPanel { get; private set; }
        [field: SerializeField] public WhereWhat LeftPanel { get; private set; }
        [field: SerializeField] public WhereWhat CenterPanel { get; private set; }
        #endregion

        #region Window
        [field: Header("Window")]
        [field: SerializeField] public TMP_Text Header { get; private set; }
        [field: SerializeField] public TMP_Text Required { get; private set; }
        [field: SerializeField] public TMP_Text WeGet { get; private set; }
        [field: SerializeField] public TMP_Text BuildSelect { get; private set; }

        #endregion

        #region Parent
        [field: Header("Parent")]
        [field: SerializeField] public Transform ParentRequired { get; private set; }
        [field: SerializeField] public Transform ParentWeGet { get; private set; }
        #endregion

        #region Button
        [field: Header("Button")]
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public Button Select { get; private set; }
        [field: SerializeField] public Button Left { get; private set; }
        [field: SerializeField] public Button Right { get; private set; }
        #endregion

        [field: SerializeField] public IconCount[] RequiredIconCount { get; private set; }
        [field: SerializeField] public IconCount[] WeGetIconCount { get; private set; }

        #region Prefabs
        [field: Header("Prefabs")]
        [field: SerializeField] public GameObject NeedResourse { get; private set; }
        [field: SerializeField] public GameObject IconCount { get; private set; }
        #endregion

        #region SSO
        [field: Header("SSO")]
        [field: SerializeField] public StringsSO HeaderSSO { get; private set; }
        [field: SerializeField] public StringsSO RequiredSSO { get; private set; }
        [field: SerializeField] public StringsSO WeGetSSO { get; private set; }
        [field: SerializeField] public StringsSO BuildSelectSSO { get; private set; }
        #endregion

        [field: SerializeField] public float DeltaX { get; private set; }
        [field: SerializeField] public float Scale { get; private set; }
        [field: SerializeField] public float Time { get; private set; }

    }
}