using Developing;
using UnityEngine;

namespace ColLobOK
{
    public class AllStructures : MonoBehaviour, IAllStructures
    {
        [field: SerializeField] public StructureData[] StructureDatas { get; private set; }
#if UNITY_EDITOR
        [ContextMenu("GetAllStructureDatasSO")]
        private void GetStructureDatasSO()
        {
            StructureDatas = MetodForEditor.GetAllAssets<StructureData>();
        }
#endif
    }
}