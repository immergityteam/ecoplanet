﻿using UnityEngine;

namespace ColLobOK
{
    public class StructureGhost : MonoBehaviour, IStructureGhost
    {
        private Rigidbody2D _rigidBody;
        private Collider2D _myCollider;
        private GameObject[] greenSquares;
        public Vector3 Position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Transform Transform
        {
            get { return transform; }
        }

        public Vector3 Scale
        {
            get { return transform.localScale; }
            set { transform.localScale = value; }
        }

        public Collider2D Collider
        {
            get { return _myCollider; }
            set { _myCollider = value; }
        }

        private void Start()
        {
            if (!gameObject.TryGetComponent(out _rigidBody))
            {
                _rigidBody = gameObject.AddComponent<Rigidbody2D>();
            }
            _rigidBody.gravityScale = 0;
            _myCollider = GetComponent<Collider2D>();
            _myCollider.isTrigger = true;
        }

        private void OnDestroy()
        {
            Destroy(_rigidBody);
            if (_myCollider != null)
            {
                _myCollider.isTrigger = false;
            }
        }
    }
}