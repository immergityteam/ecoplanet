﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IBuilderPlayer {
        StateBuilder StateBuilder { get; set; }
        Action<StateBuilder> EvChangeStateBuilder { get; set; }
        UnityAction<int> EvBuiltConstruction { get; set; }
        UnityAction<int> EvBuiltConstructionTimerOff { get; set; }
        UnityAction<int> EvBuildingDelete { get; set; }
        UnityAction<int> EvClikedStructures { get; set; }
        void SetStructureData(StructureData data);
        void SetStructureWorld(StructureData structureData, Vector3 position, float scaleX, int localID = 0);
        StructureData GetStructureData(int localID);
        StructureData GetStructureDataGlobal(int globalID);
        Vector3 GetPositionStructure(int localID);
        Vector3 GetPositionFirstStructureGlobal(int globalID);
        int GetGlobalID(int localID);

        MonoID GetMonoID(int localID);
        bool CheckBuildingLimit(int globalId);
        StructureGhost MovableBuilding { get; }
        CraftController CraftController { get; set; }
    }
}