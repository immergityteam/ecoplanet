﻿namespace ColLobOK
{
    public interface IBaseStructureIDContainerMono
    {
        MonoID[] AllBaseStructureMonoID { get; }
    }
}