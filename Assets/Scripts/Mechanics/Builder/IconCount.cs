using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IconCount : MonoBehaviour
{
    public TMP_Text count;
    public Image icon;
    public ContentSizeFitter contentSizeFitter;
}
