﻿using UnityEngine;

namespace ColLobOK
{
    public interface IStructureGhost {
        Vector3 Position { get; set; }

        Vector3 Scale { get; set; }

        Collider2D Collider { get; set; }
    }
}