﻿using UnityEditor;
using UnityEngine;

namespace ColLobOK
{
    public class BaseStructureIDContainerMono : MonoBehaviour, IBaseStructureIDContainerMono
    {
        [field: SerializeField] public MonoID[] AllBaseStructureMonoID { get; private set; }

#if UNITY_EDITOR
        [SerializeField] private int allBeautyficationCount;

        [ContextMenu("SetAllBeautyfication")]
        private void SetAllTrash()
        {
            AllBaseStructureMonoID = GetComponentsInChildren<MonoID>();
            SetAllTrashsID();
            allBeautyficationCount = AllBaseStructureMonoID.Length;
        }

        public void SetAllTrashsID()
        {
            for (int i = 0; i < AllBaseStructureMonoID.Length; i++)
            {
                Undo.RecordObject(AllBaseStructureMonoID[i], "SetID");
                AllBaseStructureMonoID[i].SetID(i+1);
            }
        }
#endif
    }
}