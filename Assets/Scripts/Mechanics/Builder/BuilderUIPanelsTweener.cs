﻿using DG.Tweening;
using UnityEngine;
using UnityDI;
using UnityEngine.Events;

namespace ColLobOK
{
    public class BuilderUIPanelsTweener : IDependent
    {
        [Dependency] public IBuilderUI BuilderPlayerUI { private get; set; }
        [Dependency] public IGameUI GameUI { private get; set; }
        [Dependency] public IBuilderWindow BuilderWindow { private get; set; }

        bool buildMode;
        bool rotatingButtons;

        public void OnInjected()
        {
            GameUI.Build.onClick.AddListener(() => ChangeBuildModes(true));
            BuilderPlayerUI.MoveButton.onClick.AddListener(() => RotatingButtons(true));
            BuilderPlayerUI.CancelMoving.onClick.AddListener(() => RotatingButtons(false));
            BuilderWindow.Close.onClick.AddListener(() => ChangeBuildModes(false));
        }

        public void ChangeBuildModes(bool val, UnityAction call = null)
        {
            buildMode = val;
            if (buildMode) BuilderPlayerUI.AllButtons.SetActive(true);

            if (!buildMode && rotatingButtons)
            {
                RotatingButtons(false, () => OpenButton());
                return;
            }

            OpenButton(call);
        }

        private void OpenButton(UnityAction call = null)
        {
            MovingButtonLine(BuilderPlayerUI.BuildButton.transform);
            MovingButtonLine(BuilderPlayerUI.DeleteButton.transform);
            MovingButtonLine(BuilderPlayerUI.MoveButton.transform, call);
        }

        private void MovingButtonLine(Transform tr, UnityAction call = null)
        {
            tr.DOKill();
            tr.DOLocalMoveY(buildMode ? BuilderPlayerUI.MinMaxMovingY.y : BuilderPlayerUI.MinMaxMovingY.x,
                BuilderPlayerUI.TimeMoving)
                .OnComplete(() => { BuilderPlayerUI.AllButtons.SetActive(buildMode); call?.Invoke(); });
        }

        public void RotatingButtons(bool val, UnityAction call = null)
        {
            if (!buildMode)
            {
                ChangeBuildModes(true, () => RotatingButtons(true));
                return;
            }
            rotatingButtons = val;
            BuilderPlayerUI.PivotButtons.transform.DOKill();
            BuilderPlayerUI.PivotButtons.transform.DOLocalRotate(rotatingButtons ? BuilderPlayerUI.Rotation : Vector3.zero, 
                BuilderPlayerUI.TimeMoving)
                .OnComplete(() => call?.Invoke());
        }
    }
}