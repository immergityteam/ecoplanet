﻿namespace ColLobOK
{
    public interface IAllStructures
    {
        StructureData[] StructureDatas { get; }
    }
}