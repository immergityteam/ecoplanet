using System;
using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

namespace ColLobOK
{
    public class DryLandController : IDryLandController, IDependent
    {
        [Dependency] public ITrashsContainerMono TrashsContainerMono { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public ITrashController TrashController { private get; set; }

        public UnityAction<Vector3> EvClearTile { get; set; }
        Dictionary<Vector2, int> sellDryCount;

        private bool cleaning = false;
        public void OnInjected()
        {
            TrashController.EvTrashOverID += ClearDry;
            sellDryCount = new Dictionary<Vector2, int>();
            for (int i = 0; i < TrashsContainerMono.AllTrash.Length; i++)
                if (TrashsContainerMono.AllTrash[i] && TrashsContainerMono.AllTrash[i].gameObject.activeSelf)
                    SetDirtAroundTrash(TrashsContainerMono.AllTrash[i]);

            foreach (var item in sellDryCount)
                MyGrid.TilemapDryLand.SetTile(new Vector3Int((int)item.Key.x, (int)item.Key.y, 0), SelectTile(item.Value));
            cleaning = true;
        }

        private TileBase SelectTile(int count)
        {
            for (int i = 0; i < MyGrid.DryTiles.Length; i++)
                if (count >= MyGrid.DryTiles[i].persent)
                    return MyGrid.DryTiles[i].tile;
            return null;
        }

        private void ClearDry(int id)
        {
            SetDirtAroundTrash(TrashsContainerMono.AllTrash[id]);
        }
        private void SetDirtAroundTrash(Trash trash)
        {
            int x = trash.SizeTile.x / 2;
            int y = trash.SizeTile.y / 2;

            Vector3Int newPosition;

            //Set Tile -1
            for (int i = -x; i < x + trash.SizeTile.x % 2; i++)
            {
                for (int j = -y; j < y + trash.SizeTile.y % 2; j++)
                {
                    newPosition = MyGrid.TilemapDryLand.WorldToCell(trash.transform.position) +
                        MyGrid.DirectionInt[0] * i + MyGrid.DirectionInt[1] * j;

                    AddDryCount((Vector2Int)newPosition, 100);
                }
            }

            for (int i = 0; i < MyGrid.DryTiles.Length; i++)
            {
                x++; y++;
                SetOneLineAround(
                     MyGrid.TilemapDryLand.WorldToCell(trash.transform.position),
                     -x, x + trash.SizeTile.x % 2,
                     -y, y + trash.SizeTile.y % 2,
                     MyGrid.DryTiles[i].persent
                 );
            }
        }
        private void SetOneLineAround(Vector3Int centerPosition, int xMin, int xMax, int yMin, int yMax, int count, bool clear = false)
        {
            for (int i = xMin; i < xMax; i++)
            {
                for (int j = yMin; j < yMax; j++)
                {
                    if (i == xMin || i == xMax - 1 || j == yMin || j == yMax - 1)
                    {
                        AddDryCount((Vector2Int)(centerPosition + MyGrid.DirectionInt[0] * i + MyGrid.DirectionInt[1] * j), count);
                    }
                }
            }
        }
        private void AddDryCount(Vector2Int position, int count)
        {
            if (MyGrid.TilemapWater.GetTile((Vector3Int)position)) return;
            if (sellDryCount.ContainsKey(position))
            {
                if (cleaning)
                {
                    sellDryCount[position] -= count;
                    MyGrid.TilemapDryLand.SetTile((Vector3Int)position, SelectTile(sellDryCount[position]));

                    if (sellDryCount[position] == 0)
                    {
                        sellDryCount.Remove(position);
                        EvClearTile?.Invoke(MyGrid.GridX2.CellToWorld((Vector3Int)position));
                    }
                }
                else
                {
                    sellDryCount[position] += count;
                }
            }
            else
            {
                if (cleaning) return;
                sellDryCount.Add(position, count);
            }
        }
    }
}