﻿using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public interface IDryLandController
    {
        UnityAction<Vector3> EvClearTile { get; set; }
    }
}