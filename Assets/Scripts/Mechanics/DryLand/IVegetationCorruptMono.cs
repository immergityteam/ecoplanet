﻿using System;

namespace ColLobOK
{
    public interface IVegetationCorruptMono
    {
        float radius { get; }
        MonoID[] Vegetation { get; }
    }
}