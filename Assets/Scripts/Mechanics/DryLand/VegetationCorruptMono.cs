using UnityEditor;
using UnityEngine;

namespace ColLobOK
{
    public class VegetationCorruptMono : MonoBehaviour, IVegetationCorruptMono
    {
        [field: SerializeField] public float radius { get; private set; }

        [field: SerializeField] public MonoID[] Vegetation { get; private set; }


#if UNITY_EDITOR

        [ContextMenu("SetAllVegetation")]
        private void SetAllVegetation()
        {
            Vegetation = GetComponentsInChildren<MonoID>();
            SetAllVegetationID();
        }

        public void SetAllVegetationID()
        {
            for (int i = 0; i < Vegetation.Length; i++)
            {
                Undo.RecordObject(Vegetation[i], "SetID");
                Vegetation[i].SetID(i + 1);
            }
        }
#endif
    }
}