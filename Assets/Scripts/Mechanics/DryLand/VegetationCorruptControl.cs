using System;
using System.Collections;
using System.Collections.Generic;
using UnityDI;
using UnityEngine;

namespace ColLobOK
{
    public class VegetationCorruptControl : IVegetationCorruptControl, IDependent
    {
        [Dependency] public IVegetationCorruptMono VegetationCorruptMono { private get; set; }
        [Dependency] public IDryLandController DryLandController { private get; set; }
        [Dependency] public IMyGrid MyGrid { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }

        List<int> idStructureDestroy;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            DryLandController.EvClearTile += ChekedVegetation;

            Load();

            for (int i = 0; i < idStructureDestroy.Count; i++)
                Creator.MyDestroy(VegetationCorruptMono.Vegetation[idStructureDestroy[i]].gameObject);
        }

        private void ChekedVegetation(Vector3 position)
        {
            for (int i = 0; i < VegetationCorruptMono.Vegetation.Length; i++)
            {
                if (VegetationCorruptMono.Vegetation[i] && VegetationCorruptMono.Vegetation[i].gameObject.activeSelf)
                {
                    if ((VegetationCorruptMono.Vegetation[i].transform.position - position).sqrMagnitude < VegetationCorruptMono.radius * VegetationCorruptMono.radius)
                    {
                        BuilderPlayer.SetStructureWorld(
                            BuilderPlayer.GetStructureDataGlobal(VegetationCorruptMono.Vegetation[i].GetGlobalID()),
                            VegetationCorruptMono.Vegetation[i].transform.position,
                            VegetationCorruptMono.Vegetation[i].transform.localScale.x
                        );
                        VegetationCorruptMono.Vegetation[i].gameObject.SetActive(false);
                        Creator.MyDestroy(VegetationCorruptMono.Vegetation[i].gameObject);
                        idStructureDestroy.Add(i);
                    }
                }
            }
        }

        ConvertDataBinary<List<int>> saveData =
            new ConvertDataBinary<List<int>>("VegetationCorruptControl");

        private void Save() => SaveAndLoad.Save(saveData.name, saveData.ToConvert(idStructureDestroy));

        private void Load() => idStructureDestroy = saveData.FromConvert(SaveAndLoad.Load(saveData.name));
    }
}