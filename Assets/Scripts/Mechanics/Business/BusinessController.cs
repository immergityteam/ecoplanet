using System.Collections.Generic;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class BusinessController : IBusinessController, IDependent
    {
        [Dependency] public IBusinessUI BusinessUI { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IContainerRecipes ContainerRecipes { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        [Dependency] public ICommonLines CommonLines { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IContainerAllResources ContainerAllResources { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public ITiker Tiker { private get; set; }

        /// <summary>
        /// Global StringSO ID
        /// </summary>
        Dictionary<int, BusinessSO> allBusiness;
        // Global StringSO ID
        Dictionary<int, BoostSO> allBoost;

        //LocalStructID
        List<int> businessesID;
        List<IBusiness> businesses;

        public void OnInjected()
        {
            SaveAndLoad.EvSave += Save;
            allBusiness = new Dictionary<int, BusinessSO>();
            allBoost = new Dictionary<int, BoostSO>();
            businessesID = new List<int>();
            businesses = new List<IBusiness>();

            for (int i = 0; i < BusinessUI.BusinessSO.Length; i++)
            {
                allBusiness.Add(BusinessUI.BusinessSO[i].nameSO.GetID(), BusinessUI.BusinessSO[i]);
            }
            for (int i = 0; i < BusinessUI.BoostSO.Length; i++)
            {
                allBoost.Add(BusinessUI.BoostSO[i].nameSO.GetID(), BusinessUI.BoostSO[i]);
            }

            BuilderPlayer.EvBuiltConstruction += AddBusiness;
            BuilderPlayer.EvBuildingDelete += RemoveBusiness;
            BuilderPlayer.EvClikedStructures += (x) =>
            {   //If there is no crafting
                if (!ContainerRecipes.IsStructureRecipeProductionSO(BuilderPlayer.GetGlobalID(x)))
                    OpenInfoPanel(x);
            };

            BusinessUI.ButtonBoost.onClick.AddListener(OpenBoostPanel);
            Tiker.SetTikerTime(1);
            Tiker.EvTickSec += UpdateTime;
            Load();
        }

        private int activeBusiness = 0;

        UnityAction temp;

        private void OpenInfoPanel(int localStructID)
        {
            //There is a business
            if (businessesID.Contains(localStructID))
            {
                Tiker.StopTiker();
                temp = () =>
                {
                    businesses[activeBusiness].EvChangeCountPay = null;
                    Tiker.StopTiker();
                    BusinessUI.Close.onClick.RemoveListener(temp);
                };
                BusinessUI.Close.onClick.AddListener(temp);

                businesses[activeBusiness].EvChangeCountPay -= () => OpenInfoPanel(businessesID[activeBusiness]);

                activeBusiness = businessesID.IndexOf(localStructID);
                BusinessSO BSO = allBusiness[BuilderPlayer.GetGlobalID(localStructID)];

                BusinessUI.Panel.SetActive(true);
                BusinessUI.Header.text = BSO.nameSO.GetString();

                BusinessUI.Price.text.text = BusinessUI.Price.SSO.GetString();
                BusinessUI.Price.count.text = BSO.price.ToString();

                BusinessUI.Periodicity.text.text = BusinessUI.Periodicity.SSO.GetString();
                BusinessUI.Periodicity.count.text = MyMethods.ToTimeHMS(BSO.time, CommonLines.FormatTime.GetString());

                BusinessUI.CountPay.text.text = BusinessUI.CountPay.SSO.GetString();
                BusinessUI.CountPay.count.text =
                    "" + (businesses[activeBusiness].GetCountPay() > 0 ?
                    businesses[activeBusiness].GetCountPay().ToString() :
                    MyMethods.PaintText(businesses[activeBusiness].GetCountPay(), Color.red))
                    + '/' + BSO.countPay;

                BusinessUI.BoostPanel.SetActive(BSO.trafficAndBoost);

                businesses[activeBusiness].EvChangeCountPay += () => OpenInfoPanel(businessesID[activeBusiness]);

                if (BSO.trafficAndBoost)
                {

                    BusinessUI.Boost.text.text = BusinessUI.Boost.SSO.GetString();
                    BusinessUI.BoostTime.text.text = BusinessUI.BoostTime.SSO.GetString();

                    BusinessUI.ButtonBoost.interactable = !businesses[activeBusiness].GetBoost();

                    if (!businesses[activeBusiness].GetBoost())
                    {
                        BusinessUI.IconBoost.sprite = BusinessUI.PlusIcon;
                        BusinessUI.Boost.count.text = "100%";
                        BusinessUI.BoostTime.count.text = MyMethods.ToTimeHMS(0, CommonLines.FormatTime.GetString());
                    }
                    else
                    {
                        BusinessUI.IconBoost.sprite =
                            ContainerAllResources.GetConfigRes(businesses[activeBusiness].GetBoost().nameSO.GetID()).sprite;
                        BusinessUI.Boost.count.text = "" + MyMethods.PaintText((businesses[activeBusiness].GetBoost().power * 100) + "%", Color.green);
                        BusinessUI.Price.count.text = MyMethods.PaintText(((int)(BSO.price * businesses[activeBusiness].GetBoost().power)).ToString(), Color.green);
                        time = businesses[activeBusiness].GetBoostTime() + 1;
                        UpdateTime();
                        if (businesses[activeBusiness].GetCountPay() > 0) Tiker.StartTiker();
                    }
                }
            }
        }

        private int time;
        private void UpdateTime()
        {
            time--;
            BusinessUI.BoostTime.count.text = MyMethods.ToTimeHMS(time, CommonLines.FormatTime.GetString());
            if (time <= 0)
            {
                Tiker.StopTiker();
            }
        }

        private void OpenBoostPanel()
        {
            Creator.ClearKids(BusinessUI.SelectBoostParent.transform);
            Resource[] Boost = WarehouseController.LookAllResType(TypeRes.Rations).ToArray();
            BoxBoostUI temp;
            for (int i = 0; i < Boost.Length; i++)
            {
                temp = Creator.GetGameObject(BusinessUI.BoxBoost.gameObject, BusinessUI.SelectBoostParent.transform).GetComponent<BoxBoostUI>();
                temp.icon.sprite = ContainerAllResources.GetConfigRes(Boost[i].ID).sprite;
                for (int j = 0; j < allBoost[Boost[i].ID].level - 1; j++)
                {
                    Creator.GetGameObject(temp.start, temp.start.transform.parent);
                }
                int k = i;
                temp.button.onClick.RemoveAllListeners();
                temp.button.onClick.AddListener(() => AddBoost(Boost[k]));
            }
        }

        private void AddBoost(Resource res)
        {
            res.count = 1;
            WarehouseController.TakeRes(res);
            businesses[activeBusiness].AddBoost(allBoost[res.ID]);
            BusinessUI.ButtonBoostClose.OnPointerClick(null);
            OpenInfoPanel(businessesID[activeBusiness]);
        }

        private void AddBusiness(int localStructID)
        {
            if (allBusiness.ContainsKey(BuilderPlayer.GetGlobalID(localStructID)))
            {
                NewBusiness(localStructID).Activation();
                businessesID.Add(localStructID);
            }
        }

        private IBusiness NewBusiness(int localStructID)
        {
            BusinessSO BSO = allBusiness[BuilderPlayer.GetGlobalID(localStructID)];
            IBusiness temp = new Business(BSO, BuilderPlayer.GetPositionStructure(localStructID));
            GameStarter.container.BuildUp(temp);
            businesses.Add(temp);
            return temp;
        }

        private void RemoveBusiness(int localStructID)
        {
            if (businessesID.Contains(localStructID))
            {
                int id = businessesID.IndexOf(localStructID);

                businesses[id].Deactivation();

                businessesID.RemoveAt(id);
                businesses.RemoveAt(id);
            }
        }

        ConvertDataBinary<List<int>> saveDataId = new ConvertDataBinary<List<int>>("BusinessControllerId");
        ConvertDataBinary<List<string>> saveDataStr = new ConvertDataBinary<List<string>>("BusinessControllerString");

        private void Save()
        {
            List<string> save = new List<string>(businessesID.Count);

            for (int i = 0; i < businessesID.Count; i++)
            {
                save.Add(businesses[i].Save());
            }
            SaveAndLoad.Save(saveDataId.name, saveDataId.ToConvert(businessesID));
            SaveAndLoad.Save(saveDataStr.name, saveDataStr.ToConvert(save));
        }
        private void Load()
        {
            businessesID = saveDataId.FromConvert(SaveAndLoad.Load(saveDataId.name));
            var str = SaveAndLoad.Load(saveDataStr.name);
            if (str == "" || businessesID.Count == 0) return;
            List<string> save = saveDataStr.FromConvert(str);
            for (int i = 0; i < businessesID.Count; i++)
            {
                string[] temp = save[i].Split('|');
                if (temp[0] == "0")
                {
                    NewBusiness(businessesID[i]).Load(save[i], null);
                }
                else
                {
                    NewBusiness(businessesID[i]).Load(save[i], allBoost[int.Parse(temp[0])]);
                }

            }
        }
    }
}