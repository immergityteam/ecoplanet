﻿using UnityEngine.Events;

namespace ColLobOK
{
    public interface IBusiness
    {
        UnityAction EvChangeCountPay { get; set; }
        void Activation();
        void Deactivation();
        string Save();
        void Load(string data, BoostSO boost);
        int GetCountPay();
        void AddBoost(BoostSO res);
        BoostSO GetBoost();
        int GetBoostTime();
    }
}