using Developing;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ColLobOK
{
    public class BusinessUI : MonoBehaviour, IBusinessUI
    {
        [field: SerializeField] public BusinessSO[] BusinessSO { get; private set; }
        [field: SerializeField] public BoostSO[] BoostSO { get; private set; }
        public GameObject Panel { get => gameObject; }
        [field: SerializeField] public GameObject BoostPanel { get; private set; }
        [field: SerializeField] public GameObject SelectBoostParent { get; private set; }
        [field: SerializeField] public TMP_Text Header { get; private set; }

        [field: SerializeField] public TextCountSO Price { get; private set; }
        [field: SerializeField] public TextCountSO Periodicity { get; private set; }
        [field: SerializeField] public TextCountSO CountPay { get; private set; }
        [field: SerializeField] public TextCountSO Boost { get; private set; }
        [field: SerializeField] public TextCountSO BoostTime { get; private set; }
        [field: SerializeField] public BoxBoostUI BoxBoost { get; private set; }
        [field: SerializeField] public Button ButtonBoost { get; private set; }
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public Image IconBoost { get; private set; }
        [field: SerializeField] public EventTrigger ButtonBoostClose { get; private set; }
        [field: SerializeField] public Sprite PlusIcon { get; private set; }


#if UNITY_EDITOR
        [ContextMenu("GetAllBusinessSO")]
        private void GetAllBusinessSO()
        {
            BusinessSO = MetodForEditor.GetAllAssets<BusinessSO>();
            BoostSO = MetodForEditor.GetAllAssets<BoostSO>();
        }
#endif
    }
}