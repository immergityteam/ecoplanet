using UnityDI;
using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class Business : IBusiness
    {
        [Dependency] public ITiker Tiker { private get; set; }
        [Dependency] public ITiker TikerSecond { private get; set; }
        [Dependency] public ITiker TikerForLoad { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public IPassiveResourcesController PassiveResourcesController { private get; set; }
        [Dependency] public IPassiveResourcesUI PassiveResourcesUI { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IMyTime MyTime { private get; set; }
        [Dependency] public IMessageSystemController MessageSystemController { private get; set; }
        [Dependency] public IBusinessBonusController BusinessBonusController { private get; set; }
        [Dependency] public ISaveAndLoad SaveAndLoad { private get; set; }
        public UnityAction EvChangeCountPay { get; set; }

        BusinessSO data;
        BoostSO boost;
        int buildingMessageID;
        struct SaveData
        {
            public double timeLastPayout;
            public int timeLeftBoost;
            public int countPay;
            public int idBoost;
        }

        SaveData saveData = new SaveData();
        Vector3 position;

        public Business(BusinessSO data, Vector3 position)
        {
            this.data = data;
            this.position = position;
        }

        public void Activation()
        {
            SaveAndLoad.EvLoadOnFocus += Calculation;
            saveData.countPay = data.countPay;
            saveData.timeLastPayout = MyTime.GetTimeSecUTC();

            TikerListener();
            Tiker.StartTiker();
        }

        public void Deactivation()
        {
            Tiker.StopTiker();
            Tiker.EvTickSec = null;
            TikerForLoad.StopTiker();
            TikerForLoad.EvTickSec = null;
            TikerSecond.StopTiker();
            TikerSecond.EvTickSec = null;
            MessageSystemController.DestroyBuildingMessage(buildingMessageID);
        }

        private void GetIncome(int countPay = 1)
        {
            float income = 1;
            int prestigeNow = PassiveResourcesController.LookOnePasRes(PassiveResourcesUI.PrestigeSO.GetID());

            income *= data.price * Mathf.Clamp(prestigeNow / data.prestige, 0.1f, 2f + BusinessBonusController.GetDeltaMaxPrestige(data.ID));

            if (data.trafficAndBoost)
            {
                if (saveData.timeLeftBoost == 0)
                {
                    boost = null;
                    TikerSecond.StopTiker();
                }

                //Traffic
                income *= PassiveResourcesController.LookOnePasRes(PassiveResourcesUI.TrafficSO.GetID());
                //Boost
                if (boost) income *= boost.power;
            }

            if (saveData.countPay < countPay) countPay = saveData.countPay;

            saveData.countPay -= countPay;
            saveData.timeLastPayout = MyTime.GetTimeSecUTC();

            if (saveData.countPay <= 0)
            {
                Tiker.StopTiker();
                TikerSecond.StopTiker();
                buildingMessageID = MessageSystemController.ShowBuildingMessage(position, Renovate);
            }
            income *= countPay;
            EvChangeCountPay?.Invoke();
            Resource resource = new Resource(ContainerMono.Money, (int)income);
            WarehouseController.AddRes(resource);
        }

        public void AddBoost(BoostSO boost)
        {
            this.boost = boost;
            saveData.timeLeftBoost = boost.time;
            saveData.idBoost = boost.nameSO.GetID();
            if (saveData.countPay > 0)
            {
                TikerSecond.StartTiker();
            }
        }

        public BoostSO GetBoost() => boost;
        public int GetBoostTime() => saveData.timeLeftBoost;
        public int GetCountPay() => saveData.countPay;
        private void Renovate()
        {
            Tiker.StartTiker();
            if (saveData.timeLeftBoost > 0)
            {
                TikerSecond.StartTiker();
            }
            saveData.countPay = data.countPay;
        }

        public string Save() => "" + saveData.idBoost + '|' + saveData.timeLastPayout + '|' + saveData.timeLeftBoost + '|' + saveData.countPay;

        public void Load(string data, BoostSO boost)
        {
            string[] temp = data.Split('|');
            saveData.idBoost = int.Parse(temp[0]);
            saveData.timeLastPayout = double.Parse(temp[1]);
            saveData.timeLeftBoost = int.Parse(temp[2]);
            saveData.countPay = int.Parse(temp[3]);
            if (boost) this.boost = boost;
            Calculation();
            TikerListener();
        }

        private void Calculation()
        {
            double deltaTime = (int)(MyTime.GetTimeSecUTC() - saveData.timeLastPayout);
            int countPay = (int)(deltaTime / data.time);


            if (countPay > saveData.countPay)
            {
                countPay = saveData.countPay;
            }
            else
            {
                TikerForLoad.SetTikerTime((int)((countPay + 1) * data.time - deltaTime));
                TikerForLoad.EvTickSec += TikForLoad;
            }

            if (boost)
            {
                int countPayBoost = saveData.timeLeftBoost / data.time;
                if (countPayBoost >= countPay)
                {
                    GetIncome(countPay);
                    saveData.timeLeftBoost -= (int)deltaTime;
                }
                else
                {
                    GetIncome(countPayBoost);
                    boost = null;
                    saveData.timeLeftBoost = 0;
                    GetIncome(countPay - countPayBoost);
                }
            }
            else
            {
                GetIncome(countPay);
            }
        }
        
        private void TikForLoad()
        {
            GetIncome();
            TikerForLoad.EvTickSec = null;
            Tiker.StartTiker();
        }

        private void TikerListener()
        {
            Tiker.EvTickSec += () => GetIncome();
            Tiker.SetTikerTime(data.time);
            TikerSecond.SetTikerTime(1);

            if (!data.trafficAndBoost) return;
            TikerSecond.EvTickSec += () =>
            {
                saveData.timeLeftBoost--;
                if (saveData.timeLeftBoost <= 0)
                {
                    saveData.timeLeftBoost = 0;
                }
            };

            if (saveData.timeLeftBoost > 0 && saveData.countPay > 0) TikerSecond.StartTiker();

        }
    }
}