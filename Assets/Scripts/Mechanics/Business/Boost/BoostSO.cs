using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BoostSO", menuName = "SO/Business/BoostSO", order = 1)]
    public class BoostSO : ScriptableObject
    {
        public StringsSO nameSO;
        public float power;
        public int time;
        public int level;
    }
}