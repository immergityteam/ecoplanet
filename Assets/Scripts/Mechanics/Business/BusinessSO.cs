using UnityEngine;

namespace ColLobOK
{
    [CreateAssetMenu(fileName = "BusinessSO", menuName = "SO/Business/BusinessSO", order = 1)]
    public class BusinessSO : ScriptableObject
    {
        public StringsSO nameSO;
        public int price;
        public int time;
        public int prestige;
        public int countPay;
        public int repair;
        public bool trafficAndBoost;

        public int ID { get => nameSO.GetID(); }
    }
}