﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ColLobOK
{

    [Serializable]
    public struct TextCountSO
    {
        public StringsSO SSO;
        public TMP_Text text;
        public TMP_Text count;
    }

    public interface IBusinessUI
    {
        BusinessSO[] BusinessSO { get; }
        BoostSO[] BoostSO { get; }
        GameObject Panel { get; }
        GameObject BoostPanel { get; }
        GameObject SelectBoostParent { get; }
        TMP_Text Header { get; }
        TextCountSO Price { get; }
        TextCountSO Periodicity { get; }
        TextCountSO CountPay { get; }
        TextCountSO Boost { get; }
        TextCountSO BoostTime { get; }
        BoxBoostUI BoxBoost { get; }
        Button ButtonBoost { get; }
        Button Close { get; }
        EventTrigger ButtonBoostClose { get; }
        Image IconBoost { get; }
        Sprite PlusIcon { get; }
    }
}