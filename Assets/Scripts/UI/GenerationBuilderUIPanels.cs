﻿using ColLobOK;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GenerationBuilderUIPanels : IGenerationBuilderUIPanels, IDependent
{

    [Dependency] public IBuilderWindow BuilderWindow { private get; set; }
    [Dependency] public IBuilderUI BuilderUI { private get; set; }
    [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
    [Dependency] public ICreator Creator { private get; set; }
    [Dependency] public IWarehouseController WarehouseController { private get; set; }
    [Dependency] public ICommonLines CommonLines { private get; set; }
    [Dependency] public IContainerAllResources AllResources { private get; set; }
    [Dependency] public IBusinessBonusController BusinessBonusController { private get; set; }

    public void OnInjected()
    {
        BuilderUI.BuildButton.onClick.AddListener(() => GenUpPanel(BuilderWindow.UpPanelData));
        BuilderWindow.Select.onClick.AddListener(BuildStructure);
        BuilderWindow.Left.onClick.AddListener(() => Rotation(-1));
        BuilderWindow.Right.onClick.AddListener(() => Rotation(1));
    }

    public UnityAction<int> EvSelectPanel { get; set; }

    public void GenUpPanel(UpPanelData data)
    {
        BuilderWindow.GameObject.SetActive(true);
        BuilderWindow.Header.text = BuilderWindow.HeaderSSO.GetString();
        BuilderWindow.Required.text = BuilderWindow.RequiredSSO.GetString();
        BuilderWindow.WeGet.text = BuilderWindow.WeGetSSO.GetString();
        BuilderWindow.BuildSelect.text = BuilderWindow.BuildSelectSSO.GetString();

        ClearKids(BuilderWindow.UpPanel.parent);
        Toggle toggle, first = null;
        for (int i = 0; i < data.leftPanelDatas.Length; i++)
        {
            int k = i;
            toggle = GenPanel(BuilderWindow.UpPanel.prefab, BuilderWindow.UpPanel.parent,
                (x) => 
                { if (x)
                    {
                        GenLeftPanel(data.leftPanelDatas[k]);
                        EvSelectPanel?.Invoke(data.leftPanelDatas[k].Name.ID);
                    }
                },
                data.leftPanelDatas[k].Name.GetString());
            if (i == 0) first = toggle;
        }
        if (first != null) first.isOn = true;
    }

    public void GenLeftPanel(LeftPanelData data)
    {
        ClearKids(BuilderWindow.LeftPanel.parent);
        Toggle toggle, first = null;
        for (int i = 0; i < data.centerPanelDatas.Length; i++)
        {
            int k = i;
            toggle = GenPanel(BuilderWindow.LeftPanel.prefab, BuilderWindow.LeftPanel.parent,
                (x) => { if (x)
                    {
                        GenCenterPanel(data.centerPanelDatas[k].structureDatas);
                        EvSelectPanel?.Invoke(data.centerPanelDatas[k].Name.ID);
                    }
                },
                data.centerPanelDatas[k].Name.GetString());
            if (i == 0) first = toggle;
        }
        if (first != null) first.isOn = true;
    }

    private Toggle GenPanel(GameObject prefab, Transform parent, UnityAction<bool> call, string str)
    {
        Toggle temp = Creator.GetGameObject(prefab, parent).GetComponent<Toggle>();
        temp.onValueChanged.AddListener(call);
        temp.group = parent.GetComponent<ToggleGroup>();
        temp.GetComponentInChildren<TMP_Text>().text = str;
        return temp;
    }


    List<BoxStructure> dataActive = new List<BoxStructure>();
    int structureActive = 0;

    public void GenCenterPanel(StructureData[] data)
    {
        ClearKids(BuilderWindow.CenterPanel.parent);
        dataActive.Clear();
        for (int i = 0; i < data.Length; i++)
        {
            if (BuilderPlayer.CheckBuildingLimit(data[i].ID)) continue;

            GameObject temp = Creator.GetGameObject(BuilderWindow.CenterPanel.prefab, BuilderWindow.CenterPanel.parent);
            var boxStructure = temp.GetComponent<BoxStructure>();

            boxStructure.header.text = data[i].Name.GetString();
            boxStructure.icon.sprite = data[i].sprite;

            int countRes;
            int j = 0;
            for (; j < data[i].needResours.Length; j++)
            {
                boxStructure.needRes[j].icon.sprite = AllResources.GetConfigRes(data[i].needResours[j].ID).sprite;

                countRes = WarehouseController.LookOneRes(data[i].needResours[j].ID);
                boxStructure.needRes[j].count.text =
                    (countRes < data[i].needResours[j].count ?
                    MyMethods.PaintText(countRes, Color.red) : countRes.ToString())
                    + "/" + data[i].needResours[j].count;
            }

            for (; j < boxStructure.needRes.Length; j++)
                boxStructure.needRes[j].gameObject.SetActive(false);

            boxStructure.structureData = data[i];
            boxStructure.transform.localScale = Vector3.zero;
            dataActive.Add(boxStructure);
        }
        structureActive = 0;
        Rotation(0);
    }

    private void Rotation(int val)
    {
        Translate(dataActive[IndexIncreaser(structureActive - 1)].transform);
        Translate(dataActive[IndexIncreaser(structureActive + 1)].transform);

        structureActive += val;
        structureActive = IndexIncreaser(structureActive);

        Translate(dataActive[IndexIncreaser(structureActive - 1)].transform, -1, BuilderWindow.Scale);
        Translate(dataActive[IndexIncreaser(structureActive + 1)].transform, 1, BuilderWindow.Scale);

        Translate(dataActive[structureActive].transform, scale: 1);

        GenerateInfoPanel(dataActive[structureActive].structureData);
        dataActive[structureActive].gameObject.transform.SetAsLastSibling();
    }

    private void Translate(Transform transform, int signDeltaX = 0, float scale = 0)
    {
        transform.DOKill();
        transform.DOLocalMoveX(BuilderWindow.DeltaX * signDeltaX, BuilderWindow.Time);
        transform.DOScale(Vector3.one * scale, BuilderWindow.Time);
    }

    private int IndexIncreaser(int i)
    {
        if (i < 0) i += dataActive.Count;
        i %= dataActive.Count;
        return i;
    }

    private void BuildStructure() => BuilderPlayer.SetStructureData(dataActive[structureActive].structureData);

    private void GenerateInfoPanel(StructureData currentData)
    {
        int i = 0, j = 0;
        BuilderWindow.RequiredIconCount[i].count.text = 
            MyMethods.ToTimeHMS((int)(currentData.timeBuild / BusinessBonusController.GetMultiplierTimeStructure(currentData.ID)), CommonLines.FormatTime.GetString());
        BuilderWindow.RequiredIconCount[i].contentSizeFitter.SetLayoutHorizontal();

        for (; j < currentData.requirementsPasRes.Length; j++)
        {
            i++;
            BuilderWindow.RequiredIconCount[i].gameObject.SetActive(true);
            BuilderWindow.RequiredIconCount[i].count.text = currentData.requirementsPasRes[j].count.ToString();
            BuilderWindow.RequiredIconCount[i].icon.sprite = AllResources.GetConfigRes(currentData.requirementsPasRes[j].ID).sprite;
            BuilderWindow.RequiredIconCount[i].contentSizeFitter.SetLayoutHorizontal();
        }
        i++;
        for (; i < BuilderWindow.RequiredIconCount.Length; i++)
            BuilderWindow.RequiredIconCount[i].gameObject.SetActive(false);

        for (j = 0; j < currentData.productionPasRes.Length; j++)
        {
            BuilderWindow.WeGetIconCount[j].gameObject.SetActive(true);
            BuilderWindow.WeGetIconCount[j].count.text = currentData.productionPasRes[j].count.ToString();
            BuilderWindow.WeGetIconCount[j].icon.sprite = AllResources.GetConfigRes(currentData.productionPasRes[j].ID).sprite;
            BuilderWindow.WeGetIconCount[j].contentSizeFitter.SetLayoutHorizontal();
        }
        for (; j < BuilderWindow.WeGetIconCount.Length; j++)
            BuilderWindow.WeGetIconCount[j].gameObject.SetActive(false);
    }

    private void ClearKids(Transform tr) => Creator.ClearKids(tr);
}
