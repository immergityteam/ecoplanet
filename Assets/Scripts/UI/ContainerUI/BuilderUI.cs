﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class BuilderUI : MonoBehaviour, IBuilderUI
    {
        [SerializeField] private ContactFilter2D _contactFilter;
        public ContactFilter2D ContactFilter
        {
            get { return _contactFilter; }
            set { _contactFilter = value; }
        }



        #region Builbing
        [field: Header("Builbing")]
        [field: SerializeField] public Button BuildButton { get; private set; }
        [field: SerializeField] public Button MoveButton { get; private set; }
        [field: SerializeField] public Button DeleteButton { get; private set; }
        [field: SerializeField] public Button BuildCancel { get; private set; }
        #endregion

        #region Moving
        [field: Header("Moving")]
        [field: SerializeField] public Button Apply { get; private set; }
        [field: SerializeField] public Button CancelMoving { get; private set; }
        [field: SerializeField] public Button Flip { get; private set; }
        #endregion

        #region Other
        [field: Header("Other")]
        [field: SerializeField] public GameObject AllButtons { get; private set; }
        [field: SerializeField] public Transform PivotButtons { get; private set; }
        [field: SerializeField] public Image Frame { get; private set; }
        #endregion

        #region DOTweens
        [field: Header("DOTweens")]
        [field: SerializeField] public float TimeMoving { get; private set; }
        [field: SerializeField] public Vector2 MinMaxMovingY { get; private set; }
        [field: SerializeField] public Vector3 Rotation { get; private set; }
        #endregion

        #region SSO
        [field: Header("SSO")]
        [field: SerializeField] public StringsSO DeleteHeader { get; private set; }
        [field: SerializeField] public StringsSO DeleteMain { get; private set; }
        [field: SerializeField] public StringsSO NotEnoughResources { get; private set; }
        [field: SerializeField] public StringsSO AlreadyGotBuilding { get; private set; }
        [field: SerializeField] public StringsSO SetBuilding { get; private set; }
        [field: SerializeField] public StringsSO CrossBuilding { get; private set; }
        [field: SerializeField] public StringsSO CanNotDeleteThis { get; private set; }
        [field: SerializeField] public StringsSO BoostUpBuilding { get; private set;}

        #endregion

        #region Color
        [field: Header("Color")]
        [field: SerializeField] public Color FrameRed { get; private set; }
        [field: SerializeField] public Color FrameGreen { get; private set; }
        #endregion
    }
}
