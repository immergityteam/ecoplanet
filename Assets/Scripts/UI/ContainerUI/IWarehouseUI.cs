using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IWarehouseUI
    {
        GameObject GameObject { get; }
        WhereWhat UpPanel { get; }
        WhereWhat CenterPanel { get; }
        Button Close { get; }
        GameObject PrefPlus { get; }
        TMP_Text Header { get;}
        StringsSO HeaderSSO { get; }
    }
}