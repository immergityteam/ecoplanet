using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class WarehouseUI : MonoBehaviour, IWarehouseUI {
        public GameObject GameObject { get => gameObject;}
        [field: SerializeField] public WhereWhat UpPanel { get; private set; }
        [field: SerializeField] public WhereWhat CenterPanel { get; private set; }
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public GameObject PrefPlus { get; private set; }
        [field: SerializeField] public TMP_Text Header { get; private set; }
        [field: SerializeField] public StringsSO HeaderSSO { get; private set; }

    }
}