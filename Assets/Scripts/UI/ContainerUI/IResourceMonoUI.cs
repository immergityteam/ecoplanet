﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK {
    internal interface IResourceMonoUI {
        Image ResurseSprite { get; }
        TMP_Text TextCount { get; }
        RectTransform ButtonPanel { get; }
        RectTransform BoxPanel { get; }
        Button Put { get; }
        Button Sell { get; }
    }
}