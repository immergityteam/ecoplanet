using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IGameUI
    {
        Button Build { get; }
        Button Warehouse { get; }
        Button Technologies { get; }
        Button Quest { get; }
        Button Profile { get; }
        Button AddDonate { get; }
        Button AddEnegry { get; }
        TMP_Text Energy { get; }
        TMP_Text Coins { get; }
        TMP_Text Donate { get; }
        Button CanselMining { get; }
    }
}