﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface IBuilderUI
    {
        ContactFilter2D ContactFilter { get; set; }

        #region Moving
        Button Apply { get; }
        Button CancelMoving { get; }
        Button Flip { get; }
        #endregion

        #region Builbing
        Button BuildButton { get; }
        Button MoveButton { get; }
        Button DeleteButton { get; }
        Button BuildCancel { get; }
        #endregion

        #region Other
        GameObject AllButtons { get; }
        Transform PivotButtons { get; }
        Image Frame { get; }
        #endregion

        #region DOTweens
        float TimeMoving { get; }
        Vector2 MinMaxMovingY { get; }
        Vector3 Rotation { get; }
        #endregion

        #region SSO
        StringsSO DeleteHeader { get; }
        StringsSO DeleteMain { get; }
        StringsSO NotEnoughResources { get; }
        StringsSO AlreadyGotBuilding { get; }
        StringsSO SetBuilding { get; }
        StringsSO CrossBuilding { get; }
        StringsSO CanNotDeleteThis { get; }
        StringsSO BoostUpBuilding { get; }
        #endregion

        #region Color
        Color FrameRed { get; }
        Color FrameGreen { get; }
        #endregion
    }
}
