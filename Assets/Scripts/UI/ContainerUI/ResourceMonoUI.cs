using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class ResourceMonoUI : MonoBehaviour, IResourceMonoUI {
        [field: SerializeField] public Image ResurseSprite { get; private set; }
        [field: SerializeField] public TMP_Text TextCount  { get; private set; }
        [field: SerializeField] public RectTransform ButtonPanel  { get; private set; }
        [field: SerializeField] public RectTransform BoxPanel  { get; private set; }
        [field: SerializeField] public Button Put  { get; private set; }
        [field: SerializeField] public Button Sell { get; private set; }
    }
}