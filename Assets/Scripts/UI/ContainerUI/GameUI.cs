using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ColLobOK
{
    public class GameUI : MonoBehaviour, IGameUI {

        [field: SerializeField] public Button Build { get; private set; }
        [field: SerializeField] public Button Warehouse { get; private set; }
        [field: SerializeField] public Button Technologies { get; private set; }
        [field: SerializeField] public Button Quest { get; private set; }
        [field: SerializeField] public Button Profile { get; private set; }
        [field: SerializeField] public Button AddDonate { get; private set; }
        [field: SerializeField] public Button AddEnegry { get; private set; }
        [field: SerializeField] public TMP_Text Energy { get; private set; }
        [field: SerializeField] public TMP_Text Coins { get; private set; }
        [field: SerializeField] public TMP_Text Donate { get; private set; }
        [field: SerializeField] public Button CanselMining { get; private set; }
    }
}