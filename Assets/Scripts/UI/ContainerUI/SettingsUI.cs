using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class SettingsUI : MonoBehaviour, ISettingsUI
    {
        public GameObject GameObject => gameObject;
        [field:SerializeField] public Button Music { get; private set; }
        [field:SerializeField] public Button Sound { get; private set; }
        [field:SerializeField] public Button ButtonShare { get; private set; }
        [field:SerializeField] public Button Close { get; private set; }
        [field:SerializeField] public Toggle SelectRus { get; private set; }
        [field:SerializeField] public Toggle SelectEng { get; private set; }
        [field:SerializeField] public GameObject MusicImageOn { get; private set; }
        [field:SerializeField] public GameObject MusicImageOff { get; private set; }
        [field:SerializeField] public GameObject SoundImageOn { get; private set; }
        [field:SerializeField] public GameObject SoundImageOff { get; private set; }
        [field:SerializeField] public TMP_Text UserName { get; private set; }
        [field:SerializeField] public TMP_Text Info { get; private set; }
        [field:SerializeField] public TMP_Text Share { get; private set; }
        [field:SerializeField] public StringsSO InfoSSO { get; private set; }
        [field:SerializeField] public StringsSO ShareSSO { get; private set; }
        [field:SerializeField] public StringsSO UserSSO { get; private set; }

    }
}
