using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface ISettingsUI
    {
        GameObject GameObject { get; }
        Button Music { get; }
        Button Sound { get; }
        Button ButtonShare { get; }
        Button Close { get; }
        Toggle SelectRus { get; }
        Toggle SelectEng { get; }
        GameObject MusicImageOn { get; }
        GameObject MusicImageOff { get; }
        GameObject SoundImageOn { get; }
        GameObject SoundImageOff { get; }
        TMP_Text UserName { get; }
        TMP_Text Info { get; }
        TMP_Text Share { get; }
        StringsSO InfoSSO { get; }
        StringsSO ShareSSO { get; }
        StringsSO UserSSO { get; }
    }
}
