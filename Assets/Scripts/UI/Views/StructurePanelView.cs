using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class StructurePanelView : MonoBehaviour
    {
        public Button ButtonBuy;
        public Button ButtonInfo;
        public TMP_Text Name;
        public Image Image;
        public GameObject ResourceParent;
    }
}
