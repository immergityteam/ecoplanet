using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace ColLobOK
{
    public class InfoPanelView : MonoBehaviour
    {
        public Image BuildingImage;
        public Transform HeadPanelStartPosition;
        public NeedResourceView[] NeedResource;
        public GameObject RequirementsPanel;
        public RequirementView[] Requirements;
        public RequirementView[] ProductionRequirement;

        public RectTransform NameTextRectTransform;
        public RectTransform HeadPanelRectTransform;

        [FormerlySerializedAs("ButtonBack")] public Button ButtonBuy;
        public StringsSO ButtonBuyLocalization;
        public TMP_Text ButtonText;
        public TMP_Text Name;
        public TMP_Text Time;
    }
}
