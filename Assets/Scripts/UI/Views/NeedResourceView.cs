using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class NeedResourceView : MonoBehaviour
    {
        public TMP_Text resourseNeededCount;
        public Image icon;
    }
}
