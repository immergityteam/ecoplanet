using System.Collections.Generic;
using ColLobOK.Mechanics.Technology;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public class TechnologyUIView : MonoBehaviour, ITechnologyUIView
    {
        public UnityAction TabOpened { get; set; }
        public GameObject ViewObject { get => gameObject;}
        [field: SerializeField] public TechnologyButtonView[] Buttons { get; private set; }
        [field: SerializeField] public Button Close { get; private set; }
        [field: SerializeField] public TMP_Text Header { get; private set; }
        [field: SerializeField] public TMP_Text Rank { get; set; }
        
        [field: SerializeField] public StringsSO HeaderSSO { get; private set; }
        [field: SerializeField] public TMP_Text BranchName { get; private set; }
        [field: SerializeField] public TMP_Text StageDescription { get; private set; }
        [field: SerializeField] public Image BranchIcon { get; set; }
        [field: SerializeField] public Slider ProgressBarFiller { get; private set; }
        [field: SerializeField] public List<Image> StageLocks { get; private set; }
        [field: SerializeField] public List<Image> Stages { get; private set; }
        [field: SerializeField] public Sprite ActiveTab { get; set; }
        [field: SerializeField] public Sprite DefaultTab { get; set; }
        [field: SerializeField] public Sprite DefaultBranch { get; set; }
        [field: SerializeField] public List<RankTabView> RankTabs { get; set; }
        [field: SerializeField] public Image[] ProgressIcons { get; set; }
        [field: SerializeField] public Image HalfProgress { get; set; }
        [field: SerializeField] public Image AllProgress { get; set; }
        [field: SerializeField] public StringsSO RankSO { get; set; }
        [field: SerializeField] public StringsSO ApprenticeRank { get; set; }
        [field: SerializeField] public StringsSO MasterRank { get; set; }
        [field: SerializeField] public TMP_Text RankName { get; set; }
        public void OpenTab()
        {
            TabOpened?.Invoke();
        }
    }
}