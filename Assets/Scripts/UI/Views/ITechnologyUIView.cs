using System.Collections.Generic;
using ColLobOK.Mechanics.Technology;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public interface ITechnologyUIView
    {
        UnityAction TabOpened { get; set; }
        GameObject ViewObject { get; }
        Button Close { get; }
        TMP_Text Header { get; }
        TMP_Text Rank { get; set; }
        TMP_Text RankName { get; set; }
        StringsSO HeaderSSO { get; }
        TechnologyButtonView[] Buttons { get; }
        TMP_Text BranchName { get; }

        TMP_Text StageDescription { get; }
        Image BranchIcon { get; set; }

        Slider ProgressBarFiller { get; }
        List<Image> StageLocks { get; }

        List<Image> Stages { get; }

        Sprite ActiveTab { get; }
        Sprite DefaultTab { get; }
        Sprite DefaultBranch { get; }
        
        List<RankTabView> RankTabs { get; set; }
        Image[] ProgressIcons { get; set; }
        Image HalfProgress { get; set; }
        Image AllProgress { get; set; }
        StringsSO RankSO { get; set; }
        StringsSO ApprenticeRank { get; set; }
        StringsSO MasterRank { get; set; }
    }
}
