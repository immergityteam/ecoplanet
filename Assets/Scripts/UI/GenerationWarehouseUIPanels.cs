using System.Collections.Generic;
using TMPro;
using UnityDI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ColLobOK
{
    public class GenerationWarehouseUIPanels : IGenerationWarehouseUIPanels, IDependent
    {

        [Dependency] public IWarehouseUI WarehouseUI { private get; set; }
        [Dependency] public IGameUI GameeUI { private get; set; }
        [Dependency] public IWarehouseController WarehouseController { private get; set; }
        [Dependency] public ICreator Creator { private get; set; }
        [Dependency] public IResourceFactory ResourceFactory { private get; set; }
        [Dependency] public IContainerMono ContainerMono { private get; set; }
        [Dependency] public IBuilderPlayer BuilderPlayer { private get; set; }
        [Dependency] public IWarehouseAddition WarehouseAddition { private get; set; }

        private UnityAction<int> temp;
        private int _currentToggleNumber;
        private bool _isWarehouseBuilt;
        private bool _isBarnBuilt;
        
        public void OnInjected()
        {
            BuilderPlayer.EvClikedStructures += OpenWarehouseWindow;
            BuilderPlayer.EvBuiltConstruction += IncludeWarehouses;
            GameeUI.Warehouse.onClick.AddListener(OpenStorege);
            WarehouseUI.Close.onClick.AddListener(ClearListener);
        }
        
        private void IncludeWarehouses(int id)
        {
            if (BuilderPlayer.GetGlobalID(id) == WarehouseAddition.Warehouse.GetID())
                _isWarehouseBuilt = true;
            else if (BuilderPlayer.GetGlobalID(id) == WarehouseAddition.Barn.GetID())
                _isBarnBuilt = true;
        }
        
        public void OnDestroy()
        {
            BuilderPlayer.EvClikedStructures -= OpenWarehouseWindow;
            BuilderPlayer.EvBuiltConstruction -= IncludeWarehouses;
        }
        
        private void OpenWarehouseWindow(int id)
        {
            if (BuilderPlayer.GetGlobalID(id) == WarehouseAddition.ProcessingPlant.GetID())
            {
                _currentToggleNumber = 0;
                OpenStorege();
            }
            else if (BuilderPlayer.GetGlobalID(id) == WarehouseAddition.Warehouse.GetID())
            {
                _currentToggleNumber = 1;
                OpenStorege();
            }
            else if (BuilderPlayer.GetGlobalID(id) == WarehouseAddition.Barn.GetID())
            {
                _currentToggleNumber = 2;
                OpenStorege();
            }
        }
        
        public void OpenStorege()
        {
            WarehouseUI.GameObject.SetActive(true);
            WarehouseUI.Header.text = WarehouseUI.HeaderSSO.GetString();
            GenUpPanel();
        }

        public void GenUpPanel()
        {
            ClearKids(WarehouseUI.UpPanel.parent);
            Toggle toggle = null, first = null;
            for (int i = 0; i < ContainerMono.TypeConfig.Length; i++)
            {
                if (ContainerMono.TypeConfig[i].type == TypeRes.Wallet) { continue;}
                
                if (_isWarehouseBuilt == false && ContainerMono.TypeConfig[i].type == TypeRes.Materials) { continue; }
                if (_isBarnBuilt == false && ContainerMono.TypeConfig[i].type == TypeRes.Rations) { continue; }
                
                int k = i;
                toggle = GenPanel(WarehouseUI.UpPanel.prefab, WarehouseUI.UpPanel.parent,
                    (x) => GenCenterPanel(ContainerMono.TypeConfig[k].type, x), ContainerMono.TypeConfig[i].Name.GetString());
                if (i == _currentToggleNumber) { first = toggle; }
            }
            if (first != null)
            {
                first.isOn = false;
                first.isOn = true;
            }
        }

        private Toggle GenPanel(GameObject prefab, Transform parent, UnityAction<bool> call, string str)
        {
            Toggle temp = Creator.GetGameObject(prefab, parent).GetComponent<Toggle>();
            temp.onValueChanged.AddListener(call);
            temp.group = parent.GetComponent<ToggleGroup>();
            temp.GetComponentInChildren<TMP_Text>().text = str;
            return temp;
        }

        public void GenCenterPanel(TypeRes type, bool on)
        {
            if (!on) return;
            ClearListener();
            ClearKids(WarehouseUI.CenterPanel.parent);
            List<Resource> data = WarehouseController.LookAllResType(type);
            for (int i = 0; i < data.Count; i++)
            {
                temp += ResourceFactory.BuildResoursePanel(data[i]);
            }
            WarehouseController.EvChangeRes += temp;
            ResourceFactory.BuildResoursePanel(type, (x) => { WarehouseController.AddMaxCellType(x, ()=> GenCenterPanel(type, true)); GenCenterPanel(type, true); });
        }
        
        private void ClearListener()
        {
            WarehouseController.EvChangeRes -= temp;
            temp = null;
        }

        private void ClearKids(Transform tr)
        {
            Creator.ClearKids(tr);
        }
    }
}