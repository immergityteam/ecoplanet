﻿namespace ColLobOK
{
    public interface IGenerationWarehouseUIPanels {

        void GenUpPanel();

        void GenCenterPanel(TypeRes type, bool on);
        void OnDestroy();
    }
}