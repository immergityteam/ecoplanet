using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ColLobOK
{
    public class ButtonClickView : MonoBehaviour, IPointerClickHandler
    {
        public AudioClip ClickSound;

        public Action DefaultClick;
        public Action Click;


        public void OnPointerClick(PointerEventData eventData)
        {
            EventManager.Instance.EvBee?.Invoke(ClickSound);
        }
    }
}
