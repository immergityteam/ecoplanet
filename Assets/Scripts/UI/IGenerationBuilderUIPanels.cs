﻿using UnityEngine.Events;

namespace ColLobOK {
    public interface IGenerationBuilderUIPanels {

        void GenUpPanel(UpPanelData data);
        void GenLeftPanel(LeftPanelData data);
        void GenCenterPanel(StructureData[] data);
        UnityAction<int> EvSelectPanel { get; set; }
    }
}