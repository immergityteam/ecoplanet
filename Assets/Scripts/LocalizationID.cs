using UnityEngine;
using UnityEngine.Events;

namespace ColLobOK
{
    public class LocalizationID : MonoBehaviour
    {
        public static LocalizationID Instance;

        public UnityAction EvChangeLanguage;

        public void Awake()
        {
            LanguageID = Application.systemLanguage == SystemLanguage.Russian ? 0 : 1;
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        public int LanguageID = 0;
        public void SetLanguage(int val)
        {
            LanguageID = val;
            EvChangeLanguage?.Invoke();
        }
    }
}