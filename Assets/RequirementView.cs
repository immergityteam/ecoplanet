using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ColLobOK
{
    public class RequirementView : MonoBehaviour
    {
        public TMP_Text count;
        public Image icon;
        public ResurceConfigSO requirementData;
    }
}
